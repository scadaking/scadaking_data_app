﻿using System;
using System.Windows.Forms;
using ScadaKing_Data_App.Class;

namespace ScadaKing_Data_App
{
    public partial class frmMDIParent : Form
    {
        private int childFormNumber = 0;
        bool formClosing;
        public frmMDIParent()
        {
            InitializeComponent();
        }

        private void ShowNewForm(object sender, EventArgs e)
        {
            Form childForm = new Form();
            childForm.MdiParent = this;
            childForm.Text = "Window " + childFormNumber++;
            childForm.Show();
        }

        private void OpenFile(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            openFileDialog.Filter = "Text Files (*.txt)|*.txt|All Files (*.*)|*.*";
            if (openFileDialog.ShowDialog(this) == DialogResult.OK)
            {
                string FileName = openFileDialog.FileName;
            }
        }

        private void SaveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            saveFileDialog.Filter = "Text Files (*.txt)|*.txt|All Files (*.*)|*.*";
            if (saveFileDialog.ShowDialog(this) == DialogResult.OK)
            {
                string FileName = saveFileDialog.FileName;
            }
        }

        private void ExitToolsStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        private void CascadeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.Cascade);
        }

        private void TileVerticalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.TileVertical);
        }

        private void TileHorizontalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.TileHorizontal);
        }

        private void ArrangeIconsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.ArrangeIcons);
        }

        private void CloseAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Form childForm in MdiChildren)
            {
                childForm.Close();
            }
        }



        private void batchForReviewToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmListingBatch frmListingBatch = new frmListingBatch();
            frmListingBatch.BatchType = 1;
            frmListingBatch.MdiParent = this;
            frmListingBatch.Show();

        }


        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormClosingEventArgs formClosing = new FormClosingEventArgs(CloseReason.ApplicationExitCall, false);
            this.frmMDIParent_FormClosing(sender, formClosing);
        }

        private void logoutAltF4ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (General.ShowYesNoMessage("Are you want to Restart " + this.Text + " ?", this.Text) == DialogResult.Yes)
            {
                this.formClosing = true;
                this.Close();
                Application.Restart();
            }



        }

        private void frmMDIParent_Load(object sender, EventArgs e)
        {
            statusStrip.Items.Add("|");
            statusStrip.Items.Add("Version :" + Application.ProductVersion);
            statusStrip.Items.Add("|");
            statusStrip.Items.Add("User Name : [" + ScadaKing_Data_App.Class.General.User_Name + "]");
            statusStrip.Items.Add("|");
            statusStrip.Items.Add("Logged Time : [" + DateTime.Now.ToString() + "]");
            //this.Text = this.Tag.ToString() + " [" + PNF.Class.General.OrganizationName + "]";


        }

        private void frmMDIParent_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.formClosing)
            {
                return;
            }

            this.formClosing = true;

            if (General.ShowYesNoMessage("Do you want to close the " + this.Text + " ?", "") == DialogResult.No)
            {
                e.Cancel = true;
                this.formClosing = false;
                return;
            }

            Application.Exit();
        }


    }
}
