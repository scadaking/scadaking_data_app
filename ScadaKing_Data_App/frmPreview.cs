﻿using System;
using System.Windows.Forms;
using System.Data;
using ScadaKing_Data_App.Class;
using System.Data.OleDb;
using System.Net;


namespace ScadaKing_Data_App
{
    public partial class frmPreview : Form
    {

        private string _batch_NO;

        private string Batch_NO
        {
            get
            {
                if (string.IsNullOrEmpty(this._batch_NO))
                {
                    this._batch_NO = General.ConvertToInteger(this._objGridRow.Cells["BatchNo"].Value.ToString()).ToString();
                }

                return this._batch_NO;
            }
            set
            {
                this._batch_NO = value;
            }
        }

        bool _reGenerate_File;

        public bool ReGenerate_File
        {
            get
            {
                return this._reGenerate_File;
            }
            set
            {
                this._reGenerate_File = value;
            }
        }

        public bool Is_Government_Batch { get; set; }
        DataGridViewRow _objGridRow;
        public DataGridViewRow objGridRow
        {
            get
            {
                return this._objGridRow;
            }

            set
            {
                this._objGridRow = value;
                this.Batch_NO = General.ConvertToInteger(this._objGridRow.Cells["BatchNo"].Value.ToString()).ToString();
            }
        }
        string htmlDetails = string.Empty;
        public frmPreview()
        {
            InitializeComponent();
        }

        private void frmPreview_Load(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;

            if (Program.rdoMCGM != "")
            {
                //rdoMCGM.Text = "";
                rdoMCGM.Text = Program.rdoMCGM;
            }
            if (Program.rdoNonMCGM != "")
            {
                rdoNonMCGM.Text = Program.rdoNonMCGM;
            }

            try
            {
                this.btnPreview_Batch_Click(this.btnPreview_Batch, e);
                if (this._reGenerate_File == true)
                {
                    if (Program.ConfigIS_CREATE_HTML_FILE == false)
                    {
                        btnUpdate.Enabled = false; 
                    }
                    else
                    {
                        this.btnUpdate.Text = this.btnUpdate.Tag.ToString();
                    }
                    if (this.Is_Government_Batch == true)
                    {
                        rdoMCGM.Checked = true;
                        rdoNonMCGM.Checked = false;
                        rdoNonMCGM.Enabled = false;
                    }
                    else
                    {
                        rdoMCGM.Checked = false;
                        rdoMCGM.Enabled = false;
                        rdoNonMCGM.Checked = true;
                        btnUpdate.Enabled = false;
                    }
                    //btnResync.Enabled = true;
                }
                else
                {
                    //btnResync.Enabled = false;                    
                }
            }
            catch (Exception)
            {
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            bool IsExit = true;
            try
            {
                this.btnPreview_Batch_Click(sender, e);
                string file_Created_Date = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss");

                #region Check data already uploaded
                DataTable dtReturnUploadedData = new DataTable();
                bool IsDataAlreadyUploaded = false;
                string strUploadedMessage = string.Empty;
                try
                {
                    //Test 2016
                    ServicePointManager.UseNagleAlgorithm = true;
                    ServicePointManager.Expect100Continue = false;
                    ServicePointManager.CheckCertificateRevocationList = true;
                    ServicePointManager.DefaultConnectionLimit = ServicePointManager.DefaultPersistentConnectionLimit;
                    //End
                    ScadaKing_Web_Service.Response_UploadData responseIsAlready_Uploaded = new ScadaKing_Web_Service.Response_UploadData();
                    ScadaKing_Web_Service.paramUploadMCGMStruture objParam = new ScadaKing_Web_Service.paramUploadMCGMStruture();
                    objParam.Batch_No = Convert.ToInt64(this.Batch_NO);
                    //objParam.PlantAppVersion = Program.PlantAppVersion;
                    //objParam.PlantAppVersion = "3.0";
                    objParam.PlantAppVersion = Program.PlantAppVersion;
                    if (Program.PlantAppVersion == "3.1")
                    {
                        objParam.PlantAppVersion = "3.0";
                    }
                    
                    ScadaKing_Web_Service.TransactionHeader TransactionHeader = new ScadaKing_Web_Service.TransactionHeader();
                    TransactionHeader.UserName = General.User_Name;
                    TransactionHeader.Password = General.ConfigService_Password;
                    TransactionHeader.DeviceID = General.ConfigMachine_Id;
                    TransactionHeader.Config_App_Name = General.ConfigAPP_Name;
                    TransactionHeader.Config_App_Version = General.ConfigAPP_Version;
                    TransactionHeader.Win_Service_Version = General.Win_Service_Version;
                    TransactionHeader.Win_Service_GUID = General.Win_Service_GUID;
                    ScadaKing_Web_Service.ScadaKing_Web_ServiceSoapClient objService = new ScadaKing_Web_Service.ScadaKing_Web_ServiceSoapClient();
                    responseIsAlready_Uploaded = objService.Check_Data_Already_Uploaded(TransactionHeader, objParam);
                    //Program.ConfigAPP_Version = "3.0";
                    if (responseIsAlready_Uploaded.SyncResponseData.Tables.Count > 0)
                    {
                        dtReturnUploadedData = responseIsAlready_Uploaded.SyncResponseData.Tables["ReturnData"];
                        if (dtReturnUploadedData != null && dtReturnUploadedData.Rows.Count > 0 && General.ConvertToBool(dtReturnUploadedData.Rows[0]["IsAlreadyExist"].ToString()) == true)
                        {
                            IsDataAlreadyUploaded = true;
                            rdoMCGM.Enabled = false;
                            rdoNonMCGM.Enabled = false;
                            if (General.ConvertToBool(dtReturnUploadedData.Rows[0]["IsMCGM"].ToString()) == true)
                            {
                                rdoMCGM.Checked = true;
                                rdoNonMCGM.Checked = false;
                                strUploadedMessage = "The govt. data[" + this.Batch_NO + "] already uploaded.";
                            }
                            else
                            {
                                rdoMCGM.Checked = false;
                                rdoNonMCGM.Checked = true;
                                strUploadedMessage = "The pvt. data[" + this.Batch_NO + "] already uploaded.";
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    // Global.WriteLog("btnUpdate_Click Exception : Line no = " + Line_No + " ,ex()-->", LogMessageType.Error, ex.Message);
                    Global.WriteLog("btnUpdate_Click Exception", LogMessageType.Error, ex.Message);
                }
                #endregion Check data already uploaded

                if (rdoMCGM.Checked == true)
                {
                    if (this.ReGenerate_File == false)
                    {
                        if (IsDataAlreadyUploaded == true)
                        {
                            if (General.ShowYesNoMessage(strUploadedMessage + Environment.NewLine + "Do you want to generate file?", this.Text) == System.Windows.Forms.DialogResult.No)
                            {
                                IsExit = false;
                                if (Program.ConfigIS_CREATE_HTML_FILE == true)
                                {
                                    btnGenerateFile.Enabled = true;
                                }
                            }
                            else
                            {
                                if (Program.ConfigIS_CREATE_HTML_FILE == true)
                                {
                                    Global.Write_HTML_File(htmlDetails, true, this.Batch_NO.ToString(), file_Created_Date);
                                    General.ShowMessage("File Generated successfully...", this.Text);
                                }
                                else
                                {
                                    General.ShowMessage("Permission denied to generate file...!!!", this.Text);
                                }
                            }
                        }
                        else
                        {
                            string strMessage = string.Empty;
                            if (Program.ConfigIS_CREATE_HTML_FILE == true)
                            {
                                strMessage = "Is this " + rdoMCGM.Text + " Data?" + Environment.NewLine + "Do you want to generate a file and upload the data?";
                            }
                            else
                            {
                                strMessage = "Is this " + rdoMCGM.Text + " Data?" + Environment.NewLine + "Do you want to upload the data?";
                            }
                            if (General.ShowYesNoMessage(strMessage, this.Text) == System.Windows.Forms.DialogResult.Yes)
                            {
                                string Truck_No = objGridRow.Cells["TruckNo"].Value == null ? string.Empty : objGridRow.Cells["TruckNo"].Value.ToString();
                                string Batch_Date = objGridRow.Cells["BatchDate"].Value == null ? string.Empty : objGridRow.Cells["BatchDate"].Value.ToString();
                                string BatchStartTime = objGridRow.Cells["BatchStartTime"].Value == null ? string.Empty : objGridRow.Cells["BatchStartTime"].Value.ToString();
                                string BatchEndTime = objGridRow.Cells["BatchEndTime"].Value == null ? string.Empty : objGridRow.Cells["BatchEndTime"].Value.ToString();
                                //string BatchDateTime = DateTime.Parse(Batch_Date).ToShortDateString() + " " + DateTime.Parse(BatchStartTime).ToShortTimeString();
                                string BatchDateTime = Batch_Date.ToString() + " " + DateTime.Parse(BatchStartTime).ToShortTimeString();
                                if (Global.Is_Validate(Truck_No, BatchDateTime, _batch_NO, BatchStartTime, BatchEndTime) == false)
                                {
                                    IsExit = false;
                                    return;
                                }
                                else
                                {
                                    if (Insert_Upload_Data_In_Replica_And_Live_DataBase(file_Created_Date, false) == true)
                                    {
                                        General.ShowMessage("Data uploaded Successfully...!", this.Text);
                                        if (Program.ConfigIS_CREATE_HTML_FILE == true)
                                        {
                                            Global.Write_HTML_File(htmlDetails, true, this.Batch_NO.ToString(), file_Created_Date);
                                        }
                                    }
                                }
                            }
                            else
                            {
                                IsExit = false;
                            }
                        }
                    }
                    else
                    {
                        if (Program.ConfigIS_CREATE_HTML_FILE == true)
                        {
                            frmHtmlPreview frmHtmlPreview = new frmHtmlPreview(this.Batch_NO.ToString(), dataConnectionConfiguration.Target_Connection_String);
                            frmHtmlPreview.ShowDialog();
                            frmHtmlPreview.Dispose();
                        }
                        else
                        {
                            IsExit = false;
                        }
                    }
                }
                else if (rdoNonMCGM.Checked == true)
                {
                    if (this.ReGenerate_File == false)
                    {
                        if (IsDataAlreadyUploaded == true)
                        {
                            General.ShowMessage(strUploadedMessage, this.Text);
                        }
                        else
                        {
                            if (General.ShowYesNoMessage("Is this " + rdoNonMCGM.Text + " Data?" + Environment.NewLine + "Do you want to upload data?", this.Text) == System.Windows.Forms.DialogResult.Yes)
                            {
                                string Truck_No = objGridRow.Cells["TruckNo"].Value == null ? string.Empty : objGridRow.Cells["TruckNo"].Value.ToString();
                                string Batch_Date = objGridRow.Cells["BatchDate"].Value == null ? string.Empty : objGridRow.Cells["BatchDate"].Value.ToString();
                                string BatchStartTime = objGridRow.Cells["BatchStartTime"].Value == null ? string.Empty : objGridRow.Cells["BatchStartTime"].Value.ToString();
                                string BatchEndTime = objGridRow.Cells["BatchEndTime"].Value == null ? string.Empty : objGridRow.Cells["BatchEndTime"].Value.ToString();
                                string BatchDateTime = Batch_Date.ToString() + " " + DateTime.Parse(BatchStartTime).ToShortTimeString();
                                //string BatchDateTime = DateTime.Parse(Batch_Date).ToShortDateString() + " " + DateTime.Parse(BatchStartTime).ToShortTimeString();

                                if (Global.Is_Validate(Truck_No, BatchDateTime, _batch_NO, BatchStartTime, BatchEndTime) == false)
                                {
                                    IsExit = false;
                                    return;
                                }
                                else
                                {
                                    if (Insert_Upload_Data_In_Replica_And_Live_DataBase(file_Created_Date, false) == true)
                                    {
                                        General.ShowMessage("Data uploaded Successfully!", this.Text);
                                    }
                                }
                            }
                            else
                            {
                                IsExit = false;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Global.WriteLog("btnUpdate_Click Exception", LogMessageType.Error, ex.Message);
                //Global.WriteLog("btnUpdate_Click Exception : Line no = " + Line_No + " ,ex()-->", LogMessageType.Error, ex.Message);
            }
            finally
            {
                this.Cursor = Cursors.Default;
                if (IsExit == true)
                {
                    this.DialogResult = System.Windows.Forms.DialogResult.OK;
                    btnExit_Click(sender, new EventArgs());
                }
            }
        }

        private bool Insert_Upload_Data_In_Replica_And_Live_DataBase(string file_Created_Date, bool IsReSync = false)
        {
            bool IsInserted = false;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                string strQuery = string.Empty;
                string strAccessQuery = string.Empty;
                string strSycnFromDBAccessConnectionString = string.Empty;
                DataSet DataSetToUpload = new DataSet();
                if (IsReSync == true)
                {
                    strSycnFromDBAccessConnectionString = dataConnectionConfiguration.Target_Connection_String;
                }
                else
                {
                    strSycnFromDBAccessConnectionString = dataConnectionConfiguration.Source_Connection_String;
                }
                #region Generate BATCH_DATA_RAW_STATUS
                DataTable dtBATCH_DATA_RAW_STATUS = new DataTable();
                dtBATCH_DATA_RAW_STATUS.Columns.Add("ORG_ID", typeof(long));
                dtBATCH_DATA_RAW_STATUS.Columns.Add("BATCH_NO", typeof(float));
                dtBATCH_DATA_RAW_STATUS.Columns.Add("BATCH_STATUS", typeof(long));
                dtBATCH_DATA_RAW_STATUS.Columns.Add("IS_MODIFIED", typeof(bool));
                dtBATCH_DATA_RAW_STATUS.Columns.Add("SYNC_STATUS", typeof(long));
                dtBATCH_DATA_RAW_STATUS.Columns.Add("IS_APPROVED", typeof(bool));
                dtBATCH_DATA_RAW_STATUS.Columns.Add("APPROVED_BY", typeof(long));
                dtBATCH_DATA_RAW_STATUS.Columns.Add("APPROVED_DATE", typeof(DateTime));
                dtBATCH_DATA_RAW_STATUS.Columns.Add("ROUTE_ID", typeof(long));
                dtBATCH_DATA_RAW_STATUS.Columns.Add("LOADING_DATETIME", typeof(DateTime));
                dtBATCH_DATA_RAW_STATUS.Columns.Add("LOADING_SITE_ID", typeof(long));
                dtBATCH_DATA_RAW_STATUS.Columns.Add("UNLOADING_DATETIME", typeof(DateTime));
                dtBATCH_DATA_RAW_STATUS.Columns.Add("UNLOADING_SITE_ID", typeof(long));
                dtBATCH_DATA_RAW_STATUS.Columns.Add("IS_MCGM", typeof(bool));
                dtBATCH_DATA_RAW_STATUS.Columns.Add("IS_NEW_QTY", typeof(bool));
                dtBATCH_DATA_RAW_STATUS.Columns.Add("IS_HTML_FILE_CREATED", typeof(bool));
                dtBATCH_DATA_RAW_STATUS.Columns.Add("IS_HTML_FILE_CREATED_DATE", typeof(DateTime));
                dtBATCH_DATA_RAW_STATUS.Columns.Add("LAST_TRACKING_DATE", typeof(string));
                dtBATCH_DATA_RAW_STATUS.Columns.Add("IS_PROCESSING", typeof(int));
                dtBATCH_DATA_RAW_STATUS.Columns.Add("START_TRACKING_DATE", typeof(string));
                dtBATCH_DATA_RAW_STATUS.Columns.Add("IS_TRACKING_DATA_SEND", typeof(int));
                dtBATCH_DATA_RAW_STATUS.Columns.Add("NO_OF_TIME_SEND_DATA", typeof(int));
                dtBATCH_DATA_RAW_STATUS.Columns.Add("IS_SENDED", typeof(bool));
                dtBATCH_DATA_RAW_STATUS.Columns.Add("IS_SEND_DATA_COUNT", typeof(int));
                dtBATCH_DATA_RAW_STATUS.Columns.Add("DEVICE_REGISTRATION_ID", typeof(string));
                dtBATCH_DATA_RAW_STATUS.TableName = "BATCH_DATA_RAW_STATUS";
                DataRow dr = dtBATCH_DATA_RAW_STATUS.NewRow();
                dr["ORG_ID"] = General.orgId;
                dr["BATCH_NO"] = this.Batch_NO;
                dr["BATCH_STATUS"] = 0;
                dr["IS_MODIFIED"] = false;
                dr["SYNC_STATUS"] = 0;
                dr["IS_APPROVED"] = 1;
                dr["APPROVED_BY"] = 0;
                dr["APPROVED_DATE"] = DBNull.Value;
                dr["ROUTE_ID"] = 0;
                dr["LOADING_DATETIME"] = DBNull.Value;
                dr["LOADING_SITE_ID"] = 0;
                dr["UNLOADING_DATETIME"] = DBNull.Value;
                dr["UNLOADING_SITE_ID"] = 0;
                dr["IS_MCGM"] = rdoMCGM.Checked;
                dr["IS_NEW_QTY"] = 0;
                dr["IS_HTML_FILE_CREATED"] = rdoMCGM.Checked;
                if (rdoMCGM.Checked == true)
                {
                    dr["IS_HTML_FILE_CREATED_DATE"] = DateTime.Parse(file_Created_Date);
                }
                else
                {
                    dr["IS_HTML_FILE_CREATED_DATE"] = DBNull.Value;
                }
                dr["LAST_TRACKING_DATE"] = DBNull.Value;
                dr["IS_PROCESSING"] = 0;
                dr["START_TRACKING_DATE"] = DBNull.Value;
                dr["IS_TRACKING_DATA_SEND"] = 0;
                dr["NO_OF_TIME_SEND_DATA"] = 0;
                dr["IS_SENDED"] = 0;
                dr["IS_SEND_DATA_COUNT"] = 0;
                dr["DEVICE_REGISTRATION_ID"] = Global.DeviceRegistrationId;
                dtBATCH_DATA_RAW_STATUS.Rows.Add(dr);
                #endregion

                #region Version 3
                if (Program.PlantAppVersion == "3")
                {
                    DataTable dtSource_Batch_Dat_Trans, dtSource_Batch_Transaction, dtBATCH_RESULT = new DataTable();

                    #region fill data table from access db to sync on live
                    strQuery = @"select * from Batch_Dat_Trans where batch_no = " + this.Batch_NO + "";
                    dtSource_Batch_Dat_Trans = DbHelper.FillDataTable(strQuery, strSycnFromDBAccessConnectionString);
                    dtSource_Batch_Dat_Trans.TableName = "Batch_Dat_Trans";

                    strQuery = @"select * from Batch_Transaction where batch_no = " + this.Batch_NO + "";
                    dtSource_Batch_Transaction = DbHelper.FillDataTable(strQuery, strSycnFromDBAccessConnectionString);
                    dtSource_Batch_Transaction.TableName = "Batch_Transaction";

                    if (Get_Table_Name("BATCH_RESULT") != null)
                    {
                        strQuery = @"select " + this.Batch_NO + " as batch_no ," + General.orgId + " as ORG_ID, * from BATCH_RESULT where Ticket_no = " + this.Batch_NO + "";
                        dtBATCH_RESULT = DbHelper.FillDataTable(strQuery, strSycnFromDBAccessConnectionString);
                        if (dtBATCH_RESULT.Columns.Contains("BATCH_RESULT.batch_no"))
                        {
                            dtBATCH_RESULT.Columns.Remove("BATCH_RESULT.batch_no");
                        }
                        dtBATCH_RESULT.TableName = "BATCH_RESULT";
                    }

                    #endregion fill data table from access db to sync on live
                    #region Sync data on live
                    DataSetToUpload.Tables.Add(dtSource_Batch_Dat_Trans);
                    DataSetToUpload.Tables.Add(dtSource_Batch_Transaction);
                    DataSetToUpload.Tables.Add(dtBATCH_DATA_RAW_STATUS);
                    if (Get_Table_Name("BATCH_RESULT") != null)
                    {
                        DataSetToUpload.Tables.Add(dtBATCH_RESULT);
                    }
                    ScadaKing_Web_Service.Response_UploadData response = new ScadaKing_Web_Service.Response_UploadData();
                    response = SyncAndReSyncData(DataSetToUpload, Program.PlantAppVersion, IsReSync);
                    #endregion
                    if (response.ErrorCode != 0)
                    {
                        string strErrMessage = string.Empty;
                        strErrMessage = "OOPS! Unable to upload the Data";
                        if (response.ErrorMessage.Contains("Cannot insert duplicate key in object") == true)
                        {
                            strErrMessage = strErrMessage + "\n Can not insert duplicate records..!!!!";
                        }
                        Global.WriteLog("Insert_Upload_Data_In_Replica_And_Live_DataBase() for Plant App Version[" + Program.PlantAppVersion + "] Response Exception: ", LogMessageType.Error, response.ErrorMessage);
                        General.ShowMessage(strErrMessage, this.Text);
                        IsInserted = false;
                        return false;
                    }
                    if (IsReSync == true)
                    {
                        IsInserted = true;
                        return true;
                    }
                    #region insert data into replica db
                    Insert_Data_In_Replica_Db(file_Created_Date);
                    #endregion
                }
                #endregion Version 3

                #region Version 3.1
                if (Program.PlantAppVersion == "3.1")
                {
                    DataTable dtSource_Batch_Dat_Trans, dtSource_Batch_Transaction, dtBATCH_RESULT = new DataTable();
                    #region fill data table from access db to sync on live
                    strQuery = @"SELECT 
		                BD.BATCH_NO AS Batch_No
		                ,FORMAT(BD.BATCH_DATE,'YYYY-MM-DD')  AS  BATCH_DATE
		                ,BD.BATCH_TIME AS Batch_Time
		                ,Trim(BD.BATCH_TIME_TEXT) AS Batch_Time_Text
		                ,FORMAT(BH.BATCH_START_TIME, 'HH:MM:SS') AS Batch_Start_Time
		                ,FORMAT(BH.BATCH_END_TIME, 'HH:MM:SS') AS Batch_End_Time
		                ,Int(BD.Batch_Year) AS Batch_Year
		                ,Trim(BH.Batcher_Name) AS Batcher_Name
		                ,Trim(BH.Batcher_User_Level) AS Batcher_User_Level
		                ,Trim(BH.Customer_Code) AS Customer_Code
		                ,Trim(BH.RecipeCode) AS Recipe_Code
		                ,Trim(BH.RecipeName) AS Recipe_Name
		                ,int(BH.MIXEDTIME) AS Mixing_Time
		                ,int(BH.Mixer_Capacity) AS Mixer_Capacity
		                ,Int(BH.strength) AS strength
		                ,Trim(BH.Site) AS Site
		                ,Trim(BH.Truck_No) AS Truck_No
		                ,Trim(BH.Truck_Driver) AS Truck_Driver
		                ,BD.PRODUCTION_QTY AS Production_Qty
		                ,BD.Ordered_Qty AS Ordered_Qty
		                ,BD.Returned_Qty AS Returned_Qty
		                ,BD.WithThisLoad AS WithThisLoad
		                ,BD.BATCH_SIZE AS Batch_Size
		                ,BH.OrderNo AS Order_No
		                ,BH.Schedule_Id AS Schedule_Id
		                ,BD.Gate1_Target AS Gate1_Target
		                ,BD.Gate2_Target AS Gate2_Target
		                ,BD.Gate3_Target AS Gate3_Target
		                ,BD.Gate4_Target AS Gate4_Target
		                ,BD.Gate5_Target AS Gate5_Target
		                ,BD.Gate6_Target AS Gate6_Target
		                ,BD.Cement1_Target AS Cement1_Target
		                ,BD.Cement2_Target AS Cement2_Target
		                ,BD.Cement3_Target AS Cement3_Target
		                ,BD.Cement4_Target AS Cement4_Target
		                ,BD.Filler1_Target AS Filler_Target
		                ,BD.Water1_Target AS Water1_Target
		                ,BD.Water2_Target AS Water2_Target
		                ,BD.Silica_Target AS Silica_Target
		                ,BD.Slurry_Target AS slurry_Target
		                ,BD.Adm1_Target1 AS Adm1_Target1
		                ,BD.Adm1_Target2 AS Adm1_Target2
		                ,BD.Adm2_Target1 AS Adm2_Target1
		                ,BD.Adm2_Target2 AS Adm2_Target2
	                FROM DOCKET_HEADER AS BH
	                ,DOCKET_DETAILS AS BD
	                WHERE  BH.BATCH_NO  = BD.BATCH_NO 
	                AND BD.BATCH_NO = " + this.Batch_NO + "";
                    dtSource_Batch_Dat_Trans = DbHelper.FillDataTable(strQuery, strSycnFromDBAccessConnectionString);
                    dtSource_Batch_Dat_Trans.TableName = "Batch_Dat_Trans";

                    strQuery = @"SELECT BD.BATCH_NO AS Batch_No
		                ,BD.BATCH_INDEX AS Batch_Index
		                ,FORMAT(BD.BATCH_DATE,'YYYY-MM-DD')  AS  BATCH_DATE
		                ,BD.BATCH_TIME AS Batch_Time
		                ,Trim(BD.Batch_Time_Text) AS Batch_Time_Text
		                ,BD.Batch_Year AS Batch_Year
		                ,BD.Consistancy AS Consistancy
		                ,BD.Production_Qty AS Production_Qty
		                ,BD.Ordered_Qty AS Ordered_Qty
		                ,BD.Returned_Qty AS Returned_Qty
		                ,BD.WithThisLoad AS WithThisLoad
		                ,BD.Batch_Size AS Batch_Size
		                ,ROUND(BD.Gate1_Actual,0) AS Gate1_Actual
		                ,ROUND(BD.Gate1_Moisture,0) AS Gate1_Moisture
		                ,ROUND(BD.Gate2_Actual,0) AS Gate2_Actual
		                ,ROUND(BD.Gate2_Moisture,0) AS Gate2_Moisture
		                ,ROUND(BD.Gate3_Actual,0) AS Gate3_Actual
		                ,ROUND(BD.Gate3_Moisture,0) AS Gate3_Moisture
		                ,ROUND(BD.Gate4_Actual,0) AS Gate4_Actual
		                ,ROUND(BD.Gate4_Moisture,0) AS Gate4_Moisture
		                ,ROUND(BD.Gate5_Actual,0) AS Gate5_Actual
		                ,ROUND(BD.Gate5_Moisture,0) AS Gate5_Moisture 
		                ,ROUND(BD.Gate6_Actual,0) AS Gate6_Actual
		                ,ROUND(BD.Gate6_Target,0) AS Gate6_Target
		                ,ROUND(BD.Gate6_Moisture,0) AS Gate6_Moisture
		                ,ROUND(BD.Cement1_Actual,0) AS Cement1_Actual
		                ,ROUND(BD.Cement1_Correction,0) AS Cement1_Correction
		                ,ROUND(BD.Cement2_Actual,0) AS Cement2_Actual
		                ,ROUND(BD.Cement2_Correction,0) AS Cement2_Correction
		                ,ROUND(BD.Cement3_Actual,0) AS Cement3_Actual
		                ,ROUND(BD.Cement3_Correction,0) AS Cement3_Correction
		                ,ROUND(BD.Cement4_Actual,0)AS Cement4_Actual
		                ,ROUND(BD.Cement4_Target,0) AS Cement4_Target
		                ,ROUND(BD.Cement4_Correction,0) AS Cement4_Correction
		                ,ROUND(BD.Filler1_Actual,0) AS Filler1_Actual
		                ,ROUND(BD.Filler1_Target,0) AS Filler1_Target
		                ,ROUND(BD.Filler1_Correction,0) AS Filler1_Correction
		                ,ROUND(BD.Water1_Actual,0) AS Water1_Actual
		                ,ROUND(BD.Water1_Correction,0) AS Water1_Correction
		                ,ROUND(BD.Water2_Actual,0)  AS Water2_Actual
		                ,ROUND(BD.Water2_Target,0) AS Water2_Target
		                ,ROUND(BD.Water2_Correction,0) AS Water2_Correction
		                ,ROUND(BD.Silica_Actual,0) AS Silica_Actual
		                ,ROUND(BD.Silica_Target,0) AS Silica_Target
		                ,ROUND(BD.Silica_Correction,0) AS Silica_Correction
		                ,ROUND(BD.Slurry_Actual,0) AS Slurry_Actual
		                ,ROUND(BD.Slurry_Target,0) AS Slurry_Target
		                ,ROUND(BD.Slurry_Correction,0) AS Slurry_Correction
		                ,ROUND(BD.Adm1_Actual1,0) AS Adm1_Actual1
		                ,ROUND(BD.Adm1_Correction1,0) AS Adm1_Correction1
		                ,ROUND(BD.Adm1_Actual2,0) AS Adm1_Actual2
		                ,ROUND(BD.Adm1_Correction2,0) AS Adm1_Correction2
		                ,ROUND(BD.Adm2_Actual1,0) AS Adm2_Actual1
		                ,ROUND(BD.Adm2_Target1,0) AS Adm2_Target1
		                ,ROUND(BD.Adm2_Correction1,0) AS Adm2_Correction1
		                ,ROUND(BD.Adm2_Actual2,0) AS Adm2_Actual2
		                ,ROUND(BD.Adm2_Target2,0) AS Adm2_Target2
		                ,ROUND(BD.Adm2_Correction2,0) AS Adm2_Correction2
		                ,ROUND(BD.Pigment_Actual,0) AS Pigment_Actual
		                ,ROUND(BD.Pigment_Target,0) AS Pigment_Target
	                FROM DOCKET_DETAILS AS BD WHERE BD.BATCH_NO = " + this.Batch_NO + "";
                    dtSource_Batch_Transaction = DbHelper.FillDataTable(strQuery, strSycnFromDBAccessConnectionString);
                    dtSource_Batch_Transaction.TableName = "Batch_Transaction";

                    if (Get_Table_Name("BATCH_RESULT") != null)
                    {
                        strQuery = @"select " + this.Batch_NO + " as batch_no ," + General.orgId + " as ORG_ID, * from BATCH_RESULT where Ticket_no = " + this.Batch_NO + "";
                        dtBATCH_RESULT = DbHelper.FillDataTable(strQuery, strSycnFromDBAccessConnectionString);
                        if (dtBATCH_RESULT.Columns.Contains("BATCH_RESULT.batch_no"))
                        {
                            dtBATCH_RESULT.Columns.Remove("BATCH_RESULT.batch_no");
                        }
                        dtBATCH_RESULT.TableName = "BATCH_RESULT";
                    }

                    #endregion fill data table from access db to sync on live
                    #region Sync data on live
                    DataSetToUpload.Tables.Add(dtSource_Batch_Dat_Trans);
                    DataSetToUpload.Tables.Add(dtSource_Batch_Transaction);
                    DataSetToUpload.Tables.Add(dtBATCH_DATA_RAW_STATUS);
                    if (Get_Table_Name("BATCH_RESULT") != null)
                    {
                        DataSetToUpload.Tables.Add(dtBATCH_RESULT);
                    }
                    ScadaKing_Web_Service.Response_UploadData response = new ScadaKing_Web_Service.Response_UploadData();
                    response = SyncAndReSyncData(DataSetToUpload, "3", IsReSync);
                    #endregion
                    if (response.ErrorCode != 0)
                    {
                        string strErrMessage = string.Empty;
                        strErrMessage = "OOPS! Unable to upload the Data";
                        if (response.ErrorMessage.Contains("Cannot insert duplicate key in object") == true)
                        {
                            strErrMessage = strErrMessage + "\n Can not insert duplicate records..!!!!";
                        }
                        Global.WriteLog("Insert_Upload_Data_In_Replica_And_Live_DataBase() for Plant App Version[" + Program.PlantAppVersion + "] Response Exception: ", LogMessageType.Error, response.ErrorMessage);
                        General.ShowMessage(strErrMessage, this.Text);
                        IsInserted = false;
                        return false;
                    }
                    if (IsReSync == true)
                    {
                        IsInserted = true;
                        return true;
                    }
                    #region insert data into replica db
                    Insert_Data_In_Replica_Db(file_Created_Date);
                    #endregion
                }
                #endregion Version 3.1

                #region Version 1
                else if (Program.PlantAppVersion == "1")
                {
                    DataTable dtSource_VITRAG_BATCH = new DataTable();
                    #region fill data table from main access db to sync on live
                    strQuery = @"SELECT
	VB.BATCH_NO
	,IIF(ISNULL(CM.CUSTOMER_NAME),'',CM.CUSTOMER_NAME) AS CUSTOMER_CODE	
	,IIF(ISNULL(SM.SITE_ADDRESS),VB.SITE_CODE,SM.SITE_ADDRESS) AS SITE_CODE
	,IIF(ISNULL(TM.TRUCK_NO),'',TM.TRUCK_NO) AS TRUCK_NO
	,VB.PLANT
	,VB.TIME_CIC
	,VB.FORMULA_NO
	,VB.FORMULA_CODE
	,VB.FORMULA_NAME
	,VB.BATCH_INDEX
	,VB.M3_QTY
	,VB.BATCH_DATE
	,format(VB.BATCH_TIME,'HH:mm:ss') AS BATCH_TIME
	,VB.TYPE
	,VB.NAME_01
	,VB.NAME_02
	,VB.NAME_03
	,VB.NAME_04
	,VB.NAME_05
	,VB.NAME_06
	,VB.NAME_07
	,VB.NAME_08
	,VB.NAME_09
	,VB.TARGET_01
	,VB.TARGET_02
	,VB.TARGET_03
	,VB.TARGET_04
	,VB.TARGET_05
	,VB.TARGET_06
	,VB.TARGET_07
	,VB.TARGET_08
	,VB.TARGET_09
	,VB.ACTUAL_01
	,VB.ACTUAL_02
	,VB.ACTUAL_03
	,VB.ACTUAL_04
	,VB.ACTUAL_05
	,VB.ACTUAL_06
	,VB.ACTUAL_07
	,VB.ACTUAL_08
	,VB.ACTUAL_09
	,IIF(ISNULL(TM.TRUCK_DRIVER),'',TM.TRUCK_DRIVER) AS TRUCK_DRIVER
	,VB.TIM_MIX
	,VB.CLS_ESP
	,VB.FLAG
	,VB.NR_START
	,VB.BATCH_ROWID
	,VB.M3_MADE
	,VB.T_CYCLE
FROM ((VITRAG_BATCH AS VB LEFT JOIN CUSTOMER_MASTER AS CM ON (VB.CUSTOMER_CODE = CM.CUSTOMER_CODE OR VB.CUSTOMER_CODE = CM.CUSTOMER_NAME))
	LEFT JOIN SITE_MASTER AS SM ON (VB.SITE_CODE = SM.SITE OR VB.SITE_CODE = SM.SITE_ADDRESS))
	LEFT JOIN TRUCK_MASTER AS TM ON (VB.TRUCK_NO = TM.TRUCK_NO OR VB.TRUCK_NO = TM.TNO)
WHERE VB.BATCH_NO = " + this.Batch_NO + "";
                    dtSource_VITRAG_BATCH = DbHelper.FillDataTable(strQuery, strSycnFromDBAccessConnectionString);
                    dtSource_VITRAG_BATCH.TableName = "VITRAG_BATCH";
                    #endregion  fill data table from main access db to sync on live

                    #region Sync data on live
                    DataSetToUpload.Tables.Add(dtBATCH_DATA_RAW_STATUS);
                    DataSetToUpload.Tables.Add(dtSource_VITRAG_BATCH);
                    ScadaKing_Web_Service.Response_UploadData response = new ScadaKing_Web_Service.Response_UploadData();
                    response = SyncAndReSyncData(DataSetToUpload, Program.PlantAppVersion, IsReSync);
                    #endregion
                    if (response.ErrorCode != 0)
                    {
                        string strErrMessage = string.Empty;
                        strErrMessage = "OOPS! Unable to upload the Data";
                        if (response.ErrorMessage.Contains("Cannot insert duplicate key in object") == true)
                        {
                            strErrMessage = strErrMessage + "\n Can not insert duplicate records..!!!!";
                        }
                        Global.WriteLog("Insert_Upload_Data_In_Replica_And_Live_DataBase() for Plant App Version[" + Program.PlantAppVersion + "] Response Exception: ", LogMessageType.Error, response.ErrorMessage);
                        General.ShowMessage(strErrMessage, this.Text);
                        IsInserted = false;
                        return false;
                    }
                    if (IsReSync == true)
                    {
                        IsInserted = true;
                        return true;
                    }
                    #region insert data into replica db

                    Insert_Data_In_Replica_Db(file_Created_Date);

                    #endregion insert data into replica db
                }
                #endregion Version 1

                #region Version 2
                else if (Program.PlantAppVersion == "2")
                {
                    DataTable dtSource_BATCH = new DataTable();
                    #region fill data table from main access db to sync on live
                    strQuery = @"select * from BATCH where batchno = " + this.Batch_NO + "";
                    dtSource_BATCH = DbHelper.FillDataTable(strQuery, strSycnFromDBAccessConnectionString);
                    dtSource_BATCH.TableName = "BATCH";
                    #endregion
                    #region Sync data on live
                    DataSetToUpload.Tables.Add(dtBATCH_DATA_RAW_STATUS);
                    DataSetToUpload.Tables.Add(dtSource_BATCH);
                    ScadaKing_Web_Service.Response_UploadData response = new ScadaKing_Web_Service.Response_UploadData();
                    response = SyncAndReSyncData(DataSetToUpload, Program.PlantAppVersion, IsReSync);
                    #endregion
                    if (response.ErrorCode != 0)
                    {
                        string strErrMessage = string.Empty;
                        strErrMessage = "OOPS! Unable to upload the Data";
                        if (response.ErrorMessage.Contains("Cannot insert duplicate key in object") == true)
                        {
                            strErrMessage = strErrMessage + "\n Can not insert duplicate records..!!!!";
                        }
                        Global.WriteLog("Insert_Upload_Data_In_Replica_And_Live_DataBase() for Plant App Version[" + Program.PlantAppVersion + "] Response Exception: ", LogMessageType.Error, response.ErrorMessage);
                        General.ShowMessage(strErrMessage, this.Text);
                        IsInserted = false;
                        return false;
                    }
                    if (IsReSync == true)
                    {
                        IsInserted = true;
                        return true;
                    }
                    #region insert data into replica db

                    Insert_Data_In_Replica_Db(file_Created_Date);

                    #endregion insert data into replica db
                }
                #endregion Version 2

                #region Version 4.0
                if (Program.PlantAppVersion == "4.0")
                {
                    DataTable dtSource_Batch_Dat_Trans, dtSource_Batch_Transaction, dtBATCH_RESULT = new DataTable();
                    #region fill data table from access db to sync on live
                    //strQuery = @"select * from BATCH_DETAIL where batch_no = " + this.Batch_NO + "";
                    strQuery = @"SELECT BATCH_NO AS Batch_No
                                        ,FORMAT(BATCH_DATE,'YYYY-MM-DD')  AS  BATCH_DATE
                                        ,BATCH_STARTTIME AS Batch_Time
                                        ,BATCH_STARTTIME AS Batch_Time_Text
                                        ,BATCH_STARTTIME AS Batch_Start_Time
                                        ,BATCH_ENDTIME AS Batch_End_Time
                                        ,FORMAT(BATCH_DATE,'YYYY') AS Batch_Year
                                        ,'' AS Batcher_Name
                                        ,BATCH_ENDTIME AS Batcher_User_Level
                                        ,CUST_NAME AS Customer_Code
                                        ,GRADE AS Recipe_Code
                                        ,GRADE AS Recipe_Name
                                        ,0 AS Mixing_Time
                                        ,1 AS Mixer_Capacity
                                        ,0 AS strength
                                        ,SITE_ADR AS Site
                                        ,TRUCK_NO AS Truck_No
                                        ,'' AS Truck_Driver
                                        ,PRODUCTION_QTY AS Production_Qty
                                        ,0 AS Ordered_Qty
                                        ,0 AS Returned_Qty
                                        ,0 AS WithThisLoad
                                        ,BATCH_SIZE AS Batch_Size
                                        ,GRADE AS Order_No
                                        ,GRADE AS Schedule_Id
                                        ,AGR1_SET AS Gate1_Target
                                        ,AGR2_SET AS Gate2_Target
                                        ,AGR3_SET AS Gate3_Target
                                        ,AGR4_SET AS Gate4_Target
                                        ,AGR5_SET AS Gate5_Target
                                        ,0 AS Gate6_Target
                                        ,CEM1_SET AS Cement1_Target
                                        ,CEM2_SET AS Cement2_Target
                                        ,CEM3_SET AS Cement3_Target
                                        ,0 AS Cement4_Target
                                        ,0 AS Filler_Target
                                        ,WAT1_SET AS Water1_Target
                                        ,0 AS slurry_Target
                                        ,0 AS Water2_Target
                                        ,0 AS Silica_Target
                                        ,ADD1_SET AS Adm1_Target1
                                        ,0 AS Adm1_Target2
                                        ,ADD2_SET AS Adm2_Target1
                                        ,0 AS Adm2_Target2
                                         from BATCH_DETAIL  where batch_no = " + this.Batch_NO + "";
                    dtSource_Batch_Dat_Trans = DbHelper.FillDataTable(strQuery, strSycnFromDBAccessConnectionString);
                    dtSource_Batch_Dat_Trans.TableName = "Batch_Dat_Trans";

                    strQuery = @"SELECT BATCH_NO AS Batch_No
                                        ,BATCH_INDEX AS Batch_Index
                                        ,FORMAT(BATCHX_DATE,'YYYY-MM-DD')  AS  BATCH_DATE
                                        ,BATCHX_ENDTIME AS Batch_Time
                                        ,BATCHX_ENDTIME AS Batch_Time_Text
                                        ,FORMAT(BATCHX_DATE,'YYYY') AS Batch_Year
                                        ,0 AS Consistancy
                                        ,PRODUCTIONX_QTY AS Production_Qty
                                        ,0 AS Ordered_Qty
                                        ,0 AS Returned_Qty
                                        ,0 AS WithThisLoad
                                        ,BATCHX_SIZE AS Batch_Size
                                        ,ROUND(AGR1_ACT,0) AS Gate1_Actual
                                        ,0 AS Gate1_Moisture
                                        ,ROUND(AGR2_ACT,0) AS Gate2_Actual
                                        ,0 AS Gate2_Moisture
                                        ,ROUND(AGR3_ACT,0) AS Gate3_Actual
                                        ,0 AS Gate3_Moisture
                                        ,ROUND(AGR4_ACT,0) AS Gate4_Actual
                                        ,0 AS Gate4_Moisture
                                        ,ROUND(AGR5_ACT,0) AS Gate5_Actual
                                        ,0 AS Gate5_Moisture 
                                        ,0 AS Gate6_Actual
                                        ,0 AS Gate6_Target
                                        ,0 AS Gate6_Moisture
                                        ,ROUND(CEM1_ACT,0) AS Cement1_Actual
                                        ,0 AS Cement1_Correction
                                        ,ROUND(CEM2_ACT,0) AS Cement2_Actual
                                        ,0 AS Cement2_Correction
                                        ,ROUND(CEM3_ACT,0) AS Cement3_Actual
                                        ,0 AS Cement3_Correction
                                        ,0 AS Cement4_Actual
                                        ,0 AS Cement4_Target
                                        ,0 AS Cement4_Correction
                                        ,0 AS Filler1_Actual
                                        ,0 AS Filler1_Target
                                        ,0 AS Filler1_Correction
                                        ,ROUND(WAT1_ACT,0) AS Water1_Actual
                                        ,0 AS Water1_Correction
                                        ,0 AS Water2_Actual
                                        ,0 AS Water2_Target
                                        ,0 AS Water2_Correction
                                        ,0 AS Silica_Actual
                                        ,0 AS Silica_Target
                                        ,0 AS Silica_Correction
                                        ,0 AS Slurry_Actual
                                        ,0 AS Slurry_Target
                                        ,0 AS Slurry_Correction
                                        ,ADD1_ACT AS Adm1_Actual1
                                        ,0 AS Adm1_Correction1
                                        ,ADD2_ACT AS Adm1_Actual2
                                        ,0 AS Adm1_Correction2
                                        ,0 AS Adm2_Actual1
                                        ,0 AS Adm2_Target1
                                        ,0 AS Adm2_Correction1
                                        ,0 AS Adm2_Actual2
                                        ,0 AS Adm2_Target2
                                        ,0 AS Adm2_Correction2
                                        ,0 AS Pigment_Actual
                                        ,0 AS Pigment_Target
                                         FROM BATCHES  where batch_no = " + this.Batch_NO + "";
                    dtSource_Batch_Transaction = DbHelper.FillDataTable(strQuery, strSycnFromDBAccessConnectionString);
                    dtSource_Batch_Transaction.TableName = "Batch_Transaction";

                    if (Get_Table_Name("BATCH_RESULT") != null)
                    {
                        strQuery = @"select " + this.Batch_NO + " as batch_no ," + General.orgId + " as ORG_ID, * from BATCH_RESULT where Ticket_no = " + this.Batch_NO + "";
                        dtBATCH_RESULT = DbHelper.FillDataTable(strQuery, strSycnFromDBAccessConnectionString);
                        if (dtBATCH_RESULT.Columns.Contains("BATCH_RESULT.batch_no"))
                        {
                            dtBATCH_RESULT.Columns.Remove("BATCH_RESULT.batch_no");
                        }
                        dtBATCH_RESULT.TableName = "BATCH_RESULT";
                    }

                    #endregion fill data table from access db to sync on live
                    #region Sync data on live
                    DataSetToUpload.Tables.Add(dtSource_Batch_Dat_Trans);
                    DataSetToUpload.Tables.Add(dtSource_Batch_Transaction);
                    DataSetToUpload.Tables.Add(dtBATCH_DATA_RAW_STATUS);
                    if (Get_Table_Name("BATCH_RESULT") != null)
                    {
                        DataSetToUpload.Tables.Add(dtBATCH_RESULT);
                    }
                    Program.PlantAppVersion = "3";
                    ScadaKing_Web_Service.Response_UploadData response = new ScadaKing_Web_Service.Response_UploadData();
                    response = SyncAndReSyncData(DataSetToUpload, Program.PlantAppVersion, IsReSync);
                    Program.PlantAppVersion = "4.0";
                    #endregion
                    if (response.ErrorCode != 0)
                    {
                        string strErrMessage = string.Empty;
                        strErrMessage = "OOPS! Unable to upload the Data";
                        if (response.ErrorMessage.Contains("Cannot insert duplicate key in object") == true)
                        {
                            strErrMessage = strErrMessage + "\n Can not insert duplicate records..!!!!";
                        }
                        Global.WriteLog("Insert_Upload_Data_In_Replica_And_Live_DataBase() for Plant App Version[" + Program.PlantAppVersion + "] Response Exception: ", LogMessageType.Error, response.ErrorMessage);
                        General.ShowMessage(strErrMessage, this.Text);
                        IsInserted = false;
                        return false;
                    }
                    if (IsReSync == true)
                    {
                        IsInserted = true;
                        return true;
                    }
                    #region insert data into replica db
                    Insert_Data_In_Replica_Db(file_Created_Date);
                    #endregion
                }
                #endregion Version 3

                IsInserted = true;
            }
            catch (Exception ex)
            {
                IsInserted = false;
                Global.WriteLog("Insert_Upload_Data_In_Replica_And_Live_DataBase() FOR Plant App Version[" + Program.PlantAppVersion + "] Application Exception: ", LogMessageType.Error, ex.InnerException.ToString());
                throw ex;
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
            return IsInserted;
        }

        private ScadaKing_Web_Service.Response_UploadData SyncAndReSyncData(DataSet DataSetToUpload, string plant_App_Version, bool IsReSync)
        {
            ScadaKing_Web_Service.Response_UploadData response = new ScadaKing_Web_Service.Response_UploadData();
            try
            {
                ScadaKing_Web_Service.paramUploadData_en objEntity = new ScadaKing_Web_Service.paramUploadData_en();
                ScadaKing_Web_Service.TransactionHeader TransactionHeader = new ScadaKing_Web_Service.TransactionHeader();
                TransactionHeader.UserName = General.User_Name;
                TransactionHeader.Password = General.ConfigService_Password;
                TransactionHeader.DeviceID = General.ConfigMachine_Id;
                TransactionHeader.Config_App_Name = General.ConfigAPP_Name;
                TransactionHeader.Config_App_Version = General.ConfigAPP_Version;
                TransactionHeader.Win_Service_Version = General.Win_Service_Version;
                TransactionHeader.Win_Service_GUID = General.Win_Service_GUID;
                ScadaKing_Web_Service.ScadaKing_Web_ServiceSoapClient objService = new ScadaKing_Web_Service.ScadaKing_Web_ServiceSoapClient();
                objEntity.DataSetToUpload = new DataSet();
                objEntity.DataSetToUpload = DataSetToUpload;
                objEntity.PlantAppVersion = plant_App_Version;
                objEntity.IsReSync = IsReSync;
                response = objService.Upload_Replica_Data(TransactionHeader, objEntity);
            }
            catch (Exception ex)
            {
                Global.WriteLog("SyncAndReSyncData() Application Exception: ", LogMessageType.Error, ex.Message);
                throw ex;
            }
            return response;
        }

        private string SqlTableColumn(DataTable dtObject)
        {
            string ReturnScript = string.Empty;
            try
            {
                string tableName = dtObject.TableName.ToString();
                string strColumns = string.Empty;
                int[] columnNumbers = new int[dtObject.Columns.Count];
                int columnNumbersIndex = 0;
                int columnIndex = 0;
                foreach (DataColumn dtcCurrenColumn in dtObject.Columns)
                {
                    strColumns += ((strColumns.Length > 0) ? ",[" + dtcCurrenColumn.ColumnName.ToString() + "]" : "[" + dtcCurrenColumn.ColumnName.ToString() + "]");
                    if (dtcCurrenColumn.DataType == typeof(DateTime))
                    {
                        columnNumbers[columnNumbersIndex] = columnIndex;
                        columnNumbersIndex++;
                    }
                    columnIndex++;
                }
                ReturnScript = strColumns;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ReturnScript;
        }

        private string SqlTableValues(DataRow CurrentRow)
        {
            string ReturnScript = string.Empty;
            try
            {
                string strValues = string.Empty;
                int ItmeCount = CurrentRow.ItemArray.Length;
                for (int i = 0; i < ItmeCount; i++)
                {
                    string strDataType = CurrentRow[i].GetType().ToString();
                    if (string.IsNullOrEmpty(strValues))
                    {
                        if (strDataType == "System.DateTime")
                        {
                            if (CurrentRow.Table.Columns[i].ColumnName == "Batch_Date")
                            {
                                DateTime dtValues = DateTime.Parse(CurrentRow[i].ToString());
                                string strdtValues = dtValues.ToShortDateString().ToString();
                                strValues += ",'" + strdtValues + "'";
                            }
                            else
                            {
                                DateTime dtValues = DateTime.Parse(CurrentRow[i].ToString());
                                string strdtValues = dtValues.ToString();
                                strValues += ",'" + strdtValues + "'";
                            }
                        }
                        else
                        {
                            strValues += "'" + CurrentRow.ItemArray.GetValue(i) + "'";
                        }
                    }
                    else
                    {
                        if (strDataType == "System.DateTime")
                        {
                            if (CurrentRow.Table.Columns[i].ColumnName == "Batch_Date")
                            {
                                DateTime dtValues = DateTime.Parse(CurrentRow[i].ToString());
                                string strdtValues = dtValues.ToShortDateString().ToString();
                                strValues += ",'" + strdtValues + "'";
                            }
                            else
                            {
                                DateTime dtValues = DateTime.Parse(CurrentRow[i].ToString());
                                string strdtValues = dtValues.ToString();
                                strValues += ",'" + strdtValues + "'";
                            }
                        }
                        else if (strDataType == "System.DBNull")
                        {
                            strValues += ",NULL";
                        }
                        else
                        {
                            strValues += ",'" + CurrentRow.ItemArray.GetValue(i) + "'";
                        }
                    }
                }
                ReturnScript = strValues;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ReturnScript;
        }

        #region [ RMC ]

        /// <summary>
        /// 
        /// </summary>
        /// <param name="is_Insert"></param>
        /// <param name="update_html_BDT_BR_BT">-1 for html ,0 for update_count</param>
        /// <param name="sourceConnectionString"></param>
        /// <param name="batch_No_Or_Row_ID"></param>
        /// <param name="batch_Index"></param>
        /// <param name="batch_Date"></param>
        /// <param name="batch_Time"></param>
        private void Insert_Update_Batch_Transaction_Upload_Status(
            string sourceConnectionString,
            string batch_No_Or_Row_ID)
        {
            string access_Query = string.Empty;
            access_Query = "UPDATE BATCH_TRANSACTION_UPLOAD_STATUS SET ";
            access_Query += "IS_HTML_FILE_CREATED = 1";
            access_Query += ",IS_MCGM_BATCH = 1";
            access_Query += ",FILE_CREATED_COUNT = 1";
            access_Query += ",IS_MANUAL_CREATED = 1";
            access_Query += ",IS_MANUAL_CREATED_DATE = #" + DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss") + "#";
            access_Query += " WHERE BATCH_NO = " + batch_No_Or_Row_ID;

            int recordsAffected = 0;
            try
            {
                OleDbConnection ole_Source_Connection;
                using (ole_Source_Connection = new OleDbConnection(sourceConnectionString))
                {
                    ole_Source_Connection.Open();
                    using (OleDbCommand accessCommand = new OleDbCommand())
                    {
                        accessCommand.Connection = ole_Source_Connection;
                        accessCommand.CommandText = access_Query;
                        recordsAffected = accessCommand.ExecuteNonQuery();
                    }
                }

                if (recordsAffected == 0)
                {
                    access_Query = "Insert into BATCH_TRANSACTION_UPLOAD_STATUS(BATCH_NO ,BATCH_INDEX";
                    access_Query += ",BATCH_DATE ,BATCH_TIME ,UPDATE_COUNT ,IS_HTML_FILE_CREATED";
                    access_Query += ",FILE_CREATED_COUNT,IS_MCGM_BATCH,IS_MANUAL_CREATED ,IS_MANUAL_CREATED_DATE)VALUES(";
                    access_Query += batch_No_Or_Row_ID;/*BATCH_NO*/
                    access_Query += ",'0'";  /*BATCH_INDEX*/
                    access_Query += ",NULL"; /*BATCH_DATE*/
                    access_Query += ",NULL"; /*BATCH_TIME*/
                    access_Query += ",1"; /*UPDATE_COUNT*/
                    access_Query += ",1"; /*IS_HTML_FILE_CREATED*/
                    access_Query += ",1"; /*FILE_CREATED_COUNT*/
                    access_Query += ",1"; /*IS_MCGM_BATCH*/
                    access_Query += ",1";/*IS_MANUAL_CREATED*/
                    access_Query += ",#" + DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss") + "#";/*IS_MANUAL_CREATED_DATE*/
                    access_Query += ")";

                    using (ole_Source_Connection = new OleDbConnection(sourceConnectionString))
                    {
                        ole_Source_Connection.Open();
                        using (OleDbCommand accessCommand = new OleDbCommand())
                        {
                            accessCommand.Connection = ole_Source_Connection;
                            accessCommand.CommandText = access_Query;
                            recordsAffected = accessCommand.ExecuteNonQuery();
                        }
                    }
                }
            }
            catch (Exception)
            {
            }
        }


        private DataTable FillDatatableOLE(string selectQuery, OleDbConnection connection)
        {
            try
            {
                if (connection.State != ConnectionState.Open)
                {
                    connection.Open();
                }

                DataTable returnData = new DataTable();
                using (OleDbDataAdapter adapter = new OleDbDataAdapter())
                {
                    adapter.SelectCommand = new OleDbCommand(selectQuery, connection);
                    adapter.SelectCommand.CommandType = CommandType.Text;
                    adapter.Fill(returnData);
                }

                return returnData;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        #endregion [ RMC ]

        private void btnPreview_Batch_Click(object sender, EventArgs e)
        {
            try
            {
                DataSet dsReturn_Value = new DataSet();
                // if (File.Exists(this.File_Name_N_Path) == false ||
                //        ((Button)sender).Name == this.btnUpdate.Name
                //    )
                //{
                #region [ HTML FILE Generator ]
                string sourceConnectionString = string.Empty;
                sourceConnectionString = dataConnectionConfiguration.Source_Connection_String;

                ScadaKing_Web_Service.Response_LocalSQLData live_Response = new ScadaKing_Web_Service.Response_LocalSQLData();
                try
                {
                    if (this._reGenerate_File == true)
                    {
                        dsReturn_Value = ScadaKing_Data_App.Controller.BatchDataController.MCGM_Transfered_Batch(Convert.ToInt64(this.Batch_NO), dataConnectionConfiguration.Target_Connection_String);
                    }
                    else
                    {
                        dsReturn_Value = ScadaKing_Data_App.Controller.BatchDataController.MCGM_Transfered_Batch(Convert.ToInt64(this.Batch_NO), dataConnectionConfiguration.Source_Connection_String);
                    }
                }
                catch (Exception ex)
                {
                    pnlWebBrowser.Visible = false;
                    pnlHTMLGrid.Visible = false;
                    Global.WriteLog("Preview", LogMessageType.Error, ex.Message);
                    return;
                }

                if (dsReturn_Value.Tables.Count > 0)
                {
                    if (dsReturn_Value.Tables.Count == 1)
                    {
                        General.ShowMessage("For Sr No. " + this.Batch_NO + " " + dsReturn_Value.Tables[0].Rows[0]["ERROR_DESCRIPTION"].ToString(), this.Text);
                    }
                    else if (dsReturn_Value.Tables[0].Rows.Count > 0)
                    {
                        try
                        {
                            htmlDetails = Global.Create_RMC_HMTL_File(dsReturn_Value, this.Batch_NO.ToString());
                        }
                        catch (Exception ex)
                        {
                            General.ShowMessage("M N Error while Generating file for Sr No(s) : " + Convert.ToString(this.Batch_NO) + Environment.NewLine + ex.Message.ToString(), this.Text);
                        }
                    }
                }
                else
                {
                    General.ShowMessage("Data not found Sr Nos(s) 001 : " + Convert.ToString(this.Batch_NO), this.Text);
                }

                #endregion [ HTML FILE Generator ]
                // }

                //this.btnUpdate.Enabled = true;
                if (Program.ConfigIS_CREATE_HTML_FILE == false && this._reGenerate_File == true)
                {
                    this.btnUpdate.Enabled = false;
                }
                else
                {
                    if (Program.ConfigIS_CREATE_HTML_FILE == true && this._reGenerate_File == true && Is_Government_Batch == false)
                    {
                        this.btnUpdate.Enabled = false;
                    }
                    else
                    {
                        this.btnUpdate.Enabled = true;
                    }
                }

                try
                {
                    if (Program.ConfigIS_CREATE_HTML_FILE == true)
                    {
                        pnlWebBrowser.Visible = true;
                        pnlHTMLGrid.Visible = false;
                        wb1.Visible = true;
                        pnlWebBrowser.Dock = DockStyle.Fill;
                        wb1.DocumentText = htmlDetails;
                    }
                    else
                    {
                        this.btnUpdate.Text = "Upload Data";
                        pnlHTMLGrid.Visible = true;
                        pnlHTMLGrid.Enabled = true;
                        pnlWebBrowser.Visible = false;
                        wb1.Visible = false;
                        pnlHTMLGrid.Dock = DockStyle.Fill;
                        DataTable dtHTMLHeader, dtHTMLDetails = new DataTable();
                        dtHTMLHeader = dsReturn_Value.Tables["HTML_HEADER"];
                        dtHTMLDetails = dsReturn_Value.Tables["HTML_DETAILS"];

                        if (dtHTMLHeader.Rows.Count > 0)
                        {
                            //txtBatch_Date.Text = Convert.ToDateTime(dtHTMLHeader.Rows[0]["BATCH_DATE"]).ToString("dd/MM/yyyy");
                            txtBatch_Date.Text = Convert.ToDateTime(dtHTMLHeader.Rows[0]["BATCH_DATE"]).ToString(Program.Date_Format);
                            txtBatch_Date.ReadOnly = true;
                            txtBatch_No.Text = dtHTMLHeader.Rows[0]["BATCH_NO"].ToString();
                            txtBatch_No.ReadOnly = true;
                            txtBatcher_Name.Text = dtHTMLHeader.Rows[0]["Batcher_Name"].ToString();
                            txtBatcher_Name.ReadOnly = true;
                            txtCustomer.Text = dtHTMLHeader.Rows[0]["CUSTOMER_NAME"].ToString();
                            txtCustomer.ReadOnly = true;
                            //txtEnd_Time.Text = dtHTMLHeader.Rows[0]["BATCH_END_TIME"].ToString();
                            txtEnd_Time.Text = Convert.ToDateTime(dtHTMLHeader.Rows[0]["BATCH_END_TIME"]).ToString(Program.Time_Format);
                            txtEnd_Time.ReadOnly = true;
                            txtPlant_Serial_No.Text = dtHTMLHeader.Rows[0]["PlantSlNO"].ToString();
                            txtPlant_Serial_No.ReadOnly = true;
                            txtProduction.Text = dtHTMLHeader.Rows[0]["Production_Qty"].ToString();
                            txtProduction.ReadOnly = true;
                            txtRecipe_Code.Text = dtHTMLHeader.Rows[0]["Recipe_Code"].ToString();
                            txtRecipe_Code.ReadOnly = true;
                            txtRecipe_Name.Text = dtHTMLHeader.Rows[0]["Recipe_Name"].ToString();
                            txtRecipe_Name.ReadOnly = true;
                            txtReturned.Text = dtHTMLHeader.Rows[0]["Returned_Qty"].ToString();
                            txtReturned.ReadOnly = true;
                            txtSite_Address.Text = dtHTMLHeader.Rows[0]["Site"].ToString();
                            txtSite_Address.ReadOnly = true;
                            txtSize.Text = dtHTMLHeader.Rows[0]["Batch_Size"].ToString();
                            txtSize.ReadOnly = true;
                            //txtStart_Time.Text = dtHTMLHeader.Rows[0]["BATCH_START_TIME"].ToString();
                            txtStart_Time.Text = Convert.ToDateTime(dtHTMLHeader.Rows[0]["BATCH_START_TIME"]).ToString(Program.Time_Format);
                            txtStart_Time.ReadOnly = true;
                            txtTotal_Order.Text = dtHTMLHeader.Rows[0]["Ordered_Qty"].ToString();
                            txtTotal_Order.ReadOnly = true;
                            txtTruck_Driver.Text = dtHTMLHeader.Rows[0]["Truck_Driver"].ToString();
                            txtTruck_Driver.ReadOnly = true;
                            txtTruck_No.Text = dtHTMLHeader.Rows[0]["Truck_No"].ToString();
                            txtTruck_No.ReadOnly = true;
                            txtwith_This_Load.Text = dtHTMLHeader.Rows[0]["WithThisLoad"].ToString();
                            txtwith_This_Load.ReadOnly = true;
                        }

                        DataTable dtgrid1 = dtHTMLDetails.Select("OrderNo in(2,3)").CopyToDataTable();
                        DataTable dtgrid2 = dtHTMLDetails.Select("OrderNo = 4", "flag").CopyToDataTable();
                        DataTable dtgrid3 = dtHTMLDetails.Select("OrderNo in(5,6)").CopyToDataTable();
                        DataTable dtRowHeader = dtHTMLDetails.Select("OrderNo = 1").CopyToDataTable();
                        if (dtRowHeader.Columns.Contains("OrderNo"))
                        {
                            dtRowHeader.Columns.Remove("OrderNo");
                        }
                        if (dtgrid1.Columns.Contains("OrderNo"))
                        {
                            dtgrid1.Columns.Remove("OrderNo");
                        }
                        if (dtgrid2.Columns.Contains("OrderNo"))
                        {
                            dtgrid2.Columns.Remove("OrderNo");
                        }
                        if (dtgrid3.Columns.Contains("OrderNo"))
                        {
                            dtgrid3.Columns.Remove("OrderNo");
                        }

                        dgvHtmlHeader.DataSource = dtgrid1;
                        for (int i = 0; i < dtRowHeader.Rows[0].ItemArray.Length; i++)
                        {
                            dgvHtmlHeader.Columns[i].HeaderText = dtRowHeader.Rows[0].ItemArray.GetValue(i).ToString();
                            dgvHtmlHeader.Columns[i].Width = 100;
                        }

                        dgvHtmlDetails.DataSource = dtgrid2;
                        foreach (DataGridViewColumn col in dgvHtmlDetails.Columns)
                        {
                            col.Width = 100;
                        }
                        dgvHtmlSum.DataSource = dtgrid3;
                        foreach (DataGridViewColumn col in dgvHtmlSum.Columns)
                        {
                            col.Width = 100;
                        }
                    }
                }
                catch (Exception)
                {
                }
            }
            catch (Exception)
            {
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            try
            {
                this.Close();
            }
            catch (Exception)
            {
            }
        }

        private void rdoSetMCGM_NonMCGM(object sender, EventArgs e)
        {
            if (((RadioButton)sender).Name == rdoMCGM.Name)
            {
                if (Program.ConfigIS_CREATE_HTML_FILE == false)
                {
                    btnUpdate.Text = "Upload Data";
                }
                else
                {
                    btnUpdate.Text = "Generate File && Upload Data";
                }
            }
            else
            {
                btnUpdate.Text = "Upload Data";
            }
        }

        private void btnResync_Click(object sender, EventArgs e)
        {
            try
            {
             
                string file_Created_Date = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss");
                string strAccessQuery = string.Empty;

                strAccessQuery = @"SELECT BDT_UPDATE_COUNT FROM BATCH_MASTER_UPLOAD_STATUS WHERE BATCH_NO = " + this.Batch_NO + "";

                object objSyncCount = DbHelper.ExecuteScalar(strAccessQuery, dataConnectionConfiguration.Target_Connection_String);
                int iSyncCount = 0;
                string strConfirmMsg = string.Empty;
                if (objSyncCount != null)
                {
                    iSyncCount = Convert.ToInt32(objSyncCount);
                }
                if (iSyncCount > 0)
                {
                    strConfirmMsg = "You have already uploaded the Data for " + iSyncCount.ToString() + " time(s)." + Environment.NewLine + "Do you want to Upload again?";
                }
                else
                {
                    strConfirmMsg = "Do you want to Re-Upload data?";
                }

                if (General.ShowYesNoMessage(strConfirmMsg, this.Text) == System.Windows.Forms.DialogResult.Yes)
                {
                    if (Insert_Upload_Data_In_Replica_And_Live_DataBase(file_Created_Date, true) == true)
                    {
                        General.ShowMessage("Data uploaded successfully", this.Text);
                        iSyncCount++;
                        strAccessQuery = @"UPDATE BATCH_MASTER_UPLOAD_STATUS SET BDT_UPDATE_COUNT = " + iSyncCount + "  WHERE BATCH_NO = " + this.Batch_NO + "";
                        DbHelper.ExecuteScalar(strAccessQuery, dataConnectionConfiguration.Target_Connection_String);
                        DbHelper.ExecuteScalar(strAccessQuery, dataConnectionConfiguration.Source_Connection_String);
                        btnExit_Click(sender, new EventArgs());
                    }
                }
            }
            catch (Exception ex)
            {
                General.ShowMessage(ex.Message, this.Text);
            }
        }

        private void btnGenerateFile_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            try
            {
                string file_Created_Date = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss");
                #region Insert Data in Replica db
                Insert_Data_In_Replica_Db(file_Created_Date);
                #endregion Insert Data in Replica db

                #region Create HTML File
                this.btnPreview_Batch_Click(sender, e);
                if (Program.ConfigIS_CREATE_HTML_FILE == true)
                {
                    Global.Write_HTML_File(htmlDetails, true, this.Batch_NO.ToString(), string.Empty);
                    General.ShowMessage("File Generated successfully...", this.Text);
                }
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
                btnExit_Click(sender, new EventArgs());
                #endregion Create HTML File
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        #region Insert Data in Replica db
        private void Insert_Data_In_Replica_Db(string file_Created_Date)
        {
            try
            {
                string strQuery = string.Empty;
                string strAccessQuery = string.Empty;

                #region Version 4.0
                if (Program.PlantAppVersion == "4.0")
                {
                    DataTable dtSource_Batch_Dat_Trans, dtSource_Batch_Transaction, dtBATCH_RESULT = new DataTable();
                    #region fill data table from main access db
                    strQuery = @"select * from BATCH_DETAIL where batch_no = " + this.Batch_NO + "";
                    dtSource_Batch_Dat_Trans = DbHelper.FillDataTable(strQuery, dataConnectionConfiguration.Source_Connection_String);
                    dtSource_Batch_Dat_Trans.TableName = "BATCH_DETAIL";

                    strQuery = @"select * from BATCHES where batch_no = " + this.Batch_NO + "";
                    dtSource_Batch_Transaction = DbHelper.FillDataTable(strQuery, dataConnectionConfiguration.Source_Connection_String);
                    dtSource_Batch_Transaction.TableName = "BATCHES";

                    if (Get_Table_Name("BATCH_RESULT") != null)
                    {
                        strQuery = @"select * from BATCH_RESULT where Ticket_no = " + this.Batch_NO + "";
                        dtBATCH_RESULT = DbHelper.FillDataTable(strQuery, dataConnectionConfiguration.Source_Connection_String);
                        dtBATCH_RESULT.TableName = "BATCH_RESULT";
                    }

                    #endregion fill data table from access db

                    strQuery = @"DELETE * FROM BATCH_DETAIL WHERE BATCH_NO = " + this.Batch_NO + "";
                    DbHelper.ExecuteNonQuery(strQuery, dataConnectionConfiguration.Target_Connection_String);
                    foreach (DataRow row in dtSource_Batch_Dat_Trans.Rows)
                    {
                        strAccessQuery = string.Empty;
                        strAccessQuery = @"Insert into BATCH_DETAIL(" + SqlTableColumn(dtSource_Batch_Dat_Trans);
                        strAccessQuery += @")values(";
                        strAccessQuery += SqlTableValues(row);
                        strAccessQuery += ")";
                        DbHelper.ExecuteNonQuery(strAccessQuery, dataConnectionConfiguration.Target_Connection_String);
                    }

                    strQuery = @"DELETE * FROM BATCHES WHERE BATCH_NO = " + this.Batch_NO + "";
                    DbHelper.ExecuteNonQuery(strQuery, dataConnectionConfiguration.Target_Connection_String);
                    foreach (DataRow row in dtSource_Batch_Transaction.Rows)
                    {
                        strAccessQuery = string.Empty;
                        strAccessQuery = @"Insert into BATCHES(" + SqlTableColumn(dtSource_Batch_Transaction);
                        strAccessQuery += @")values(";
                        strAccessQuery += SqlTableValues(row);
                        strAccessQuery += ")";
                        DbHelper.ExecuteNonQuery(strAccessQuery, dataConnectionConfiguration.Target_Connection_String);
                    }

                    if (Get_Table_Name("BATCH_RESULT") != null)
                    {
                        strQuery = @"DELETE * FROM BATCH_RESULT WHERE TICKET_NO = " + this.Batch_NO + "";
                        DbHelper.ExecuteNonQuery(strQuery, dataConnectionConfiguration.Target_Connection_String);
                        foreach (DataRow row in dtBATCH_RESULT.Rows)
                        {
                            strAccessQuery = string.Empty;
                            strAccessQuery = @"Insert into batch_result(" + SqlTableColumn(dtBATCH_RESULT);
                            strAccessQuery += @")values(";
                            strAccessQuery += SqlTableValues(row);
                            strAccessQuery += ")";
                            DbHelper.ExecuteNonQuery(strAccessQuery, dataConnectionConfiguration.Target_Connection_String);
                        }
                    }

                }
                #endregion Version 4.0
                #region Version 3
                if (Program.PlantAppVersion == "3")
                {
                    DataTable dtSource_Batch_Dat_Trans, dtSource_Batch_Transaction, dtBATCH_RESULT = new DataTable();
                    #region fill data table from main access db
                    strQuery = @"select * from Batch_Dat_Trans where batch_no = " + this.Batch_NO + "";
                    dtSource_Batch_Dat_Trans = DbHelper.FillDataTable(strQuery, dataConnectionConfiguration.Source_Connection_String);
                    dtSource_Batch_Dat_Trans.TableName = "Batch_Dat_Trans";

                    strQuery = @"select * from Batch_Transaction where batch_no = " + this.Batch_NO + "";
                    dtSource_Batch_Transaction = DbHelper.FillDataTable(strQuery, dataConnectionConfiguration.Source_Connection_String);
                    dtSource_Batch_Transaction.TableName = "Batch_Transaction";

                    if (Get_Table_Name("BATCH_RESULT") != null)
                    {
                        strQuery = @"select * from BATCH_RESULT where Ticket_no = " + this.Batch_NO + "";
                        dtBATCH_RESULT = DbHelper.FillDataTable(strQuery, dataConnectionConfiguration.Source_Connection_String);
                        dtBATCH_RESULT.TableName = "BATCH_RESULT";
                    }

                    #endregion fill data table from access db

                    strQuery = @"DELETE * FROM BATCH_DAT_TRANS WHERE BATCH_NO = " + this.Batch_NO + "";
                    DbHelper.ExecuteNonQuery(strQuery, dataConnectionConfiguration.Target_Connection_String);
                    foreach (DataRow row in dtSource_Batch_Dat_Trans.Rows)
                    {
                        strAccessQuery = string.Empty;
                        strAccessQuery = @"Insert into BATCH_DAT_TRANS(" + SqlTableColumn(dtSource_Batch_Dat_Trans);
                        strAccessQuery += @")values(";
                        strAccessQuery += SqlTableValues(row);
                        strAccessQuery += ")";
                        DbHelper.ExecuteNonQuery(strAccessQuery, dataConnectionConfiguration.Target_Connection_String);
                    }

                    strQuery = @"DELETE * FROM BATCH_TRANSACTION WHERE BATCH_NO = " + this.Batch_NO + "";
                    DbHelper.ExecuteNonQuery(strQuery, dataConnectionConfiguration.Target_Connection_String);
                    foreach (DataRow row in dtSource_Batch_Transaction.Rows)
                    {
                        strAccessQuery = string.Empty;
                        strAccessQuery = @"Insert into BATCH_TRANSACTION(" + SqlTableColumn(dtSource_Batch_Transaction);
                        strAccessQuery += @")values(";
                        strAccessQuery += SqlTableValues(row);
                        strAccessQuery += ")";
                        DbHelper.ExecuteNonQuery(strAccessQuery, dataConnectionConfiguration.Target_Connection_String);
                    }

                    if (Get_Table_Name("BATCH_RESULT") != null)
                    {
                        strQuery = @"DELETE * FROM BATCH_RESULT WHERE TICKET_NO = " + this.Batch_NO + "";
                        DbHelper.ExecuteNonQuery(strQuery, dataConnectionConfiguration.Target_Connection_String);
                        foreach (DataRow row in dtBATCH_RESULT.Rows)
                        {
                            strAccessQuery = string.Empty;
                            strAccessQuery = @"Insert into batch_result(" + SqlTableColumn(dtBATCH_RESULT);
                            strAccessQuery += @")values(";
                            strAccessQuery += SqlTableValues(row);
                            strAccessQuery += ")";
                            DbHelper.ExecuteNonQuery(strAccessQuery, dataConnectionConfiguration.Target_Connection_String);
                        }
                    }

                    #region Insert Missing  Master Entiry for the current batch in Replica db
                    DataTable dtSourceMastersId, dtMaster = new DataTable();
                    strQuery = @"SELECT	
	                            SM.SITE
	                            ,TM.TRUCK_NO
	                            ,CM.CUSTOMER_CODE
                            FROM ((BATCH_DAT_TRANS AS BDT LEFT JOIN CUSTOMER_MASTER AS CM ON BDT.CUSTOMER_CODE = CM.CUSTOMER_CODE) 
                            LEFT JOIN SITE_MASTER AS SM ON BDT.CUSTOMER_CODE = SM.CUSTOMER_CODE AND BDT.SITE = SM.SITE) 
                            LEFT JOIN TRUCK_MASTER AS TM ON BDT.TRUCK_NO = TM.TRUCK_NO
                            WHERE (BDT.BATCH_NO) = " + this.Batch_NO + "";
                    dtSourceMastersId = DbHelper.FillDataTable(strQuery, dataConnectionConfiguration.Source_Connection_String);
                    dtSourceMastersId.TableName = "Masters";
                    //WHERE CDBL(BDT.BATCH_NO) = " + this.Batch_NO + "";
                    if (dtSourceMastersId != null && dtSourceMastersId.Rows.Count > 0)
                    {
                        string strCM_Id, strSM_Id, strTM_Id = string.Empty;
                        strCM_Id = dtSourceMastersId.Rows[0]["CUSTOMER_CODE"].ToString();
                        strSM_Id = dtSourceMastersId.Rows[0]["SITE"].ToString();
                        strTM_Id = dtSourceMastersId.Rows[0]["TRUCK_NO"].ToString();

                        strAccessQuery = @"SELECT *  FROM CUSTOMER_MASTER WHERE CUSTOMER_CODE = '" + strCM_Id + "'";
                        dtMaster = DbHelper.FillDataTable(strAccessQuery, dataConnectionConfiguration.Source_Connection_String);
                        dtMaster.TableName = "CUSTOMER_MASTER";
                        if (dtMaster != null && dtMaster.Rows.Count > 0 && string.IsNullOrEmpty(strCM_Id) == false)
                        {
                            strAccessQuery = @"SELECT COUNT(CUSTOMER_CODE) AS COUNT_ROW  FROM CUSTOMER_MASTER WHERE CUSTOMER_CODE = '" + strCM_Id + "'";
                            object COUNT_ROW = DbHelper.ExecuteScalar(strAccessQuery, dataConnectionConfiguration.Target_Connection_String);
                            if (COUNT_ROW == null || Convert.ToInt32(COUNT_ROW) == 0)
                            {
                                strAccessQuery = @"Insert into CUSTOMER_MASTER(" + SqlTableColumn(dtMaster);
                                strAccessQuery += @")values(";
                                strAccessQuery += SqlTableValues(dtMaster.Rows[0]);
                                strAccessQuery += ")";
                                DbHelper.ExecuteNonQuery(strAccessQuery, dataConnectionConfiguration.Target_Connection_String);
                            }
                        }

                        strAccessQuery = @"SELECT *  FROM SITE_MASTER WHERE SITE  = '" + strSM_Id + "' AND CUSTOMER_CODE = '" + strCM_Id + "'";
                        dtMaster = DbHelper.FillDataTable(strAccessQuery, dataConnectionConfiguration.Source_Connection_String);
                        dtMaster.TableName = "SITE_MASTER";
                        if (dtMaster != null && dtMaster.Rows.Count > 0 && string.IsNullOrEmpty(strSM_Id) == false && string.IsNullOrEmpty(strCM_Id) == false)
                        {
                            strAccessQuery = @"SELECT COUNT(SITE) AS COUNT_ROW  FROM SITE_MASTER WHERE SITE = '" + strSM_Id + "' AND CUSTOMER_CODE = '" + strCM_Id + "'";
                            object COUNT_ROW = DbHelper.ExecuteScalar(strAccessQuery, dataConnectionConfiguration.Target_Connection_String);
                            if (COUNT_ROW == null || Convert.ToInt32(COUNT_ROW) == 0)
                            {
                                strAccessQuery = @"Insert into SITE_MASTER(" + SqlTableColumn(dtMaster);
                                strAccessQuery += @")values(";
                                strAccessQuery += SqlTableValues(dtMaster.Rows[0]);
                                strAccessQuery += ")";
                                DbHelper.ExecuteNonQuery(strAccessQuery, dataConnectionConfiguration.Target_Connection_String);
                            }
                        }
                        strAccessQuery = @"SELECT *  FROM TRUCK_MASTER WHERE TRUCK_NO  = '" + strTM_Id + "'";
                        dtMaster = DbHelper.FillDataTable(strAccessQuery, dataConnectionConfiguration.Source_Connection_String);
                        dtMaster.TableName = "SITE_MASTER";
                        if (dtMaster != null && dtMaster.Rows.Count > 0 && string.IsNullOrEmpty(strTM_Id) == false)
                        {
                            strAccessQuery = @"SELECT COUNT(Tno) AS COUNT_ROW  FROM TRUCK_MASTER WHERE TRUCK_NO = '" + strTM_Id + "'";
                            object COUNT_ROW = DbHelper.ExecuteScalar(strAccessQuery, dataConnectionConfiguration.Target_Connection_String);
                            if (COUNT_ROW == null || Convert.ToInt32(COUNT_ROW) == 0)
                            {
                                strAccessQuery = @"Insert into TRUCK_MASTER(" + SqlTableColumn(dtMaster);
                                strAccessQuery += @")values(";
                                strAccessQuery += SqlTableValues(dtMaster.Rows[0]);
                                strAccessQuery += ")";
                                DbHelper.ExecuteNonQuery(strAccessQuery, dataConnectionConfiguration.Target_Connection_String);
                            }
                        }
                    }
                    #endregion Insert Missing  Master Entiry for the current batch in Replica db

                }
                #endregion Version 3
                #region Version 3.1
                if (Program.PlantAppVersion == "3.1")
                {
                    DataTable dtSource_Batch_Dat_Trans, dtSource_Batch_Transaction, dtBATCH_RESULT = new DataTable();
                    #region fill data table from main access db
                    strQuery = @"select * from DOCKET_HEADER where batch_no = " + this.Batch_NO + "";
                    dtSource_Batch_Dat_Trans = DbHelper.FillDataTable(strQuery, dataConnectionConfiguration.Source_Connection_String);
                    dtSource_Batch_Dat_Trans.TableName = "DOCKET_HEADER";

                    strQuery = @"select * from DOCKET_DETAILS where batch_no = " + this.Batch_NO + "";
                    dtSource_Batch_Transaction = DbHelper.FillDataTable(strQuery, dataConnectionConfiguration.Source_Connection_String);
                    dtSource_Batch_Transaction.TableName = "DOCKET_DETAILS";

                    #endregion fill data table from access db

                    strQuery = @"DELETE * FROM DOCKET_HEADER WHERE BATCH_NO = " + this.Batch_NO + "";
                    DbHelper.ExecuteNonQuery(strQuery, dataConnectionConfiguration.Target_Connection_String);
                    foreach (DataRow row in dtSource_Batch_Dat_Trans.Rows)
                    {
                        strAccessQuery = string.Empty;
                        strAccessQuery = @"Insert into DOCKET_HEADER(" + SqlTableColumn(dtSource_Batch_Dat_Trans);
                        strAccessQuery += @")values(";
                        strAccessQuery += SqlTableValues(row);
                        strAccessQuery += ")";
                        DbHelper.ExecuteNonQuery(strAccessQuery, dataConnectionConfiguration.Target_Connection_String);
                    }

                    strQuery = @"DELETE * FROM DOCKET_DETAILS WHERE BATCH_NO = " + this.Batch_NO + "";
                    DbHelper.ExecuteNonQuery(strQuery, dataConnectionConfiguration.Target_Connection_String);
                    foreach (DataRow row in dtSource_Batch_Transaction.Rows)
                    {
                        strAccessQuery = string.Empty;
                        strAccessQuery = @"Insert into DOCKET_DETAILS(" + SqlTableColumn(dtSource_Batch_Transaction);
                        strAccessQuery += @")values(";
                        strAccessQuery += SqlTableValues(row);
                        strAccessQuery += ")";
                        DbHelper.ExecuteNonQuery(strAccessQuery, dataConnectionConfiguration.Target_Connection_String);
                    }
                }
                #endregion Version 3.1
                #region Version 1
                else if (Program.PlantAppVersion == "1")
                {
                    DataTable dtSource_VITRAG_BATCH = new DataTable();
                    #region fill data table from main access db
                    strQuery = @"select * from VITRAG_BATCH where batch_no = " + this.Batch_NO + "";
                    dtSource_VITRAG_BATCH = DbHelper.FillDataTable(strQuery, dataConnectionConfiguration.Source_Connection_String);
                    dtSource_VITRAG_BATCH.TableName = "VITRAG_BATCH";
                    #endregion fill data table from main access db

                    #region insert data into replica db

                    strQuery = @"delete * from VITRAG_BATCH where batch_no = " + this.Batch_NO + "";
                    DbHelper.ExecuteNonQuery(strQuery, dataConnectionConfiguration.Target_Connection_String);
                    foreach (DataRow row in dtSource_VITRAG_BATCH.Rows)
                    {
                        strAccessQuery = string.Empty;
                        strAccessQuery = @"Insert into VITRAG_BATCH(" + SqlTableColumn(dtSource_VITRAG_BATCH);
                        strAccessQuery += @")values(";
                        strAccessQuery += SqlTableValues(row);
                        strAccessQuery += ")";
                        DbHelper.ExecuteNonQuery(strAccessQuery, dataConnectionConfiguration.Target_Connection_String);
                    }

                    #region Insert Missing  Master Entiry for the current batch in Replica db
                    DataTable dtSourceMastersId, dtMaster = new DataTable();
                    strQuery = @"SELECT	
	                        SM.SITE AS SM_ID
	                        ,TM.TNO
	                        ,CM.CUSTOMER_CODE
                        FROM VITRAG_BATCH AS BDT
                         ,CUSTOMER_MASTER AS CM
                        ,SITE_MASTER AS SM
                        ,TRUCK_MASTER AS TM
                        WHERE
	                        (BDT.CUSTOMER_CODE = CM.CUSTOMER_CODE)
                        AND BDT.SITE_CODE = SM.SITE
                        AND BDT.TRUCK_NO = TM.TNO
                        AND CDBL(BDT.BATCH_NO) = " + this.Batch_NO + "";
                    dtSourceMastersId = DbHelper.FillDataTable(strQuery, dataConnectionConfiguration.Source_Connection_String);
                    dtSourceMastersId.TableName = "Masters";

                    if (dtSourceMastersId != null && dtSourceMastersId.Rows.Count > 0)
                    {
                        string strCM_Id, strSM_Id, strTM_Id = string.Empty;
                        strCM_Id = dtSourceMastersId.Rows[0]["CUSTOMER_CODE"].ToString();
                        strSM_Id = dtSourceMastersId.Rows[0]["SM_ID"].ToString();
                        strTM_Id = dtSourceMastersId.Rows[0]["TNO"].ToString();

                        strAccessQuery = @"SELECT *  FROM CUSTOMER_MASTER WHERE CUSTOMER_CODE = '" + strCM_Id + "'";
                        dtMaster = DbHelper.FillDataTable(strAccessQuery, dataConnectionConfiguration.Source_Connection_String);
                        dtMaster.TableName = "CUSTOMER_MASTER";
                        if (dtMaster != null && dtMaster.Rows.Count > 0)
                        {
                            strAccessQuery = @"SELECT COUNT(CUSTOMER_CODE) AS COUNT_ROW  FROM CUSTOMER_MASTER WHERE CUSTOMER_CODE = '" + strCM_Id + "'";
                            object COUNT_ROW = DbHelper.ExecuteScalar(strAccessQuery, dataConnectionConfiguration.Target_Connection_String);
                            if (COUNT_ROW == null || Convert.ToInt32(COUNT_ROW) == 0)
                            {
                                strAccessQuery = @"Insert into CUSTOMER_MASTER(" + SqlTableColumn(dtMaster);
                                strAccessQuery += @")values(";
                                strAccessQuery += SqlTableValues(dtMaster.Rows[0]);
                                strAccessQuery += ")";
                                DbHelper.ExecuteNonQuery(strAccessQuery, dataConnectionConfiguration.Target_Connection_String);
                            }
                        }

                        strAccessQuery = @"SELECT *  FROM SITE_MASTER WHERE SITE  = '" + strSM_Id + "'";
                        dtMaster = DbHelper.FillDataTable(strAccessQuery, dataConnectionConfiguration.Source_Connection_String);
                        dtMaster.TableName = "SITE_MASTER";
                        if (dtMaster != null && dtMaster.Rows.Count > 0)
                        {
                            strAccessQuery = @"SELECT COUNT(SITE) AS COUNT_ROW  FROM SITE_MASTER WHERE SITE = '" + strSM_Id + "'";
                            object COUNT_ROW = DbHelper.ExecuteScalar(strAccessQuery, dataConnectionConfiguration.Target_Connection_String);
                            if (COUNT_ROW == null || Convert.ToInt32(COUNT_ROW) == 0)
                            {
                                strAccessQuery = @"Insert into SITE_MASTER(" + SqlTableColumn(dtMaster);
                                strAccessQuery += @")values(";
                                strAccessQuery += SqlTableValues(dtMaster.Rows[0]);
                                strAccessQuery += ")";
                                DbHelper.ExecuteNonQuery(strAccessQuery, dataConnectionConfiguration.Target_Connection_String);
                            }
                        }
                        strAccessQuery = @"SELECT *  FROM TRUCK_MASTER WHERE Tno  = '" + strTM_Id + "'";
                        dtMaster = DbHelper.FillDataTable(strAccessQuery, dataConnectionConfiguration.Source_Connection_String);
                        dtMaster.TableName = "SITE_MASTER";
                        if (dtMaster != null && dtMaster.Rows.Count > 0)
                        {
                            strAccessQuery = @"SELECT COUNT(Tno) AS COUNT_ROW  FROM TRUCK_MASTER WHERE Tno = '" + strTM_Id + "'";
                            object COUNT_ROW = DbHelper.ExecuteScalar(strAccessQuery, dataConnectionConfiguration.Target_Connection_String);
                            if (COUNT_ROW == null || Convert.ToInt32(COUNT_ROW) == 0)
                            {
                                strAccessQuery = @"Insert into TRUCK_MASTER(" + SqlTableColumn(dtMaster);
                                strAccessQuery += @")values(";
                                strAccessQuery += SqlTableValues(dtMaster.Rows[0]);
                                strAccessQuery += ")";
                                DbHelper.ExecuteNonQuery(strAccessQuery, dataConnectionConfiguration.Target_Connection_String);
                            }
                        }
                    }
                    #endregion Insert Missing  Master Entiry for the current batch in Replica db

                    #endregion insert data into replica db
                }
                #endregion Version 1
                #region Version 2
                else if (Program.PlantAppVersion == "2")
                {
                    DataTable dtSource_BATCH = new DataTable();
                    #region fill data table from main access db
                    strQuery = @"select * from BATCH where batchno = " + this.Batch_NO + "";
                    dtSource_BATCH = DbHelper.FillDataTable(strQuery, dataConnectionConfiguration.Source_Connection_String);
                    dtSource_BATCH.TableName = "BATCH";
                    #endregion fill data table from main access db

                    #region insert data into replica db
                    strQuery = @"delete * from BATCH where batchno = " + this.Batch_NO + "";
                    DbHelper.ExecuteNonQuery(strQuery, dataConnectionConfiguration.Target_Connection_String);
                    foreach (DataRow row in dtSource_BATCH.Rows)
                    {
                        strAccessQuery = string.Empty;
                        strAccessQuery = @"Insert into BATCH(" + SqlTableColumn(dtSource_BATCH);
                        strAccessQuery += @")values(";
                        strAccessQuery += SqlTableValues(row);
                        strAccessQuery += ")";
                        DbHelper.ExecuteNonQuery(strAccessQuery, dataConnectionConfiguration.Target_Connection_String);
                    }
                    #endregion insert data into replica db
                }
                #endregion Version 2

                #region Insert BATCH_MASTER_UPLOAD_STATUS Data in Replica db and main db for all version
                DataTable dtBATCH_MASTER_UPLOAD_STATUS = new DataTable();
                try
                {
                    strAccessQuery = string.Empty;
                    strAccessQuery = @"SELECT TOP 1 * FROM BATCH_MASTER_UPLOAD_STATUS WHERE ROW_INDEX = 0";
                    dtBATCH_MASTER_UPLOAD_STATUS = DbHelper.FillDataTable(strAccessQuery, dataConnectionConfiguration.Target_Connection_String); //Replica db
                }
                catch
                {
                    strAccessQuery = string.Empty;
                    strAccessQuery = @"CREATE TABLE BATCH_MASTER_UPLOAD_STATUS(
                            ROW_INDEX Counter(1,1)
                            ,BATCH_NO	Double
                            ,BDT_UPDATE_COUNT	Double
                            ,BR_UPDATE_COUNT	Double
                            ,BT_UPDATE_COUNT	Double
                            ,IS_HTML_FILE_CREATED	Double
                            ,FILE_CREATED_COUNT	Double
                            ,IS_MCGM_BATCH	Double
                            ,IS_MANUAL_CREATED	Double
                            ,IS_MANUAL_CREATED_DATE	Datetime
                            ,BT_UPDATE_COUNT_LIVE	Double
                            ,FILE_CREATED_DATE	Datetime
                            ,FILE_CREATED_FLAG	Double
                            )";
                    DbHelper.ExecuteNonQuery(strAccessQuery, dataConnectionConfiguration.Target_Connection_String);

                    strAccessQuery = string.Empty;
                    strAccessQuery = @"SELECT TOP 1 * FROM BATCH_MASTER_UPLOAD_STATUS WHERE ROW_INDEX = 0";
                    dtBATCH_MASTER_UPLOAD_STATUS = DbHelper.FillDataTable(strAccessQuery, dataConnectionConfiguration.Target_Connection_String);
                }

                if (dtBATCH_MASTER_UPLOAD_STATUS.Columns.Count > 0)
                {
                    strAccessQuery = string.Empty;
                    int IsMCHM = rdoMCGM.Checked == true ? 1 : 0;
                    if (dtBATCH_MASTER_UPLOAD_STATUS.Columns.Contains("ROW_INDEX"))
                    {
                        dtBATCH_MASTER_UPLOAD_STATUS.Columns.Remove("ROW_INDEX");
                    }
                    strAccessQuery = @"Insert into BATCH_MASTER_UPLOAD_STATUS(" + SqlTableColumn(dtBATCH_MASTER_UPLOAD_STATUS);
                    strAccessQuery += @")values(";
                    string strRowValues = string.Empty;
                    foreach (DataColumn col in dtBATCH_MASTER_UPLOAD_STATUS.Columns)
                    {
                        if (col.DataType == typeof(DateTime))
                        {
                            if (col.ColumnName == "FILE_CREATED_DATE")
                            {
                                if (string.IsNullOrEmpty(strRowValues) == true)
                                {
                                    if (IsMCHM == 1)
                                    {
                                        strRowValues += "#" + Convert.ToString(file_Created_Date) + "#";
                                    }
                                    else
                                    {
                                        strRowValues += "NULL";
                                    }
                                }
                                else
                                {
                                    if (IsMCHM == 1)
                                    {
                                        strRowValues += ",#" + Convert.ToString(file_Created_Date) + "#";
                                    }
                                    else
                                    {
                                        strRowValues += ",NULL";
                                    }
                                }
                            }
                            else
                            {
                                if (string.IsNullOrEmpty(strRowValues) == true)
                                {
                                    strRowValues += "NULL";
                                }
                                else
                                {
                                    strRowValues += ",NULL";
                                }
                            }
                        }
                        else if (col.DataType == typeof(double))
                        {
                            if (col.ColumnName == "BATCH_NO")
                            {
                                if (string.IsNullOrEmpty(strRowValues) == true)
                                {
                                    strRowValues += this.Batch_NO;
                                }
                                else
                                {
                                    strRowValues += "," + this.Batch_NO + "";
                                }
                            }
                            else if (col.ColumnName == "IS_MCGM_BATCH")
                            {
                                if (string.IsNullOrEmpty(strRowValues) == true)
                                {
                                    strRowValues += IsMCHM;
                                }
                                else
                                {
                                    strRowValues += "," + IsMCHM + "";
                                }
                            }
                            else if (col.ColumnName == "IS_HTML_FILE_CREATED")
                            {
                                if (string.IsNullOrEmpty(strRowValues) == true)
                                {
                                    strRowValues += IsMCHM;
                                }
                                else
                                {
                                    strRowValues += "," + IsMCHM + "";
                                }
                            }
                            else
                            {
                                if (string.IsNullOrEmpty(strRowValues) == true)
                                {
                                    strRowValues += 0;
                                }
                                else
                                {
                                    strRowValues += "," + 0;
                                }
                            }
                        }
                        else
                        {
                            if (string.IsNullOrEmpty(strRowValues) == true)
                            {
                                strRowValues += "null";
                            }
                            else
                            {
                                strRowValues += ",null";
                            }
                        }
                    }
                    strAccessQuery += strRowValues;
                    strAccessQuery += ")";

                    DbHelper.ExecuteNonQuery("DELETE * FROM BATCH_MASTER_UPLOAD_STATUS WHERE BATCH_NO = " + this.Batch_NO + "", dataConnectionConfiguration.Target_Connection_String);
                    DbHelper.ExecuteNonQuery(strAccessQuery, dataConnectionConfiguration.Target_Connection_String);
                    string strExecuteInsertQuery = string.Empty;
                    strExecuteInsertQuery = strAccessQuery;

                    ///check BATCH_MASTER_UPLOAD_STATUS table exist and insert data in main db
                    try
                    {
                        strAccessQuery = string.Empty;
                        strAccessQuery = @"SELECT TOP 1 * FROM BATCH_MASTER_UPLOAD_STATUS WHERE ROW_INDEX = 0";
                        dtBATCH_MASTER_UPLOAD_STATUS = DbHelper.FillDataTable(strAccessQuery, dataConnectionConfiguration.Source_Connection_String);
                    }
                    catch
                    {
                        strAccessQuery = string.Empty;
                        strAccessQuery = @"CREATE TABLE BATCH_MASTER_UPLOAD_STATUS(
                            ROW_INDEX Counter(1,1)
                            ,BATCH_NO	Double
                            ,BDT_UPDATE_COUNT	Double
                            ,BR_UPDATE_COUNT	Double
                            ,BT_UPDATE_COUNT	Double
                            ,IS_HTML_FILE_CREATED	Double
                            ,FILE_CREATED_COUNT	Double
                            ,IS_MCGM_BATCH	Double
                            ,IS_MANUAL_CREATED	Double
                            ,IS_MANUAL_CREATED_DATE	Datetime
                            ,BT_UPDATE_COUNT_LIVE	Double
                            ,FILE_CREATED_DATE	Datetime
                            ,FILE_CREATED_FLAG	Double
                            )";
                        DbHelper.ExecuteNonQuery(strAccessQuery, dataConnectionConfiguration.Source_Connection_String);

                        strAccessQuery = string.Empty;
                        strAccessQuery = @"SELECT TOP 1 * FROM BATCH_MASTER_UPLOAD_STATUS WHERE ROW_INDEX = 0";
                        dtBATCH_MASTER_UPLOAD_STATUS = DbHelper.FillDataTable(strAccessQuery, dataConnectionConfiguration.Source_Connection_String);
                    }

                    if (dtBATCH_MASTER_UPLOAD_STATUS.Columns.Count > 0)
                    {
                        DbHelper.ExecuteNonQuery("DELETE * FROM BATCH_MASTER_UPLOAD_STATUS WHERE BATCH_NO = " + this.Batch_NO + "", dataConnectionConfiguration.Source_Connection_String);
                        DbHelper.ExecuteNonQuery(strExecuteInsertQuery, dataConnectionConfiguration.Source_Connection_String);
                    }
                }
                #endregion Insert BATCH_MASTER_UPLOAD_STATUS Data in Replica db and main db for all version
            }
            catch (Exception ex)
            {
                Global.WriteLog("Insert_Data_In_Replica_Db() FOR Plant App Version[" + Program.PlantAppVersion + "] Application Exception: ", LogMessageType.Error, ex.InnerException.ToString());
                throw ex;
            }
        }
        #endregion Insert Data in Replica db

        private void dgvHtmlDetails_Scroll(object sender, ScrollEventArgs e)
        {
            if (((DataGridView)sender).Name == dgvHtmlDetails.Name)
            {
                dgvHtmlHeader.HorizontalScrollingOffset = dgvHtmlDetails.HorizontalScrollingOffset;
                dgvHtmlSum.HorizontalScrollingOffset = dgvHtmlDetails.HorizontalScrollingOffset;
            }
            else if (((DataGridView)sender).Name == dgvHtmlHeader.Name)
            {
                dgvHtmlDetails.HorizontalScrollingOffset = dgvHtmlHeader.HorizontalScrollingOffset;
                dgvHtmlSum.HorizontalScrollingOffset = dgvHtmlHeader.HorizontalScrollingOffset;
            }
            else if (((DataGridView)sender).Name == dgvHtmlSum.Name)
            {
                dgvHtmlDetails.HorizontalScrollingOffset = dgvHtmlSum.HorizontalScrollingOffset;
                dgvHtmlHeader.HorizontalScrollingOffset = dgvHtmlSum.HorizontalScrollingOffset;
            }
        }

        private DataTable Get_Table_Name(string TableName)
        {
            try
            {
                string StrTable = string.Empty;
                DataTable dtTableName = new DataTable();
                StrTable = @"SELECT TOP 1 * FROM " + TableName;
                dtTableName = DbHelper.FillDataTable(StrTable, dataConnectionConfiguration.Source_Connection_String);
                //dtTableName = dbConnection.FillDataTable(StrTable, dbConnection.Connection_String);
                return dtTableName;
            }
            catch (Exception)
            {
                return null;
            }
        }

    }
}