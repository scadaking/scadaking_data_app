﻿#region [ Using's ]
using System;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Diagnostics;

#endregion [ Using's ]

namespace PNF
{
    public class RI_syncUtility : IDisposable
    {
        /// <summary>
        /// Upload access data
        /// </summary>
        /// <param name="uploadDataSet">Dataset to upload the data</param>
        /// <param name="presponseDs"></param>
        /// <param name="dtXMLList">XML table details</param>
        /// <param name="plantAppVersion">Plant version</param>
        /// <returns>no of records affected</returns>
        public int UploadAccessData(
            DataSet uploadDataSet,
            DataSet presponseDs,
            DataTable dtXMLList,
            string plantAppVersion,
            string targetConnectionString,
            out string returnQuery
            )
        {
            int updateRecords = 0;
            int insertRecords = 0;
            int rowIndex;
            returnQuery = string.Empty;

            string[] arrayColumn;
            string xmlKeyName = "XmlKey";
            string tableName = string.Empty;

            DataColumn[] dtcUploadPrimaryColumns;
            DataColumn[] dtcCurrentPrimaryColumns;
            DataColumn[] dtcPrimaryColumns;

            DataRow[] dtrXMLrow;
            DataRow[] dtrSelectedRows;
            DataRow[] dtrxmlAddrow;

            DataTable dtUploadData = new DataTable();
            DataTable dtSelectedTableData;

            using (Ut_sysUtility _objutility = new Ut_sysUtility())
            {
                try
                {
                    foreach (DataTable dtCurrentTable in presponseDs.Tables)
                    {
                        tableName = dtCurrentTable.TableName.ToString();
                        dtSelectedTableData = new DataTable(tableName);
                        dtrXMLrow = dtXMLList.Select("XmlRelation ='" + tableName + "' and XmlName='Key'");
                        dtCurrentTable.PrimaryKey = null;

                        dtcPrimaryColumns = new DataColumn[dtrXMLrow.Length];
                        rowIndex = 0;

                        for (rowIndex = 0; rowIndex < dtcPrimaryColumns.Count(); rowIndex++)
                        {
                            dtcPrimaryColumns[rowIndex] = dtCurrentTable.Columns[dtrXMLrow[rowIndex][xmlKeyName].ToString()];
                        }

                        dtCurrentTable.PrimaryKey = dtcPrimaryColumns;

                        #region [ if (dtrXMLrow.Length > 0) ]

                        if (dtrXMLrow.Length > 0)
                        {
                            if (dtrXMLrow.Length == 1)
                            {
                                #region [ One Primary Column Logic ]
                                var qry1 = uploadDataSet.Tables[tableName].AsEnumerable().Select(a => new
                                {
                                    Id1 = a[dtrXMLrow[0][xmlKeyName].ToString()].ToString()
                                });

                                var qry2 = dtCurrentTable.AsEnumerable().Select(b => new
                                {
                                    Id1 = b[dtrXMLrow[0][xmlKeyName].ToString()].ToString()
                                });

                                var vInterset = qry1.Intersect(qry2);

                                if (vInterset != null && vInterset.Count() > 0)
                                {
                                    dtSelectedTableData = (from a in uploadDataSet.Tables[tableName].AsEnumerable()
                                                           join ab in vInterset on
                                                           a[dtrXMLrow[0][xmlKeyName].ToString()].ToString() equals ab.Id1
                                                           select a).CopyToDataTable();
                                }
                                #endregion [ One Primary Column Logic ]
                            }
                            else if (dtrXMLrow.Length == 2)
                            {
                                #region [ Two Primary Column Logic ]
                                var qry1 = uploadDataSet.Tables[tableName].AsEnumerable().Select(a => new
                                {
                                    Id1 = a[dtrXMLrow[0][xmlKeyName].ToString()].ToString(),
                                    Id2 = a[dtrXMLrow[1][xmlKeyName].ToString()].ToString()
                                });

                                var qry2 = dtCurrentTable.AsEnumerable().Select(b => new
                                {
                                    Id1 = b[dtrXMLrow[0][xmlKeyName].ToString()].ToString(),
                                    Id2 = b[dtrXMLrow[1][xmlKeyName].ToString()].ToString()
                                });

                                var vInterset = qry1.Intersect(qry2);
                                if (vInterset != null && vInterset.Count() > 0)
                                {
                                    dtSelectedTableData = (from a in uploadDataSet.Tables[tableName].AsEnumerable()
                                                           join ab in vInterset on
                                                           a[dtrXMLrow[0][xmlKeyName].ToString()].ToString() equals ab.Id1
                                                           where
                                                           a[dtrXMLrow[1][xmlKeyName].ToString()].ToString() == ab.Id2
                                                           select a).CopyToDataTable();
                                }
                                #endregion [ Two Primary Column Logic ]
                            }
                            else if (dtrXMLrow.Length == 3)
                            {
                                #region [ Three Primary Column Logic ]
                                var qry1 = uploadDataSet.Tables[tableName].AsEnumerable().Select(a => new
                                {
                                    Id1 = a[dtrXMLrow[0][xmlKeyName].ToString()].ToString(),
                                    Id2 = a[dtrXMLrow[1][xmlKeyName].ToString()].ToString(),
                                    Id3 = a[dtrXMLrow[2][xmlKeyName].ToString()].ToString()
                                });

                                var qry2 = dtCurrentTable.AsEnumerable().Select(b => new
                                {
                                    Id1 = b[dtrXMLrow[0][xmlKeyName].ToString()].ToString(),
                                    Id2 = b[dtrXMLrow[1][xmlKeyName].ToString()].ToString(),
                                    Id3 = b[dtrXMLrow[2][xmlKeyName].ToString()].ToString()
                                });

                                var vInterset = qry1.Intersect(qry2);
                                if (vInterset != null && vInterset.Count() > 0)
                                {
                                    dtSelectedTableData = (from a in uploadDataSet.Tables[tableName].AsEnumerable()
                                                           join ab in vInterset on
                                                           a[dtrXMLrow[0][xmlKeyName].ToString()].ToString() equals ab.Id1
                                                           where
                                                           a[dtrXMLrow[1][xmlKeyName].ToString()].ToString() == ab.Id2 &&
                                                           a[dtrXMLrow[2][xmlKeyName].ToString()].ToString() == ab.Id3
                                                           select a).CopyToDataTable();
                                }
                                #endregion [ Three Primary Column Logic ]
                            }
                            else
                            {
                                #region [ Four Primary Column Logic ]
                                var qry1 = uploadDataSet.Tables[tableName].AsEnumerable().Select(a => new
                                {
                                    Id1 = a[dtrXMLrow[0][xmlKeyName].ToString()].ToString(),
                                    Id2 = a[dtrXMLrow[1][xmlKeyName].ToString()].ToString(),
                                    Id3 = a[dtrXMLrow[2][xmlKeyName].ToString()].ToString(),
                                    Id4 = a[dtrXMLrow[3][xmlKeyName].ToString()].ToString()
                                });

                                var qry2 = dtCurrentTable.AsEnumerable().Select(b => new
                                {
                                    Id1 = b[dtrXMLrow[0][xmlKeyName].ToString()].ToString(),
                                    Id2 = b[dtrXMLrow[1][xmlKeyName].ToString()].ToString(),
                                    Id3 = b[dtrXMLrow[2][xmlKeyName].ToString()].ToString(),
                                    Id4 = b[dtrXMLrow[3][xmlKeyName].ToString()].ToString()
                                });

                                var vInterset = qry1.Intersect(qry2);
                                if (vInterset != null && vInterset.Count() > 0)
                                {
                                    dtSelectedTableData = (from a in uploadDataSet.Tables[tableName].AsEnumerable()
                                                           join ab in vInterset on
                                                           a[dtrXMLrow[0][xmlKeyName].ToString()].ToString() equals ab.Id1
                                                           where
                                                           a[dtrXMLrow[1][xmlKeyName].ToString()].ToString() == ab.Id2 &&
                                                           a[dtrXMLrow[2][xmlKeyName].ToString()].ToString() == ab.Id3 &&
                                                           a[dtrXMLrow[3][xmlKeyName].ToString()].ToString() == ab.Id4
                                                           select a).CopyToDataTable();
                                }
                                #endregion [ One Primary Column Logic ]
                            }
                        }
                        #endregion [ if (dtrXMLrow.Length > 0) ]

                        #region [ if (dtSelectedTableData.Rows.Count > 0) ]
                        if (dtSelectedTableData != null && dtSelectedTableData.Rows.Count > 0)
                        {
                            if (dtSelectedTableData.Rows.Count > 0)
                            {
                                if (dtSelectedTableData.Columns.Contains("UpdateInsert"))
                                {
                                    dtSelectedTableData.PrimaryKey = null;
                                    rowIndex = 0;
                                    for (rowIndex = 0; rowIndex < dtcPrimaryColumns.Count(); rowIndex++)
                                    {
                                        dtcPrimaryColumns[rowIndex] = dtSelectedTableData.Columns[dtrXMLrow[rowIndex][xmlKeyName].ToString()];
                                    }

                                    dtSelectedTableData.PrimaryKey = dtcPrimaryColumns;

                                    dtrxmlAddrow = dtXMLList.Select("XmlRelation ='" + tableName + "' and XmlName='AddKey'");
                                    if (dtrxmlAddrow.Length > 0)
                                    {
                                        rowIndex = 0;
                                        for (rowIndex = 0; rowIndex < dtrxmlAddrow.Length; rowIndex++)
                                        {
                                            arrayColumn = dtrxmlAddrow[rowIndex][xmlKeyName].ToString().Split(new string[] { "," }, StringSplitOptions.None);
                                            if (arrayColumn.Count() == 2)
                                            {
                                                if (dtSelectedTableData.Columns.Contains(arrayColumn[0].ToString()))
                                                {
                                                    dtSelectedTableData.Columns.Remove(arrayColumn[0].ToString());
                                                }
                                            }
                                        }
                                    }

                                    dtrSelectedRows = dtSelectedTableData.Select("UpdateInsert='Insert'");
                                    if (dtrSelectedRows != null)
                                    {
                                        if (dtrSelectedRows.Length > 0)
                                        {
                                            dtUploadData = dtrSelectedRows.CopyToDataTable();
                                            dtUploadData.TableName = tableName;
                                            dtUploadData.Columns.Remove("UpdateInsert");

                                            insertRecords = _objutility.InsertinDatabase(
                                                dtUploadData,
                                                plantAppVersion,
                                                targetConnectionString,
                                                out returnQuery);

                                        }
                                    }

                                    dtrSelectedRows = null;
                                    dtUploadData = null;

                                    dtrSelectedRows = dtSelectedTableData.Select("UpdateInsert='Update'");
                                    if (dtrSelectedRows != null)
                                    {
                                        if (dtrSelectedRows.Length > 0)
                                        {
                                            dtUploadData = dtrSelectedRows.CopyToDataTable();
                                            dtUploadData.TableName = tableName;
                                            dtUploadData.Columns.Remove("UpdateInsert");

                                            dtcUploadPrimaryColumns = new DataColumn[dtCurrentTable.PrimaryKey.Count()];
                                            dtcCurrentPrimaryColumns = dtCurrentTable.PrimaryKey;

                                            rowIndex = 0;
                                            for (rowIndex = 0; rowIndex < dtcUploadPrimaryColumns.Count(); rowIndex++)
                                            {
                                                dtcUploadPrimaryColumns[rowIndex] = dtUploadData.Columns[dtcCurrentPrimaryColumns[rowIndex].ColumnName.ToString()];
                                            }

                                            dtUploadData.PrimaryKey = dtcUploadPrimaryColumns;

                                            updateRecords = _objutility.UpdateinDatabase(
                                                dtUploadData,
                                                plantAppVersion,
                                                targetConnectionString,
                                                out returnQuery);

                                            dtUploadData.Dispose();
                                        }
                                    }
                                    dtrSelectedRows = null;
                                }
                            }
                        }
                        #endregion [ if (dtSelectedTableData.Rows.Count > 0) ]
                    }

                    return insertRecords + updateRecords;
                }
                catch (Exception ex)
                {
                    throw new Exception("class RI_syncUtility::UploadAccessData " + ex.ToString());
                }
            }
        }

        /// <summary>
        /// Update Access Database With Downloaded Data
        /// </summary>
        /// <param name="downLoad">Data set downloaded</param>
        /// <param name="plantAppVersion">Plant version</param>
        /// <returns>data table </returns>
        public DataTable UpdateAccessDatabaseWithDownloadedData(
            DataTable dtDownloadedMain,
            string plantAppVersion,
            string UTCDifference,
            string sourceConnectionString,
            string targetConnectionString,
            out int return_recordsAffected,
            out string returnQuery
            )
        {
            return_recordsAffected = 0;
            bool rollbackTransaction = false;

            int[] columnNumbers;
            int columnNumbersIndex;
            int columnIndex;
            int recordsAffected;
            returnQuery = string.Empty;

            string tableName = string.Empty;

            DataColumn[] dtcPrimaryKeyList = new DataColumn[0];

            DataTable returnData = new DataTable();

            OleDbConnection connectionMain = new OleDbConnection(sourceConnectionString);
            OleDbConnection connectionReplica = new OleDbConnection(targetConnectionString);
            connectionMain.Open();
            connectionReplica.Open();

            OleDbTransaction transactionMain = connectionMain.BeginTransaction();
            OleDbTransaction transactionReplica = connectionReplica.BeginTransaction();

            TimeSpan utcTimeSpan;

            try
            {
                Ut_sysUtility _objutility = new Ut_sysUtility();
                if (dtDownloadedMain.PrimaryKey.Count() > 0)
                {
                    dtDownloadedMain.ExtendedProperties["UTCDifference"] = UTCDifference;
                    utcTimeSpan = GetTimeSpanToAdd(UTCDifference);

                    columnNumbers = new int[dtDownloadedMain.Columns.Count];
                    columnNumbersIndex = 0;
                    columnIndex = 0;

                    foreach (DataColumn dcCurrentColumn in dtDownloadedMain.Columns)
                    {
                        if (dcCurrentColumn.DataType == typeof(DateTime))
                        {
                            columnNumbers[columnNumbersIndex] = columnIndex;
                            columnNumbersIndex++;
                        }

                        columnIndex++;
                    }

                    foreach (DataRow dtrCurrentRow in dtDownloadedMain.Rows)
                    {
                        AdjustDateTimeValues(dtrCurrentRow, columnNumbers, columnNumbersIndex, utcTimeSpan);
                        dtrCurrentRow.AcceptChanges();
                    }

                    if (dtDownloadedMain.Columns.Contains("TableName"))
                    {
                        dtDownloadedMain.Columns.Remove("TableName");
                    }

                    #region [ Normal Statement for Batch_Result ,Batch_Dat_Trans ,Batch ,Vitrag ,Asphalt ]

                    if (_objutility.UpdateAccessDatabaseWithDownloadedData(dtDownloadedMain, transactionMain, plantAppVersion, true, out returnQuery) > 0)
                    {
                        recordsAffected = 0;
                        tableName = dtDownloadedMain.TableName.ToString().ToLower();
                        recordsAffected = _objutility.UpdateAccessDatabaseWithDownloadedData(dtDownloadedMain, transactionReplica, plantAppVersion, true, out returnQuery);
                        bool addtoSyncResponse = false;
                        return_recordsAffected = recordsAffected;
                        if (recordsAffected > 0)
                        {
                            if (returnData == null || returnData.Rows.Count == 0)
                            {
                                returnData = new DataTable(tableName);
                                DataTable syncDataTable = null;
                                if (tableName == "batch")
                                {
                                    syncDataTable = dtDownloadedMain.DefaultView.ToTable(true, "batchno");

                                    dtcPrimaryKeyList = new DataColumn[1];
                                    dtcPrimaryKeyList[0] = syncDataTable.Columns["batchno"];
                                    syncDataTable.PrimaryKey = dtcPrimaryKeyList;

                                    addtoSyncResponse = true;
                                }
                                else
                                {
                                    if (tableName != "batch_transaction" && tableName != "batch_result")
                                    {
                                        syncDataTable = dtDownloadedMain;

                                        dtcPrimaryKeyList = new DataColumn[dtDownloadedMain.PrimaryKey.Count()];
                                        dtcPrimaryKeyList = dtDownloadedMain.PrimaryKey;
                                        syncDataTable.PrimaryKey = dtcPrimaryKeyList;

                                        addtoSyncResponse = true;
                                    }
                                }

                                if (addtoSyncResponse == true)
                                {
                                    string[] columnsList = new string[dtcPrimaryKeyList.Count()];
                                    int columnListIndex = 0;
                                    foreach (DataColumn primaryKeyColumn in dtcPrimaryKeyList)
                                    {
                                        columnsList[columnListIndex] = primaryKeyColumn.ColumnName.ToString();
                                        columnListIndex++;
                                    }

                                    returnData = syncDataTable.DefaultView.ToTable(true, columnsList);
                                    ////return_recordsAffected = 0;

                                    if (tableName == "batch")
                                    {
                                        if (returnData.Columns.Contains("batchno"))
                                        {
                                            returnData.Columns["batchno"].ColumnName = "Batch_no";
                                        }
                                    }
                                }
                            }
                        }
                    }

                    #endregion [ Normal Statement for Batch_Result ,Batch_Dat_Trans ,Batch ,Vitrag ,Asphalt ]
                }
            }
            catch (Exception ex)
            {
                rollbackTransaction = true;
                throw (ex);
            }
            finally
            {
                if (rollbackTransaction)
                {
                    transactionReplica.Rollback();
                    transactionMain.Rollback();
                    returnData = new DataTable();
                }
                else
                {
                    transactionMain.Commit();
                    transactionReplica.Commit();
                }

                if (connectionReplica != null)
                {
                    connectionReplica.Close();
                }

                if (connectionMain != null)
                {
                    connectionMain.Close();
                }
            }

            return returnData;
        }

        /// <summary>
        /// AddData To Sync Response
        /// </summary>
        /// <param name="mainTable">main table</param>
        /// <param name="primaryKeyColumnList">primary Keys Column List</param>
        /// <param name="dtrSync">data row for sync</param>
        private void AddDataToSyncResponse(ref DataTable mainTable, ref DataColumn[] primaryKeyColumnList, DataRow dtrSync)
        {
            DataRow dtrNewRow = mainTable.NewRow();
            foreach (DataColumn primaryColumn in primaryKeyColumnList)
            {
                dtrNewRow[primaryColumn.ColumnName.ToString()] = dtrSync[primaryColumn.ColumnName.ToString()];
            }

            mainTable.Rows.Add(dtrNewRow);
            mainTable.AcceptChanges();
        }

        /// <summary>
        /// GetTimeSpanToAdd
        /// </summary>
        /// <param name="strUTCDifference_Ticks">time differnce</param>
        /// <returns>timespan</returns>
        private TimeSpan GetTimeSpanToAdd(string strUTCDifference_Ticks)
        {
            long sourceTicks = long.Parse(strUTCDifference_Ticks);
            /*Obtain the UTC offset for the remote computer*/
            DateTime baseUTC = DateTime.Now;
            long UtcTickslocal = TimeZone.CurrentTimeZone.GetUtcOffset(baseUTC).Ticks;
            /*Obtain the time difference between the sender computer and the remote computer.*/
            long ticksDifference = sourceTicks - UtcTickslocal;
            return new TimeSpan(ticksDifference);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dtrCurrentRow"></param>
        /// <param name="columnNumbers"></param>
        /// <param name="columnCount"></param>
        /// <param name="timespan"></param>
        private void AdjustDateTimeValues(DataRow dtrCurrentRow, int[] columnNumbers, int columnCount, TimeSpan timespan)
        {
            DateTime original;
            DateTime modifiedDateTime;
            int columnIndex;

            for (int i = 0; i < columnCount; i++)
            {
                columnIndex = columnNumbers[i];
                original = (DateTime)dtrCurrentRow[columnIndex];
                modifiedDateTime = original.Add(timespan);
                dtrCurrentRow[columnIndex] = modifiedDateTime;
            }
        }

        /// <summary>
        /// Deleted_ResyncBatchdata
        /// </summary>
        /// <param name="dtResync"></param>
        /// <param name="xmlDatatable"></param>
        /// <param name="plantAppVersion">Plant Version</param>
        /// <returns>returns Datatable</returns>
        public DataTable Deleted_ResyncBatchdata(DataTable dtResync, DataTable xmlDatatable, string plantAppVersion, string targetConnectionString)
        {
            bool rollbackTransaction = false;
            DataTable returnData = new DataTable();

            OleDbConnection connectionMain = new OleDbConnection(targetConnectionString);
            connectionMain.Open();
            OleDbTransaction transactionMain = connectionMain.BeginTransaction();
            try
            {
                bool blncheck = false;
                using (Ut_sysUtility _objutility = new Ut_sysUtility())
                {
                    blncheck = _objutility.Delete_SelectedData(dtResync, transactionMain, plantAppVersion, xmlDatatable);
                    returnData = dtResync;
                }
            }
            catch (Exception ex)
            {
                rollbackTransaction = true;
                throw (ex);
            }
            finally
            {
                if (rollbackTransaction)
                {
                    transactionMain.Rollback();
                    returnData = new DataTable();
                }
                else
                {
                    transactionMain.Commit();
                }

                if (connectionMain != null)
                {
                    connectionMain.Dispose();
                    connectionMain.Close();
                }
            }

            return returnData;
        }

        #region [ Dispose Method ]

        // Pointer to an external unmanaged resource. 
        private IntPtr handle;

        // Other managed resource this class uses. 
        private Component component = new Component();

        // Track whether Dispose has been called. 
        private bool disposed = false;

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Dispose(bool disposing) executes in two distinct scenarios. 
        /// If disposing equals true, the method has been called directly 
        /// or indirectly by a user's code. Managed and unmanaged resources 
        /// can be disposed. 
        /// If disposing equals false, the method has been called by the 
        /// runtime from inside the finalizer and you should not reference 
        /// other objects. Only unmanaged resources can be disposed. 
        /// </summary>
        /// <param name="disposing"></param>
        protected virtual void Dispose(bool disposing)
        {
            // Check to see if Dispose has already been called. 
            if (!this.disposed)
            {
                // If disposing equals true, dispose all managed 
                // and unmanaged resources. 
                if (disposing)
                {
                    // Dispose managed resources.
                    component.Dispose();
                }

                // Call the appropriate methods to clean up 
                // unmanaged resources here. 
                // If disposing is false, 
                // only the following code is executed.
                CloseHandle(handle);
                handle = IntPtr.Zero;

                // Note disposing has been done.
                disposed = true;

            }
        }

        /// <summary>
        /// Use interop to call the method necessary 
        /// to clean up the unmanaged resource.
        /// </summary>
        /// <param name="handle"></param>
        /// <returns></returns>
        [System.Runtime.InteropServices.DllImport("Kernel32")]
        private extern static Boolean CloseHandle(IntPtr handle);

        /// <summary>
        /// Use C# destructor syntax for finalization code. 
        /// This destructor will run only if the Dispose method 
        /// does not get called. 
        /// It gives your base class the opportunity to finalize. 
        /// Do not provide destructors in types derived from this class.
        /// </summary>
        ~RI_syncUtility()
        {
            // Do not re-create Dispose clean-up code here. 
            // Calling Dispose(false) is optimal in terms of 
            // readability and maintainability.
            Dispose(false);
        }

        #endregion [ Dispose Method ]

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dtDownloadedMain"></param>
        /// <param name="sourceDatabase"></param>
        /// <returns></returns>
        public DataTable RemoveUnwantedFields(DataTable dtDownloadedMain, string sourceConnectionString, bool removeRowID_Column)
        {
            Ut_sysUtility _objutility = new Ut_sysUtility();

            DataTable downloadDataTable = _objutility.RemoveUnwantedFields(dtDownloadedMain, sourceConnectionString, removeRowID_Column);
            downloadDataTable.AcceptChanges();
            return downloadDataTable;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dtDownloadedMain"></param>
        /// <param name="sourceDatabase"></param>
        /// <returns></returns>
        public DataTable RemoveUnwantedFields(DataTable dtDownloadedMain, string sourceConnectionString)
        {
            Ut_sysUtility _objutility = new Ut_sysUtility();

            DataTable downloadDataTable = _objutility.RemoveUnwantedFields(dtDownloadedMain, sourceConnectionString);
            downloadDataTable.AcceptChanges();
            return downloadDataTable;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dtInsertRows"></param>
        /// <param name="plantAppVersion"></param>
        /// <param name="UTCDifference"></param>
        /// <returns></returns>
        public int InsertAccessDatabaseWithDownloadedData_MCGM(
            DataTable dtDownloadedMain,
            string plantAppVersion,
            string UTCDifference,
            string targetConnectionString,
            out string returnQuery)
        {
            bool rollbackTransaction = false;
            returnQuery = string.Empty;

            int columnNumbersIndex;
            int columnIndex;
            int recordsAffected;
            int[] columnNumbers;

            string tableName = string.Empty;
            recordsAffected = 0;

            if (dtDownloadedMain.PrimaryKey.Count() == 0)
            {
                return recordsAffected;
            }

            OleDbConnection connectionReplica = new OleDbConnection(targetConnectionString);
            connectionReplica.Open();
            OleDbTransaction transactionReplica = null;

            try
            {
                Ut_sysUtility _objutility = new Ut_sysUtility();

                #region [ Check Record Exists ]

                string[] rowList;
                rowList = new string[dtDownloadedMain.Rows.Count];
                string outPrimaryFields = string.Empty;
                int row_Index = 0;

                for (row_Index = 0; row_Index < dtDownloadedMain.Rows.Count; row_Index++)
                {
                    string whereString = _objutility.WhereString(dtDownloadedMain, row_Index, out outPrimaryFields);
                    if (_objutility.IsRecordExists(dtDownloadedMain.TableName, whereString, connectionReplica) == true)
                    {
                        rowList[row_Index] = row_Index.ToString();
                    }
                }

                foreach (string rowValue in rowList)
                {
                    if (!string.IsNullOrEmpty(rowValue))
                    {
                        row_Index = ConvertToInt32(rowValue);
                        dtDownloadedMain.Rows[row_Index].Delete();
                    }
                }
                dtDownloadedMain.AcceptChanges();

                #endregion [ Check Record Exists ]

                if (dtDownloadedMain.Rows.Count == 0)
                {
                    return recordsAffected;
                }

                transactionReplica = connectionReplica.BeginTransaction();

                dtDownloadedMain.ExtendedProperties["UTCDifference"] = UTCDifference;
                TimeSpan utcTimeSpan;
                utcTimeSpan = GetTimeSpanToAdd(UTCDifference);

                columnNumbers = new int[dtDownloadedMain.Columns.Count];
                columnNumbersIndex = 0;
                columnIndex = 0;

                foreach (DataColumn dcCurrentColumn in dtDownloadedMain.Columns)
                {
                    if (dcCurrentColumn.DataType == typeof(DateTime))
                    {
                        columnNumbers[columnNumbersIndex] = columnIndex;
                        columnNumbersIndex++;
                    }

                    columnIndex++;
                }

                foreach (DataRow dtrCurrentRow in dtDownloadedMain.Rows)
                {
                    AdjustDateTimeValues(dtrCurrentRow, columnNumbers, columnNumbersIndex, utcTimeSpan);
                    dtrCurrentRow.AcceptChanges();
                }

                if (dtDownloadedMain.Columns.Contains("TableName"))
                {
                    dtDownloadedMain.Columns.Remove("TableName");
                }
                dtDownloadedMain.AcceptChanges();
                recordsAffected = _objutility.InsertAccessDatabaseWithDownloadedData(dtDownloadedMain, transactionReplica, plantAppVersion, out returnQuery);

            }
            catch (Exception ex)
            {
                rollbackTransaction = true;
                throw (ex);
            }
            finally
            {
                if (transactionReplica != null)
                {
                    if (rollbackTransaction)
                    {
                        transactionReplica.Rollback();
                        //transactionMain.Rollback();
                        recordsAffected = 0;
                    }
                    else
                    {
                        //transactionMain.Commit();
                        transactionReplica.Commit();
                    }

                }

                if (connectionReplica != null)
                {
                    connectionReplica.Close();
                }
            }

            return recordsAffected;
        }

        private int ConvertToInt32(string rowValue)
        {
            int returnData = 0;
            if (int.TryParse(rowValue.ToString(), out returnData) == false)
            {
                returnData = 0;
            }
            return returnData;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dtInsertRows"></param>
        /// <param name="plantAppVersion"></param>
        /// <param name="UTCDifference"></param>
        /// <returns></returns>
        public int InsertAccessDatabaseWithDownloadedData(
            DataTable dtDownloadedMain,
            string plantAppVersion,
            string UTCDifference,
            string sourceConnectionString,
            string targetConnectionString,
            out string returnQuery)
        {
            bool rollbackTransaction = false;
            returnQuery = string.Empty;

            int[] columnNumbers;
            int columnNumbersIndex;
            int columnIndex;
            int recordsAffected;

            string tableName = string.Empty;

            OleDbConnection connectionMain = new OleDbConnection(sourceConnectionString);
            connectionMain.Open();
            OleDbConnection connectionReplica = new OleDbConnection(targetConnectionString);
            connectionReplica.Open();

            OleDbTransaction transactionMain = connectionMain.BeginTransaction();
            OleDbTransaction transactionReplica = connectionReplica.BeginTransaction();

            TimeSpan utcTimeSpan;
            recordsAffected = 0;

            try
            {
                Ut_sysUtility _objutility = new Ut_sysUtility();
                {
                    if (dtDownloadedMain.PrimaryKey.Count() > 0)
                    {
                        dtDownloadedMain.ExtendedProperties["UTCDifference"] = UTCDifference;
                        utcTimeSpan = GetTimeSpanToAdd(UTCDifference);

                        columnNumbers = new int[dtDownloadedMain.Columns.Count];
                        columnNumbersIndex = 0;
                        columnIndex = 0;

                        foreach (DataColumn dcCurrentColumn in dtDownloadedMain.Columns)
                        {
                            if (dcCurrentColumn.DataType == typeof(DateTime))
                            {
                                columnNumbers[columnNumbersIndex] = columnIndex;
                                columnNumbersIndex++;
                            }

                            columnIndex++;
                        }

                        foreach (DataRow dtrCurrentRow in dtDownloadedMain.Rows)
                        {
                            AdjustDateTimeValues(dtrCurrentRow, columnNumbers, columnNumbersIndex, utcTimeSpan);
                            dtrCurrentRow.AcceptChanges();
                        }

                        if (dtDownloadedMain.Columns.Contains("TableName"))
                        {
                            dtDownloadedMain.Columns.Remove("TableName");
                        }
                        dtDownloadedMain.AcceptChanges();

                        if (_objutility.InsertAccessDatabaseWithDownloadedData(dtDownloadedMain, transactionMain, plantAppVersion, out returnQuery) > 0)
                        {
                            dtDownloadedMain.AcceptChanges();
                            tableName = dtDownloadedMain.TableName.ToString().ToLower();
                            recordsAffected = _objutility.InsertAccessDatabaseWithDownloadedData(dtDownloadedMain, transactionReplica, plantAppVersion, out returnQuery);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                rollbackTransaction = true;
                throw (ex);
            }
            finally
            {
                if (rollbackTransaction)
                {
                    transactionReplica.Rollback();
                    transactionMain.Rollback();
                    recordsAffected = 0;
                }
                else
                {
                    transactionMain.Commit();
                    transactionReplica.Commit();
                }

                if (connectionReplica != null)
                {
                    connectionReplica.Close();
                }

                if (connectionMain != null)
                {
                    connectionMain.Close();
                }
            }

            return recordsAffected;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dtAsphaltNewBatches"></param>
        /// <param name="plantAppVersion"></param>
        /// <param name="UTCDifference"></param>
        /// <param name="sourceConnectionString"></param>
        /// <returns></returns>
        public DataTable InsertAsphaltNewBatches(
            DataTable dtAsphaltNewBatches,
            string plantAppVersion,
            string UTCDifference,
            string sourceConnectionString)
        {
            bool rollbackTransaction = false;

            int[] columnNumbers;
            int columnNumbersIndex;
            int columnIndex;
            int recordsAffected;

            string tableName = string.Empty;

            OleDbConnection connectionMain = new OleDbConnection(sourceConnectionString);
            connectionMain.Open();

            OleDbTransaction transactionMain = connectionMain.BeginTransaction();
            TimeSpan utcTimeSpan;
            recordsAffected = 0;

            try
            {
                Ut_sysUtility _objutility = new Ut_sysUtility();
                {
                    dtAsphaltNewBatches.ExtendedProperties["UTCDifference"] = UTCDifference;
                    utcTimeSpan = GetTimeSpanToAdd(UTCDifference);

                    columnNumbers = new int[dtAsphaltNewBatches.Columns.Count];
                    columnNumbersIndex = 0;
                    columnIndex = 0;

                    foreach (DataColumn dcCurrentColumn in dtAsphaltNewBatches.Columns)
                    {
                        if (dcCurrentColumn.DataType == typeof(DateTime))
                        {
                            columnNumbers[columnNumbersIndex] = columnIndex;
                            columnNumbersIndex++;
                        }

                        columnIndex++;
                    }

                    foreach (DataRow dtrCurrentRow in dtAsphaltNewBatches.Rows)
                    {
                        AdjustDateTimeValues(dtrCurrentRow, columnNumbers, columnNumbersIndex, utcTimeSpan);
                        dtrCurrentRow.AcceptChanges();
                    }

                    if (dtAsphaltNewBatches.Columns.Contains("TableName"))
                    {
                        dtAsphaltNewBatches.Columns.Remove("TableName");
                    }

                    dtAsphaltNewBatches.AcceptChanges();
                    tableName = dtAsphaltNewBatches.TableName.ToString().ToLower();

                    dtAsphaltNewBatches = _objutility.InsertAsphaltNewBatches(dtAsphaltNewBatches, plantAppVersion, transactionMain);
                    dtAsphaltNewBatches.TableName = tableName;
                }
            }
            catch (Exception ex)
            {
                rollbackTransaction = true;
                dtAsphaltNewBatches = null;
                throw (ex);
            }
            finally
            {
                if (rollbackTransaction)
                {
                    transactionMain.Rollback();
                    recordsAffected = 0;
                    dtAsphaltNewBatches = null;
                }
                else
                {
                    transactionMain.Commit();
                    recordsAffected = dtAsphaltNewBatches.Rows.Count;
                }

                if (connectionMain != null)
                {
                    connectionMain.Close();
                }
            }
            return dtAsphaltNewBatches;
        }


        #region [ Desilting ]

        /// <summary>
        /// Update Access Database With Downloaded Data
        /// </summary>
        /// <param name="downLoad">Data set downloaded</param>
        /// <param name="plantAppVersion">Plant version</param>
        /// <returns>data table </returns>
        public DataTable UpdateAccessDatabaseWithDownloadedData(
            DataTable dtDownloadedMain,
            string plantAppVersion,
            string UTCDifference,
            string sourceConnectionString,
            string targetConnectionString,
            bool isInsert,
            out string returnQuery
            )
        {
            bool rollbackTransaction = false;
            returnQuery = string.Empty;

            int[] columnNumbers;
            int columnNumbersIndex;
            int columnIndex;
            int recordsAffected;

            string tableName = string.Empty;

            DataColumn[] dtcPrimaryKeyList = new DataColumn[0];

            DataTable returnData = new DataTable();

            OleDbConnection connectionMain = new OleDbConnection(sourceConnectionString);
            OleDbConnection connectionReplica = new OleDbConnection(targetConnectionString);
            connectionMain.Open();
            connectionReplica.Open();

            OleDbTransaction transactionMain = connectionMain.BeginTransaction();
            OleDbTransaction transactionReplica = connectionReplica.BeginTransaction();

            TimeSpan utcTimeSpan;

            try
            {
                Ut_sysUtility _objutility = new Ut_sysUtility();
                if (dtDownloadedMain.PrimaryKey.Count() > 0)
                {
                    dtDownloadedMain.ExtendedProperties["UTCDifference"] = UTCDifference;
                    utcTimeSpan = GetTimeSpanToAdd(UTCDifference);

                    columnNumbers = new int[dtDownloadedMain.Columns.Count];
                    columnNumbersIndex = 0;
                    columnIndex = 0;

                    foreach (DataColumn dcCurrentColumn in dtDownloadedMain.Columns)
                    {
                        if (dcCurrentColumn.DataType == typeof(DateTime))
                        {
                            columnNumbers[columnNumbersIndex] = columnIndex;
                            columnNumbersIndex++;
                        }

                        columnIndex++;
                    }

                    foreach (DataRow dtrCurrentRow in dtDownloadedMain.Rows)
                    {
                        AdjustDateTimeValues(dtrCurrentRow, columnNumbers, columnNumbersIndex, utcTimeSpan);
                        dtrCurrentRow.AcceptChanges();
                    }

                    if (dtDownloadedMain.Columns.Contains("TableName"))
                    {
                        dtDownloadedMain.Columns.Remove("TableName");
                    }

                    #region [ Normal Statement for Batch_Result ,Batch_Dat_Trans ,Batch ,Vitrag ,Asphalt ]

                    if (_objutility.UpdateAccessDatabaseWithDownloadedData(dtDownloadedMain, transactionMain, plantAppVersion, true, isInsert, out returnQuery) > 0)
                    {
                        recordsAffected = 0;
                        tableName = dtDownloadedMain.TableName.ToString().ToLower();
                        recordsAffected = _objutility.UpdateAccessDatabaseWithDownloadedData(dtDownloadedMain, transactionReplica, plantAppVersion, true, isInsert, out returnQuery);
                        bool addtoSyncResponse = false;

                        if (recordsAffected > 0)
                        {
                            if (returnData == null || returnData.Rows.Count == 0)
                            {
                                returnData = new DataTable(tableName);
                                DataTable syncDataTable = null;
                                if (tableName == "batch")
                                {
                                    syncDataTable = dtDownloadedMain.DefaultView.ToTable(true, "batchno");

                                    dtcPrimaryKeyList = new DataColumn[1];
                                    dtcPrimaryKeyList[0] = syncDataTable.Columns["batchno"];
                                    syncDataTable.PrimaryKey = dtcPrimaryKeyList;

                                    addtoSyncResponse = true;
                                }
                                else
                                {
                                    if (tableName != "batch_transaction" && tableName != "batch_result")
                                    {
                                        syncDataTable = dtDownloadedMain;

                                        dtcPrimaryKeyList = new DataColumn[dtDownloadedMain.PrimaryKey.Count()];
                                        dtcPrimaryKeyList = dtDownloadedMain.PrimaryKey;
                                        syncDataTable.PrimaryKey = dtcPrimaryKeyList;

                                        addtoSyncResponse = true;
                                    }
                                }

                                if (addtoSyncResponse == true)
                                {
                                    string[] columnsList = new string[dtcPrimaryKeyList.Count()];
                                    int columnListIndex = 0;
                                    foreach (DataColumn primaryKeyColumn in dtcPrimaryKeyList)
                                    {
                                        columnsList[columnListIndex] = primaryKeyColumn.ColumnName.ToString();
                                        columnListIndex++;
                                    }

                                    returnData = syncDataTable.DefaultView.ToTable(true, columnsList);

                                    if (tableName == "batch")
                                    {
                                        if (returnData.Columns.Contains("batchno"))
                                        {
                                            returnData.Columns["batchno"].ColumnName = "Batch_no";
                                        }
                                    }
                                }
                            }
                        }
                    }

                    #endregion [ Normal Statement for Batch_Result ,Batch_Dat_Trans ,Batch ,Vitrag ,Asphalt ]
                }
            }
            catch (Exception ex)
            {
                rollbackTransaction = true;
                throw (ex);
            }
            finally
            {
                if (rollbackTransaction)
                {
                    transactionReplica.Rollback();
                    transactionMain.Rollback();
                    returnData = new DataTable();
                }
                else
                {
                    transactionMain.Commit();
                    transactionReplica.Commit();
                }

                if (connectionReplica != null)
                {
                    connectionReplica.Close();
                }

                if (connectionMain != null)
                {
                    connectionMain.Close();
                }
            }

            return returnData;
        }

        #endregion
    }
}
