﻿using System;
using ScadaKing_Data_App.Class;
using System.Data;

namespace ScadaKing_Data_App.BusinessLayer.DataLayer
{
    internal class BatchDatTransRaw
    {
        #region Constructor

        /// <summary>
        /// Class constructor
        /// </summary>
        public BatchDatTransRaw()
        {
            // Nothing for now.
        }
        #endregion Constructor
        internal DataTable GetOriginalBatchList(long orgID, string customerCode, long fromBatchNo, long toBatchNo, DateTime? fromBatchDate, DateTime? toBatchDate, int batchType)
        {
            DataTable dtValues = new DataTable();
            try
            {
                ScadaKing_Web_Service.ScadaKing_Web_ServiceSoapClient ObjectService = new ScadaKing_Web_Service.ScadaKing_Web_ServiceSoapClient();
                //dtValues = ObjectService.GetOriginalBatchList();
             }
            catch (Exception ex)
            {
                throw ex;
            }
            return dtValues;
        }
        /*
        internal ScadaKing_Web_Service.Data_Trans_Field[] GetOriginalBatchList(long orgID, string customerCode, long fromBatchNo, long toBatchNo, DateTime? fromBatchDate, DateTime? toBatchDate, int batchType)
        {
            try
            {
                ScadaKing_Web_Service.PNF_GM_Sync_WSSoapClient objMCGM_Batch = new ScadaKing_Web_Service.PNF_GM_Sync_WSSoapClient();
                MCGM_BATCH.ScadaKing_Web_Service.TransactionHeader local_Transaction = new ScadaKing_Web_Service.TransactionHeader();
                
                local_Transaction.DeviceID = General.ConfigMachine_Id;
                local_Transaction.Password = General.ConfigService_Password;
                local_Transaction.UserName = MCGM_BATCH.Class.General.User_Name;
                local_Transaction.Config_App_Version = General.ConfigAPP_Version;
                local_Transaction.Config_App_Name = General.ConfigAPP_Name;
                local_Transaction.Win_Service_GUID = General.Win_Service_GUID;
                local_Transaction.Win_Service_Version = General.Win_Service_Version;
               
                MCGM_BATCH.ScadaKing_Web_Service.UploadLoginData UploadData = new ScadaKing_Web_Service.UploadLoginData();

                UploadData.OrgId = orgID;
                UploadData.CustomerCode = customerCode;
                UploadData.FromBatchNo = fromBatchNo;
                UploadData.ToBatchNo = toBatchNo;
                UploadData.FromBatchDate = fromBatchDate;
                UploadData.ToBatchDate = toBatchDate;
                UploadData.BatchType = batchType;

                ScadaKing_Web_Service.Data_Trans_Field[] DataTransObject = objMCGM_Batch.GetOriginalBatchList(local_Transaction, UploadData);

                return DataTransObject;
            }
            catch (Exception ex)
            {

                throw new Exception("OriginalBatchList::GetOriginalBatchList::Error occured.", ex);
            }

        }
        */
        /// <summary>
        /// 
        /// </summary>
        /// <param name="BatchNo"></param>
        /// <returns></returns>
         /*
        internal MCGM_BATCH.ScadaKing_Web_Service.Response_LocalSQLData MCGM_Transfered_Batch(long BatchNo, bool show_Raw_Data)
        {
            try
            {
               
                 ScadaKing_Web_Service.PNF_GM_Sync_WSSoapClient objMCGM_Batch = new ScadaKing_Web_Service.PNF_GM_Sync_WSSoapClient();
               
                MCGM_BATCH.ScadaKing_Web_Service.TransactionHeader local_Transaction = new ScadaKing_Web_Service.TransactionHeader();
                MCGM_BATCH.ScadaKing_Web_Service.paramUploadMCGMStruture pUploadData = new ScadaKing_Web_Service.paramUploadMCGMStruture();

                local_Transaction.DeviceID = General.ConfigMachine_Id;
                local_Transaction.Password = General.ConfigService_Password;
                local_Transaction.UserName = MCGM_BATCH.Class.General.User_Name;
                local_Transaction.Config_App_Version = General.ConfigAPP_Version;
                local_Transaction.Config_App_Name = General.ConfigAPP_Name;
                local_Transaction.Win_Service_GUID = General.Win_Service_GUID;
                local_Transaction.Win_Service_Version = General.Win_Service_Version;

                pUploadData.Batch_No = BatchNo;
                
                pUploadData.Show_Raw_Data = show_Raw_Data;
                
                return objMCGM_Batch.MCGM_Transfered_Batch(local_Transaction, pUploadData);
                 
            }
            catch (Exception ex)
            {
                throw new Exception("MCGM_Transfered_Batch::Error occured.", ex);
            }
        }
          */
        /// <summary>
        /// 
        /// </summary>
        /// <param name="batch_No"></param>
        /// <param name="file_Created_Date"></param>
        /// <returns></returns>
        internal bool Update_HTML_File_Creation_Status(string batch_No, string file_Created_Date)
        {
            try
            {
                /*
                ScadaKing_Web_Service.PNF_GM_Sync_WSSoapClient objMCGM_Batch = new ScadaKing_Web_Service.PNF_GM_Sync_WSSoapClient();
                */
                ScadaKing_Web_Service.TransactionHeader local_Transaction = new ScadaKing_Web_Service.TransactionHeader();
                ScadaKing_Web_Service.paramUploadMCGMStruture pUploadData = new ScadaKing_Web_Service.paramUploadMCGMStruture();
                local_Transaction.DeviceID = General.ConfigMachine_Id;
                local_Transaction.Password = General.ConfigService_Password;
                local_Transaction.UserName = ScadaKing_Data_App.Class.General.User_Name;
                local_Transaction.Config_App_Version = General.ConfigAPP_Version;
                local_Transaction.Config_App_Name = General.ConfigAPP_Name;
                local_Transaction.Win_Service_GUID = General.Win_Service_GUID;
                local_Transaction.Win_Service_Version = General.Win_Service_Version;
                pUploadData.Batch_No = Convert.ToInt32(batch_No);
                /*
                pUploadData.HTML_FILE_CREATED_DATE = file_Created_Date;
                MCGM_BATCH.ScadaKing_Web_Service.Response_LocalSQLData returnValue = objMCGM_Batch.Update_HTML_File_Creation_Status(local_Transaction, pUploadData);
                 */
            }
            catch (Exception ex)
            {
                throw new Exception("Update_HTML_File_Creation_Status::Error occured.", ex);
            }

            return true;
        }
    }
}