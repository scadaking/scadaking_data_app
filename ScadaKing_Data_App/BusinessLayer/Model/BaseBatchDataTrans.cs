﻿using System;

namespace ScadaKing_Data_App.BusinessLayer.Model
{
    public class BaseBatchDataTrans :ScadaKing_Data_App.BusinessLayer.Factory.BusinessObjectBase
    {
        #region InnerClass
        public enum BaseBatchDataTransFields
        {
            BATCH_NO,
            BATCH_DATE,
            BATCH_TIME,
            BATCH_TIME_TEXT,
            BATCH_START_TIME,
            BATCH_END_TIME,
            BATCH_YEAR,
            BATCHER_NAME,
            BATCHER_USER_LEVEL,
            CUSTOMER_CODE,
            CUSTOMER_NAME,
            RECIPE_CODE,
            RECIPE_NAME,
            MIXING_TIME,
            MIXER_CAPACITY,
            STRENGTH,
            SITE,
            TRUCK_NO,
            TRUCK_DRIVER,
            PRODUCTION_QTY,
            ORDERED_QTY,
            RETURNED_QTY,
            WITHTHISLOAD,
            BATCH_SIZE,
            ORDER_NO,
            SCHEDULE_ID,
            ORG_ID,
            BATCH_START_TIME_FLOAT,
            BATCH_END_TIME_FLOAT,
            ROUTE_ID,
            IS_MCGM,
            LOADING_DATETIME,
            UNLOADING_DATETIME,
            REPLAYSTATUS,
            CEM1_SETWT,
            VALID
        }
        #endregion

        #region Data Members

        double _batchno;
        DateTime? _batchdate;
        DateTime? _batchtime;
        string _batchtimetext;
        string _batchstarttime;
        ////DateTime _bthstarttime;
        string _batchendtime;
        int? _batchyear;
        string _batchername;
        string _batcheruserlevel;
        string _customercode;
        string _recipecode;
        string _recipename;
        double? _mixingtime;
        double? _mixercapacity;
        int? _strength;
        string _site;
        string _truckno;
        string _truckdriver;
        double _productionqty;
        double? _orderedqty;
        double? _returnedqty;
        double? _withthisload;
        double? _batchsize;
        string _orderno;
        string _scheduleid;
        long _orgid;
        string _customerName;
        ////string _organizationName;
        double _appVersion;
        double? _batchstarttimefloat;
        double? _batchendtimefloat;
        long? _routeid;
        bool? _ismcgm;
        double _cem1setwt;
        bool _valid;

        #endregion

        #region Properties

        public double BatchNo
        {
            get { return _batchno; }
            set
            {
                if (_batchno != value)
                {
                    _batchno = value;
                    PropertyHasChanged("BatchNo");
                }
            }
        }

        public DateTime? BatchDate
        {
            get { return _batchdate; }
            set
            {
                if (_batchdate != value)
                {
                    _batchdate = value;
                    PropertyHasChanged("BatchDate");
                }
            }
        }

        public DateTime? BatchTime
        {
            get { return _batchtime; }
            set
            {
                if (_batchtime != value)
                {
                    _batchtime = value;
                    PropertyHasChanged("BatchTime");
                }
            }
        }

        public string BatchTimeText
        {
            get { return _batchtimetext; }
            set
            {
                if (_batchtimetext != value)
                {
                    _batchtimetext = value;
                    PropertyHasChanged("BatchTimeText");
                }
            }
        }

        public string BatchStartTime
        {
            get { return _batchstarttime; }
            set
            {
                if (_batchstarttime != value)
                {
                    _batchstarttime = value;
                    PropertyHasChanged("BatchStartTime");
                }
            }
        }

        public string BatchEndTime
        {
            get { return _batchendtime; }
            set
            {
                if (_batchendtime != value)
                {
                    _batchendtime = value;
                    PropertyHasChanged("BatchEndTime");
                }
            }
        }

        public int? BatchYear
        {
            get { return _batchyear; }
            set
            {
                if (_batchyear != value)
                {
                    _batchyear = value;
                    PropertyHasChanged("BatchYear");
                }
            }
        }

        public string BatcherName
        {
            get { return _batchername; }
            set
            {
                if (_batchername != value)
                {
                    _batchername = value;
                    PropertyHasChanged("BatcherName");
                }
            }
        }

        public string BatcherUserLevel
        {
            get { return _batcheruserlevel; }
            set
            {
                if (_batcheruserlevel != value)
                {
                    _batcheruserlevel = value;
                    PropertyHasChanged("BatcherUserLevel");
                }
            }
        }

        public string CustomerCode
        {
            get { return _customercode; }
            set
            {
                if (_customercode != value)
                {
                    _customercode = value;
                    PropertyHasChanged("CustomerCode");
                }
            }
        }

        public string RecipeCode
        {
            get { return _recipecode; }
            set
            {
                if (_recipecode != value)
                {
                    _recipecode = value;
                    PropertyHasChanged("RecipeCode");
                }
            }
        }

        public string RecipeName
        {
            get { return _recipename; }
            set
            {
                if (_recipename != value)
                {
                    _recipename = value;
                    PropertyHasChanged("RecipeName");
                }
            }
        }

        public double? MixingTime
        {
            get { return _mixingtime; }
            set
            {
                if (_mixingtime != value)
                {
                    _mixingtime = value;
                    PropertyHasChanged("MixingTime");
                }
            }
        }

        public double? MixerCapacity
        {
            get { return _mixercapacity; }
            set
            {
                if (_mixercapacity != value)
                {
                    _mixercapacity = value;
                    PropertyHasChanged("MixerCapacity");
                }
            }
        }

        public int? Strength
        {
            get { return _strength; }
            set
            {
                if (_strength != value)
                {
                    _strength = value;
                    PropertyHasChanged("Strength");
                }
            }
        }

        public string Site
        {
            get { return _site; }
            set
            {
                if (_site != value)
                {
                    _site = value;
                    PropertyHasChanged("Site");
                }
            }
        }

        public string TruckNo
        {
            get { return _truckno; }
            set
            {
                if (_truckno != value)
                {
                    _truckno = value;
                    PropertyHasChanged("TruckNo");
                }
            }
        }

        public string TruckDriver
        {
            get { return _truckdriver; }
            set
            {
                if (_truckdriver != value)
                {
                    _truckdriver = value;
                    PropertyHasChanged("TruckDriver");
                }
            }
        }

        public double ProductionQty
        {
            get { return _productionqty; }
            set
            {
                if (_productionqty != value)
                {
                    _productionqty = Convert.ToDouble(value.ToString("0.00"));
                    PropertyHasChanged("ProductionQty");
                }
            }
        }

        public double? OrderedQty
        {
            get { return _orderedqty; }
            set
            {
                if (_orderedqty != value)
                {
                    _orderedqty = Convert.ToDouble(value.Value.ToString("0.00"));
                    PropertyHasChanged("OrderedQty");
                }
            }
        }

        public double? ReturnedQty
        {
            get { return _returnedqty; }
            set
            {
                if (_returnedqty != value)
                {
                    _returnedqty = value;
                    PropertyHasChanged("ReturnedQty");
                }
            }
        }

        public double? Withthisload
        {
            get { return _withthisload; }
            set
            {
                if (_withthisload != value)
                {
                    _withthisload = value;
                    PropertyHasChanged("Withthisload");
                }
            }
        }

        public double? BatchSize
        {
            get { return _batchsize; }
            set
            {
                if (_batchsize != value)
                {
                    _batchsize = value;
                    PropertyHasChanged("BatchSize");
                }
            }
        }

        public string OrderNo
        {
            get { return _orderno; }
            set
            {
                if (_orderno != value)
                {
                    _orderno = value;
                    PropertyHasChanged("OrderNo");
                }
            }
        }

        public string ScheduleId
        {
            get { return _scheduleid; }
            set
            {
                if (_scheduleid != value)
                {
                    _scheduleid = value;
                    PropertyHasChanged("ScheduleId");
                }
            }
        }

        public long OrgId
        {
            get { return _orgid; }
            set
            {
                if (_orgid != value)
                {
                    _orgid = value;
                    PropertyHasChanged("OrgId");
                }
            }
        }

        public string CustomerName
        {
            get { return _customerName; }
            set
            {
                if (_customerName != value)
                {
                    _customerName = value;
                    PropertyHasChanged("CustomerName");
                }
            }
        }

        public double? BatchStartTimeFloat
        {
            get { return _batchstarttimefloat; }
            set
            {
                if (_batchstarttimefloat != value)
                {
                    _batchstarttimefloat = value;
                    PropertyHasChanged("BatchStartTimeFloat");
                }
            }
        }

        public double? BatchEndTimeFloat
        {
            get { return _batchendtimefloat; }
            set
            {
                if (_batchendtimefloat != value)
                {
                    _batchendtimefloat = value;
                    PropertyHasChanged("BatchEndTimeFloat");
                }
            }
        }

        public long? RouteId
        {
            get { return _routeid; }
            set
            {
                if (_routeid != value)
                {
                    _routeid = value;
                    PropertyHasChanged("RouteId");
                }
            }
        }

        public double AppVersion
        {
            get { return _appVersion; }
            set
            {
                if (_appVersion != value)
                {
                    _appVersion = value;
                    PropertyHasChanged("AppVersion");
                }
            }
        }

        public bool? IsMCGM
        {
            get { return _ismcgm; }
            set
            {
                if (_ismcgm != value)
                {
                    _ismcgm = value;
                    PropertyHasChanged("IsMCGM");
                }
            }
        }

        public double Cem1SetWt
        {
            get { return _cem1setwt; }
            set
            {
                if (_cem1setwt != value)
                {
                    _cem1setwt = value;
                    PropertyHasChanged("CEM1SETWT");
                }
            }
        }

        public bool Valid
        {
            get { return _valid; }
            set
            {
                if (_valid != value)
                {
                    _valid = value;
                    PropertyHasChanged("VALID");
                }
            }
        }

        #endregion

      
    }
}
