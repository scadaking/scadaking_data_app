﻿using System;

namespace ScadaKing_Data_App.BusinessLayer.Model
{
    public class BatchDatTransRaw : BaseBatchDataTrans
    {
        public BatchDatTransRaw()
        { }

        public DateTime? LoadingDateTime { get; set; }

        public DateTime? UnLoadingDateTime { get; set; }

        public string ReplayStatus { get; set; }

    }
}
