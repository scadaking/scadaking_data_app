﻿namespace PNF
{
    #region [ Using's ]
    using System;
    using System.ComponentModel;
    using System.Configuration;
    using System.Data;
    using System.Data.OleDb;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Windows.Forms;
    #endregion [ Using's ]

    /// <summary>
    /// Ut_sysUtility
    /// </summary>
    public class Ut_sysUtility : IDisposable
    {

        /// <summary>
        /// InsertinDatabase
        /// </summary>
        /// <param name="dtInsertRecords">Table for Inserting the Records</param>
        /// <param name="blnole">Access Database</param>
        /// <param name="blnsql">Sql Database</param>
        /// <param name="plantAppVersion">Plant Version</param>
        /// <returns>No of Records affected</returns>
        public int InsertinDatabase(
            DataTable dtInsertRecords,
            string plantAppVersion,
            string targetConnectionString,
            out string returnQuery)
        {
            int returnData = 0;
            returnQuery = string.Empty;
            try
            {

                dtInsertRecords = this.RemoveUnwantedFields(dtInsertRecords, targetConnectionString);
                dtInsertRecords.AcceptChanges();

                using (OleDbConnection connection = new OleDbConnection(targetConnectionString))
                {
                    connection.Open();
                    using (OleDbTransaction transaction = connection.BeginTransaction())
                    {
                        try
                        {
                            returnData = this.InsertinAccessDatabase(dtInsertRecords, plantAppVersion, transaction, out returnQuery);

                            transaction.Commit();
                        }
                        catch (Exception ex)
                        {
                            transaction.Rollback();
                            throw ex;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("class RI_syncUtility::Ut_sysUtility::InsertinDatabase " + ex.ToString());
            }

            returnQuery = string.Empty;
            return returnData;
        }

        /// <summary>
        /// Update in database
        /// </summary>
        /// <param name="dtUpdateRecords">Table for Updating the Records</param>
        /// <param name="blnole">Access Database</param>
        /// <param name="blnsql">Sql Database</param>
        /// <param name="plantAppVersion">Plant Version</param>
        /// <returns>No of Records affected</returns>
        public int UpdateinDatabase(
            DataTable dtUpdateRecords,
            string plantAppVersion,
            string targetConnectionString,
            out string returnQuery)
        {
            int returnData = 0;
            returnQuery = string.Empty;
            try
            {

                dtUpdateRecords = this.RemoveUnwantedFields(dtUpdateRecords, targetConnectionString);
                dtUpdateRecords.AcceptChanges();

                using (OleDbConnection connection = new OleDbConnection(targetConnectionString))
                {
                    connection.Open();
                    using (OleDbTransaction transaction = connection.BeginTransaction())
                    {
                        try
                        {
                            returnData = this.UpdateinAccessDatabase(dtUpdateRecords, plantAppVersion, transaction, out returnQuery);
                            transaction.Commit();
                        }
                        catch (Exception ex)
                        {
                            transaction.Rollback();
                            throw ex;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("class RI_syncUtility::Ut_sysUtility::UpdateinDatabase " + ex.ToString());
            }

            returnQuery = string.Empty;
            return returnData;
        }

        /// <summary>
        /// UpdateAccessDatabaseWithDownloadedData
        /// </summary>
        /// <param name="dtUpdateRecords">Table for Updating the Records</param>
        /// <param name="transaction">Connection Transaction</param>
        /// <param name="plantAppVersion">Plant Version</param>
        /// <param name="queryExecute"></param>
        /// <returns>No of Records affected</returns>
        public int UpdateAccessDatabaseWithDownloadedData(
            DataTable dtUpdateRecords,
            OleDbTransaction transaction,
            string plantAppVersion,
            bool queryExecute,
            out string returnQuery)
        {
            int totalRowsAffected = 0;
            string updateQuery;
            returnQuery = string.Empty;

            try
            {
                DataTable tempTable;

                /* build string for where clause */
                string strWhere;
                string executionType;
                string tableName;
                string commaValue;
                string primaryFields;
                string columnValue;
                string columnName;

                int loopColumnIndex;
                int loopRowIndex;
                int columnsCount;
                int rowsCount;
                int rowsAffected;
                bool executeQueryLogic;

                executeQueryLogic = false;
                primaryFields = string.Empty;
                strWhere = string.Empty;

                tableName = dtUpdateRecords.TableName.ToString();

                strWhere = WhereString(dtUpdateRecords, out primaryFields);
                if (strWhere.Length > 0)
                {
                    executionType = ConfigurationSettings.AppSettings["ExecutionType"].ToString().ToUpper();
                    if ((plantAppVersion == "2" || plantAppVersion == "2.0") || executionType == "QUERY".ToUpper())
                    {
                        if (executionType == "QUERY".ToUpper())
                        {
                            executeQueryLogic = true;
                        }
                        else
                        {
                            using (OleDbCommand commandUpdate = new OleDbCommand("DELETE FROM " + tableName + strWhere, transaction.Connection, transaction))
                            {
                                rowsAffected = commandUpdate.ExecuteNonQuery();
                            }

                            rowsAffected = this.InsertinAccessDatabase(dtUpdateRecords, plantAppVersion, transaction, out returnQuery);
                            totalRowsAffected = rowsAffected;
                        }
                    }
                    else
                    {
                        if (queryExecute == false)
                        {
                            #region [ Data Adapter Logic ]
                            DataSet localDataSet = new DataSet("TempDataSet");
                            OleDbCommand selectCommand;
                            OleDbDataAdapter dataAdapter;
                            OleDbCommandBuilder commandBuilder;

                            localDataSet = new DataSet("TempDataSet");
                            selectCommand = new OleDbCommand("Select * from " + tableName + strWhere, transaction.Connection, transaction);
                            dataAdapter = new OleDbDataAdapter(selectCommand);
                            commandBuilder = new OleDbCommandBuilder(dataAdapter);
                            dataAdapter.ContinueUpdateOnError = true;
                            dataAdapter.Fill(localDataSet, tableName);

                            StringBuilder sb = new StringBuilder();
                            sb.Append("Update " + dtUpdateRecords.TableName + " SET ");
                            string strwhere = string.Empty;

                            #region [ First Execption ]
                            try
                            {
                                commaValue = string.Empty;
                                foreach (DataColumn item in dtUpdateRecords.Columns)
                                {
                                    if (primaryFields.Contains(item.Caption) == false)
                                    {
                                        sb.Append(commaValue + item.Caption + "=@" + item.Caption);
                                        commaValue = ",";
                                    }
                                }

                                foreach (DataColumn dtCol in dtUpdateRecords.PrimaryKey)
                                {
                                    strwhere = strwhere + ((strwhere.Length > 0) ? " and " : "") + dtCol.ColumnName.ToString() + " = @" + dtCol.ColumnName.ToString();
                                }

                                if (strwhere.Length > 0)
                                {
                                    sb.Append(" WHERE " + strwhere);
                                }
                            }
                            catch (Exception ex)
                            {
                                throw new Exception("class RI_syncUtility::Ut_sysUtility::UpdateAccessDatabase::First Exception " + ex.ToString());
                            }
                            #endregion [ First Execption ]

                            #region [ Second Execption ]
                            try
                            {
                                OleDbCommand DAUpdateCmd = new OleDbCommand(sb.ToString(), dataAdapter.SelectCommand.Connection, transaction);
                                {
                                    foreach (DataColumn item in dtUpdateRecords.Columns)
                                    {
                                        OleDbParameter Param = new OleDbParameter();
                                        Param.ParameterName = "@" + item.Caption;
                                        Param.SourceColumn = item.Caption;

                                        bool blnprimerykey = false;
                                        foreach (DataColumn dtcol in dtUpdateRecords.PrimaryKey)
                                        {
                                            if (item.ColumnName.ToString() == dtcol.ColumnName.ToString())
                                            {
                                                blnprimerykey = true;
                                                break;
                                            }
                                        }

                                        if (blnprimerykey)   /* ** COMPARE WITH PRIMARY KEY COLUMNS*/
                                        {
                                            Param.SourceVersion = DataRowVersion.Original;
                                        }
                                        else
                                        {
                                            Param.SourceVersion = DataRowVersion.Current;
                                        }

                                        DAUpdateCmd.Parameters.Add(Param);
                                    }

                                    dataAdapter.UpdateCommand = DAUpdateCmd;
                                }
                            }
                            catch (Exception ex)
                            {
                                throw new Exception("class RI_syncUtility::Ut_sysUtility::UpdateAccessDatabase::Second Exception " + ex.ToString());
                            }

                            #endregion [ Second Execption ]

                            #region [ Third Execption ]
                            try
                            {
                                DataColumn[] dtprimery = dtUpdateRecords.PrimaryKey;

                                foreach (DataRow dtrUpdate in dtUpdateRecords.Rows)
                                {

                                    strwhere = "";
                                    foreach (DataColumn dtCol in dtUpdateRecords.PrimaryKey)
                                    {
                                        strwhere += ((strwhere.Length > 0) ? " and " : "") + dtCol.ColumnName.ToString() + " = '" + dtrUpdate[dtCol.ColumnName.ToString()] + "'";
                                    }

                                    DataRow[] _objTmp = dtUpdateRecords.Select(strwhere);
                                    if (_objTmp == null || _objTmp.Length == 0)
                                    {
                                        /*No Record found in local database (Access)*/
                                    }
                                    else
                                    {
                                        foreach (DataColumn item in dtrUpdate.Table.Columns)
                                        {
                                            _objTmp[0][item.ColumnName] = dtrUpdate[item.ColumnName];
                                        }

                                        DataRow _dr = dtUpdateRecords.NewRow();
                                        foreach (DataColumn _PkCols in dtUpdateRecords.PrimaryKey)
                                        {
                                            _dr[_PkCols.ColumnName.ToString()] = dtrUpdate[_PkCols.ColumnName.ToString()];
                                        }

                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                throw new Exception("class RI_syncUtility::Ut_sysUtility::UpdateAccessDatabase::Third Exception " + ex.ToString());
                            }
                            #endregion [ Third Execption ]

                            totalRowsAffected = dataAdapter.Update(dtUpdateRecords);

                            localDataSet.Dispose();
                            dataAdapter.Dispose();
                            commandBuilder.Dispose();

                            #endregion [ Data Adapter Logic ]

                            dtUpdateRecords.AcceptChanges();
                        }
                        else
                        {
                            executeQueryLogic = true;
                        }
                    }
                }

                if (executeQueryLogic == true)
                {
                    #region [ Update Query Logic]
                    string dateTimeFormat = ConfigurationSettings.AppSettings["dateTimeFormat"].ToString(); /*"dd/MM/yyyy hh:mm tt";*/
                    string dateFormat = ConfigurationSettings.AppSettings["dateFormat"].ToString(); /*"dd/MM/yyyy";*/

                    commaValue = string.Empty;
                    columnValue = string.Empty;
                    columnName = string.Empty;
                    columnsCount = dtUpdateRecords.Columns.Count;
                    rowsCount = dtUpdateRecords.Rows.Count;

                    tempTable = dtUpdateRecords.Clone();

                    for (loopRowIndex = 0; loopRowIndex < rowsCount; loopRowIndex++)
                    {
                        tempTable.ImportRow(dtUpdateRecords.Rows[loopRowIndex]);
                        strWhere = WhereString(tempTable);
                        tempTable.Rows.Clear();

                        updateQuery = "Update " + tableName + " SET ";
                        commaValue = string.Empty;
                        loopColumnIndex = 0;

                        for (loopColumnIndex = 0; loopColumnIndex < columnsCount; loopColumnIndex++)
                        {
                            columnValue = dtUpdateRecords.Rows[loopRowIndex][loopColumnIndex].ToString();
                            columnName = dtUpdateRecords.Columns[loopColumnIndex].ColumnName;
                            if (primaryFields.Contains(columnName) == false)
                            {
                                if (dtUpdateRecords.Columns[loopColumnIndex].DataType.Name == "String")
                                {
                                    if (string.IsNullOrEmpty(columnValue) == false)
                                    {
                                        updateQuery += commaValue + "[" + columnName + "] = '" + columnValue + "'";
                                    }
                                }
                                else if (dtUpdateRecords.Columns[loopColumnIndex].DataType.Name == "DateTime")
                                {
                                    if (columnName.ToLower().Contains("time"))
                                    {
                                        updateQuery += commaValue + "[" + columnName + "] = #" + Convert.ToDateTime(columnValue).ToString(dateTimeFormat) + "#";
                                    }
                                    else
                                    {
                                        updateQuery += commaValue + "[" + columnName + "] = #" + Convert.ToDateTime(columnValue).ToString(dateFormat) + "#";
                                    }

                                }
                                else
                                {
                                    updateQuery += commaValue + "[" + columnName + "] = " + (string.IsNullOrEmpty(columnValue) ? "0" : columnValue);
                                }

                                commaValue = Environment.NewLine + ",";
                            }
                        }

                        updateQuery = updateQuery + " " + strWhere;
                        returnQuery = updateQuery;

                        rowsAffected = 0;
                        using (OleDbCommand commandUpdate = new OleDbCommand(updateQuery, transaction.Connection, transaction))
                        {
                            rowsAffected = commandUpdate.ExecuteNonQuery();
                        }

                        totalRowsAffected += rowsAffected;
                    }

                    #endregion [ Update Query Logic]
                }
            }
            catch (Exception ex)
            {
                throw new Exception("class RI_syncUtility::Ut_sysUtility::UpdateAccessDatabase " + ex.ToString());
            }

            returnQuery = string.Empty;
            return totalRowsAffected;
        }

        /// <summary>
        /// InsertCSVDataToMainDatabase Method
        /// </summary>
        /// <param name="dtInsertRecords">Table for Inserting the Records</param>
        /// <param name="plantAppVersion">Plant Version</param>
        /// <param name="sourceDatabase"></param>
        /// <returns>No of Records affected</returns>
        public int InsertCSVDataToMainDatabase(
            DataTable dtInsertRecords,
            string plantAppVersion,
            bool sourceDatabase,
            string sourceConnectionString,
            string targetConnectionString
            )
        {
            int returnData = 0;

            try
            {
                string connectionString = string.Empty;
                if (sourceDatabase)
                {
                    connectionString = sourceConnectionString;
                }
                else
                {
                    connectionString = targetConnectionString;
                }

                dtInsertRecords = this.RemoveUnwantedFields(dtInsertRecords, connectionString);
                dtInsertRecords.AcceptChanges();

                using (OleDbConnection connection = new OleDbConnection(connectionString))
                {
                    if (connection.State == ConnectionState.Broken ||
                        connection.State == ConnectionState.Closed)
                    {
                        connection.Open();
                    }

                    using (OleDbTransaction transaction = connection.BeginTransaction())
                    {
                        try
                        {
                            if (sourceDatabase)
                            {
                                using (OleDbCommand commandUpdate = new OleDbCommand("DELETE FROM " + dtInsertRecords.TableName.ToString(), transaction.Connection, transaction))
                                {
                                    returnData = commandUpdate.ExecuteNonQuery();
                                }
                            }


                            returnData = this.InsertinAccessDatabaseFromCSV(dtInsertRecords, plantAppVersion, transaction);
                            transaction.Commit();
                        }
                        catch (Exception ex)
                        {
                            transaction.Rollback();
                            throw ex;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("class RI_syncUtility::Ut_sysUtility::InsertCSVDataToMainDatabase " + ex.ToString());
            }

            return returnData;
        }

        /// <summary>
        /// InsertAccessDatabaseWithDownloadedData
        /// </summary>
        /// <param name="dtInsertRecords"></param>
        /// <param name="transaction"></param>
        /// <param name="plantAppVersion"></param>
        /// <returns></returns>
        public int InsertAccessDatabaseWithDownloadedData(
            DataTable dtInsertRecords,
            OleDbTransaction transaction,
            string plantAppVersion,
            out string returnQuery)
        {
            int totalRowsAffected = 0;
            try
            {
                /* build string for where clause */
                totalRowsAffected = this.InsertinAccessDatabase(dtInsertRecords, plantAppVersion, transaction, out returnQuery);
            }
            catch (Exception ex)
            {
                throw new Exception("class RI_syncUtility::Ut_sysUtility::UpdateAccessDatabase " + ex.ToString());
            }

            returnQuery = string.Empty;
            return totalRowsAffected;
        }

        /// <summary>
        /// Delete_SelectedData method
        /// </summary>
        /// <param name="dtdelteRecords"></param>
        /// <param name="transaction">Connection Transaction</param>
        /// <param name="plantAppVersion">Plant Version</param>
        /// <param name="xmlDataTable"></param>
        /// <returns>returns true/false</returns>
        public bool Delete_SelectedData(
            DataTable dtdelteRecords,
            OleDbTransaction transaction,
            string plantAppVersion,
            DataTable xmlDataTable)
        {
            bool returnData = false;
            try
            {
                DataTable dtTablesList = null;
                string strWhere = string.Empty;
                string columnName = string.Empty;

                OleDbCommand cmdDelete = transaction.Connection.CreateCommand();

                if (dtTablesList == null)
                {
                    string[] restrictions = new string[4];
                    restrictions[3] = "Table";
                    dtTablesList = transaction.Connection.GetSchema("Tables", restrictions);
                }

                columnName = "Batch_No";
                var names = (from a in dtdelteRecords.AsEnumerable()
                             select (string)a[columnName].ToString()).Distinct().ToArray();

                if (names != null && names.Count() > 0)
                {
                    string[] arr = names.ToArray();
                    string str = string.Join(", ", arr);
                    strWhere = string.Format(" where {0} in ({1}) ", columnName, str);
                }

                string[] strtableName = new string[7];
                strtableName[0] = "tb_batch";
                strtableName[1] = "Batch";
                strtableName[2] = "Batch_Transaction";
                strtableName[3] = "Batch_result";
                strtableName[4] = "Batch_dat_trans";
                strtableName[5] = "VITRAG_BATCH";
                strtableName[6] = "batchlist";

                if (strWhere.Trim().Length > 0)
                {
                    foreach (string tableName in strtableName)
                    {
                        if (dtTablesList.Select("TABLE_NAME = '" + tableName + "'").Length > 0)
                        {
                            string strquery = this.Createquery(tableName, transaction, xmlDataTable);
                            switch (tableName)
                            {
                                case "tb_batch":
                                    strquery += strWhere.Replace("Batch_No", "id");
                                    break;
                                case "Batch_result":
                                    strquery += strWhere.Replace("Batch_No", "Ticket_no");
                                    break;
                                case "Batch":
                                case "batchlist":
                                    strquery += strWhere.Replace("Batch_No", "BatchNo");
                                    break;
                                default:
                                    strquery += strWhere;
                                    break;
                            }

                            cmdDelete.CommandText = strquery;
                            cmdDelete.Transaction = transaction;
                            if (cmdDelete.ExecuteNonQuery() > 0)
                            {
                                dtdelteRecords.TableName = tableName;
                                returnData = true;
                            }
                        }
                    }
                }

                cmdDelete.Dispose();
            }
            catch (Exception ex)
            {
                returnData = false;
                throw new Exception("class RI_syncUtility::Delete_SelectedData " + ex.ToString());
            }

            return returnData;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dtInsertRecords"></param>
        /// <param name="plantAppVersion"></param>
        /// <param name="transaction"></param>
        /// <returns></returns>
        public DataTable InsertAsphaltNewBatches(DataTable dtInsertRecords, string plantAppVersion, OleDbTransaction transaction)
        {
            string tableName = string.Empty;
            int totalRowsAffected = 0;
            DataTable returnData = new DataTable();
            DataSet localDataSet = null;
            returnData.Columns.Add("Row_Id", typeof(System.String));
            returnData.Columns.Add("New_Id", typeof(System.String));
            string sline_no = "1";
            try
            {
                tableName = dtInsertRecords.TableName.ToString();
                int loopColumnIndex;
                int columnsCount = dtInsertRecords.Columns.Count;
                string fieldNames = string.Empty;
                string commaValue = string.Empty;
                {
                    #region [ Insert Query Logic]

                    string dateTimeFormat = ConfigurationSettings.AppSettings["dateTimeFormat"].ToString(); /*"dd/MM/yyyy hh:mm tt";*/
                    string dateFormat = ConfigurationSettings.AppSettings["dateFormat"].ToString(); /*"dd/MM/yyyy";*/

                    string insertQuery;
                    string columnValue;
                    string columnName;
                    string blankParameter;
                    string doublQuote;
                    string query2;

                    int loopRowIndex;
                    int rowsCount;
                    int rowsAffected;
                    int returnID;
                    int OldID;

                    blankParameter = "@blankParameter";
                    query2 = "Select @@Identity";

                    doublQuote = string.Empty;
                    commaValue = string.Empty;
                    columnValue = string.Empty;
                    columnName = string.Empty;

                    returnID = 0;
                    OldID = 0;

                    rowsCount = dtInsertRecords.Rows.Count;
                    sline_no = "2";
                    for (loopRowIndex = 0; loopRowIndex < rowsCount; loopRowIndex++)
                    {
                        commaValue = string.Empty;
                        loopColumnIndex = 0;
                        insertQuery = string.Empty;
                        fieldNames = string.Empty;

                        sline_no = "3-" + loopRowIndex.ToString();
                        OldID = ConvertToInt32(dtInsertRecords.Rows[loopRowIndex]["Row_Id"].ToString());

                        for (loopColumnIndex = 0; loopColumnIndex < columnsCount; loopColumnIndex++)
                        {
                            columnValue = dtInsertRecords.Rows[loopRowIndex][loopColumnIndex].ToString();
                            columnName = dtInsertRecords.Columns[loopColumnIndex].ColumnName;
                            if (columnName.ToLower() != "Row_Id".ToLower() && columnName.ToLower() != "New_Id".ToLower())
                            {
                                if (!string.IsNullOrEmpty(columnValue))
                                {
                                    fieldNames += commaValue + "[" + columnName + "]";

                                    if (dtInsertRecords.Columns[loopColumnIndex].DataType.Name == "String")
                                    {
                                        insertQuery += commaValue + "'" + columnValue + "'";
                                    }
                                    else if (dtInsertRecords.Columns[loopColumnIndex].DataType.Name == "DateTime")
                                    {
                                        if (columnName.ToLower().Contains("time"))
                                        {
                                            insertQuery += commaValue + "#" + Convert.ToDateTime(columnValue).ToString(dateTimeFormat) + "#";
                                        }
                                        else
                                        {
                                            insertQuery += commaValue + "#" + Convert.ToDateTime(columnValue).ToString(dateFormat) + "#";
                                        }
                                    }
                                    else
                                    {
                                        insertQuery += commaValue + (string.IsNullOrEmpty(columnValue) ? "0" : columnValue);
                                    }
                                    commaValue = Environment.NewLine + ",";
                                }
                            }

                        }

                        insertQuery = "INSERT INTO " + tableName + " ( " + fieldNames + ") VALUES(" + Environment.NewLine + insertQuery + ")";
                        insertQuery = insertQuery.Replace("''", blankParameter);
                        commaValue = string.Empty;
                        loopColumnIndex = 0;
                        returnID = 0;
                        rowsAffected = 0;
                        sline_no = "3-" + loopRowIndex.ToString() + " -0";
                        try
                        {
                            using (OleDbCommand commandUpdate = new OleDbCommand(insertQuery, transaction.Connection, transaction))
                            {
                                commandUpdate.Parameters.Add(blankParameter, OleDbType.VarChar).Value = doublQuote;
                                rowsAffected = commandUpdate.ExecuteNonQuery();
                                commandUpdate.CommandText = query2;
                                returnID = (int)commandUpdate.ExecuteScalar();
                            }
                            sline_no = "4-" + loopRowIndex.ToString();

                            DataRow dtRow = returnData.NewRow();
                            dtRow["Row_Id"] = OldID;
                            dtRow["New_ID"] = returnID;
                            returnData.Rows.Add(dtRow);
                            returnData.AcceptChanges();
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("class RI_syncUtility::Ut_sysUtility::InsertinAccessDatabase " + ex.ToString() + Environment.NewLine + insertQuery + Environment.NewLine + " Error Line No : " + sline_no);
                        }

                        totalRowsAffected += rowsAffected;
                    }

                    #endregion [ Insert Query Logic]
                }
            }
            catch (Exception ex)
            {
                throw new Exception("class RI_syncUtility::Ut_sysUtility::InsertinAccessDatabase " + ex.ToString() + Environment.NewLine + " Error Line No : " + sline_no);
            }
            finally
            {
                if (localDataSet != null)
                {
                    localDataSet.Dispose();
                }
                localDataSet = null;
            }
            return returnData;
        }

        #region [ Desilting]

        /// <summary>
        /// UpdateAccessDatabaseWithDownloadedData
        /// </summary>
        /// <param name="dtUpdateRecords">Table for Updating the Records</param>
        /// <param name="transaction">Connection Transaction</param>
        /// <param name="plantAppVersion">Plant Version</param>
        /// <param name="queryExecute"></param>
        /// <returns>No of Records affected</returns>
        public int UpdateAccessDatabaseWithDownloadedData(
            DataTable dtUpdateRecords,
            OleDbTransaction transaction,
            string plantAppVersion,
            bool queryExecute,
            bool isInsert,
            out string returnQuery)
        {
            int totalRowsAffected = 0;
            returnQuery = string.Empty;

            try
            {
                DataTable tempTable;

                /* build string for where clause */
                string strWhere;
                string executionType;
                string tableName;
                string commaValue;
                string primaryFields;
                string updateQuery;
                string columnValue;
                string columnName;

                int loopColumnIndex;
                int loopRowIndex;
                int columnsCount;
                int rowsCount;
                int rowsAffected;
                bool executeQueryLogic;

                executeQueryLogic = false;
                primaryFields = string.Empty;
                strWhere = string.Empty;

                tableName = dtUpdateRecords.TableName.ToString();

                strWhere = WhereString(dtUpdateRecords, out primaryFields);
                if (strWhere.Length > 0)
                {
                    executionType = ConfigurationSettings.AppSettings["ExecutionType"].ToString().ToUpper();
                    if ((plantAppVersion == "2" || plantAppVersion == "2.0" || plantAppVersion == "21")
                        || executionType == "QUERY".ToUpper()
                        )
                    {
                        if (executionType == "QUERY".ToUpper() || plantAppVersion == "21")
                        {
                            executeQueryLogic = true;
                        }
                        else
                        {
                            using (OleDbCommand commandUpdate = new OleDbCommand("DELETE FROM " + tableName + strWhere, transaction.Connection, transaction))
                            {
                                rowsAffected = commandUpdate.ExecuteNonQuery();
                            }

                            rowsAffected = this.InsertinAccessDatabase(dtUpdateRecords, plantAppVersion, transaction, out returnQuery);
                            totalRowsAffected = rowsAffected;
                        }
                    }
                    else
                    {
                        if (queryExecute == false)
                        {
                            #region [ Data Adapter Logic ]
                            DataSet localDataSet = new DataSet("TempDataSet");
                            OleDbCommand selectCommand;
                            OleDbDataAdapter dataAdapter;
                            OleDbCommandBuilder commandBuilder;

                            localDataSet = new DataSet("TempDataSet");
                            selectCommand = new OleDbCommand("Select * from " + tableName + strWhere, transaction.Connection, transaction);
                            dataAdapter = new OleDbDataAdapter(selectCommand);
                            commandBuilder = new OleDbCommandBuilder(dataAdapter);
                            dataAdapter.ContinueUpdateOnError = true;
                            dataAdapter.Fill(localDataSet, tableName);

                            StringBuilder sb = new StringBuilder();
                            sb.Append("Update " + dtUpdateRecords.TableName + " SET ");
                            string strwhere = string.Empty;

                            #region [ First Execption ]
                            try
                            {
                                commaValue = string.Empty;
                                foreach (DataColumn item in dtUpdateRecords.Columns)
                                {
                                    if (primaryFields.Contains(item.Caption) == false)
                                    {
                                        sb.Append(commaValue + item.Caption + "=@" + item.Caption);
                                        commaValue = ",";
                                    }
                                }

                                foreach (DataColumn dtCol in dtUpdateRecords.PrimaryKey)
                                {
                                    strwhere = strwhere + ((strwhere.Length > 0) ? " and " : "") + dtCol.ColumnName.ToString() + " = @" + dtCol.ColumnName.ToString();
                                }

                                if (strwhere.Length > 0)
                                {
                                    sb.Append(" WHERE " + strwhere);
                                }
                            }
                            catch (Exception ex)
                            {
                                throw new Exception("class RI_syncUtility::Ut_sysUtility::UpdateAccessDatabase::First Exception " + ex.ToString());
                            }
                            #endregion [ First Execption ]

                            #region [ Second Execption ]
                            try
                            {
                                OleDbCommand DAUpdateCmd = new OleDbCommand(sb.ToString(), dataAdapter.SelectCommand.Connection, transaction);
                                {
                                    foreach (DataColumn item in dtUpdateRecords.Columns)
                                    {
                                        OleDbParameter Param = new OleDbParameter();
                                        Param.ParameterName = "@" + item.Caption;
                                        Param.SourceColumn = item.Caption;

                                        bool blnprimerykey = false;
                                        foreach (DataColumn dtcol in dtUpdateRecords.PrimaryKey)
                                        {
                                            if (item.ColumnName.ToString() == dtcol.ColumnName.ToString())
                                            {
                                                blnprimerykey = true;
                                                break;
                                            }
                                        }

                                        if (blnprimerykey)   /* ** COMPARE WITH PRIMARY KEY COLUMNS*/
                                        {
                                            Param.SourceVersion = DataRowVersion.Original;
                                        }
                                        else
                                        {
                                            Param.SourceVersion = DataRowVersion.Current;
                                        }

                                        DAUpdateCmd.Parameters.Add(Param);
                                    }

                                    dataAdapter.UpdateCommand = DAUpdateCmd;
                                }
                            }
                            catch (Exception ex)
                            {
                                throw new Exception("class RI_syncUtility::Ut_sysUtility::UpdateAccessDatabase::Second Exception " + ex.ToString());
                            }

                            #endregion [ Second Execption ]

                            #region [ Third Execption ]
                            try
                            {
                                DataColumn[] dtprimery = dtUpdateRecords.PrimaryKey;

                                foreach (DataRow dtrUpdate in dtUpdateRecords.Rows)
                                {

                                    strwhere = "";
                                    foreach (DataColumn dtCol in dtUpdateRecords.PrimaryKey)
                                    {
                                        strwhere += ((strwhere.Length > 0) ? " and " : "") + dtCol.ColumnName.ToString() + " = '" + dtrUpdate[dtCol.ColumnName.ToString()] + "'";
                                    }

                                    DataRow[] _objTmp = dtUpdateRecords.Select(strwhere);
                                    if (_objTmp == null || _objTmp.Length == 0)
                                    {
                                        /*No Record found in local database (Access)*/
                                    }
                                    else
                                    {
                                        foreach (DataColumn item in dtrUpdate.Table.Columns)
                                        {
                                            _objTmp[0][item.ColumnName] = dtrUpdate[item.ColumnName];
                                        }

                                        DataRow _dr = dtUpdateRecords.NewRow();
                                        foreach (DataColumn _PkCols in dtUpdateRecords.PrimaryKey)
                                        {
                                            _dr[_PkCols.ColumnName.ToString()] = dtrUpdate[_PkCols.ColumnName.ToString()];
                                        }

                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                throw new Exception("class RI_syncUtility::Ut_sysUtility::UpdateAccessDatabase::Third Exception " + ex.ToString());
                            }
                            #endregion [ Third Execption ]

                            totalRowsAffected = dataAdapter.Update(dtUpdateRecords);

                            localDataSet.Dispose();
                            dataAdapter.Dispose();
                            commandBuilder.Dispose();

                            #endregion [ Data Adapter Logic ]

                            dtUpdateRecords.AcceptChanges();
                        }
                        else
                        {
                            executeQueryLogic = true;
                        }
                    }

                    if (executeQueryLogic == true)
                    {
                        if (plantAppVersion == "21" && isInsert == true)
                        {
                            #region [ Insert Query Logic]
                            string dateTimeFormat = ConfigurationSettings.AppSettings["dateTimeFormat"].ToString(); /*"dd/MM/yyyy hh:mm tt";*/
                            string dateFormat = ConfigurationSettings.AppSettings["dateFormat"].ToString(); /*"dd/MM/yyyy";*/
                            string insertQuery;
                            string blankParameter = "@blankParameter";
                            string doublQuote = string.Empty;
                            string fieldNames;

                            fieldNames = string.Empty;
                            commaValue = string.Empty;
                            columnValue = string.Empty;
                            columnName = string.Empty;
                            columnsCount = dtUpdateRecords.Columns.Count;
                            rowsCount = dtUpdateRecords.Rows.Count;
                            for (loopRowIndex = 0; loopRowIndex < rowsCount; loopRowIndex++)
                            {
                                commaValue = string.Empty;
                                loopColumnIndex = 0;
                                insertQuery = string.Empty;
                                fieldNames = string.Empty;

                                for (loopColumnIndex = 0; loopColumnIndex < columnsCount; loopColumnIndex++)
                                {
                                    columnValue = dtUpdateRecords.Rows[loopRowIndex][loopColumnIndex].ToString();
                                    columnName = dtUpdateRecords.Columns[loopColumnIndex].ColumnName;

                                    if (!string.IsNullOrEmpty(columnValue))
                                    {
                                        fieldNames += commaValue + "[" + columnName + "]";

                                        if (dtUpdateRecords.Columns[loopColumnIndex].DataType.Name == "String")
                                        {
                                            insertQuery += commaValue + "'" + columnValue + "'";
                                        }
                                        else if (dtUpdateRecords.Columns[loopColumnIndex].DataType.Name == "DateTime")
                                        {
                                            if (columnName.ToLower().Contains("time"))
                                            {
                                                insertQuery += commaValue + "#" + Convert.ToDateTime(columnValue).ToString(dateTimeFormat) + "#";
                                            }
                                            else
                                            {
                                                insertQuery += commaValue + "#" + Convert.ToDateTime(columnValue).ToString(dateFormat) + "#";
                                            }
                                        }
                                        else
                                        {
                                            insertQuery += commaValue + (string.IsNullOrEmpty(columnValue) ? "0" : columnValue);
                                        }
                                    }

                                    commaValue = Environment.NewLine + ",";
                                }

                                insertQuery = "INSERT INTO " + tableName + " ( " + fieldNames + ") VALUES(" + Environment.NewLine + insertQuery + ")";
                                insertQuery = insertQuery.Replace("''", blankParameter);
                                commaValue = string.Empty;
                                loopColumnIndex = 0;

                                rowsAffected = 0;
                                try
                                {
                                    returnQuery = insertQuery;

                                    using (OleDbCommand commandUpdate = new OleDbCommand(insertQuery, transaction.Connection, transaction))
                                    {
                                        commandUpdate.Parameters.Add(blankParameter, OleDbType.VarChar).Value = doublQuote;
                                        rowsAffected = commandUpdate.ExecuteNonQuery();
                                    }
                                }
                                catch (Exception ex)
                                {
                                    throw ex;
                                }

                                totalRowsAffected += rowsAffected;
                            }

                            #endregion [ Insert Query Logic]
                        }
                        else
                        {
                            #region [ Update Query Logic]
                            commaValue = string.Empty;
                            columnValue = string.Empty;
                            columnName = string.Empty;
                            columnsCount = dtUpdateRecords.Columns.Count;
                            rowsCount = dtUpdateRecords.Rows.Count;

                            tempTable = dtUpdateRecords.Clone();

                            for (loopRowIndex = 0; loopRowIndex < rowsCount; loopRowIndex++)
                            {
                                tempTable.ImportRow(dtUpdateRecords.Rows[loopRowIndex]);
                                strWhere = WhereString(tempTable);
                                tempTable.Rows.Clear();

                                updateQuery = "Update " + tableName + " SET ";
                                commaValue = string.Empty;
                                loopColumnIndex = 0;

                                for (loopColumnIndex = 0; loopColumnIndex < columnsCount; loopColumnIndex++)
                                {
                                    columnValue = dtUpdateRecords.Rows[loopRowIndex][loopColumnIndex].ToString();
                                    columnName = dtUpdateRecords.Columns[loopColumnIndex].ColumnName;
                                    if (primaryFields.Contains(columnName) == false)
                                    {
                                        if (dtUpdateRecords.Columns[loopColumnIndex].DataType.Name == "String")
                                        {
                                            if (string.IsNullOrEmpty(columnValue) == false)
                                            {
                                                updateQuery += commaValue + "[" + columnName + "] = '" + columnValue + "'";
                                            }
                                        }
                                        else if (dtUpdateRecords.Columns[loopColumnIndex].DataType.Name == "DateTime")
                                        {
                                            updateQuery += commaValue + "[" + columnName + "] = #" + columnValue + "#";
                                        }
                                        else
                                        {
                                            updateQuery += commaValue + "[" + columnName + "] = " + (string.IsNullOrEmpty(columnValue) ? "0" : columnValue);
                                        }

                                        commaValue = Environment.NewLine + ",";
                                    }
                                }

                                updateQuery = updateQuery + " " + strWhere;
                                returnQuery = updateQuery;
                                rowsAffected = 0;
                                using (OleDbCommand commandUpdate = new OleDbCommand(updateQuery, transaction.Connection, transaction))
                                {
                                    rowsAffected = commandUpdate.ExecuteNonQuery();
                                }

                                totalRowsAffected += rowsAffected;
                            }

                            #endregion [ Update Query Logic]
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("class RI_syncUtility::Ut_sysUtility::UpdateAccessDatabase " + ex.ToString());
            }

            returnQuery = string.Empty;
            return totalRowsAffected;
        }

        #endregion [ Desilting ]

        #region [ Write Log ]

        /// <summary>
        /// Write Log method
        /// </summary>
        /// <param name="eventName">Method name</param>
        /// <param name="exString">Error String</param>
        public void WriteLog(string eventName, string exString)
        {
            exString = "Event Name : " + eventName.Trim() + " - " + exString.Trim();
            LogEvent(exString);
        }

        /// <summary>
        /// Log Event method
        /// </summary>
        /// <param name="message">Message string</param>
        public void LogEvent(string message)
        {
            string infoFilePath = string.Empty;

            string directoryPath = Path.Combine(Application.StartupPath.ToString(), "LogInf");
            if (!Directory.Exists(directoryPath))
            {
                Directory.CreateDirectory(directoryPath);
            }

            infoFilePath = directoryPath + "\\Info_" + DateTime.Now.ToString("ddMMyyyy") + ".TXT";

            message = message.Replace("Name", "N");
            string sLog = Environment.NewLine + DateTime.Now.ToString("HH:mm:ss") + " " + message;

            if (File.Exists(infoFilePath))
            {
                File.AppendAllText(infoFilePath, sLog, Encoding.ASCII);
            }
            else
            {
                File.WriteAllText(infoFilePath, sLog, Encoding.ASCII);
            }
        }

        #endregion [ Write Log ]

        #region [ Private Methods ]

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dtInsertRecords">Table for Inserting the Records</param>
        /// <param name="plantAppVersion">Plant Version</param>
        /// <param name="transaction">Connection Transaction</param>
        /// <returns>No of Records affected</returns>
        private int InsertinAccessDatabase(DataTable dtInsertRecords, string plantAppVersion, OleDbTransaction transaction, out string returnQuery)
        {
            string tableName = string.Empty;
            int totalRowsAffected = 0;
            DataSet localDataSet = null;
            returnQuery = string.Empty;

            try
            {
                tableName = dtInsertRecords.TableName.ToString();

                int loopColumnIndex;
                int columnsCount = dtInsertRecords.Columns.Count;
                string fieldNames = string.Empty;
                string commaValue = string.Empty;
                string executionType;

                for (loopColumnIndex = 0; loopColumnIndex < columnsCount; loopColumnIndex++)
                {
                    fieldNames += commaValue + "[" + dtInsertRecords.Columns[loopColumnIndex].ColumnName + "]";
                    commaValue = Environment.NewLine + ",";
                }

                executionType = ConfigurationSettings.AppSettings["ExecutionType"].ToString().ToUpper();
                if ((plantAppVersion == "2" || plantAppVersion == "2.0") || executionType == "QUERY".ToUpper())
                {
                    #region [ Insert Query Logic]
                    string dateTimeFormat = ConfigurationSettings.AppSettings["dateTimeFormat"].ToString(); /*"dd/MM/yyyy hh:mm tt";*/
                    string dateFormat = ConfigurationSettings.AppSettings["dateFormat"].ToString(); /*"dd/MM/yyyy";*/
                    string insertQuery;
                    string columnValue;
                    string columnName;

                    int loopRowIndex;
                    int rowsCount;
                    int rowsAffected;
                    string blankParameter = "@blankParameter";
                    string doublQuote = string.Empty;
                    string strWhere = string.Empty;

                    commaValue = string.Empty;
                    columnValue = string.Empty;
                    columnName = string.Empty;
                    rowsCount = dtInsertRecords.Rows.Count;

                    DataTable tempTable;
                    tempTable = dtInsertRecords.Clone();

                    for (loopRowIndex = 0; loopRowIndex < rowsCount; loopRowIndex++)
                    {
                        tempTable.ImportRow(dtInsertRecords.Rows[loopRowIndex]);
                        strWhere = this.WhereString(tempTable);
                        tempTable.Rows.Clear();

                        commaValue = string.Empty;
                        loopColumnIndex = 0;
                        insertQuery = string.Empty;
                        fieldNames = string.Empty;

                        for (loopColumnIndex = 0; loopColumnIndex < columnsCount; loopColumnIndex++)
                        {
                            columnValue = dtInsertRecords.Rows[loopRowIndex][loopColumnIndex].ToString();
                            columnName = dtInsertRecords.Columns[loopColumnIndex].ColumnName;

                            if (!string.IsNullOrEmpty(columnValue))
                            {
                                fieldNames += commaValue + "[" + columnName + "]";

                                if (dtInsertRecords.Columns[loopColumnIndex].DataType.Name == "String")
                                {
                                    insertQuery += commaValue + "'" + columnValue + "'";
                                }
                                else if (dtInsertRecords.Columns[loopColumnIndex].DataType.Name == "DateTime")
                                {
                                    if (columnName.ToLower().Contains("time"))
                                    {
                                        insertQuery += commaValue + "#" + Convert.ToDateTime(columnValue).ToString(dateTimeFormat) + "#";
                                    }
                                    else
                                    {
                                        insertQuery += commaValue + "#" + Convert.ToDateTime(columnValue).ToString(dateFormat) + "#";
                                    }
                                }
                                else
                                {
                                    insertQuery += commaValue + (string.IsNullOrEmpty(columnValue) ? "0" : columnValue);
                                }
                            }

                            commaValue = Environment.NewLine + ",";
                        }

                        insertQuery = "INSERT INTO " + tableName + " ( " + fieldNames + ") VALUES(" + Environment.NewLine + insertQuery + ")";
                        insertQuery = insertQuery.Replace("''", blankParameter);
                        commaValue = string.Empty;
                        loopColumnIndex = 0;

                        rowsAffected = 0;
                        returnQuery = insertQuery;
                        try
                        {
                            using (OleDbCommand commandUpdate = new OleDbCommand(insertQuery, transaction.Connection, transaction))
                            {
                                commandUpdate.Parameters.Add(blankParameter, OleDbType.VarChar).Value = doublQuote;
                                rowsAffected = commandUpdate.ExecuteNonQuery();
                            }
                        }
                        catch (Exception ex)
                        {
                            throw ex;
                        }

                        totalRowsAffected += rowsAffected;
                    }

                    #endregion [ Insert Query Logic]
                }
                else
                {
                    #region [ DataAdpter Logic ]
                    OleDbCommand selectCommand = new OleDbCommand("Select TOP 1 " + fieldNames + " from " + tableName + " where 1=2 ", transaction.Connection, transaction);
                    using (OleDbDataAdapter oleDataAdapter = new OleDbDataAdapter(selectCommand))
                    {
                        using (OleDbCommandBuilder commandBuilder = new OleDbCommandBuilder(oleDataAdapter))
                        {
                            oleDataAdapter.ContinueUpdateOnError = false;
                            localDataSet = new DataSet("TempDataSet");
                            oleDataAdapter.Fill(localDataSet, tableName);

                            foreach (DataRow _objRow in dtInsertRecords.Rows)
                            {
                                _objRow.SetAdded();
                                localDataSet.Tables[tableName].ImportRow(_objRow);
                            }

                            totalRowsAffected = oleDataAdapter.Update(localDataSet, tableName);
                        }
                    }
                    #endregion [ DataAdpter Logic ]
                }
            }
            catch (Exception ex)
            {
                throw new Exception("class RI_syncUtility::Ut_sysUtility::InsertinAccessDatabase " + ex.ToString());
            }
            finally
            {
                if (localDataSet != null)
                {
                    localDataSet.Dispose();
                }

                localDataSet = null;
            }

            returnQuery = string.Empty;
            return totalRowsAffected;
        }

        /// <summary>
        /// is Record exists in the table name
        /// </summary>
        /// <param name="tableName">table name</param>
        /// <param name="strWhere">where condition with prefix where</param>
        /// <param name="oleDbConnection">in which data base you want to check</param>
        /// <returns>true for exists , false for not</returns>
        internal bool IsRecordExists(string tableName, string strWhere, OleDbConnection oleDbConnection)
        {
            bool returnData = false;
            DataTable returnDataTable = null;
            string sqlQuery = " SELECT COUNT(1) AS RCOUNT FROM " + tableName + strWhere;

            returnDataTable = this.FillDatatableOLE(sqlQuery, oleDbConnection);
            if (returnDataTable != null)
            {
                if (returnDataTable.Rows.Count > 0)
                {
                    if (ConvertToInt32(returnDataTable.Rows[0]["RCOUNT"].ToString()) > 0)
                    {
                        returnData = true;
                    }
                }
            }

            return returnData;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dtInsertRecords">Table for Inserting the Records</param>
        /// <param name="plantAppVersion">Plant Version</param>
        /// <param name="transaction">Connection Transaction</param>
        /// <returns>No of Records affected</returns>
        private int InsertinAccessDatabaseFromCSV(DataTable dtInsertRecords, string plantAppVersion, OleDbTransaction transaction)
        {
            string tableName = string.Empty;
            int totalRowsAffected = 0;
            DataSet localDataSet = new DataSet("TempDataSet");
            DataTable dtMainTarget;
            try
            {
                tableName = dtInsertRecords.TableName.ToString();

                OleDbCommand selectCommand1 = new OleDbCommand("Select * from " + tableName + " WHERE 1=2", transaction.Connection, transaction);
                OleDbDataAdapter dataAdapter = new OleDbDataAdapter(selectCommand1);
                dataAdapter.Fill(localDataSet, tableName);
                dtMainTarget = localDataSet.Tables[0];

                int loopColumnIndex;
                int columnsCount = dtInsertRecords.Columns.Count;
                string fieldNames = string.Empty;
                string commaValue = string.Empty;
                string executionType;
                string plantName;

                for (loopColumnIndex = 0; loopColumnIndex < columnsCount; loopColumnIndex++)
                {
                    fieldNames += commaValue + "[" + dtInsertRecords.Columns[loopColumnIndex].ColumnName + "]";
                    commaValue = Environment.NewLine + ",";
                }

                executionType = ConfigurationSettings.AppSettings["ExecutionType"].ToString().ToUpper();
                plantName = ConfigurationSettings.AppSettings["PlantName"].ToString().ToUpper();
                if ((plantAppVersion == "2" || plantAppVersion == "2.0") || executionType == "QUERY".ToUpper())
                {
                    #region [ Insert Query Logic]
                    string dateTimeFormat = ConfigurationSettings.AppSettings["dateTimeFormat"].ToString(); /*"dd/MM/yyyy hh:mm tt";*/
                    string dateFormat = ConfigurationSettings.AppSettings["dateFormat"].ToString(); /*"dd/MM/yyyy";*/
                    string insertQuery;
                    string columnValue;
                    string columnName;
                    int loopRowIndex;
                    int rowsCount;
                    int rowsAffected;
                    string blankParameter = "@blankParameter";
                    int batch_Rowid = 0;
                    string doublQuote = string.Empty;

                    commaValue = string.Empty;
                    columnValue = string.Empty;
                    columnName = string.Empty;
                    rowsCount = dtInsertRecords.Rows.Count;
                    for (loopRowIndex = 0; loopRowIndex < rowsCount; loopRowIndex++)
                    {
                        commaValue = string.Empty;
                        loopColumnIndex = 0;
                        insertQuery = string.Empty;
                        fieldNames = string.Empty;
                        batch_Rowid = 0;

                        for (loopColumnIndex = 0; loopColumnIndex < columnsCount; loopColumnIndex++)
                        {
                            columnValue = dtInsertRecords.Rows[loopRowIndex][loopColumnIndex].ToString();
                            columnName = dtInsertRecords.Columns[loopColumnIndex].ColumnName;

                            if (columnName == "BATCH_ROWID")
                            {
                                batch_Rowid = ConvertToInt32(columnValue);
                            }
                            else if (columnName == "DELETED_REC_FLAG")
                            {
                                columnValue = this.GetDeleted_Rec_Flag(dtInsertRecords.Rows[loopRowIndex][loopColumnIndex].ToString());
                            }

                            if (!string.IsNullOrEmpty(columnValue))
                            {
                                fieldNames += commaValue + "[" + columnName + "]";
                                if (dtMainTarget.Columns[columnName].DataType.Name == "DateTime"
                                    || (plantName == "VITRAG" && columnName.ToUpper() == "Time_Cic".ToUpper()))
                                {
                                    if (columnName.ToLower() == "batch_date")
                                    {
                                        if (columnValue.Length == 7)
                                        {
                                            columnValue = "0" + columnValue;
                                        }

                                        string temp_ColumnValue = string.Empty;
                                        if (columnValue.Length >= 8)
                                        {
                                            temp_ColumnValue = columnValue.Substring(2, 2) + "/" + columnValue.Substring(0, 2) + "/" + columnValue.Substring(4, 4);
                                        }

                                        try
                                        {
                                            columnValue = DateTime.Parse(temp_ColumnValue).ToString(dateFormat);
                                        }
                                        catch (Exception)
                                        {
                                            columnValue = DateTime.Parse(columnValue).ToString(dateFormat);
                                        }

                                        insertQuery += commaValue + "#" + columnValue + "#";
                                    }
                                    else
                                    {
                                        if (columnValue.Length <= 6)
                                        {
                                            columnValue = "01/01/1900 " + columnValue.Substring(0, 2) + ":" + columnValue.Substring(2, 2) + ":" + columnValue.Substring(4, 2);
                                        }

                                        insertQuery += commaValue + "#" + Convert.ToDateTime(columnValue).ToString(dateTimeFormat) + "#";
                                    }

                                }
                                else if (dtMainTarget.Columns[columnName].DataType.Name == "String")
                                {
                                    insertQuery += commaValue + "'" + columnValue + "'";
                                }
                                else
                                {
                                    insertQuery += commaValue + (string.IsNullOrEmpty(columnValue) ? "0" : columnValue);
                                }
                            }

                            commaValue = Environment.NewLine + ",";
                        }

                        insertQuery = "INSERT INTO " + tableName + " ( " + fieldNames + ") VALUES(" + Environment.NewLine + insertQuery + ")";
                        insertQuery = insertQuery.Replace("''", blankParameter);
                        commaValue = string.Empty;
                        loopColumnIndex = 0;

                        try
                        {
                            rowsAffected = 0;
                            using (OleDbCommand commandUpdate = new OleDbCommand(insertQuery, transaction.Connection, transaction))
                            {
                                commandUpdate.Parameters.Add(blankParameter, OleDbType.VarChar).Value = doublQuote;
                                rowsAffected = commandUpdate.ExecuteNonQuery();
                            }


                        }
                        catch (Exception ex)
                        {
                            throw new Exception("class RI_syncUtility::Ut_sysUtility::InsertinAccessDatabase 1 " + ex.ToString());
                        }

                        totalRowsAffected += rowsAffected;
                    }

                    #endregion [ Insert Query Logic]
                }
                else
                {
                    #region [ DataAdpter Logic ]
                    OleDbCommand selectCommand = new OleDbCommand("Select TOP 1 " + fieldNames + " from " + tableName + " where 1=2 ", transaction.Connection, transaction);
                    using (OleDbDataAdapter oleDataAdapter = new OleDbDataAdapter(selectCommand))
                    {
                        using (OleDbCommandBuilder commandBuilder = new OleDbCommandBuilder(oleDataAdapter))
                        {
                            oleDataAdapter.ContinueUpdateOnError = false;
                            localDataSet = new DataSet("TempDataSet");
                            oleDataAdapter.Fill(localDataSet, tableName);

                            foreach (DataRow _objRow in dtInsertRecords.Rows)
                            {
                                _objRow.SetAdded();
                                localDataSet.Tables[tableName].ImportRow(_objRow);
                            }

                            totalRowsAffected = oleDataAdapter.Update(localDataSet, tableName);
                        }
                    }
                    #endregion [ DataAdpter Logic ]
                }
            }
            catch (Exception ex)
            {
                throw new Exception("class RI_syncUtility::Ut_sysUtility::InsertinAccessDatabase 2 " + ex.ToString());
            }
            finally
            {
                if (localDataSet != null)
                {
                    localDataSet.Dispose();
                }

                localDataSet = null;
            }

            return totalRowsAffected;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        private string GetDeleted_Rec_Flag(string value)
        {
            string returnData = string.Empty;
            value = value.Trim().ToLower();

            switch (value)
            {
                case "":
                case "no":
                case "n":
                case "0":
                case "false":
                    returnData = "NO";
                    break;
                default:
                    returnData = "YES";
                    break;
            }

            return returnData;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="selectQuery"></param>
        /// <param name="connection"></param>
        /// <returns>returns Datatable</returns>
        private DataTable FillDatatableOLE(string selectQuery, OleDbConnection connection)
        {
            try
            {
                if (connection.State != ConnectionState.Open)
                {
                    connection.Open();
                }

                DataTable returnData = new DataTable();
                using (OleDbDataAdapter adapter = new OleDbDataAdapter())
                {
                    adapter.SelectCommand = new OleDbCommand(selectQuery, connection);
                    adapter.SelectCommand.CommandType = CommandType.Text;
                    adapter.Fill(returnData);
                }

                return returnData;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        /// <summary>
        /// UpdateinAccessDatabase Method
        /// </summary>
        /// <param name="dtUpdateRecords">Table for Updating the Records</param>
        /// <param name="plantAppVersion">Plant Version</param>
        /// <param name="transaction">Connection Transaction</param>
        /// <returns>No of Records affected</returns>
        private int UpdateinAccessDatabase(DataTable dtUpdateRecords, string plantAppVersion, OleDbTransaction transaction, out string returnQuery)
        {
            int totalRowsAffected = 0;
            DataSet localDataSet = null;
            string strWhere = string.Empty;
            string updateQuery = string.Empty;
            returnQuery = string.Empty;

            try
            {

                string tableName;
                string executionType;
                tableName = dtUpdateRecords.TableName.ToString();
                strWhere = WhereString(dtUpdateRecords);
                executionType = ConfigurationSettings.AppSettings["ExecutionType"].ToString().ToUpper();
                if ((plantAppVersion == "2" || plantAppVersion == "2.0") || executionType == "QUERY".ToUpper())
                {
                    int rowsAffected;
                    if (executionType == "QUERY".ToUpper())
                    {
                        #region [ Update Query Logic]
                        string commaValue;
                        string columnValue;
                        string columnName;

                        int loopColumnIndex;
                        int loopRowIndex;
                        int columnsCount;
                        int rowsCount;
                        string blankParameter = "@blankParameter";
                        char myDoubleQuote = (char)34;
                        string doublQuote = myDoubleQuote.ToString() + myDoubleQuote.ToString();

                        commaValue = string.Empty;
                        columnValue = string.Empty;
                        columnName = string.Empty;

                        columnsCount = dtUpdateRecords.Columns.Count;
                        rowsCount = dtUpdateRecords.Rows.Count;

                        DataTable tempTable;
                        tempTable = dtUpdateRecords.Clone();
                        bool blncheckPrimaryKey = false;

                        for (loopRowIndex = 0; loopRowIndex < rowsCount; loopRowIndex++)
                        {
                            tempTable.ImportRow(dtUpdateRecords.Rows[loopRowIndex]);
                            strWhere = this.WhereString(tempTable);
                            tempTable.Rows.Clear();

                            updateQuery = "Update " + tableName + " SET ";
                            commaValue = string.Empty;
                            loopColumnIndex = 0;

                            for (loopColumnIndex = 0; loopColumnIndex < columnsCount; loopColumnIndex++)
                            {
                                blncheckPrimaryKey = false;
                                columnName = dtUpdateRecords.Columns[loopColumnIndex].ColumnName;
                                foreach (DataColumn dtcol in tempTable.PrimaryKey)
                                {
                                    if (columnName == dtcol.ColumnName)
                                    {
                                        blncheckPrimaryKey = true;
                                    }
                                }

                                if (!blncheckPrimaryKey)
                                {
                                    columnValue = dtUpdateRecords.Rows[loopRowIndex][loopColumnIndex].ToString();

                                    if (!string.IsNullOrEmpty(columnValue))
                                    {
                                        if (dtUpdateRecords.Columns[loopColumnIndex].DataType.Name == "String")
                                        {
                                            updateQuery += commaValue + "[" + columnName + "] = '" + columnValue + "'";
                                        }
                                        else if (dtUpdateRecords.Columns[loopColumnIndex].DataType.Name == "DateTime")
                                        {
                                            updateQuery += commaValue + "[" + columnName + "] = #" + columnValue + "#";
                                        }
                                        else
                                        {
                                            updateQuery += commaValue + "[" + columnName + "] = " + (string.IsNullOrEmpty(columnValue) ? "0" : columnValue);
                                        }

                                        commaValue = Environment.NewLine + ",";
                                    }
                                }
                            }

                            updateQuery = updateQuery + " " + strWhere;
                            updateQuery = updateQuery.Replace("''", blankParameter);

                            rowsAffected = 0;
                            using (OleDbCommand commandUpdate = new OleDbCommand(updateQuery, transaction.Connection, transaction))
                            {
                                commandUpdate.Parameters.Add(blankParameter, OleDbType.VarChar).Value = doublQuote;
                                rowsAffected = commandUpdate.ExecuteNonQuery();
                            }

                            totalRowsAffected += rowsAffected;
                        }

                        #endregion [ Update Query Logic]
                    }
                    else
                    {
                        using (OleDbCommand commandUpdate = new OleDbCommand("DELETE FROM " + tableName + strWhere, transaction.Connection, transaction))
                        {
                            rowsAffected = commandUpdate.ExecuteNonQuery();
                        }

                        rowsAffected = this.InsertinAccessDatabase(dtUpdateRecords, plantAppVersion, transaction, out returnQuery);
                        totalRowsAffected = rowsAffected;
                    }
                }
                else
                {
                    #region [ DataAdapter Logic ]
                    localDataSet = new DataSet("TempDataSet");
                    totalRowsAffected = 0;
                    {
                        OleDbCommand selectCommand = new OleDbCommand("Select * from " + tableName + strWhere, transaction.Connection, transaction);
                        OleDbDataAdapter oleDataAdapter = new OleDbDataAdapter(selectCommand);
                        {
                            OleDbCommandBuilder commandBuilder = new OleDbCommandBuilder(oleDataAdapter);
                            {
                                oleDataAdapter.ContinueUpdateOnError = false;
                                oleDataAdapter.Fill(localDataSet, tableName);
                                StringBuilder sb = new StringBuilder();
                                sb.Append("Update " + localDataSet.Tables[0].TableName + " SET ");

                                string commaValue = string.Empty;

                                foreach (DataColumn item in localDataSet.Tables[tableName].Columns)
                                {
                                    sb.Append(commaValue + item.Caption + "=@" + item.Caption);
                                    commaValue = ",";
                                }


                                string strwhere = string.Empty;
                                foreach (DataColumn dtCol in dtUpdateRecords.PrimaryKey)
                                {
                                    strwhere = strwhere + ((strwhere.Length > 0) ? " and " : "") + dtCol.ColumnName.ToString() + " = @" + dtCol.ColumnName.ToString();
                                }


                                if (strwhere.Length > 0)
                                {
                                    sb.Append(" WHERE " + strwhere);
                                }

                                OleDbCommand DAUpdateCmd = new OleDbCommand(sb.ToString(), oleDataAdapter.SelectCommand.Connection, transaction);
                                {
                                    foreach (DataColumn item in localDataSet.Tables[tableName].Columns)
                                    {
                                        OleDbParameter Param = new OleDbParameter();
                                        Param.ParameterName = "@" + item.Caption;
                                        Param.SourceColumn = item.Caption;
                                        bool blnprimerykey = false;
                                        foreach (DataColumn dtcol in dtUpdateRecords.PrimaryKey)
                                        {
                                            if (item.ColumnName.ToString() == dtcol.ColumnName.ToString())
                                            {
                                                blnprimerykey = true;
                                                break;
                                            }
                                        }

                                        if (blnprimerykey)   /* ** COMPARE WITH PRIMARY KEY COLUMNS*/
                                        {
                                            Param.SourceVersion = DataRowVersion.Original;
                                        }
                                        else
                                        {
                                            Param.SourceVersion = DataRowVersion.Current;
                                        }

                                        DAUpdateCmd.Parameters.Add(Param);
                                    }
                                    oleDataAdapter.UpdateCommand = DAUpdateCmd;
                                }

                                foreach (DataRow dtrow in dtUpdateRecords.Rows)
                                {
                                    strwhere = "";
                                    foreach (DataColumn dtCol in dtUpdateRecords.PrimaryKey)
                                    {
                                        strwhere += ((strwhere.Length > 0) ? " and " : "") + dtCol.ColumnName.ToString() + " = '" + dtrow[dtCol.ColumnName.ToString()] + "'";
                                    }

                                    DataRow[] _objTmp = localDataSet.Tables[tableName].Select(strwhere);
                                    _objTmp[0].ItemArray = dtrow.ItemArray;
                                }

                                totalRowsAffected = oleDataAdapter.Update(localDataSet, tableName);
                            }
                        }
                    }
                    #endregion [ DataAdapter Logic ]
                }
            }
            catch (Exception ex)
            {

                throw new Exception("class RI_syncUtility::Ut_sysUtility::UpdateinAccessDatabase : " + ex.ToString() + " " + updateQuery);
            }

            return totalRowsAffected;
        }

        /// <summary>
        /// WhereString Method
        /// </summary>
        /// <param name="dtWhere">Data table for Generating Where Condition</param>
        /// <returns>returns string value</returns>
        private string WhereString(DataTable dtWhere)
        {
            string outFields;
            return WhereString(dtWhere, out outFields);
        }

        /// <summary>
        /// WhereString Method
        /// </summary>
        /// <param name="dtWhere">Data table for Generating Where Condition</param>
        /// <param name="outPrimaryFields">Primary Fields</param>
        /// <returns>returns string value</returns>
        internal string WhereString(DataTable dtWhere, out string outPrimaryFields)
        {
            string strwhere = string.Empty;
            string[] arr;
            string str;
            outPrimaryFields = string.Empty;
            string commaValue = string.Empty;
            string dateTimeFormat = ConfigurationSettings.AppSettings["dateTimeFormat"].ToString(); /*"dd/MM/yyyy hh:mm tt";*/
            string dateFormat = ConfigurationSettings.AppSettings["dateFormat"].ToString(); /*"dd/MM/yyyy";*/

            foreach (DataColumn dtcol in dtWhere.PrimaryKey)
            {
                var names = (from a in dtWhere.AsEnumerable()
                             select (string)a[dtcol.ColumnName.ToString()].ToString()).Distinct().ToArray();

                if (names != null && names.Count() > 0)
                {
                    arr = names.ToArray();
                    if (dtcol.DataType.Name.ToLower() == "string")
                    {
                        str = string.Join("','", arr);
                        strwhere += ((strwhere.Length > 0) ? " and " : "") + dtcol.ColumnName.ToString() + " in ('" + str + "') ";
                    }
                    else if (dtcol.DataType.Name.ToLower() == "datetime")
                    {
                        string columnValue = "";
                        for (int i = 0; i < arr.Length; i++)
                        {
                            if (dtcol.ColumnName.ToLower().Contains("time"))
                            {
                                columnValue += (columnValue.Length > 0 ? "#,#" : "") + Convert.ToDateTime(arr[0].ToString()).ToString(dateTimeFormat);
                            }
                            else
                            {
                                columnValue += (columnValue.Length > 0 ? "#,#" : "") + Convert.ToDateTime(arr[0].ToString()).ToString(dateFormat);
                            }
                        }

                        //strwhere = strwhere + ((strwhere.Length > 0) ? " and " : " ") + dtcol.ColumnName.ToString() + " in ('" + columnValue + "') ";
                        strwhere += ((strwhere.Length > 0) ? " and " : "") + dtcol.ColumnName.ToString() + " in (#" + columnValue + "#) ";

                        //str = string.Join("#,#", arr);


                        //if (dtcol.ColumnName.ToLower().Contains("time"))
                        //{
                        //    strwhere += ((strwhere.Length > 0) ? " and " : "") + dtcol.ColumnName.ToString() + " in (#" + Convert.ToDateTime(str).ToString(dateTimeFormat) + "#) ";
                        //}
                        //else
                        //{
                        //    strwhere += ((strwhere.Length > 0) ? " and " : "") + dtcol.ColumnName.ToString() + " in (#" + Convert.ToDateTime(str).ToString(dateFormat) + "#) ";
                        //}

                    }
                    else
                    {
                        str = string.Join(", ", arr);
                        strwhere += ((strwhere.Length > 0) ? " and " : "") + dtcol.ColumnName.ToString() + " in (" + str + ") ";
                    }
                }

                outPrimaryFields += commaValue + dtcol.ColumnName.ToString();

                commaValue = ",";
            }

            strwhere = ((strwhere.Length > 0) ? " where " : "") + strwhere;
            return strwhere;
        }

        /// <summary>
        /// WhereString Method
        /// </summary>
        /// <param name="dtWhere">Data table for Generating Where Condition</param>
        /// <param name="outPrimaryFields">Primary Fields</param>
        /// <returns>returns string value</returns>
        internal string WhereString(DataTable dtWhere, int row_index, out string outPrimaryFields)
        {
            string strwhere = string.Empty;
            string[] arr;
            string str;
            outPrimaryFields = string.Empty;
            string commaValue = string.Empty;
            string dateTimeFormat = ConfigurationSettings.AppSettings["dateTimeFormat"].ToString(); /*"dd/MM/yyyy hh:mm tt";*/
            string dateFormat = ConfigurationSettings.AppSettings["dateFormat"].ToString(); /*"dd/MM/yyyy";*/

            foreach (DataColumn dtcol in dtWhere.PrimaryKey)
            {
                var names = (from a in dtWhere.AsEnumerable()
                             select (string)a[dtcol.ColumnName.ToString()].ToString()).Distinct().ToArray();

                if (names != null && names.Count() > 0)
                {
                    arr = names.ToArray();
                    for (int arrayIndex = 0; arrayIndex < arr.Length; arrayIndex++)
                    {
                        if (dtWhere.Rows[row_index][dtcol.ColumnName].ToString() == arr[arrayIndex])
                        {
                        }
                        else
                        {
                            arr[arrayIndex] = "-99999";
                        }
                    }

                    if (dtcol.DataType.Name.ToLower() == "string")
                    {
                        str = string.Join("','", arr);
                        strwhere += ((strwhere.Length > 0) ? " and " : "") + dtcol.ColumnName.ToString() + " in ('" + str + "') ";
                    }
                    else if (dtcol.DataType.Name.ToLower() == "datetime")
                    {
                        str = string.Join("#,#", arr);

                        if (dtcol.ColumnName.ToLower().Contains("time"))
                        {
                            strwhere += ((strwhere.Length > 0) ? " and " : "") + dtcol.ColumnName.ToString() + " in (#" + Convert.ToDateTime(str).ToString(dateTimeFormat) + "#) ";
                        }
                        else
                        {
                            strwhere += ((strwhere.Length > 0) ? " and " : "") + dtcol.ColumnName.ToString() + " in (#" + Convert.ToDateTime(str).ToString(dateFormat) + "#) ";
                        }
                    }
                    else
                    {
                        str = string.Join(", ", arr);
                        strwhere += ((strwhere.Length > 0) ? " and " : "") + dtcol.ColumnName.ToString() + " in (" + str + ") ";
                    }
                }

                outPrimaryFields += commaValue + dtcol.ColumnName.ToString();

                commaValue = ",";
            }

            strwhere = ((strwhere.Length > 0) ? " where " : "") + strwhere;
            return strwhere;
        }

        /// <summary>
        /// RemoveUnwantedFields methods
        /// </summary>
        /// <param name="dtRemoveFields">Table for Remove the fields</param>
        /// <param name="transaction">Connection Transaction</param>
        /// <returns>returns Datatable</returns>
        public DataTable RemoveUnwantedFields(DataTable dtRemoveFields, string connectionString, bool removeRowID_Column)
        {
            try
            {
                using (OleDbConnection connection = new OleDbConnection(connectionString))
                {
                    connection.Open();
                    string tableName = dtRemoveFields.TableName;
                    using (DataSet accessDataSet = new DataSet("TempDataSet"))
                    {
                        using (OleDbCommand selectCommand = new OleDbCommand("Select * from " + tableName + " WHERE 1 = 2", connection))
                        {
                            using (OleDbDataAdapter dataAdapter = new OleDbDataAdapter(selectCommand))
                            {
                                OleDbCommandBuilder commandBuilder = new OleDbCommandBuilder(dataAdapter);
                                dataAdapter.ContinueUpdateOnError = true;
                                dataAdapter.Fill(accessDataSet, tableName);
                            }

                            dtRemoveFields = this.RemoveUnwantedFields(dtRemoveFields, accessDataSet.Tables[tableName], removeRowID_Column);
                            dtRemoveFields.AcceptChanges();
                        }
                    }
                }
            }
            catch (Exception)
            {
            }

            return dtRemoveFields;
        }

        /// <summary>
        /// RemoveUnwantedFields methods
        /// </summary>
        /// <param name="dtRemoveFields">Table for Remove the fields</param>
        /// <param name="accessDataTable">exist Access database Table</param>
        /// <returns>returns Datatable</returns>
        private DataTable RemoveUnwantedFields(DataTable dtRemoveFields, DataTable accessDataTable, bool removeRowID_Column)
        {
            try
            {
                string columnName = string.Empty;
                string[] columnArray = new string[dtRemoveFields.Columns.Count];

                for (int i = 0; i < dtRemoveFields.Columns.Count; i++)
                {
                    columnName = dtRemoveFields.Columns[i].ColumnName;
                    if (!accessDataTable.Columns.Contains(columnName))
                    {
                        if (removeRowID_Column == false && (columnName.ToLower() == "row_id" || columnName.ToLower() == "new_id"))
                        { }
                        else
                        {
                            columnArray[i] = columnName;
                        }
                    }
                }

                foreach (string item in columnArray)
                {
                    if (!string.IsNullOrEmpty(item))
                    {
                        dtRemoveFields.Columns.Remove(item);
                    }
                }

            }
            catch (Exception ex)
            {
                throw new Exception("class RI_syncUtility::Ut_sysUtility::UpdateAccessDatabase::RemoveUnwantedFields " + ex.ToString());
            }

            return dtRemoveFields;
        }

        /// <summary>
        /// RemoveUnwantedFields methods
        /// </summary>
        /// <param name="dtRemoveFields">Table for Remove the fields</param>
        /// <param name="transaction">Connection Transaction</param>
        /// <returns>returns Datatable</returns>
        public DataTable RemoveUnwantedFields(DataTable dtRemoveFields, string connectionString)
        {
            try
            {
                using (OleDbConnection connection = new OleDbConnection(connectionString))
                {
                    connection.Open();
                    string tableName = dtRemoveFields.TableName;
                    using (DataSet accessDataSet = new DataSet("TempDataSet"))
                    {
                        using (OleDbCommand selectCommand = new OleDbCommand("Select * from " + tableName + " WHERE 1 = 2", connection))
                        {
                            using (OleDbDataAdapter dataAdapter = new OleDbDataAdapter(selectCommand))
                            {
                                OleDbCommandBuilder commandBuilder = new OleDbCommandBuilder(dataAdapter);
                                dataAdapter.ContinueUpdateOnError = true;
                                dataAdapter.Fill(accessDataSet, tableName);
                            }

                            dtRemoveFields = this.RemoveUnwantedFields(dtRemoveFields, accessDataSet.Tables[tableName]);
                            dtRemoveFields.AcceptChanges();
                        }
                    }
                }
            }
            catch (Exception)
            {
            }

            return dtRemoveFields;
        }

        /// <summary>
        /// RemoveUnwantedFields methods
        /// </summary>
        /// <param name="dtRemoveFields">Table for Remove the fields</param>
        /// <param name="accessDataTable">exist Access database Table</param>
        /// <returns>returns Datatable</returns>
        private DataTable RemoveUnwantedFields(DataTable dtRemoveFields, DataTable accessDataTable)
        {
            try
            {
                string columnName = string.Empty;
                string[] columnArray = new string[dtRemoveFields.Columns.Count];

                for (int i = 0; i < dtRemoveFields.Columns.Count; i++)
                {
                    columnName = dtRemoveFields.Columns[i].ColumnName;
                    if (!accessDataTable.Columns.Contains(columnName))
                    {
                        columnArray[i] = columnName;
                    }
                }

                foreach (string item in columnArray)
                {
                    if (!string.IsNullOrEmpty(item))
                    {
                        dtRemoveFields.Columns.Remove(item);
                    }
                }

            }
            catch (Exception ex)
            {
                throw new Exception("class RI_syncUtility::Ut_sysUtility::UpdateAccessDatabase::RemoveUnwantedFields " + ex.ToString());
            }

            return dtRemoveFields;
        }

        /// <summary>
        /// Create query method
        /// </summary>
        /// <param name="TableName">Table name</param>
        /// <param name="transaction">Connection Transaction</param>
        /// <param name="dtTbList">Table List</param>
        /// <returns>returns string value</returns>
        private string Createquery(string TableName, OleDbTransaction transaction, DataTable dtTbList)
        {
            string strquery = string.Empty;
            try
            {
                string result = string.Empty;
                bool blnAutoNumber = false;
                string[] restrictions = new string[4];
                DataTable tableColumnList;
                restrictions[2] = TableName;
                tableColumnList = transaction.Connection.GetOleDbSchemaTable(OleDbSchemaGuid.Columns, restrictions);
                DataRow[] drprimarycolumnlist = dtTbList.Select("XmlRelation ='" + TableName.ToString() + "' and XmlName='Key'");

                if (drprimarycolumnlist.Count() > 0)
                {
                    string[] strXml = new string[drprimarycolumnlist.Count()];

                    for (int coutnrow = 0; coutnrow < drprimarycolumnlist.Count(); coutnrow++)
                    {
                        strXml[coutnrow] = drprimarycolumnlist[coutnrow]["XmlKey"].ToString();
                    }

                    foreach (string strColNmae in strXml)
                    {
                        foreach (DataRow dtrow in tableColumnList.Select("COLUMN_NAME = '" + strColNmae.ToString() + "'"))
                        {
                            if (dtrow["DATA_TYPE"].ToString() == "3" && dtrow["COLUMN_FLAGS"].ToString() == "90")
                            {
                                blnAutoNumber = true;
                            }
                        }
                    }

                    if (blnAutoNumber)
                    {
                        result = string.Empty;
                        for (int loopColumnIndex = 0; loopColumnIndex < tableColumnList.Rows.Count; loopColumnIndex++)
                        {
                            foreach (string strColNmae in strXml)
                            {
                                if (strColNmae != tableColumnList.Rows[loopColumnIndex]["COLUMN_NAME"].ToString())
                                {
                                    result += (result.Length > 0 ? "," : "") + tableColumnList.Rows[loopColumnIndex]["COLUMN_NAME"].ToString() + " = "
                                        + (tableColumnList.Rows[loopColumnIndex]["Data_type"].ToString() == "7" ? "#1900/01/01#" : "0");
                                    /*DBNull.Value.ToString();*/
                                }
                            }
                        }

                        strquery = " update " + TableName.ToString() + " set " + result;
                    }
                    else
                    {
                        strquery = " Delete from " + TableName.ToString();
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
            return strquery;
        }

        /// <summary>
        /// Get Config Value method
        /// </summary>
        /// <param name="value">Config Key value</param>
        /// <returns>returns string value</returns>
        private string GetConfigValue(string value)
        {
            return ConfigurationSettings.AppSettings[value].ToString();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        private int ConvertToInt32(object value)
        {
            int returnData = 0;
            if (int.TryParse(value.ToString(), out returnData) == false)
            {
                returnData = 0;
            }
            return returnData;
        }

        #endregion [ Private Methods ]

        #region [ Dispose Method ]

        // Pointer to an external unmanaged resource. 
        private IntPtr handle;

        // Other managed resource this class uses. 
        private Component component = new Component();

        // Track whether Dispose has been called. 
        private bool disposed = false;

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Dispose(bool disposing) executes in two distinct scenarios. 
        /// If disposing equals true, the method has been called directly 
        /// or indirectly by a user's code. Managed and unmanaged resources 
        /// can be disposed. 
        /// If disposing equals false, the method has been called by the 
        /// runtime from inside the finalizer and you should not reference 
        /// other objects. Only unmanaged resources can be disposed. 
        /// </summary>
        /// <param name="disposing"></param>
        protected virtual void Dispose(bool disposing)
        {
            // Check to see if Dispose has already been called. 
            if (!this.disposed)
            {
                // If disposing equals true, dispose all managed 
                // and unmanaged resources. 
                if (disposing)
                {
                    // Dispose managed resources.
                    component.Dispose();
                }

                // Call the appropriate methods to clean up 
                // unmanaged resources here. 
                // If disposing is false, 
                // only the following code is executed.
                CloseHandle(handle);
                handle = IntPtr.Zero;

                // Note disposing has been done.
                disposed = true;

            }
        }

        /// <summary>
        /// Use interop to call the method necessary 
        /// to clean up the unmanaged resource.
        /// </summary>
        /// <param name="handle"></param>
        /// <returns></returns>
        [System.Runtime.InteropServices.DllImport("Kernel32")]
        private extern static Boolean CloseHandle(IntPtr handle);

        /// <summary>
        /// Use C# destructor syntax for finalization code. 
        /// This destructor will run only if the Dispose method 
        /// does not get called. 
        /// It gives your base class the opportunity to finalize. 
        /// Do not provide destructors in types derived from this class.
        /// </summary>
        ~Ut_sysUtility()
        {
            // Do not re-create Dispose clean-up code here. 
            // Calling Dispose(false) is optimal in terms of 
            // readability and maintainability.
            Dispose(false);
        }

        #endregion [ Dispose Method ]
    }
}