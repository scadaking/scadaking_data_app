﻿


namespace PNF
{
    #region [ Using's ]
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Configuration;
    using System.Data;
    using System.Data.OleDb;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using System.ServiceModel;
    using System.Text;
    using System.Xml;
    using System.Windows.Forms;
    using PNF.Global;
    #endregion [ Using's ]

    #region [ Class ]
    /// <summary>
    /// CompareDatabase class
    /// </summary>
    internal class CompareDatabase : IDisposable
    {
        /// <summary>
        /// To Sync Table Details
        /// </summary>
        DataTable xmlSync_Tables_Details;

        /// <summary>
        /// CSV Table and column Details
        /// </summary>
        DataTable xmlCSV_Tables_Details;

        /// <summary>
        /// Access Table List
        /// </summary>
        DataTable dtTablesList = null;

        /// <summary>
        /// Compare Access Database
        /// </summary>
        /// <param name="sourceConnectionString"></param>
        /// <param name="targetConnectionString"></param>
        internal void CompareAccessDatabase(
            string plc_SourceConnectionString,
            string sourceConnectionString,
            string targetConnectionString,
            string targetConnectionString_Live,
            string targetConnectionString_MCGM
            )
        {
            int html_File_Creation = 1;
            try
            {
                this.HTML_File_Creation(sourceConnectionString, ref html_File_Creation);                    //HTML_File_Creation 1

                #region [ Variables n Initializing ]

                #region [ Variables ]
                string[] restrictions;

                DataSet dsOutPut;

                DataTable dtMainSource;
                DataTable dtMainLocal_Target;
                DataTable dtMainLive_Target;
                DataTable dtUpload;
                DataTable dtFinal;
                DataTable dtDuplicateRows;

                DataRow[] dtrowWhere;

                string tableName;
                string sqlSubQueryWhere;
                string sqlAnd;
                string sqlSubQuery;
                string sqlQuery;
                string strwhere;
                string deleteDuplicatRows_by_Query;

                int rowCounts;
                int startCount;
                int endCount;
                int forIndex;
                int recordsAffected;

                bool comparingTables;

                OleDbConnection oleSourceConnection;
                OleDbConnection oleTargetConnection;
                #endregion [ Variables ]

                restrictions = new string[4];
                dsOutPut = new DataSet();
                dtUpload = new DataTable();
                dtFinal = new DataTable();
                dtDuplicateRows = new DataTable();

                rowCounts = 0;
                startCount = 0;
                endCount = 0;
                forIndex = 0;
                recordsAffected = 0;

                tableName = string.Empty;
                sqlSubQueryWhere = string.Empty;
                sqlAnd = string.Empty;
                sqlSubQuery = string.Empty;
                sqlQuery = string.Empty;
                strwhere = string.Empty;

                comparingTables = false;

                deleteDuplicatRows_by_Query = Program.ConfigDeleteDuplicatRows_by_Query;
                #endregion [ Variables n Initializing ]

                #region [ Read xml File ]
                try
                {
                    xmlSync_Tables_Details = this.BuildXML(sourceConnectionString, targetConnectionString);
                    this.GenerateXMLTableforCSVFiles();
                }
                catch (Exception ex)
                {
                    Global.Global.WriteLog("CAD : Read xml File throws Exception", LogMessageType.Error, ex.Message);
                }
                #endregion [ Read xml File ]

                #region [ Execute CSV Format Files ]
                if (Program.ConfigCSVFormat == "YES")
                {
                    try
                    {
                        if (this.ExecuteCSVFormatFilestoAccess(sourceConnectionString, targetConnectionString))
                        {
                            Global.Global.WriteLog("Step 0.0:", LogMessageType.Information, "CSV File found n Upload to Main Access");
                        }
                    }
                    catch (Exception ex)
                    {
                        Global.Global.WriteLog("CAD : ExecuteCSVFormatFilestoAccess throws Exception", LogMessageType.Error, ex.Message);
                    }
                }
                #endregion [ Execute CSV Format Files ]

                #region [ Relcon ]
                if (Program.Config_Transfer_data_to_main_database == true)
                {
                    Program.Config_Transfer_data_to_main_database = false;
                    /*---- BECAUSE WE HAVE CREATED A PLC SYNC SERVICES SO BELOW COMMENTED LINE IS NOT REQURIED 
                     AND IF IN CONFIG FILE Config_Transfer_data_to_main_database VALUE IS TRUE THEN HERE WE HAVE SETTED TO FALSE
                     */
                    ////this.TransferDataToMainDatabase(plc_SourceConnectionString, sourceConnectionString);
                }
                #endregion [ Relcon ]

                //for (int iforce_Fully = 0; iforce_Fully < 300; iforce_Fully++)
                {
                    #region [ Comparing the Access Database and Upload to Web Service]

                    try
                    {
                        oleSourceConnection = null;
                        oleTargetConnection = null;

                        /* below region creates the Batch_Master_Upload_Status n Batch_Transaction_Upload_Status tables in source database*/
                        {
                            #region [ Create Upload Status tables ]
                            dtFinal.TableName = "Batch_Transaction";
                            this.CheckWith_Batch_Upload_Status(sourceConnectionString, dtFinal, true);
                            dtFinal.TableName = "Batch_Dat_Trans";
                            this.CheckWith_Batch_Upload_Status(sourceConnectionString, dtFinal, true);
                            #endregion [ Create Upload Status tables ]
                        }

                        #region [ Get the Mismatch Tables ]
                        if (dtTablesList == null)
                        {
                            restrictions[3] = "Table";
                            using (oleTargetConnection = new OleDbConnection(targetConnectionString))
                            {
                                oleTargetConnection.Open();

                                dtTablesList = oleTargetConnection.GetSchema("Tables", restrictions);

                                oleTargetConnection.Close();
                                oleTargetConnection.Dispose();
                            }

                            oleTargetConnection = null;
                        }

                        foreach (DataRow dtrXMLTable in xmlSync_Tables_Details.Select("XmlName = 'Name'"))
                        {
                            recordsAffected = 0;
                            tableName = dtrXMLTable["XmlRelation"].ToString();

                            if (dtTablesList.Select("TABLE_NAME = '" + tableName + "'").Length > 0)
                            {
                                recordsAffected = 1;
                            }

                            if (recordsAffected > 0)
                            {
                                this.HTML_File_Creation(sourceConnectionString, ref html_File_Creation);                    //HTML_File_Creation 1.1

                                Global.Global.WriteLog("CAD : ", LogMessageType.Information, "Process started for table :" + tableName);

                                #region [ Get Source Data ]

                                //tableName
                                strwhere = string.Empty;
                                //add for where condition in select table
                                dtrowWhere = xmlSync_Tables_Details.Select("XmlRelation ='" + tableName.ToString() + "' and XmlName='AddWhere'");
                                if (dtrowWhere.Count() > 0)
                                {
                                    foreach (DataRow dtrow in dtrowWhere)
                                    {
                                        strwhere = strwhere + ((strwhere.Length > 0) ? " and " : string.Empty) + dtrow["XmlKey"].ToString();
                                    }

                                    strwhere = ((strwhere.Length > 0) ? " where " + strwhere : string.Empty);
                                }

                                dtMainSource = new DataTable();
                                dtMainLocal_Target = new DataTable();
                                dtMainLive_Target = new DataTable();

                                {
                                    List<string> keyColumns1 = new List<string>();

                                    #region [ Get Source Table Data ]
                                    using (oleSourceConnection = new OleDbConnection(sourceConnectionString))
                                    {
                                        oleSourceConnection.Open();
                                        if (deleteDuplicatRows_by_Query == "true")
                                        {
                                            /*add for SubQueryWhere condition in select table*/
                                            dtrowWhere = xmlSync_Tables_Details.Select("XmlRelation ='" + tableName.ToString() + "' and XmlName='Key'");
                                            if (dtrowWhere.Count() > 0)
                                            {
                                                sqlSubQueryWhere = string.Empty;
                                                sqlAnd = string.Empty;

                                                foreach (DataRow dtrow in dtrowWhere)
                                                {
                                                    sqlSubQueryWhere += sqlAnd + dtrow["XmlKey"].ToString() + " = MT." + dtrow["XmlKey"].ToString();
                                                    keyColumns1.Add(dtrow["XmlKey"].ToString());
                                                    sqlAnd = " AND ";
                                                }

                                                sqlSubQueryWhere = ((sqlSubQueryWhere.Length > 0) ? (strwhere.Length > 0 ? strwhere + " AND " : " WHERE ") + sqlSubQueryWhere : string.Empty);
                                            }

                                            sqlSubQuery = "(SELECT Count(1) FROM " + tableName + sqlSubQueryWhere + " ) AS Rank";
                                            sqlQuery = "SELECT " + sqlSubQuery + " ,* FROM " + tableName + " AS MT " + strwhere;
                                        }
                                        else
                                        {
                                            /*add for SubQueryWhere condition in select table*/
                                            dtrowWhere = xmlSync_Tables_Details.Select("XmlRelation ='" + tableName.ToString() + "' and XmlName='Key'");
                                            if (dtrowWhere.Count() > 0)
                                            {
                                                foreach (DataRow dtrow in dtrowWhere)
                                                {
                                                    keyColumns1.Add(dtrow["XmlKey"].ToString());
                                                }
                                            }

                                            sqlQuery = "SELECT * FROM " + tableName + " AS MT " + strwhere;

                                            //////tableName = tableName.ToLower();
                                            //////if (Check_For_MCGM_Tables(tableName))
                                            //////{
                                            //////    if (
                                            //////        tableName == "batch_dat_trans"
                                            //////        || tableName == "batch_result"
                                            //////        )
                                            //////    {
                                            //////        sqlQuery += (strwhere.Length == 0 ? " WHERE " : " AND ") + "NOT BATCH_NO IN ( SELECT DISTINCT BATCH_NO FROM BATCH_MASTER_UPLOAD_STATUS WHERE IS_HTML_FILE_CREATED = 1)";
                                            //////    }
                                            //////    else
                                            //////    {
                                            //////        string field_Name = GetField_Name(tableName);

                                            //////        sqlQuery += (strwhere.Length == 0 ? " WHERE " : " AND ") + " NOT " + field_Name + " IN ( SELECT DISTINCT BATCH_NO FROM BATCH_TRANSACTION_UPLOAD_STATUS WHERE IS_HTML_FILE_CREATED = 1)";
                                            //////    }
                                            //////}
                                        }

                                        dtMainSource = this.FillDatatableOLE(sqlQuery, oleSourceConnection);
                                        oleSourceConnection.Close();
                                        oleSourceConnection.Dispose();
                                    }
                                    oleSourceConnection = null;

                                    #endregion [ Get Source Table Data ]

                                    if (deleteDuplicatRows_by_Query == "true")
                                    {
                                        #region [ Delete Duplicate Rows by query logic ]
                                        dtDuplicateRows = new DataTable();
                                        if (dtMainSource.Select("Rank > 1").Length > 0)
                                        {
                                            dtDuplicateRows = dtMainSource.Select("Rank > 1").CopyToDataTable();
                                            dtDuplicateRows.Columns.Remove("Rank");
                                        }

                                        if (dtMainSource.Select("Rank = 1").Length > 0)
                                        {
                                            dtMainSource = dtMainSource.Select("Rank = 1").CopyToDataTable();
                                            dtMainSource.Columns.Remove("Rank");
                                        }

                                        sqlSubQueryWhere = string.Empty;
                                        foreach (DataRow item in dtDuplicateRows.Rows)
                                        {
                                            if (dtrowWhere.Count() > 0)
                                            {
                                                sqlSubQueryWhere = string.Empty;
                                                sqlAnd = string.Empty;
                                                foreach (DataRow dtrow in dtrowWhere)
                                                {
                                                    if (item[dtrow["XmlKey"].ToString()].GetType() == typeof(String)
                                                        || item[dtrow["XmlKey"].ToString()].GetType() == typeof(DateTime)
                                                        )
                                                    {
                                                        sqlSubQueryWhere += sqlAnd + dtrow["XmlKey"].ToString() + " = '" + item[dtrow["XmlKey"].ToString()] + "'";
                                                    }
                                                    else
                                                    {
                                                        sqlSubQueryWhere += sqlAnd + dtrow["XmlKey"].ToString() + " = " + item[dtrow["XmlKey"].ToString()];
                                                    }

                                                    sqlAnd = " AND ";
                                                }
                                            }

                                            if (dtMainSource.Select(sqlSubQueryWhere).Length == 0)
                                            {
                                                dtMainSource.ImportRow(item);
                                            }
                                        }
                                        #endregion [ Delete Duplicate Rows by query logic ]
                                    }
                                    else
                                    {
                                        this.RemoveDuplicatesRows(dtMainSource, keyColumns1);
                                    }
                                }

                                #endregion [ Get Source Data ]

                                if (dtMainSource.Rows.Count > 0)
                                {
                                    #region [ Get Target Data ]

                                    using (oleTargetConnection = new OleDbConnection(targetConnectionString))
                                    {
                                        oleTargetConnection.Open();

                                        dtMainLocal_Target = this.FillDatatableOLE("SELECT * FROM " + tableName + strwhere, oleTargetConnection);

                                        oleTargetConnection.Close();
                                        oleTargetConnection.Dispose();
                                    }

                                    oleTargetConnection = null;
                                    bool bInsertinto_LiveSQLSERVER = false;
                                    if (Program.PlantAppVersion == "11")
                                    {
                                        if (Program.IsLocalHosting == "true")
                                        {
                                            if (string.IsNullOrEmpty(targetConnectionString_Live) == false)
                                            {
                                                try
                                                {
                                                    using (oleTargetConnection = new OleDbConnection(targetConnectionString_Live))
                                                    {
                                                        oleTargetConnection.Open();

                                                        dtMainLive_Target = this.FillDatatableOLE("SELECT * FROM " + tableName + strwhere, oleTargetConnection);

                                                        oleTargetConnection.Close();
                                                        oleTargetConnection.Dispose();
                                                    }

                                                    oleTargetConnection = null;
                                                    bInsertinto_LiveSQLSERVER = true;
                                                }
                                                catch (Exception)
                                                {
                                                    bInsertinto_LiveSQLSERVER = false;
                                                }
                                            }
                                        }
                                    }
                                    #endregion [ Get Target Data ]

                                    #region [ Upload to Webservice ]
                                    try
                                    {
                                        dtFinal = new DataTable();

                                        if (Program.Config_remove_truck_no_from_batch_result)
                                        {
                                            if (tableName == "batch_result")
                                            {
                                                if (dtMainSource.Columns.Contains("Truck_no"))
                                                {
                                                    dtMainSource.Columns.Remove("Truck_no");
                                                }

                                                if (dtMainLive_Target.Columns.Contains("Truck_no"))
                                                {
                                                    dtMainLive_Target.Columns.Remove("Truck_no");
                                                }

                                                if (dtMainLocal_Target.Columns.Contains("Truck_no"))
                                                {
                                                    dtMainLocal_Target.Columns.Remove("Truck_no");
                                                }
                                            }
                                        }

                                        #region [ Upload ]

                                        bool skipLocalUpload = false;
                                        if (Program.PlantAppVersion == "11")
                                        {
                                            if (Program.ConfigApplication_Type == "0")
                                            {
                                                skipLocalUpload = true;
                                            }
                                        }

                                        dtMainSource.TableName = tableName;
                                        if (dtMainSource.Rows.Count > 0)
                                        {
                                            if (this.Check_For_MCGM_Tables(tableName) == true)
                                            {
                                                dtMainSource = this.CheckWith_Batch_Upload_Status(sourceConnectionString, dtMainSource, false);
                                            }

                                        }

                                        if (skipLocalUpload == false)
                                        {
                                            #region [ Local Upload ]

                                            forIndex = 0;
                                            dtFinal = this.CompareAccessDatabasebyLinq(dtMainSource, dtMainLocal_Target, tableName, true);
                                            dtFinal.TableName = tableName;
                                            tableName = tableName.ToLower();
                                            if (dtFinal.Rows.Count > 0)
                                            {
                                                comparingTables = true;
                                                startCount = 0;
                                                rowCounts = 2;
                                                endCount = Program.ConfigTopCount;
                                                if (Program.Config_write_XML_Upload)
                                                {
                                                    dtFinal.TableName = tableName;
                                                    this.Write_XML_File("Upload", dtFinal, MethodBase.GetCurrentMethod().Name + "_local");
                                                }

                                                for (forIndex = 0; forIndex <= rowCounts; forIndex++)
                                                {
                                                    dtUpload = dtFinal.Select(string.Format("Identity_Column > {0} and Identity_Column <= {1}", startCount.ToString(), endCount.ToString())).CopyToDataTable();

                                                    #region [ Set the Primary Keys to DataTable ]

                                                    dtUpload.TableName = tableName.ToString();
                                                    {
                                                        DataRow[] dtrXML = xmlSync_Tables_Details.Select("XmlRelation ='" + tableName.ToString() + "' AND XmlName='Key'");
                                                        if (dtrXML.Length > 0)
                                                        {
                                                            DataColumn[] dataColumns = new DataColumn[dtrXML.Length + 1];
                                                            for (int i = 0; i < dataColumns.Count() - 1; i++)
                                                            {
                                                                dataColumns[i] = dtUpload.Columns[dtrXML[i]["XmlKey"].ToString()];
                                                            }

                                                            if (dtUpload.Columns.Contains("org_id"))
                                                            {
                                                                dataColumns[dataColumns.Length - 1] = dtUpload.Columns["org_id"];
                                                            }

                                                            dtUpload.PrimaryKey = dataColumns;
                                                        }
                                                    }

                                                    #region [ Remove Identity_Column ]
                                                    dtUpload.Columns.Remove("Identity_Column");
                                                    dtUpload.TableName = tableName;
                                                    dtUpload.AcceptChanges();
                                                    #endregion [ Remove Identity_Column ]

                                                    dsOutPut = new DataSet();
                                                    dsOutPut.Tables.Add(dtUpload);

                                                    #endregion [ Set the Primary Keys to DataTable ]

                                                    this.UploadDataToWebService(
                                                        dsOutPut,
                                                        targetConnectionString,
                                                        true,
                                                        forIndex,
                                                        skipLocalUpload,
                                                        targetConnectionString_MCGM,
                                                        sourceConnectionString
                                                        );

                                                    startCount = endCount;
                                                    if (endCount < dtFinal.Rows.Count)
                                                    {
                                                        rowCounts++;
                                                    }
                                                    else
                                                    {
                                                        break;
                                                    }

                                                    endCount += Program.ConfigTopCount;
                                                }
                                            }
                                            #endregion [ Local Upload ]
                                        }

                                        if (Program.PlantAppVersion == "11")
                                        {
                                            if (bInsertinto_LiveSQLSERVER)
                                            {
                                                #region [ Live Upload ]
                                                if (Program.IsLocalHosting == "true")
                                                {
                                                    forIndex = 0;
                                                    dtFinal = new DataTable();
                                                    dtFinal = this.CompareAccessDatabasebyLinq(dtMainSource, dtMainLive_Target, tableName, false);
                                                    if (dtFinal.Rows.Count > 0)
                                                    {
                                                        comparingTables = true;
                                                        startCount = 0;
                                                        rowCounts = 2;
                                                        endCount = Program.ConfigTopCount;
                                                        if (Program.Config_write_XML_Upload)
                                                        {
                                                            dtFinal.TableName = tableName;
                                                            this.Write_XML_File("Upload", dtFinal, MethodBase.GetCurrentMethod().Name + "_live");
                                                        }

                                                        for (forIndex = 0; forIndex <= rowCounts; forIndex++)
                                                        {
                                                            dtUpload = dtFinal.Select(string.Format("Identity_Column > {0} and Identity_Column <= {1}", startCount.ToString(), endCount.ToString())).CopyToDataTable();

                                                            #region [ Set the Primary Keys to DataTable ]

                                                            dtUpload.TableName = tableName.ToString();
                                                            {
                                                                DataRow[] dtrXML = xmlSync_Tables_Details.Select("XmlRelation ='" + tableName.ToString() + "' AND XmlName='Key'");
                                                                if (dtrXML.Length > 0)
                                                                {
                                                                    DataColumn[] dataColumns = new DataColumn[dtrXML.Length + 1];
                                                                    for (int i = 0; i < dataColumns.Count() - 1; i++)
                                                                    {
                                                                        dataColumns[i] = dtUpload.Columns[dtrXML[i]["XmlKey"].ToString()];
                                                                    }

                                                                    if (dtUpload.Columns.Contains("org_id"))
                                                                    {
                                                                        dataColumns[dataColumns.Length - 1] = dtUpload.Columns["org_id"];
                                                                    }

                                                                    dtUpload.PrimaryKey = dataColumns;
                                                                }
                                                            }

                                                            #region [ Remove Identity_Column ]
                                                            dtUpload.Columns.Remove("Identity_Column");
                                                            dtUpload.TableName = tableName;
                                                            dtUpload.AcceptChanges();
                                                            #endregion [ Remove Identity_Column ]

                                                            dsOutPut = new DataSet();
                                                            dsOutPut.Tables.Add(dtUpload);

                                                            #endregion [ Set the Primary Keys to DataTable ]

                                                            this.UploadDataToWebService(
                                                                dsOutPut,
                                                                targetConnectionString_Live,
                                                                false,
                                                                forIndex,
                                                                skipLocalUpload,
                                                                targetConnectionString_MCGM,
                                                                sourceConnectionString
                                                                );

                                                            startCount = endCount;
                                                            if (endCount < dtFinal.Rows.Count)
                                                            {
                                                                rowCounts++;
                                                            }
                                                            else
                                                            {
                                                                break;
                                                            }

                                                            endCount += Program.ConfigTopCount;
                                                        }
                                                    }
                                                }
                                                #endregion [ Live Upload ]
                                            }
                                        }

                                        #endregion [ Upload ]

                                    }
                                    catch (Exception ex)
                                    {
                                        Global.Global.WriteLog("CAD :", LogMessageType.Error, ex.Message.ToString());
                                    }

                                    #endregion [ Upload to Webservice  ]
                                }
                                Global.Global.WriteLog("CAD : ", LogMessageType.Information, "Process completed for table :" + tableName);

                                this.HTML_File_Creation(sourceConnectionString, ref html_File_Creation);                    //HTML_File_Creation 1.2

                                if (dtMainSource != null)
                                {
                                    dtMainSource.Dispose();
                                }

                                if (dtMainLocal_Target != null)
                                {
                                    dtMainLocal_Target.Dispose();
                                }

                                if (dtMainLive_Target != null)
                                {
                                    dtMainLive_Target.Dispose();
                                }

                                if (dtFinal != null)
                                {
                                    dtFinal.Dispose();
                                }

                                GC.Collect();
                            }
                        }

                        #endregion [ Get the Mismatch Tables ]

                        if (oleSourceConnection != null)
                        {
                            oleSourceConnection.Dispose();
                        }

                        if (oleTargetConnection != null)
                        {
                            oleTargetConnection.Dispose();
                        }

                        oleSourceConnection = null;
                        oleTargetConnection = null;

                        if (comparingTables)
                        {
                            Global.Global.WriteLog("Step 1.0:", LogMessageType.Information, "Data found n Upload to SQL");
                        }
                    }
                    catch (Exception ex)
                    {
                        Global.Global.WriteLog("CAD : throws Exception for table " + tableName, LogMessageType.Error, ex.Message);
                    }

                    #endregion [ Comparing the Access Database and Upload to Web Service]

                    this.HTML_File_Creation(sourceConnectionString, ref html_File_Creation);                    //HTML_File_Creation 2
                }


                #region [ ReSync Data ]
                try
                {
                    this.Delete_SyncBatchData(targetConnectionString);
                }
                catch (Exception ex)
                {
                    Global.Global.WriteLog("CAD : Delete_SyncBatchData throws Exception", LogMessageType.Error, ex.Message);
                }
                #endregion [ ReSync Data ]

                if (Program.ConfigApplication_Type == "1")
                {
                    #region [ Download the Data ]
                    try
                    {
                        this.DownloadDataFromWebService(
                            sourceConnectionString,
                            targetConnectionString,
                            targetConnectionString_MCGM
                            );
                    }
                    catch (Exception ex)
                    {
                        Global.Global.WriteLog("CAD : DDFWS throws Exception", LogMessageType.Error, ex.Message);
                    }
                    #endregion [ Download the Data ]
                    this.HTML_File_Creation(sourceConnectionString, ref html_File_Creation);                //HTML_File_Creation 3

                    #region [ Upload/Download Data from Local/Live SQL Server]
                    //if (Program.PlantAppVersion != "11")
                    {
                        if (Program.IsLocalHosting == "true")
                        {
                            try
                            {
                                this.UploadLocalSQLServerDataToLiveSQLServerData(3);/*Tables Wise*/
                            }
                            catch (Exception ex)
                            {
                                Global.Global.WriteLog("CAD : Upload throws Exception", LogMessageType.Error, ex.Message);
                            }

                            try
                            {
                                this.DownloadLiveSQLServerDataToLocalSQLServerData(3); /*Tables Wise*/
                            }
                            catch (Exception ex)
                            {
                                Global.Global.WriteLog("CAD : Download throws Exception", LogMessageType.Error, ex.Message);
                            }
                        }
                    }
                    #endregion [ Upload/Download Data from Local/Live SQL Server]
                    this.HTML_File_Creation(sourceConnectionString, ref html_File_Creation);                //HTML_File_Creation 4

                    #region [ Asphalt New batches ]
                    if (Program.PlantAppVersion == "11")
                    {
                        try
                        {
                            this.GetAsphaltNewBatches(sourceConnectionString, targetConnectionString);
                        }
                        catch (Exception ex)
                        {
                            Global.Global.WriteLog("CAD : Asphalt New batches GetAsphaltNewBatches throws Exception", LogMessageType.Error, ex.Message);
                        }
                    }
                    #endregion [ Asphalt New batches ]
                    this.HTML_File_Creation(sourceConnectionString, ref html_File_Creation);                //HTML_File_Creation 5

                    #region  [ Re-Sync Master Table like Customer , Site , Truck , Truck_Devic_mapping ]
                    try
                    {
                        this.Resync_Masters_Data();
                    }
                    catch (Exception ex)
                    {
                        Global.Global.WriteLog("CAD : Re-Sync Master Resync_Masters_Data throws Exception", LogMessageType.Error, ex.Message);
                    }
                    #endregion [ Re-Sync Master Table like Customer , Site , Truck , Truck_Devic_mapping ]
                    this.HTML_File_Creation(sourceConnectionString, ref html_File_Creation);                //HTML_File_Creation 6
                }

                this.HTML_File_Creation(sourceConnectionString, ref html_File_Creation);                    //HTML_File_Creation 7

                #region [ Desilting Plant ]

                if (Program.PlantAppVersion == "21")
                {
                    try
                    {
                        this.GetMastersData(sourceConnectionString, targetConnectionString);
                    }
                    catch (Exception ex)
                    {
                        Global.Global.WriteLog("CAD : Desilting Plant GetMastersData throws Exception", LogMessageType.Error, ex.Message);
                    }

                    try
                    {
                        this.Get_DesiltingTrips(sourceConnectionString, targetConnectionString);
                    }
                    catch (Exception ex)
                    {
                        Global.Global.WriteLog("CAD : Desilting Plant Get_DesiltingTrips throws Exception", LogMessageType.Error, ex.Message);
                    }
                }

                #endregion [ Desilting Plant ]

            }
            catch (Exception ex)
            {
                Global.Global.WriteLog("CAD : Exception", LogMessageType.Error, ex.Message);
            }
        }

        private void HTML_File_Creation(string sourceConnectionString, ref int html_File_Creation)
        {
            try
            {
                if (
                    Program.PlantAppVersion == "21"
                    || Program.PlantAppVersion == string.Empty)
                {
                }
                else if (
                    Program.PlantAppVersion == "11"
                    )
                {
                    #region [ HTML File Creation for Asphalt Version 11 ]
                    try
                    {
                        this.GetMCGM_HTML_ASPHALT_DATA();
                    }
                    catch (Exception ex)
                    {
                        Global.Global.WriteLog("CAD : HTML_ASPHALT_DATA throws Exception", LogMessageType.Error, ex.Message);
                    }
                    #endregion [ HTML File Creation for Asphalt Version 11 ]
                }
                else if (
                    Program.PlantAppVersion == "1"
                    || Program.PlantAppVersion == "2"
                    || Program.PlantAppVersion == "2.0"
                    || Program.PlantAppVersion == "3"
                    || Program.PlantAppVersion == "3.0"
                    || Program.PlantAppVersion == "6"
                    || Program.PlantAppVersion == "6.0"
                    || Program.PlantAppVersion == "6.2"
                    )
                {
                    #region [ HTML File Creation for RMC Version 1 ,2 ,2.0 ,3 ,3.0 ,6 ,6.0 ,6.2 ]

                    try
                    {
                        this.GetMCGM_HTML_RMC_DATA(sourceConnectionString);
                    }
                    catch (Exception ex)
                    {
                        Global.Global.WriteLog("CAD : HTML_RMC_DATA throws Exception", LogMessageType.Error, ex.Message);
                    }
                    #endregion [ HTML File Creation for RMC Version 1 ,2 ,2.0 ,3 ,3.0 ,6 ,6.0 ,6.2 ]
                }
            }
            catch (Exception ex)
            {
                Global.Global.WriteLog("HTML_File_Creation throws Exception", LogMessageType.Error, ex.Message);
            }
            finally
            {
                html_File_Creation++;
            }
        }

        #region [ Read xml File ]
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sourceConnectionString"></param>
        /// <param name="targetConnectionString"></param>
        /// <returns></returns>
        private DataTable BuildXML(string sourceConnectionString, string targetConnectionString)
        {
            try
            {
                string[] restrictions = new string[4];

                string readerName;
                string readerRelation;
                string prevoiusValue;
                string currentValue;
                string plantName;
                string attributeValue;

                DataTable dtColumnList;
                OleDbConnection oleTargetConnection;

                XmlTextReader reader = new XmlTextReader(Program.CurrentApplicationPath + "\\Sync_Tables.xml");

                readerName = string.Empty;
                readerRelation = string.Empty;
                prevoiusValue = string.Empty;
                currentValue = string.Empty;
                plantName = string.Empty;
                attributeValue = string.Empty;
                dtColumnList = new DataTable();

                #region [ Inner region for Read xml File ]
                {
                    xmlSync_Tables_Details = new DataTable();

                    if (dtTablesList == null)
                    {
                        restrictions[3] = "Table";
                        using (oleTargetConnection = new OleDbConnection(targetConnectionString))
                        {
                            oleTargetConnection.Open();

                            dtTablesList = oleTargetConnection.GetSchema("Tables", restrictions);

                            oleTargetConnection.Close();
                            oleTargetConnection.Dispose();
                        }
                        oleTargetConnection = null;
                    }

                    xmlSync_Tables_Details.Columns.AddRange(new DataColumn[]{
                            new DataColumn("XmlName",typeof(string)),
                            new DataColumn("XmlKey",typeof(string)),
                            new DataColumn("XmlRelation",typeof(string))});

                    plantName = Program.ConfigPlantName;

                    while (reader.Read())
                    {
                        if ((prevoiusValue == currentValue) && prevoiusValue != string.Empty)
                        {
                            if (reader.NodeType == XmlNodeType.Element)
                            {
                                if (readerRelation != reader.GetAttribute("id") && reader.GetAttribute("id") != null)
                                {
                                    readerRelation = reader.GetAttribute("id");

                                    xmlSync_Tables_Details.Rows.Add(new object[] { reader.Name, readerRelation, readerRelation });
                                }

                                readerName = reader.Name;
                            }

                            if (reader.NodeType == XmlNodeType.Text)
                            {
                                string columnName = reader.Value.Trim();
                                readerRelation = ((readerRelation.ToString() == string.Empty) ? columnName : readerRelation);
                                if (readerName.ToUpper() == "KEY")
                                {
                                    if (dtColumnList.TableName != readerRelation)
                                    {
                                        dtColumnList = this.GetTableColumns(readerRelation, sourceConnectionString);
                                    }

                                    if (dtColumnList.Columns.Contains(columnName))
                                    {
                                        xmlSync_Tables_Details.Rows.Add(new object[] { readerName, columnName, readerRelation });
                                    }
                                }
                                else
                                {
                                    xmlSync_Tables_Details.Rows.Add(new object[] { readerName, columnName, readerRelation });
                                }
                            }
                        }

                        if (reader.Name == "PlantName")
                        {
                            if (ConvertToInt32(reader.AttributeCount) > 0)
                            {
                                attributeValue = reader.GetAttribute("id").ToUpper();
                                if ((prevoiusValue != attributeValue) && (prevoiusValue != string.Empty) && (plantName == attributeValue))
                                {
                                    prevoiusValue = attributeValue;
                                    currentValue = attributeValue;
                                }
                                else if (prevoiusValue == string.Empty && (plantName == attributeValue))
                                {
                                    prevoiusValue = attributeValue;
                                    currentValue = attributeValue;
                                }
                                else if (plantName != attributeValue)
                                {
                                    prevoiusValue = string.Empty;
                                    currentValue = string.Empty;
                                }
                            }
                        }
                    }

                    dtTablesList = null;
                }
                #endregion [ Inner region for Read xml File ]

            }
            catch (Exception)
            {
                throw new Exception("Error in Reading XML File");
            }

            return xmlSync_Tables_Details;
        }

        /// <summary>
        /// Xml Table for CSV files
        /// </summary>
        private void GenerateXMLTableforCSVFiles()
        {
            try
            {
                {
                    xmlCSV_Tables_Details = new DataTable();

                    xmlCSV_Tables_Details.Columns.AddRange(new DataColumn[]{
                        new DataColumn("TableName",typeof(string)),
                        new DataColumn("ColName",typeof(string)),
                        new DataColumn("Value",typeof(string)),
                        new DataColumn("DataType",typeof(string))});

                    XmlDocument objDocument = new XmlDocument();
                    XmlElement rootxml;
                    string[] arrayColumn;

                    string readerRelation = string.Empty;
                    string xmlName = string.Empty;
                    string readertable = string.Empty;

                    objDocument.Load(Program.CurrentApplicationPath + "\\ReadCsv.xml");
                    rootxml = objDocument.DocumentElement;

                    foreach (XmlElement xmlchildNode in rootxml)
                    {
                        if (xmlchildNode.Name.ToLower() == "tablename")
                        {
                            readertable = xmlchildNode.GetAttribute("id");
                            foreach (XmlElement xmlsubchild in xmlchildNode.ChildNodes)
                            {
                                readerRelation = xmlsubchild.GetAttribute("id");

                                arrayColumn = null;

                                arrayColumn = xmlsubchild.InnerText.ToString().Trim().Split(new string[] { "," }, StringSplitOptions.None);
                                if (arrayColumn.Count() == 2)
                                {
                                    xmlCSV_Tables_Details.Rows.Add(new object[] { readertable, readerRelation, arrayColumn[0].ToString(), arrayColumn[1].ToString() });
                                }
                                else
                                {
                                    xmlCSV_Tables_Details.Rows.Add(new object[] { readertable, readerRelation, xmlsubchild.InnerText.ToString().Trim(), string.Empty });
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                throw (ex);
            }

        }
        #endregion [ Read xml File ]

        #region [ Relcon ]
        /// <summary>
        /// 
        /// </summary>
        /// <param name="plc_SourceConnectionString"></param>
        /// <param name="sourceConnectionString"></param>
        private void TransferDataToMainDatabase(string plc_SourceConnectionString, string sourceConnectionString)
        {
            #region [ Get PLC Source Table Data ]

            DataTable dt_PLC_Main_Data = new DataTable();
            string access_Query = string.Empty;
            string tableNameasFileName = "DATALOGGER";
            OleDbConnection plc_Source_Connection;

            using (plc_Source_Connection = new OleDbConnection(plc_SourceConnectionString))
            {
                access_Query = "SELECT * FROM DATALOGGER ORDER BY TIMECOL, BATCHNO";

                plc_Source_Connection.Open();
                dt_PLC_Main_Data = this.FillDatatableOLE(access_Query, plc_Source_Connection);
                dt_PLC_Main_Data.TableName = tableNameasFileName;
                plc_Source_Connection.Close();
                plc_Source_Connection.Dispose();
            }
            plc_Source_Connection = null;

            #endregion [ Get PLC Source Table Data ]

            #region [ Swap the PLC Source data to Main Data Table]
            DataTable dtMain_Data = new DataTable();
            if (dt_PLC_Main_Data.Rows.Count > 0)
            {
                if (Program.Config_write_XML_Upload)
                {
                    this.Write_XML_File("Upload", dt_PLC_Main_Data, MethodBase.GetCurrentMethod().Name + "_Relcon");
                }

                dtMain_Data = dt_PLC_Main_Data.Select().CopyToDataTable();
                dtMain_Data.TableName = tableNameasFileName;
                dtMain_Data = this.RenameFieldsofDataTable(dtMain_Data);

                DataRow[] dtrColumnName = xmlCSV_Tables_Details.Select("TableName = '" + tableNameasFileName + "' and colName = '" + tableNameasFileName + "'");
                if (dtrColumnName.Length > 0)
                {
                    dtMain_Data.TableName = dtrColumnName[0]["Value"].ToString();

                    using (PNF.RI_syncUtility objupload = new PNF.RI_syncUtility())
                    {
                        dtMain_Data = objupload.RemoveUnwantedFields(dtMain_Data, sourceConnectionString);
                    }
                }
            }
            else
            {
                return;
            }
            #endregion [ Swap the PLC Source data to Main Data Table]

            #region [ Insert int Source Database Table(s) ]
            string tableName = dtMain_Data.TableName;
            string dateTimeFormat = Convert.ToString(ConfigurationSettings.AppSettings["dateTimeFormat"].ToString()); /*"dd/MM/yyyy hh:mm tt"*/
            string dateFormat = Convert.ToString(ConfigurationSettings.AppSettings["dateFormat"].ToString()); /*"dd/MM/yyyy";*/

            int loopRowIndex = 0;
            int loopColumnIndex = 0;
            int rowsCount = dtMain_Data.Rows.Count;
            int columnsCount = dtMain_Data.Columns.Count;

            int totalRowsAffected = 0;
            string insertQuery;
            string commaValue = string.Empty;
            string columnValue;
            string columnName;
            string fieldNames;
            int rowsAffected;
            string blankParameter = "@blankParameter";
            string doublQuote = string.Empty;

            using (OleDbConnection connection = new OleDbConnection(sourceConnectionString))
            {
                connection.Open();
                using (OleDbTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        for (loopRowIndex = 0; loopRowIndex < rowsCount; loopRowIndex++)
                        {
                            commaValue = string.Empty;
                            loopColumnIndex = 0;
                            insertQuery = string.Empty;
                            fieldNames = string.Empty;

                            for (loopColumnIndex = 0; loopColumnIndex < columnsCount; loopColumnIndex++)
                            {
                                columnValue = dtMain_Data.Rows[loopRowIndex][loopColumnIndex].ToString();
                                columnName = dtMain_Data.Columns[loopColumnIndex].ColumnName;

                                if (!string.IsNullOrEmpty(columnValue))
                                {
                                    fieldNames += commaValue + "[" + columnName + "]";

                                    if (dtMain_Data.Columns[loopColumnIndex].DataType.Name == "String")
                                    {
                                        insertQuery += commaValue + "'" + columnValue + "'";
                                    }
                                    else if (dtMain_Data.Columns[loopColumnIndex].DataType.Name == "DateTime")
                                    {
                                        if (columnName.ToLower().Contains("time"))
                                        {
                                            insertQuery += commaValue + "#" + Convert.ToDateTime(columnValue).ToString(dateTimeFormat) + "#";
                                        }
                                        else
                                        {
                                            insertQuery += commaValue + "#" + Convert.ToDateTime(columnValue).ToString(dateFormat) + "#";
                                        }
                                    }
                                    else
                                    {
                                        insertQuery += commaValue + (string.IsNullOrEmpty(columnValue) ? "0" : columnValue);
                                    }
                                }

                                commaValue = Environment.NewLine + ",";
                            }

                            insertQuery = "INSERT INTO " + tableName + " ( " + fieldNames + ") VALUES(" + Environment.NewLine + insertQuery + ")";
                            insertQuery = insertQuery.Replace("''", blankParameter);
                            commaValue = string.Empty;
                            loopColumnIndex = 0;

                            rowsAffected = 0;
                            try
                            {
                                using (OleDbCommand commandUpdate = new OleDbCommand(insertQuery, transaction.Connection, transaction))
                                {
                                    commandUpdate.Parameters.Add(blankParameter, OleDbType.VarChar).Value = doublQuote;
                                    rowsAffected = commandUpdate.ExecuteNonQuery();
                                }
                            }
                            catch (Exception ex)
                            {
                                throw ex;
                            }

                            totalRowsAffected += rowsAffected;
                        }

                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw ex;
                    }
                }
            }

            #endregion [ Insert int Source Database Table(s) ]

            if (totalRowsAffected > 0)
            {
                #region [ Delete the PLC Source Table Data ]
                string delete_Query = string.Empty;
                loopRowIndex = 0;
                totalRowsAffected = 0;
                rowsCount = dt_PLC_Main_Data.Rows.Count;
                tableName = dt_PLC_Main_Data.TableName;

                string dtrMin_Time_Col = dt_PLC_Main_Data.Compute("Min(TIMECOL)", string.Empty).ToString();
                string dtrMax_Time_Col = dt_PLC_Main_Data.Compute("MAX(TIMECOL)", string.Empty).ToString();
                string dtrMin_BatchNo = dt_PLC_Main_Data.Compute("Min(BATCHNO)", string.Empty).ToString();
                string dtrMax_BatchNo = dt_PLC_Main_Data.Compute("MAX(BATCHNO)", string.Empty).ToString();

                using (OleDbConnection connection = new OleDbConnection(plc_SourceConnectionString))
                {
                    connection.Open();
                    using (OleDbTransaction transaction = connection.BeginTransaction())
                    {
                        try
                        {
                            delete_Query = "DELETE FROM " + tableName;
                            commaValue = " WHERE ";
                            columnName = "TIMECOL";
                            delete_Query += commaValue + "(" + columnName + " >= #" + Convert.ToDateTime(dtrMin_Time_Col).ToString(dateTimeFormat) + "#";
                            commaValue = " AND ";

                            delete_Query += commaValue + columnName + " <= #" + Convert.ToDateTime(dtrMax_Time_Col).ToString(dateTimeFormat) + "#)";
                            columnName = "BATCHNO";
                            delete_Query += commaValue + "(" + columnName + " >= " + dtrMin_BatchNo;
                            delete_Query += commaValue + columnName + " <= " + dtrMax_BatchNo + ")";
                            try
                            {
                                using (OleDbCommand commandUpdate = new OleDbCommand(delete_Query, transaction.Connection, transaction))
                                {
                                    rowsAffected = commandUpdate.ExecuteNonQuery();
                                }
                            }
                            catch (Exception ex)
                            {
                                throw ex;
                            }

                            transaction.Commit();
                        }
                        catch (Exception ex)
                        {
                            transaction.Rollback();
                            throw ex;
                        }
                    }
                }

                #endregion [ Delete the PLC Source Table Data ]
            }
        }
        #endregion [ Relcon ]

        #region [ Execute CSV Format Files ]

        /// <summary>
        /// Execute CSVFormat or Text Files to Access
        /// </summary>
        private bool ExecuteCSVFormatFilestoAccess(string sourceConnectionString, string targetConnectionString)
        {
            bool returnData = false;
            try
            {
                DataTable excelMainDataTable;
                DataTable excelReplicatDataTable;
                string[] arrayFiles;

                string mainFileName = string.Empty;
                string replicaFileName = string.Empty;
                string fileExtension = string.Empty;
                string replicaFolderPath;
                string mainFolderPath;

                int arrayIndex = 0;
                bool fileModified;

                mainFolderPath = Program.ConfigCSVFilesMainFolderpath;
                if (!Directory.Exists(mainFolderPath))
                {
                    Global.Global.WriteLog("ExecuteCSVFormatFilestoAccess", LogMessageType.Warning, mainFolderPath + " Folder is missing");
                    returnData = false;
                    return returnData;
                }

                /*this.GenerateXMLTableforCSVFiles();*/

                replicaFolderPath = Program.ConfigCSVFilesReplicaFolderpath;
                arrayFiles = Directory.GetFiles(mainFolderPath);

                for (arrayIndex = 0; arrayIndex < arrayFiles.Length; arrayIndex++)
                {
                    mainFileName = arrayFiles[arrayIndex];
                    if (xmlCSV_Tables_Details.Select("TableName = '" + Path.GetFileNameWithoutExtension(mainFileName) + "'").Count() > 0)
                    {
                        fileExtension = Path.GetExtension(mainFileName).ToLower();

                        if (fileExtension == ".csv"
                            || fileExtension == ".txt"
                            || fileExtension == ".xls"
                            || fileExtension == ".xlsx"
                            )
                        {
                            replicaFileName = replicaFolderPath + "\\" + Path.GetFileName(mainFileName);
                            fileModified = this.CheckModifiedFile(mainFileName, replicaFileName);
                            if (fileModified == true)   /*main file is modified*/
                            {
                                excelMainDataTable = this.ReadCSVnTextFile(mainFileName);

                                for (int i = 0; i < excelMainDataTable.Columns.Count; i++)
                                {
                                    excelMainDataTable.Columns[i].ColumnName = excelMainDataTable.Columns[i].ColumnName.ToString().Trim();
                                }

                                excelReplicatDataTable = excelMainDataTable.Clone();

                                if (File.Exists(replicaFileName))
                                {
                                    excelReplicatDataTable = this.ReadCSVnTextFile(replicaFileName);
                                    for (int i = 0; i < excelReplicatDataTable.Columns.Count; i++)
                                    {
                                        excelReplicatDataTable.Columns[i].ColumnName = excelReplicatDataTable.Columns[i].ColumnName.ToString().Trim();
                                    }
                                }

                                this.CompareFileAndInsertInAccess(
                                    excelMainDataTable,
                                    excelReplicatDataTable,
                                    mainFileName,
                                    replicaFileName,
                                    sourceConnectionString,
                                    targetConnectionString
                                    );

                                returnData = true;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Global.Global.WriteLog("ExecuteCSVFormatFilestoAccess:", LogMessageType.Error, ex.Message);
                throw (ex);
            }

            return returnData;
        }

        /// <summary>
        /// Check the files are modified or not
        /// </summary>
        /// <param name="mainFileName">First file name</param>
        /// <param name="replicaFileName">Comparing the file name</param>
        /// <returns></returns>
        private bool CheckModifiedFile(string mainFileName, string replicaFileName)
        {
            bool returnData = false;
            bool returnValue = false;

            returnValue = this.IsFileUsedbyAnotherProcess(mainFileName);
            if (returnValue == false)
            {
                DateTime dtLastTimeofMain;
                DateTime dtLastTimeofReplica;
                int differnce;

                dtLastTimeofMain = File.GetLastWriteTime(mainFileName);
                dtLastTimeofReplica = File.GetLastWriteTime(replicaFileName);

                differnce = dtLastTimeofMain.CompareTo(dtLastTimeofReplica);
                if (differnce != 0)
                {
                    returnData = true;
                }
            }

            return returnData;
        }

        /// <summary>
        /// File is used by other process
        /// </summary>
        /// <param name="mainFileName">file Name</param>
        /// <returns>true / false</returns>
        private bool IsFileUsedbyAnotherProcess(string mainFileName)
        {
            bool returnData = false;
            try
            {
                using (FileStream fs = new FileStream(mainFileName, FileMode.OpenOrCreate))
                {
                    returnData = false;
                }
            }
            catch (IOException)
            {
                returnData = true;
            }

            return returnData;
        }

        /// <summary>
        /// Read CSV n Text file
        /// </summary>
        /// <param name="fileNameWithPath">file Name with Path</param>
        /// <returns>build Datatable</returns>
        private DataTable ReadCSVnTextFile(string fileNameWithPath)
        {
            string tableName = string.Empty;
            string fileName = Path.GetFileNameWithoutExtension(fileNameWithPath);
            string excelQuery;
            int rowIndex;

            DataTable returnData = new DataTable();
            try
            {

                string connectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + fileNameWithPath
                    + ";Extended Properties=\"Excel 12.0;IMEX=2;HDR=YES;TypeGuessRows=0;ImportMixedTypes=Text\"";
                using (OleDbConnection excelConnection = new OleDbConnection(connectionString))
                {
                    excelConnection.Open();
                    using (DataTable dtSheets = excelConnection.GetSchema("Tables"))
                    {
                        rowIndex = 0;
                        tableName = dtSheets.Rows[rowIndex]["TABLE_NAME"].ToString();
                        excelQuery = "SELECT * FROM [" + tableName + "] ";
                        using (OleDbDataAdapter excelDataAdapter = new OleDbDataAdapter(excelQuery, excelConnection))
                        {
                            excelDataAdapter.Fill(returnData);
                            returnData.TableName = fileName;
                        }
                    }
                }
            }
            catch (Exception)
            {
                /*this.DebugPrint(System.DateTime.Now.ToLongTimeString());*/
                returnData = ReadTextFile(fileNameWithPath);
                returnData.TableName = fileName + tableName;
                /*this.DebugPrint(System.DateTime.Now.ToLongTimeString());*/
            }

            return returnData;
        }

        /// <summary>
        /// Read Text file
        /// </summary>
        /// <param name="fileNameWithPath">file Name with Path</param>
        /// <returns>build Datatable</returns>
        private DataTable ReadTextFile(string fileNameWithPath)
        {
            DataTable returnData = new DataTable();
            string rowLine;
            string[] columnsofRowLine;
            int columnCount;
            int columnIndex;

            try
            {

                string fileName = Path.GetFileNameWithoutExtension(fileNameWithPath);

                using (StreamReader readFile = new StreamReader(fileNameWithPath))
                {
                    rowLine = readFile.ReadLine();
                    columnsofRowLine = rowLine.Split(';');
                    columnCount = columnsofRowLine.Length;

                    for (columnIndex = 0; columnIndex < columnCount; columnIndex++)
                    {
                        returnData.Columns.Add(columnsofRowLine[columnIndex]);
                        returnData.Columns[columnsofRowLine[columnIndex]].DataType = this.GetDataType(fileName, columnsofRowLine[columnIndex]);
                    }

                    rowLine = readFile.ReadLine();
                    if (rowLine == null)
                    {
                    }
                    else
                    {
                        int rowId = 0;
                        int rowIndex = 1;
                        if (fileName.ToString().ToUpper() == "CIC_DOS")
                        {
                            do
                            {
                                rowId = 0;
                                rowLine = rowLine.Replace(",", ".").Replace(Convert.ToChar(34).ToString(), string.Empty);
                                rowLine = rowLine.Replace("; ", ";").Replace(" ;", ";");

                                columnsofRowLine = rowLine.Split(';');
                                try
                                {
                                    if (!string.IsNullOrEmpty(columnsofRowLine[1]))
                                    {
                                        rowId = ConvertToInt32(columnsofRowLine[309].ToString().Trim());
                                        if (rowId > 0)
                                        {
                                            if (returnData.Select("NR_FC ='" + rowId.ToString() + "'").Length == 0)
                                            {
                                                for (int columnsofRowLineIndex = 0; columnsofRowLineIndex < columnsofRowLine.Length; columnsofRowLineIndex++)
                                                {
                                                    columnsofRowLine[columnsofRowLineIndex] = columnsofRowLine[columnsofRowLineIndex].ToString().Trim();
                                                }

                                                ////this.DebugPrint(rowId.ToString() + " " + rowIndex.ToString());
                                                returnData.Rows.Add(columnsofRowLine);
                                            }
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    this.DebugPrint(rowLine);
                                    Global.Global.WriteLog("ReadTextFile File Name:" + fileName, LogMessageType.Error, ex.Message);
                                }

                                rowIndex++;
                                rowLine = readFile.ReadLine();
                            }
                            while (rowLine != null);
                        }
                        else
                        {
                            do
                            {
                                rowLine = rowLine.Replace(",", ".").Replace(Convert.ToChar(34).ToString(), string.Empty);
                                columnsofRowLine = rowLine.Split(';');

                                try
                                {
                                    returnData.Rows.Add(columnsofRowLine);
                                }
                                catch (Exception ex)
                                {
                                    Global.Global.WriteLog("ReadTextFile File Name:" + fileName, LogMessageType.Error, ex.Message);
                                }

                                rowLine = readFile.ReadLine();
                            }
                            while (rowLine != null);

                            returnData.Rows.RemoveAt(0);
                        }

                    }
                }

            }
            catch (Exception ex)
            {
                throw (ex);
            }

            return returnData;
        }

        /// <summary>
        /// Get Data Type of the Column
        /// </summary>
        /// <param name="tableNameasFileName">table Name</param>
        /// <param name="columName">Column Name</param>
        /// <returns>Type of the System</returns>
        private Type GetDataType(string tableNameasFileName, string columName)
        {
            Type returnData = typeof(System.String);
            foreach (DataRow dtcsvrow in xmlCSV_Tables_Details.Select("TableName = '" + tableNameasFileName + "' AND colName = '" + columName + "'"))
            {
                if (dtcsvrow["DataType"].ToString().Trim().Length > 0)
                {
                    if (dtcsvrow["DataType"].ToString().Trim() == "int")
                    {
                        returnData = typeof(Int32);
                    }
                    else if (dtcsvrow["Value"].ToString().Trim() == "decimal")
                    {
                        returnData = typeof(decimal);
                    }
                    else if (dtcsvrow["Value"].ToString().Trim() == "double")
                    {
                        returnData = typeof(double);
                    }
                    else if (dtcsvrow["Value"].ToString().Trim() == "string")
                    {
                        returnData = typeof(string);
                    }
                    else if (dtcsvrow["Value"].ToString().Trim() == "date")
                    {
                        returnData = typeof(DateTime);
                    }
                }
            }

            return returnData;
        }

        /// <summary>
        /// Compare the CSV Datatable (file's) and Insert in Access Database
        /// </summary>
        /// <param name="excelMainDataTable">Main DataTable</param>
        /// <param name="excelReplicatDataTable">Replica DataTable</param>
        /// <param name="mainFilePath">Main file Name and Path</param>
        /// <param name="replicaFilePath">Replicat file Name and Path</param>
        private void CompareFileAndInsertInAccess(
            DataTable excelMainDataTable,
            DataTable excelReplicatDataTable,
            string mainFilePath,
            string replicaFilePath,
            string sourceConnectionString,
            string targetConnectionString)
        {
            try
            {
                int replaceFile = 0;
                var varMismatch1 = excelMainDataTable.AsEnumerable().Except(excelReplicatDataTable.AsEnumerable(), DataRowComparer.Default);
                PNF.Ut_sysUtility objnew = new PNF.Ut_sysUtility();
                string csvFileName = excelMainDataTable.TableName;
                if (varMismatch1 != null && varMismatch1.Count() > 0)
                {
                    #region [ Insert in Main Access Database ]

                    if (excelMainDataTable.TableName.ToString().ToUpper() == "CIC_DOS")
                    {
                        DataRow[] dtfilter = excelMainDataTable.Select("nr_bolla <> '0'");
                        excelMainDataTable = dtfilter.CopyToDataTable();
                    }

                    string tableName = string.Empty;
                    string columnName = string.Empty;
                    string[] columnsList = new string[xmlCSV_Tables_Details.Select("TableName = '" + csvFileName + "'").Count() - 1];
                    int columnIndex = 0;

                    for (int i = 0; i < excelMainDataTable.Columns.Count; i++)
                    {
                        Debug.Print(":" + excelMainDataTable.Columns[i].ColumnName.ToString() + ":");
                    }

                    foreach (DataRow dtcsvrow in xmlCSV_Tables_Details.Select("TableName = '" + csvFileName + "'"))
                    {
                        if (dtcsvrow["ColName"].ToString().Trim().ToUpper() == csvFileName.Trim().ToUpper())
                        {
                            tableName = dtcsvrow["Value"].ToString().Trim();
                        }
                        else
                        {
                            columnName = dtcsvrow["ColName"].ToString();

                            Debug.Print("1:" + columnName + ":");

                            if (excelMainDataTable.Columns.Contains(columnName))
                            {
                                Debug.Print("2:" + columnName + ":");
                                excelMainDataTable.Columns[columnName].ColumnName = dtcsvrow["Value"].ToString().Trim();
                                columnsList[columnIndex] = dtcsvrow["Value"].ToString().Trim();

                                columnIndex++;
                            }
                        }
                    }

                    DataTable dtMainSave = new DataTable(tableName);
                    dtMainSave = new DataView(excelMainDataTable).ToTable(true, columnsList);
                    dtMainSave.TableName = tableName;

                    #region [ Delete Duplicate Records ]
                    List<string> keyColumns1 = new List<string>();
                    DataRow[] dtrowWhere = xmlSync_Tables_Details.Select("XmlRelation ='" + tableName.ToString() + "' and XmlName='Key'");
                    if (dtrowWhere.Count() > 0)
                    {
                        foreach (DataRow dtrow in dtrowWhere)
                        {
                            keyColumns1.Add(dtrow["XmlKey"].ToString());
                        }
                    }

                    this.RemoveDuplicatesRows(dtMainSave, keyColumns1);

                    #endregion [ Delete Duplicate Records ]

                    replaceFile = objnew.InsertCSVDataToMainDatabase(
                        dtMainSave,
                        Program.PlantAppVersion,
                        true,
                        sourceConnectionString,
                        targetConnectionString);

                    #endregion [ Insert in Main Access Database]

                    if (replaceFile > 0)
                    {
                        if (File.Exists(replicaFilePath))
                        {
                            File.Delete(replicaFilePath);
                        }

                        File.Copy(mainFilePath, replicaFilePath);
                    }
                }
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        #endregion [ Execute CSV Format Files ]

        #region [ Comparing the Access Database and Upload to Web Service]
        /// <summary>
        /// Upload Data to Live SQL Server
        /// </summary>
        /// <param name="dsDetails"></param>
        private void UploadDataToWebService(
            DataSet dsDetails,
            string targetConnectionString,
            bool local_Replica,
            int forIndex,
            bool skipLocalUpload,
            string targetConnectionString_MCGM,
            string sourceConnectionString
            )
        {
            int errorCode = 0;
            string errorMessage = string.Empty;
            string tableName = string.Empty;
            string preTableName = string.Empty;
            DataSet dsResponse = new DataSet();
            string uploaded_to = string.Empty;
            string returnQuery = string.Empty;

            uploaded_to = local_Replica == true ? "Local :" : "Live :";
            try
            {
                bool uploadtoLive = false;
                #region [ Upload the Data Tables ]
                foreach (DataTable dtUpload in dsDetails.Tables)
                {
                    dsResponse = new DataSet();
                    tableName = dtUpload.TableName.ToString();
                    preTableName = tableName;

                    #region [ Rename the Data Table ]

                    /*Check alias name and Set it to alias name*/
                    DataRow[] dtrow = xmlSync_Tables_Details.Select("XmlRelation ='" + tableName + "' and XmlName='TableAliasNm'");
                    if (dtrow.Count() > 0)
                    {
                        preTableName = tableName;
                        dsResponse.Tables.AddRange(new DataTable[] { dtUpload.Copy() });
                        dsResponse.Tables[0].TableName = dtrow[0]["xmlKey"].ToString();
                    }
                    else
                    {
                        preTableName = tableName;
                        dsResponse.Tables.AddRange(new DataTable[] { dtUpload.Copy() });
                    }

                    #endregion[ Rename the Data Table ]

                    DateTime clientDateTime = DateTime.Now;
                    dsResponse.ExtendedProperties["UTCDifference"] = TimeZone.CurrentTimeZone.GetUtcOffset(clientDateTime).Ticks.ToString();

                    uploadtoLive = false;
                    if (local_Replica == true)
                    {
                        if (Program.ConfigApplication_Type == "1")
                        {
                            #region [ Upload Local ]
                            using (Scada_Sync_WebService.PNF_GM_Sync_WSSoapClient local_Client = new Scada_Sync_WebService.PNF_GM_Sync_WSSoapClient())
                            {
                                Scada_Sync_WebService.paramUploadData_en local_Parameters = new Scada_Sync_WebService.paramUploadData_en();
                                local_Parameters.DataSetToUpload = dsResponse;

                                Scada_Sync_WebService.TransactionHeader local_Transaction = new Scada_Sync_WebService.TransactionHeader();
                                local_Transaction.DeviceID = Program.ConfigMachine_Id;
                                local_Transaction.Password = Program.ConfigService_Password;
                                local_Transaction.UserName = Program.ConfigService_LoginID;
                                local_Transaction.Config_App_Version = Program.ConfigAPP_Version;
                                local_Transaction.Config_App_Name = Program.ConfigAPP_Name;
                                local_Transaction.Win_Service_GUID = Program.Win_Service_GUID;
                                local_Transaction.Win_Service_Version = Program.Win_Service_Version;

                                Scada_Sync_WebService.Response_UploadData local_Response;

                                if (Program.Config_only_insert_web_service == true)
                                {
                                    local_Response = local_Client.SendDatatoSQL_ReadRecord(local_Transaction, local_Parameters);
                                }
                                else
                                {
                                    local_Response = local_Client.SendDatatoSQL(local_Transaction, local_Parameters);
                                }

                                errorCode = local_Response.ErrorCode;
                                errorMessage = local_Response.ErrorMessage;
                            }
                            #endregion [ Upload Local ]
                        }
                        else
                        {
                            uploadtoLive = true;
                        }
                    }
                    else
                    {
                        uploadtoLive = true;
                    }

                    if (uploadtoLive == true)
                    {
                        #region [ Upload Live ]

                        //using (Scada_Sync_WebService.Skada_Sync_WSSoapClient live_Client = new Scada_Sync_WebService.Skada_Sync_WSSoapClient())
                        //{
                        //    Scada_Sync_WebService.paramUploadData_en live_Parameters = new Scada_Sync_WebService.paramUploadData_en();
                        //    live_Parameters.DataSetToUpload = dsResponse;

                        //    Scada_Sync_WebService.TransactionHeader live_Transaction = new Scada_Sync_WebService.TransactionHeader();
                        //    live_Transaction.DeviceID = Program.ConfigMachine_Id;
                        //    live_Transaction.Password = Program.ConfigService_Password;
                        //    live_Transaction.UserName = Program.ConfigService_LoginID;
                        //    live_Transaction.Config_App_Version = Program.ConfigAPP_Version;
                        //    live_Transaction.Config_App_Name = Program.ConfigAPP_Name;
                        //    live_Transaction.Win_Service_GUID = Program.Win_Service_GUID;
                        //    live_Transaction.Win_Service_Version = Program.Win_Service_Version;

                        //    Scada_Sync_WebService.Response_UploadData live_Response;
                        //    if (Program.Config_only_insert_web_service == true)
                        //    {
                        //        live_Response = live_Client.UploadData_ReadRecord(live_Transaction, live_Parameters);
                        //    }
                        //    else
                        //    {
                        //        live_Response = live_Client.UploadData(live_Transaction, live_Parameters);
                        //    }

                        //    errorCode = live_Response.ErrorCode;
                        //    errorMessage = live_Response.ErrorMessage;
                        //}
                        #endregion [ Upload Live ]
                    }

                    /*reset the old name*/
                    dsResponse.Tables[0].TableName = preTableName;
                }
                #endregion [ Upload the Data Tables ]

                #region [ Insert / Update to Replica Table ]
                if (errorCode == 2627 || errorCode == 1007 || errorCode == 0)
                {
                    if (errorCode == 2627 || errorCode == 1007)
                    {
                        Global.Global.WriteLog(uploaded_to + "UDTWS return from Webservice", LogMessageType.Information, errorMessage.ToString() + " for Table " + tableName);
                    }

                    int recordsAffected = 0;
                    using (PNF.RI_syncUtility objupload = new PNF.RI_syncUtility())
                    {
                        recordsAffected = objupload.UploadAccessData(
                            dsDetails,
                            dsResponse,
                            xmlSync_Tables_Details,
                            Program.PlantAppVersion,
                            targetConnectionString,
                            out returnQuery);

                        if (recordsAffected > 0)
                        {

                            if (this.Check_For_MCGM_Tables(tableName) == true)
                            {
                                #region [ FOR MCGM LIVE WORKS ]
                                int row_Index = 0;
                                int row_Count = dsDetails.Tables[0].Rows.Count;
                                bool is_Insert = false;
                                string batch_No = string.Empty;
                                string batch_Index = string.Empty;
                                string batch_Date = string.Empty;
                                string batch_Time = string.Empty;
                                tableName = tableName.ToLower();
                                if (
                                    tableName == "batch_dat_trans"
                                    || tableName == "batch_result"
                                    )
                                {
                                    for (row_Index = 0; row_Index < row_Count; row_Index++)
                                    {
                                        batch_No = dsDetails.Tables[0].Rows[row_Index]["Batch_No"].ToString();
                                        is_Insert = false;
                                        if (dsDetails.Tables[0].Rows[row_Index]["UpdateInsert"].ToString() == "Insert")
                                        {
                                            is_Insert = true;
                                        }

                                        this.Insert_Update_Batch_Master_Upload_Status(
                                            is_Insert,
                                            tableName == "Batch_Dat_Trans" ? 0 : 1,
                                            sourceConnectionString,
                                            batch_No);
                                    }
                                }
                                else
                                {
                                    for (row_Index = 0; row_Index < row_Count; row_Index++)
                                    {
                                        batch_No = string.Empty;
                                        batch_Index = string.Empty;
                                        batch_Date = string.Empty;
                                        batch_Time = string.Empty;

                                        if (
                                            tableName == "batch_transaction"
                                            || tableName == "vitrag_batch"
                                            )
                                        {
                                            batch_No = dsDetails.Tables[0].Rows[row_Index]["Batch_No"].ToString();
                                            batch_Index = dsDetails.Tables[0].Rows[row_Index]["Batch_Index"].ToString();
                                            batch_Date = Convert.ToDateTime(dsDetails.Tables[0].Rows[row_Index]["Batch_Date"].ToString()).ToString();
                                            batch_Time = Convert.ToDateTime(dsDetails.Tables[0].Rows[row_Index]["Batch_Time"].ToString()).ToString();

                                        }
                                        else if (tableName == "batch")
                                        {
                                            batch_No = dsDetails.Tables[0].Rows[row_Index]["BatchNo"].ToString();
                                            if (dsDetails.Tables[0].Columns.Contains("Date"))
                                            {
                                                batch_Date = Convert.ToDateTime(dsDetails.Tables[0].Rows[row_Index]["Date"].ToString()).ToString();
                                            }

                                            if (dsDetails.Tables[0].Columns.Contains("Time"))
                                            {
                                                if (dsDetails.Tables[0].Columns["Time"].DataType == System.Type.GetType("System.String"))
                                                {
                                                    batch_Time = "01/01/1900 " + dsDetails.Tables[0].Rows[row_Index]["Time"].ToString();
                                                }
                                                else
                                                {
                                                    batch_Time = Convert.ToDateTime(dsDetails.Tables[0].Rows[row_Index]["Time"].ToString()).ToString();
                                                }
                                            }
                                        }
                                        else if (tableName == "readrecord")
                                        {
                                            batch_No = dsDetails.Tables[0].Rows[row_Index]["Row_id"].ToString();
                                            if (dsDetails.Tables[0].Columns.Contains("Date"))
                                            {
                                                batch_Date = Convert.ToDateTime(dsDetails.Tables[0].Rows[row_Index]["Date"].ToString()).ToString();
                                            }

                                            if (dsDetails.Tables[0].Columns.Contains("Time"))
                                            {
                                                batch_Time = Convert.ToDateTime(dsDetails.Tables[0].Rows[row_Index]["Time"].ToString()).ToString();
                                            }
                                        }
                                        else if (
                                            tableName == "tb_batch"
                                            || tableName == "desilting_trips"
                                            )
                                        {
                                            batch_No = dsDetails.Tables[0].Rows[row_Index]["Id"].ToString();

                                            if (dsDetails.Tables[0].Columns.Contains("Batch_time"))
                                            {
                                                batch_Date = Convert.ToDateTime(dsDetails.Tables[0].Rows[row_Index]["Batch_time"].ToString()).ToString();
                                            }

                                            if (dsDetails.Tables[0].Columns.Contains("Time"))
                                            {
                                                batch_Time = string.IsNullOrEmpty(dsDetails.Tables[0].Rows[row_Index]["Time"].ToString()) ? dsDetails.Tables[0].Rows[row_Index]["Batch_time"].ToString() : Convert.ToDateTime(dsDetails.Tables[0].Rows[row_Index]["Time"].ToString()).ToString();
                                            }
                                        }

                                        is_Insert = false;
                                        if (dsDetails.Tables[0].Rows[row_Index]["UpdateInsert"].ToString() == "Insert")
                                        {
                                            is_Insert = true;
                                        }

                                        if (local_Replica == false)
                                        {
                                            is_Insert = false;
                                        }

                                        this.Insert_Update_Batch_Transaction_Upload_Status(is_Insert, 0, sourceConnectionString,
                                            batch_No,       /*batch_No_Or_Row_ID*/
                                            batch_Index,    /*batch_Index*/
                                            batch_Date,     /*batch_Date*/
                                            batch_Time      /*batch_Time*/
                                            );

                                        if (tableName == "batch_transaction")
                                        {
                                            this.Insert_Update_Batch_Master_Upload_Status(false, 2, sourceConnectionString, batch_No);
                                        }

                                    }
                                }

                                #endregion [ FOR MCGM LIVE WORKS ]
                            }

                            forIndex = forIndex + 1;
                            Global.Global.WriteLog(uploaded_to + "UDTWS Access Database Row No. " + forIndex, LogMessageType.Information, string.Format("{0} ({1} row(s) affected)", tableName, recordsAffected));

                            if (
                                Program.ConfigApplication_Type == "0"
                                || uploadtoLive
                                || skipLocalUpload
                                )
                            {
                                if (string.IsNullOrEmpty(targetConnectionString_MCGM) == false)
                                {
                                    int recordsAffected_MCGM = 0;
                                    int rowIndex = 0;
                                    string UTCDifference = string.Empty;
                                    string xmlKeyName = "XmlKey";
                                    DataTable dtDownloadedMain = dsDetails.Tables[0];
                                    DataRow[] dtrXMLrow;
                                    DataColumn[] dtcPrimaryColumns_MCGM;

                                    dtDownloadedMain.PrimaryKey = null;
                                    dtrXMLrow = xmlSync_Tables_Details.Select("XmlRelation ='" + tableName + "' and XmlName='Key'");
                                    dtcPrimaryColumns_MCGM = new DataColumn[dtrXMLrow.Length];

                                    for (rowIndex = 0; rowIndex < dtcPrimaryColumns_MCGM.Count(); rowIndex++)
                                    {
                                        dtcPrimaryColumns_MCGM[rowIndex] = dtDownloadedMain.Columns[dtrXMLrow[rowIndex][xmlKeyName].ToString()];
                                    }

                                    dtDownloadedMain.PrimaryKey = dtcPrimaryColumns_MCGM;

                                    UTCDifference = dsResponse.ExtendedProperties["UTCDifference"].ToString();

                                    dtDownloadedMain = objupload.RemoveUnwantedFields(dtDownloadedMain, targetConnectionString);
                                    dtDownloadedMain.AcceptChanges();

                                    recordsAffected_MCGM = objupload.InsertAccessDatabaseWithDownloadedData_MCGM(
                                        dtDownloadedMain,
                                        Program.PlantAppVersion,
                                        UTCDifference,
                                        targetConnectionString_MCGM,
                                        out returnQuery
                                        );

                                    if (recordsAffected_MCGM > 0)
                                    {
                                        Global.Global.WriteLog(uploaded_to + "UDTWS Access Database Row No. " + forIndex, LogMessageType.Information, string.Format("{0} ({1} row(s) affected)", tableName, recordsAffected_MCGM), true);
                                    }

                                }
                            }
                        }
                    }
                }
                else if (errorCode < 0)
                {
                    Global.Global.WriteLog(uploaded_to + "UDTWS Access Database", LogMessageType.UploadError, errorMessage.ToString());
                    Global.Global.WriteLog("UDTWS Access Database", LogMessageType.Information, returnQuery);
                }
                #endregion [ Insert / Update to Replica Table ]
            }
            catch (CommunicationException ex)
            {
                Global.Global.WriteLog(uploaded_to + "UDTWS(CommunicationException)", LogMessageType.Error, ex.Message.ToString());
            }
            catch (Exception ex)
            {
                Global.Global.WriteLog(uploaded_to + "UDTWS(Exception)", LogMessageType.UploadError, ex.Message.ToString());
            }
        }
        #endregion [ Comparing the Access Database and Upload to Web Service]

        #region [ ReSync Data ]
        /// <summary>
        /// delete record from replica database for sync again :
        /// </summary>
        /// <param name="targetConnectionString"></param>
        /// <returns></returns>
        private bool Delete_SyncBatchData(string targetConnectionString)
        {
            bool returnData = false;
            DataSet dtset;
            DataTable dtresponseReSyncData;
            try
            {
                if (Program.ConfigApplication_Type == "1")
                {
                    using (Scada_Sync_WebService.PNF_GM_Sync_WSSoapClient local_Client = new Scada_Sync_WebService.PNF_GM_Sync_WSSoapClient())
                    {
                        Scada_Sync_WebService.TransactionHeader local_Transaction = new Scada_Sync_WebService.TransactionHeader();
                        local_Transaction.DeviceID = Program.ConfigMachine_Id;
                        local_Transaction.Password = Program.ConfigService_Password;
                        local_Transaction.UserName = Program.ConfigService_LoginID;
                        local_Transaction.Config_App_Version = Program.ConfigAPP_Version;
                        local_Transaction.Config_App_Name = Program.ConfigAPP_Name;
                        local_Transaction.Win_Service_GUID = Program.Win_Service_GUID;
                        local_Transaction.Win_Service_Version = Program.Win_Service_Version;

                        Scada_Sync_WebService.Respone_ReSyncData local_Response = local_Client.ReSync_Batchs(local_Transaction);
                        dtset = local_Response.ReSync_Batchs;
                        if (dtset.Tables.Count > 0)
                        {
                            try
                            {
                                dtresponseReSyncData = new DataTable();
                                using (PNF.RI_syncUtility objupload = new PNF.RI_syncUtility())
                                {
                                    dtresponseReSyncData = objupload.Deleted_ResyncBatchdata(dtset.Tables[0], xmlSync_Tables_Details, Program.PlantAppVersion, targetConnectionString);
                                }

                                if (dtresponseReSyncData.Rows.Count > 0)
                                {
                                    Scada_Sync_WebService.paramDownloadSyncStatus objResync = new Scada_Sync_WebService.paramDownloadSyncStatus();
                                    objResync.DownloadedSyncedData = dtset.Tables[0];
                                    objResync.SyncType = Scada_Sync_WebService.SyncDataType.ReSyncData;

                                    Scada_Sync_WebService.Response_DownloadedSyncStaus objResposeDownload = local_Client.Update_SyncStaus(local_Transaction, objResync);
                                    returnData = true;
                                    Global.Global.WriteLog("Delete_SyncBatchData : Message from Webservice for ReSync data", LogMessageType.Information, objResposeDownload.ErrorMessage.ToString());
                                }
                                else
                                {
                                    Global.Global.WriteLog("Delete_SyncBatchData Sync data update 0.1", LogMessageType.Error, "Sync Data not update.");
                                }
                            }
                            catch (Exception ex)
                            {
                                Global.Global.WriteLog("Delete_SyncBatchData Sync data update 0.2", LogMessageType.Error, ex.Message);
                            }
                        }
                    }
                }
                else
                {
                    //using (Scada_Sync_WebService.Skada_Sync_WSSoapClient live_Client = new Scada_Sync_WebService.Skada_Sync_WSSoapClient())
                    //{
                    //    Scada_Sync_WebService.TransactionHeader live_Transaction = new Scada_Sync_WebService.TransactionHeader();
                    //    live_Transaction.DeviceID = Program.ConfigMachine_Id;
                    //    live_Transaction.Password = Program.ConfigService_Password;
                    //    live_Transaction.UserName = Program.ConfigService_LoginID;
                    //    live_Transaction.Config_App_Version = Program.ConfigAPP_Version;
                    //    live_Transaction.Config_App_Name = Program.ConfigAPP_Name;
                    //    live_Transaction.Win_Service_GUID = Program.Win_Service_GUID;
                    //    live_Transaction.Win_Service_Version = Program.Win_Service_Version;

                    //    Scada_Sync_WebService.Respone_ReSyncData live_Response = live_Client.ReSyncBatchData(live_Transaction);
                    //    dtset = live_Response.ReSyncBatchData;
                    //    if (dtset.Tables.Count > 0)
                    //    {
                    //        try
                    //        {
                    //            dtresponseReSyncData = new DataTable();
                    //            using (PNF.RI_syncUtility objupload = new PNF.RI_syncUtility())
                    //            {
                    //                dtresponseReSyncData = objupload.Deleted_ResyncBatchdata(dtset.Tables[0], xmlSync_Tables_Details, Program.PlantAppVersion, targetConnectionString);
                    //            }

                    //            if (dtresponseReSyncData.Rows.Count > 0)
                    //            {
                    //                Scada_Sync_WebService.paramDownloadSyncStatus objResync = new Scada_Sync_WebService.paramDownloadSyncStatus();
                    //                objResync.DownloadedSyncedData = dtset.Tables[0];
                    //                objResync.SyncType = Scada_Sync_WebService.SyncDataType.ReSyncData;

                    //                Scada_Sync_WebService.Response_DownloadedSyncStaus objResposeDownload = live_Client.DownloadedSyncStaus(live_Transaction, objResync);
                    //                returnData = true;
                    //                Global.Global.WriteLog("Delete_SyncBatchData : Message from Webservice for ReSync data", LogMessageType.Information, objResposeDownload.ErrorMessage.ToString());
                    //            }
                    //            else
                    //            {
                    //                Global.Global.WriteLog("Delete_SyncBatchData Sync data update 0.3", LogMessageType.Error, "Sync Data not update.");
                    //            }
                    //        }
                    //        catch (Exception ex)
                    //        {
                    //            Global.Global.WriteLog("Delete_SyncBatchData Sync data update 0.4", LogMessageType.Error, ex.Message);
                    //        }
                    //    }
                    //}
                }
            }
            catch (CommunicationException ex)
            {
                Global.Global.WriteLog("M N Delete_SyncDate ", LogMessageType.Error, ex.Message.ToString());
            }
            catch (Exception exDe)
            {
                Global.Global.WriteLog("M N Delete_SyncDate Deleting Batch data", LogMessageType.DownloadError, exDe.Message.ToString());
            }

            return returnData;
        }
        #endregion [ ReSync Data ]

        #region [if (Program.ConfigApplication_Version == "1")]

        #region [ Download the Data ]
        /// <summary>
        /// Download the Data from Live SQL Server
        /// </summary>
        private bool DownloadDataFromWebService(
            string sourceConnectionString,
            string targetConnectionString,
            string targetConnectionString_MCGM)
        {

            bool returnData = false;
            int recordsAffected = 0;
            int tableIndex = 0;

            string sqlTableName;
            string accessTableName;
            string tableName = string.Empty;
            string errorMessage = string.Empty;
            string UTCDifference = string.Empty;
            string NameBatch_Dat_Trans = "Batch_Dat_Trans";
            string[] tableList;
            string returnQuery = string.Empty;


            DataSet dtResponseModified = null;
            DataTable dtDownloadedMain = new DataTable();
            DataTable dtResponse4WebService = new DataTable();
            DataTable downloadDataTable = new DataTable();
            DataTable dtBatch_Dat_Trans = null;
            string line_no = "0";
            try
            {
                #region [ Get the Data from Local SQL Server Db ]
                Scada_Sync_WebService.TransactionHeader local_Transaction = new Scada_Sync_WebService.TransactionHeader();
                local_Transaction.DeviceID = Program.ConfigMachine_Id;
                local_Transaction.Password = Program.ConfigService_Password;
                local_Transaction.UserName = Program.ConfigService_LoginID;
                local_Transaction.Config_App_Version = Program.ConfigAPP_Version;
                local_Transaction.Config_App_Name = Program.ConfigAPP_Name;
                local_Transaction.Win_Service_GUID = Program.Win_Service_GUID;
                local_Transaction.Win_Service_Version = Program.Win_Service_Version;

                /*for variable local_Response*/
                {
                    Scada_Sync_WebService.Response_ModifiedBatchData local_Response;
                    using (Scada_Sync_WebService.PNF_GM_Sync_WSSoapClient local_Client = new Scada_Sync_WebService.PNF_GM_Sync_WSSoapClient())
                    {
                        local_Response = local_Client.Fetch_ModifiedBatchs(local_Transaction);
                    }
                    dtResponseModified = local_Response.Upgraded_Batchs;
                }
                line_no = "1";
                if (dtResponseModified.Tables.Count == 0)
                {
                    return returnData;
                }

                #endregion [ Get the Data from Local SQL Server Db ]

                #region [ Validation ]

                tableList = new string[dtResponseModified.Tables.Count];
                tableIndex = 0;
                sqlTableName = string.Empty;
                accessTableName = string.Empty;
                foreach (DataTable item in dtResponseModified.Tables)
                {
                    sqlTableName = item.TableName;
                    if (dtTablesList.Select("TABLE_NAME = '" + sqlTableName + "'").Length <= 0)
                    {
                        accessTableName = string.Empty;
                        if (GetAccessTableName(sqlTableName, out accessTableName, Program.ConfigMAP_TABLE_FORMAT))
                        {
                            item.TableName = accessTableName;
                        }
                        else
                        {
                            tableList[tableIndex] = sqlTableName;
                            tableIndex++;
                        }
                    }
                }

                foreach (string tableName_1 in tableList)
                {
                    tableName = tableName_1;
                    if (!string.IsNullOrEmpty(tableName))
                    {
                        dtResponseModified.Tables.Remove(tableName);
                    }
                }

                tableIndex = 0;
                tableList = new string[dtResponseModified.Tables.Count];
                foreach (DataTable item in dtResponseModified.Tables)
                {
                    sqlTableName = item.TableName;
                    if (xmlSync_Tables_Details.Select("XmlRelation = '" + sqlTableName + "'").Length <= 0)
                    {
                        accessTableName = string.Empty;
                        tableList[tableIndex] = sqlTableName;
                        tableIndex++;
                    }
                }

                foreach (string tableName_1 in tableList)
                {
                    tableName = tableName_1;
                    if (!string.IsNullOrEmpty(tableName))
                    {
                        dtResponseModified.Tables.Remove(tableName);
                    }
                }


                #endregion [ Validation ]
                line_no = "2";

                Global.Global.WriteLog("DDFWS Sync data updating", LogMessageType.Information, "Sync Data found.");
                dtDownloadedMain = new DataTable();
                try
                {
                    #region [ Insert / Update the Access DB n Upload the Acknowledgement to Local SQL Server DB ]
                    UTCDifference = dtResponseModified.ExtendedProperties["UTCDifference"].ToString();
                    dtResponse4WebService = new DataTable();
                    downloadDataTable = new DataTable();
                    DataTable dtIS_MCGM_Batch = new DataTable();
                    using (PNF.RI_syncUtility objupload = new PNF.RI_syncUtility())
                    {
                        #region [ Insert / Update to Main n Replica Access DB ]

                        if (dtResponseModified.Tables.Contains(NameBatch_Dat_Trans))
                        {
                            dtBatch_Dat_Trans = dtResponseModified.Tables[NameBatch_Dat_Trans];
                            if (
                                Program.PlantAppVersion == "2"
                                || Program.PlantAppVersion == "2.0"
                                || Program.PlantAppVersion == "2.00"
                                || Program.PlantAppVersion == "3"
                                || Program.PlantAppVersion == "3.0"
                                || Program.PlantAppVersion == "3.00"
                                || Program.PlantAppVersion == "6"
                                || Program.PlantAppVersion == "6.0"
                                || Program.PlantAppVersion == "6.00"
                                || Program.PlantAppVersion == "6.2"
                                || Program.PlantAppVersion == "6.20"
                                )
                            {
                                dtIS_MCGM_Batch = dtBatch_Dat_Trans.Select("IS_MCGM = 1").CopyToDataTable();
                            }

                            dtResponseModified.Tables.Remove(NameBatch_Dat_Trans);
                        }

                        if (
                                Program.PlantAppVersion == "2"
                                || Program.PlantAppVersion == "2.0")
                        {
                            dtIS_MCGM_Batch = dtResponseModified.Tables[0].Select("IS_MCGM = 1").CopyToDataTable();

                            string[] columnsList = new string[2];
                            int columnListIndex = 0;
                            columnsList[columnListIndex] = "BatchNo";
                            columnListIndex++;
                            columnsList[columnListIndex] = "IS_MCGM";

                            dtIS_MCGM_Batch = dtIS_MCGM_Batch.DefaultView.ToTable(true, columnsList);

                        }

                        recordsAffected = 0;
                        tableName = string.Empty;
                        line_no = "3";

                        for (int i = 0; i < dtResponseModified.Tables.Count; i++)
                        {

                            dtDownloadedMain = new DataTable();
                            dtDownloadedMain = dtResponseModified.Tables[i];
                            tableName = dtDownloadedMain.TableName.ToLower();

                            if (Program.Config_write_XML_Download)
                            {
                                this.Write_XML_File("Download", dtDownloadedMain, MethodBase.GetCurrentMethod().Name + "Other_Tables");
                            }

                            if (Program.ConfigMAP_TABLE_FORMAT)
                            {
                                dtDownloadedMain = this.RenameFieldsofDataTable(dtDownloadedMain);
                            }
                            line_no = "4.1." + i.ToString();

                            if (tableName == "batch_transaction")
                            {
                                line_no = "4.1.1." + i.ToString();
                                #region [ Batch Transaction ]
                                DataRow[] dtrDownloadedMain;
                                DataColumn[] dtcPrimaryKeyList = new DataColumn[dtDownloadedMain.PrimaryKey.Count()];

                                dtcPrimaryKeyList = dtDownloadedMain.PrimaryKey;

                                int rowIndex = 0;
                                int primaryKeyListCount = dtDownloadedMain.PrimaryKey.Count();

                                dtrDownloadedMain = dtDownloadedMain.Select("IsNewRecord = 'false'");
                                if (dtrDownloadedMain.Length > 0)
                                {
                                    downloadDataTable = dtrDownloadedMain.CopyToDataTable();
                                    downloadDataTable.TableName = tableName;

                                    downloadDataTable = objupload.RemoveUnwantedFields(downloadDataTable, sourceConnectionString);
                                    downloadDataTable.AcceptChanges();

                                    downloadDataTable.PrimaryKey = null;
                                    rowIndex = 0;
                                    for (rowIndex = 0; rowIndex < primaryKeyListCount; rowIndex++)
                                    {
                                        dtcPrimaryKeyList[rowIndex] = downloadDataTable.Columns[dtcPrimaryKeyList[rowIndex].ColumnName.ToString()];
                                    }
                                    downloadDataTable.PrimaryKey = dtcPrimaryKeyList;

                                    downloadDataTable.AcceptChanges();
                                    line_no = "4.1.2." + i.ToString();

                                    dtResponse4WebService = objupload.UpdateAccessDatabaseWithDownloadedData(
                                        downloadDataTable,
                                        Program.PlantAppVersion,
                                        UTCDifference,
                                        sourceConnectionString,
                                        targetConnectionString,
                                        out recordsAffected,
                                        out returnQuery
                                        );

                                    line_no = "4.1.3." + i.ToString();
                                }

                                dtrDownloadedMain = dtDownloadedMain.Select("IsNewRecord = 'true'");
                                if (dtrDownloadedMain.Length > 0)
                                {
                                    downloadDataTable = dtrDownloadedMain.CopyToDataTable();
                                    downloadDataTable.TableName = tableName;

                                    downloadDataTable = objupload.RemoveUnwantedFields(downloadDataTable, sourceConnectionString);
                                    downloadDataTable.AcceptChanges();

                                    downloadDataTable.PrimaryKey = null;
                                    rowIndex = 0;
                                    for (rowIndex = 0; rowIndex < primaryKeyListCount; rowIndex++)
                                    {
                                        dtcPrimaryKeyList[rowIndex] = downloadDataTable.Columns[dtcPrimaryKeyList[rowIndex].ColumnName.ToString()];
                                    }

                                    downloadDataTable.PrimaryKey = dtcPrimaryKeyList;

                                    downloadDataTable.AcceptChanges();
                                    line_no = "4.1.4." + i.ToString();

                                    recordsAffected = objupload.InsertAccessDatabaseWithDownloadedData(
                                        downloadDataTable,
                                        Program.PlantAppVersion,
                                        UTCDifference,
                                        sourceConnectionString,
                                        targetConnectionString,
                                        out returnQuery
                                        );
                                    line_no = "4.1.5." + i.ToString();

                                    if (recordsAffected > 0)
                                    {
                                        Global.Global.WriteLog("DDFWS Message from Webservice for Sync data", LogMessageType.Information, "New records inserted in Batch_Transaction table");
                                    }
                                }

                                if (string.IsNullOrEmpty(targetConnectionString_MCGM) == false)
                                {
                                    dtDownloadedMain = objupload.RemoveUnwantedFields(dtDownloadedMain, sourceConnectionString);
                                    dtDownloadedMain.AcceptChanges();

                                    int recordsAffected_MCGM = 0;
                                    recordsAffected_MCGM = objupload.InsertAccessDatabaseWithDownloadedData_MCGM(
                                        dtDownloadedMain,
                                        Program.PlantAppVersion,
                                        UTCDifference,
                                        targetConnectionString_MCGM,
                                        out returnQuery
                                        );

                                    if (recordsAffected > 0)
                                    {
                                        Global.Global.WriteLog(
                                            "DDFWS Message from Webservice for Sync data",
                                            LogMessageType.Information,
                                            string.Format("{0} ({1} row(s) affected)", tableName, recordsAffected_MCGM),
                                            true);
                                    }
                                }

                                #endregion [ Batch Transaction ]
                                line_no = "4.1.6." + i.ToString();
                            }
                            else if (Program.PlantAppVersion == "2" || Program.PlantAppVersion == "2.0")
                            {
                                #region [ PlantAppVersion = 2 / 2.0]
                                DataRow[] dtrDownloadedMain;
                                DataColumn[] dtcPrimaryKeyList = new DataColumn[dtDownloadedMain.PrimaryKey.Count()];

                                dtcPrimaryKeyList = dtDownloadedMain.PrimaryKey;

                                int rowIndex = 0;
                                int primaryKeyListCount = dtDownloadedMain.PrimaryKey.Count();

                                dtrDownloadedMain = dtDownloadedMain.Select("IsNewRecord = 'false'");
                                if (dtrDownloadedMain.Length > 0)
                                {
                                    downloadDataTable = dtrDownloadedMain.CopyToDataTable();
                                    downloadDataTable.TableName = tableName;

                                    downloadDataTable.PrimaryKey = null;
                                    rowIndex = 0;
                                    for (rowIndex = 0; rowIndex < primaryKeyListCount; rowIndex++)
                                    {
                                        dtcPrimaryKeyList[rowIndex] = downloadDataTable.Columns[dtcPrimaryKeyList[rowIndex].ColumnName.ToString()];
                                    }
                                    downloadDataTable.PrimaryKey = dtcPrimaryKeyList;

                                    downloadDataTable = objupload.RemoveUnwantedFields(downloadDataTable, sourceConnectionString);
                                    downloadDataTable.AcceptChanges();

                                    dtResponse4WebService = objupload.UpdateAccessDatabaseWithDownloadedData(
                                        downloadDataTable,
                                        Program.PlantAppVersion,
                                        UTCDifference,
                                        sourceConnectionString,
                                        targetConnectionString,
                                        out recordsAffected,
                                        out returnQuery
                                        );
                                }

                                dtrDownloadedMain = dtDownloadedMain.Select("IsNewRecord = 'true'");
                                if (dtrDownloadedMain.Length > 0)
                                {
                                    downloadDataTable = dtrDownloadedMain.CopyToDataTable();
                                    downloadDataTable.TableName = tableName;

                                    downloadDataTable.PrimaryKey = null;
                                    rowIndex = 0;
                                    for (rowIndex = 0; rowIndex < primaryKeyListCount; rowIndex++)
                                    {
                                        dtcPrimaryKeyList[rowIndex] = downloadDataTable.Columns[dtcPrimaryKeyList[rowIndex].ColumnName.ToString()];
                                    }

                                    downloadDataTable.PrimaryKey = dtcPrimaryKeyList;

                                    downloadDataTable = objupload.RemoveUnwantedFields(downloadDataTable, sourceConnectionString);
                                    downloadDataTable.AcceptChanges();

                                    recordsAffected = objupload.InsertAccessDatabaseWithDownloadedData(
                                        downloadDataTable,
                                        Program.PlantAppVersion,
                                        UTCDifference,
                                        sourceConnectionString,
                                        targetConnectionString,
                                        out returnQuery
                                        );

                                    if (recordsAffected > 0)
                                    {
                                        returnData = true;
                                        Global.Global.WriteLog("DDFWS Message from Webservice for Sync data", LogMessageType.Information, "New records inserted in " + downloadDataTable.TableName + "  table");
                                    }
                                }

                                if (dtResponse4WebService.Rows.Count > 0)
                                {
                                    if (string.IsNullOrEmpty(targetConnectionString_MCGM) == false)
                                    {
                                        dtDownloadedMain = objupload.RemoveUnwantedFields(dtDownloadedMain, sourceConnectionString);
                                        dtDownloadedMain.AcceptChanges();
                                        int recordsAffected_MCGM = 0;
                                        recordsAffected_MCGM = objupload.InsertAccessDatabaseWithDownloadedData_MCGM(
                                            dtDownloadedMain,
                                            Program.PlantAppVersion,
                                            UTCDifference,
                                            targetConnectionString_MCGM,
                                            out returnQuery
                                            );

                                        if (recordsAffected > 0)
                                        {
                                            Global.Global.WriteLog(
                                                "DDFWS Message from Webservice for Sync data",
                                                LogMessageType.Information,
                                                string.Format("{0} ({1} row(s) affected)", tableName, recordsAffected_MCGM),
                                                true);
                                        }
                                    }

                                    Global.Global.WriteLog("DDFWS Sync data update 0.1", LogMessageType.Information, "Sync Data update successfully in Source and Target Database.");

                                    Scada_Sync_WebService.paramDownloadSyncStatus local_Parameters = new Scada_Sync_WebService.paramDownloadSyncStatus();
                                    local_Parameters.DownloadedSyncedData = dtResponse4WebService;
                                    local_Parameters.SyncType = Scada_Sync_WebService.SyncDataType.DownloadedSyncData;
                                    Scada_Sync_WebService.Response_DownloadedSyncStaus local_Respose;
                                    using (Scada_Sync_WebService.PNF_GM_Sync_WSSoapClient local_Client = new Scada_Sync_WebService.PNF_GM_Sync_WSSoapClient())
                                    {
                                        local_Respose = local_Client.Update_SyncStaus(local_Transaction, local_Parameters);
                                        errorMessage = local_Respose.ErrorMessage.ToString();
                                    }

                                    Global.Global.WriteLog("DDFWS Message from Webservice for Sync data", LogMessageType.Information, errorMessage);

                                    returnData = true;
                                }
                                else
                                {
                                    Global.Global.WriteLog("DDFWS Sync data update 0.2:" + tableName, LogMessageType.Error, ":Sync Data not update.");
                                }

                                #endregion [ PlantAppVersion = 2 / 2.0]
                            }
                            else
                            {
                                #region [ Other Tables ]
                                DataTable dtBatch_nos = new DataTable();
                                if (Program.PlantAppVersion == "11")
                                {
                                    dtBatch_nos = dtDownloadedMain.DefaultView.ToTable(true, "batch_No");
                                }

                                downloadDataTable = objupload.RemoveUnwantedFields(dtDownloadedMain, sourceConnectionString);
                                downloadDataTable.AcceptChanges();

                                Global.Global.WriteLog("DDFWS Sync data update 0.2.1", LogMessageType.Information, "No of rows " + downloadDataTable.Rows.Count.ToString());

                                dtResponse4WebService = objupload.UpdateAccessDatabaseWithDownloadedData(
                                    downloadDataTable,
                                    Program.PlantAppVersion,
                                    UTCDifference,
                                    sourceConnectionString,
                                    targetConnectionString,
                                    out recordsAffected,
                                    out returnQuery
                                    );

                                if (dtResponse4WebService.Rows.Count > 0)
                                {
                                    if (Program.PlantAppVersion == "11")
                                    {

                                        int up_Status_Row_Index;
                                        string batch_No = string.Empty;

                                        string field_Name = this.GetField_Name(tableName);
                                        for (up_Status_Row_Index = 0; up_Status_Row_Index < dtResponse4WebService.Rows.Count; up_Status_Row_Index++)
                                        {
                                            batch_No = dtResponse4WebService.Rows[up_Status_Row_Index][field_Name].ToString();
                                            this.Insert_Update_Batch_Transaction_Upload_Status(false, 1, sourceConnectionString,
                                                batch_No, string.Empty, string.Empty, string.Empty);
                                        }

                                        string preTableName = string.Empty;
                                        DataRow[] dtrow = xmlSync_Tables_Details.Select("XmlRelation ='" + dtResponse4WebService.TableName.ToString() + "' and XmlName='TableAliasNm'");
                                        if (dtrow.Count() > 0)
                                        {
                                            preTableName = dtResponse4WebService.TableName.ToString();
                                            dtResponse4WebService = dtBatch_nos;
                                            dtResponse4WebService.TableName = dtrow[0]["xmlKey"].ToString();
                                        }
                                        else
                                        {
                                            if (Program.ConfigMAP_TABLE_FORMAT)
                                            {
                                                preTableName = dtResponse4WebService.TableName.ToString();
                                                dtResponse4WebService = dtBatch_nos;
                                                dtResponse4WebService.TableName = "ASPHALT_BATCH";
                                            }
                                            else
                                            {
                                                preTableName = dtResponse4WebService.TableName.ToString();
                                            }
                                        }

                                    }

                                    Global.Global.WriteLog("DDFWS Sync data update 0.3", LogMessageType.Information, "Sync Data update successfully in Source and Target Database.");

                                    Scada_Sync_WebService.paramDownloadSyncStatus local_Parameters = new Scada_Sync_WebService.paramDownloadSyncStatus();
                                    local_Parameters.DownloadedSyncedData = dtResponse4WebService;
                                    local_Parameters.SyncType = Scada_Sync_WebService.SyncDataType.DownloadedSyncData;
                                    Scada_Sync_WebService.Response_DownloadedSyncStaus local_Respose;
                                    using (Scada_Sync_WebService.PNF_GM_Sync_WSSoapClient local_Client = new Scada_Sync_WebService.PNF_GM_Sync_WSSoapClient())
                                    {
                                        local_Respose = local_Client.Update_SyncStaus(local_Transaction, local_Parameters);
                                        errorMessage = local_Respose.ErrorMessage.ToString();
                                    }

                                    Global.Global.WriteLog("DDFWS Message from Webservice for Sync data", LogMessageType.Information, errorMessage);
                                }
                                else
                                {
                                    if (recordsAffected > 0)
                                    {
                                        if (string.IsNullOrEmpty(targetConnectionString_MCGM) == false)
                                        {
                                            int recordsAffected_MCGM = 0;
                                            recordsAffected_MCGM = objupload.InsertAccessDatabaseWithDownloadedData_MCGM(
                                                downloadDataTable,
                                                Program.PlantAppVersion,
                                                UTCDifference,
                                                targetConnectionString_MCGM,
                                                out returnQuery
                                                );

                                            if (recordsAffected > 0)
                                            {
                                                Global.Global.WriteLog("DDFWS Message from Webservice for Sync data", LogMessageType.Information, string.Format("{0} ({1} row(s) affected)", tableName, recordsAffected_MCGM), true);
                                            }
                                        }

                                        Global.Global.WriteLog("DDFWS Sync data update 0.3", LogMessageType.Information, "Sync Data update successfully in Source and Target Database.");
                                    }
                                    else
                                    {
                                        Global.Global.WriteLog("DDFWS Sync data update 0.4:" + tableName, LogMessageType.Error, ":Sync Data not update.");
                                    }
                                }
                                #endregion [ Other Tables ]
                            }
                        }
                        #endregion [ Insert / Update to Main n Replica Access DB ]
                        line_no = "5";

                        #region [ Insert / Update Batch Dat Trans to Main n Replica Access DB n Upload the Acknowledgement to Local SQL Server DB ]
                        if (dtBatch_Dat_Trans != null)
                        {
                            line_no = "5.0.1";
                            if (Program.Config_write_XML_Download)
                            {
                                this.Write_XML_File("Download", dtDownloadedMain, MethodBase.GetCurrentMethod().Name + "_Dat_Trans");
                            }
                            line_no = "5.0.2";

                            downloadDataTable = objupload.RemoveUnwantedFields(dtBatch_Dat_Trans, sourceConnectionString);
                            downloadDataTable.AcceptChanges();

                            line_no = "5.0.3";
                            Global.Global.WriteLog("DDFWS Message from Webservice for Sync data", LogMessageType.Information, string.Format("{0} ({1} No of row(s))", tableName, downloadDataTable.Rows.Count), true);
                            dtResponse4WebService = objupload.UpdateAccessDatabaseWithDownloadedData(
                                downloadDataTable,
                                Program.PlantAppVersion,
                                UTCDifference,
                                sourceConnectionString,
                                targetConnectionString,
                                out recordsAffected,
                                out returnQuery
                                );

                            line_no = "5.0.4";

                            if (dtResponse4WebService.Rows.Count > 0)
                            {
                                if (string.IsNullOrEmpty(targetConnectionString_MCGM) == false)
                                {
                                    int recordsAffected_MCGM = 0;
                                    recordsAffected_MCGM = objupload.InsertAccessDatabaseWithDownloadedData_MCGM(downloadDataTable, Program.PlantAppVersion, UTCDifference, targetConnectionString_MCGM, out returnQuery);

                                    if (recordsAffected > 0)
                                    {
                                        Global.Global.WriteLog("DDFWS Message from Webservice for Sync data", LogMessageType.Information, string.Format("{0} ({1} row(s) affected)", tableName, recordsAffected_MCGM), true);
                                    }
                                }

                                string batch_No = string.Empty;
                                int mcgm_Row_Index;
                                int mcgm_Row_Count = dtIS_MCGM_Batch.Rows.Count;
                                for (mcgm_Row_Index = 0; mcgm_Row_Index < mcgm_Row_Count; mcgm_Row_Index++)
                                {
                                    batch_No = dtIS_MCGM_Batch.Rows[mcgm_Row_Index]["batch_No"].ToString();
                                    this.Insert_Update_Batch_Transaction_Upload_Status(false, 2, sourceConnectionString,
                                        batch_No, string.Empty, string.Empty, string.Empty);
                                }

                                int html_File_Creation = 0;
                                this.HTML_File_Creation(sourceConnectionString, ref html_File_Creation);                //HTML_File_Creation 8.0

                                Global.Global.WriteLog("DDFWS Sync data update 0.5", LogMessageType.Information, "Sync Data update successfully in Source and Target Database.");
                                Scada_Sync_WebService.paramDownloadSyncStatus local_Parameters = new Scada_Sync_WebService.paramDownloadSyncStatus();
                                local_Parameters.DownloadedSyncedData = dtResponse4WebService;
                                local_Parameters.SyncType = Scada_Sync_WebService.SyncDataType.DownloadedSyncData;
                                /*for variable local_Response*/
                                {
                                    Scada_Sync_WebService.Response_DownloadedSyncStaus local_Response;
                                    using (Scada_Sync_WebService.PNF_GM_Sync_WSSoapClient local_Client = new Scada_Sync_WebService.PNF_GM_Sync_WSSoapClient())
                                    {
                                        local_Response = local_Client.Update_SyncStaus(local_Transaction, local_Parameters);
                                        errorMessage = local_Response.ErrorMessage.ToString();
                                    }
                                }

                                returnData = false;
                                Global.Global.WriteLog("DDFWS Message from Webservice for Sync data", LogMessageType.Information, errorMessage);
                            }
                            //else
                            //{
                            //    Global.Global.WriteLog("DDFWS Sync data update 0.6 line_no = " + line_no + " ", LogMessageType.Error, "Sync Data not update.");
                            //    Global.Global.WriteLog("DDFWS Sync data update 0.6 line_no = " + line_no + " ", LogMessageType.Information, returnQuery);
                            //}
                        }
                        else
                        {

                            if (
                               Program.PlantAppVersion == "2"
                               || Program.PlantAppVersion == "2.0")
                            {
                                string batch_No = string.Empty;
                                int mcgm_Row_Index;
                                int mcgm_Row_Count = dtIS_MCGM_Batch.Rows.Count;
                                for (mcgm_Row_Index = 0; mcgm_Row_Index < mcgm_Row_Count; mcgm_Row_Index++)
                                {
                                    batch_No = dtIS_MCGM_Batch.Rows[mcgm_Row_Index]["batchNo"].ToString();
                                    this.Insert_Update_Batch_Transaction_Upload_Status(false, 2, sourceConnectionString,
                                        batch_No, string.Empty, string.Empty, string.Empty);
                                }

                                int html_File_Creation = 0;
                                this.HTML_File_Creation(sourceConnectionString, ref html_File_Creation);                //HTML_File_Creation 8.0
                            }
                        }
                        #endregion [ Insert / Update Batch Dat Trans to Main n Replica Access DB n Upload the Acknowledgement to Local SQL Server DB ]
                    }

                    #endregion [ Insert / Update the Access DB n Upload the Acknowledgement to Local SQL Server DB ]
                }
                catch (Exception ex)
                {
                    Global.Global.WriteLog("DDFWS Sync data update 0.7 for tableName = " + tableName + " line_no = " + line_no, LogMessageType.Error, ex.Message);
                    Global.Global.WriteLog("DDFWS Sync data update 0.7 for tableName = " + tableName + " line_no = " + line_no, LogMessageType.Information, returnQuery);
                }
            }
            catch (CommunicationException ex)
            {
                Global.Global.WriteLog("DDFWS for tableName = " + tableName + " line_no = " + line_no, LogMessageType.Error, ex.Message.ToString());
            }
            catch (Exception Modifiedex)
            {
                Global.Global.WriteLog("DDFWS Updating Batch data for tableName = " + tableName + " line_no = " + line_no, LogMessageType.DownloadError, Modifiedex.Message.ToString());
            }

            return returnData;
        }
        #endregion [ Download the Data ]

        #region [ Upload/Download Data from Local/Live SQL Server]

        #region [ Upload Local SQL Server to Live SQL Server Data ]

        /// <summary>
        /// Upload Local SQLServer Data To Live SQLServer Data
        /// </summary>
        /// <param name="execute"></param>
        /// <returns></returns>
        private bool UploadLocalSQLServerDataToLiveSQLServerData(int execute)
        {
            bool returnData = false;
            try
            {
                DataSet dsOutPut = new DataSet();
                DataTable dtFinal;
                DataTable dtPrimaryColumnDetails;

                string tableName;
                string columnName;
                int tableIndex;

                #region [ Get Local SQL Server ]

                Scada_Sync_WebService.Response_LocalSQLData local_Response;
                using (Scada_Sync_WebService.PNF_GM_Sync_WSSoapClient local_Client = new Scada_Sync_WebService.PNF_GM_Sync_WSSoapClient())
                {
                    Scada_Sync_WebService.TransactionHeader local_Transaction = new Scada_Sync_WebService.TransactionHeader();
                    local_Transaction.DeviceID = Program.ConfigMachine_Id;
                    local_Transaction.Password = Program.ConfigService_Password;
                    local_Transaction.UserName = Program.ConfigService_LoginID;
                    local_Transaction.Config_App_Version = Program.ConfigAPP_Version;
                    local_Transaction.Config_App_Name = Program.ConfigAPP_Name;
                    local_Transaction.Win_Service_GUID = Program.Win_Service_GUID;
                    local_Transaction.Win_Service_Version = Program.Win_Service_Version;

                    local_Response = local_Client.Get_Except_TableList(local_Transaction, execute, 0);
                }

                #endregion [ Get Local SQL Server ]

                if (local_Response.ErrorCode == -1)
                {
                    Global.Global.WriteLog("ULSSDTLSS", LogMessageType.Error, local_Response.ErrorMessage);
                    returnData = false;
                    return returnData;
                }

                DataTable dtTableNames = new DataTable();
                dtPrimaryColumnDetails = new DataTable();
                if (execute == 3)
                {
                    tableIndex = 0;
                    dtFinal = new DataTable();
                    dtFinal = local_Response.LocalSQLData.Tables[tableIndex];

                    if (dtFinal.Rows.Count > 0)
                    {
                        #region [ Remove Table Name Column from the DataTable ]
                        {
                            tableName = string.Empty;
                            columnName = dtFinal.Columns[0].ColumnName;
                            tableName = dtFinal.Rows[0][columnName].ToString();
                            dtFinal.Columns.Remove(columnName);
                        }
                        #endregion [ Remove Table Name Column from the DataTable ]

                        if (tableName == "Table_PrimaryKey_Details")
                        {
                            string[] columnsName = new string[1];
                            columnsName[0] = "Table_Name";

                            dtTableNames = local_Response.LocalSQLData.Tables[tableIndex].DefaultView.ToTable(true, columnsName);
                        }
                    }

                    if (dtTableNames.Rows.Count > 0)
                    {
                        for (int tableNameIndex = 0; tableNameIndex < dtTableNames.Rows.Count; tableNameIndex++)
                        {
                            tableName = dtTableNames.Rows[tableNameIndex]["Table_Name"].ToString();
                            dtPrimaryColumnDetails = local_Response.LocalSQLData.Tables[tableIndex].Select("Table_Name = '" + tableName + "'").CopyToDataTable();
                            dtPrimaryColumnDetails.TableName = tableName;

                            try
                            {
                                returnData = this.LoopTransactionData(
                                        tableName,
                                        dtPrimaryColumnDetails
                                        );
                            }
                            catch (Exception)
                            {
                            }
                        }
                    }
                }
                else
                {
                    returnData = this.UploadLocalSQLServerDataToLiveSQLServerDataTableWise(
                            local_Response,
                            dtPrimaryColumnDetails
                            );
                }
            }
            catch (Exception ex)
            {
                Global.Global.WriteLog("ULSSDTLSS Exception", LogMessageType.Error, ex.Message);
            }

            return returnData;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="localTransaction"></param>
        /// <param name="liveTransaction"></param>
        /// <param name="dtPrimaryColumnDetails"></param>
        /// <returns></returns>
        private bool LoopTransactionData(
            string tableName,
            DataTable dtPrimaryColumnDetails)
        {
            bool returnData = false;
            Int64 rowValue = 0;
            int rowCounts = 0;
            int forIndex;
            string fieldName = string.Empty;

            tableName = tableName.ToLower();
            if (Program.PlantAppVersion == "1" || Program.PlantAppVersion == "1.0")
            {
                if (tableName == "vitrag_batch")
                {
                    fieldName = "Batch_No";
                }
            }
            else if (Program.PlantAppVersion == "2" || Program.PlantAppVersion == "2.0")
            {
                if (tableName == "batch")
                {
                    fieldName = "BatchNo";
                }
            }
            else if (Program.PlantAppVersion == "11" || Program.PlantAppVersion == "11.0")
            {
                if (tableName == "asphalt_batch")
                {
                    fieldName = "Id";
                }
            }
            else
            {
                if (
                    tableName == "batch_transaction"
                    || tableName == "batch_dat_trans"
                    || tableName == "batch_result"
                    || tableName == "batch_transaction_final"
                    || tableName == "batch_dat_trans_final"
                    || tableName == "batch_result_final"
                    )
                {
                    fieldName = "Batch_No";
                }
            }

            if (string.IsNullOrEmpty(fieldName) == false)
            {
                rowValue = ConvertToInt32(dtPrimaryColumnDetails.Rows[0]["Row_Count"].ToString());
                rowCounts = 1;
            }

            try
            {
                for (forIndex = 0; forIndex <= rowCounts; forIndex++)
                {
                    Scada_Sync_WebService.Response_LocalSQLData local_Response;
                    using (Scada_Sync_WebService.PNF_GM_Sync_WSSoapClient local_Client = new Scada_Sync_WebService.PNF_GM_Sync_WSSoapClient())
                    {
                        Scada_Sync_WebService.TransactionHeader local_Transaction = new Scada_Sync_WebService.TransactionHeader();
                        local_Transaction.DeviceID = Program.ConfigMachine_Id;
                        local_Transaction.Password = Program.ConfigService_Password;
                        local_Transaction.UserName = Program.ConfigService_LoginID;
                        local_Transaction.Config_App_Version = Program.ConfigAPP_Version;
                        local_Transaction.Config_App_Name = Program.ConfigAPP_Name;
                        local_Transaction.Win_Service_GUID = Program.Win_Service_GUID;
                        local_Transaction.Win_Service_Version = Program.Win_Service_Version;

                        string searchCriteria = string.Empty;
                        local_Response = local_Client.GetExceptData_TableName(local_Transaction, tableName, searchCriteria);
                    }

                    if (local_Response.ErrorCode == 0)
                    {
                        if (local_Response.LocalSQLData.Tables[0].Rows.Count > 0)
                        {
                            returnData = this.UploadLocalSQLServerDataToLiveSQLServerDataTableWise(
                                    local_Response,
                                    dtPrimaryColumnDetails);

                            if (string.IsNullOrEmpty(fieldName) == false)
                            {
                                if (Convert.ToInt64(local_Response.LocalSQLData.Tables[0].Compute("MAX(" + fieldName + ")", string.Empty).ToString()) < rowValue)
                                {
                                    rowCounts++;
                                }
                            }
                        }
                        else
                        {
                            break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Global.Global.WriteLog("LoopTransactionData Exception for " + tableName + ":", LogMessageType.Error, ex.Message);
                returnData = false;
            }

            return returnData;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="response_LocalSQLData"></param>
        /// <param name="localTransaction"></param>
        /// <param name="liveTransaction"></param>
        /// <param name="dtPrimaryColumnDetails"></param>
        /// <returns></returns>
        private bool UploadLocalSQLServerDataToLiveSQLServerDataTableWise(
            Scada_Sync_WebService.Response_LocalSQLData response_LocalSQLData,
            DataTable dtPrimaryColumnDetails)
        {
            bool returnData = false;
            #region [ Variables ]

            string oldTableName;

            DataSet dsOutPut;
            DataTable dtUpload;
            DataTable dtFinal;
            string columnName;
            string tableName;
            string globalTableName;

            int startCount;
            int rowCounts;
            int uploadRows;
            int totalUploadRows;
            int endCount;
            int forIndex;
            int tableIndex;
            int tableCount;

            DateTime clientDateTime;

            Scada_Sync_WebService.Response_UploadData live_Response;
            Scada_Sync_WebService.Response_UploadData local_Response;
            #endregion [ Variables ]

            tableCount = response_LocalSQLData.LocalSQLData.Tables.Count;
            try
            {
                #region [ Upload data to Live SQL Server and Update in Replica Table(s) of Local Sql Server ]
                uploadRows = 0;
                totalUploadRows = 0;
                for (tableIndex = 0; tableIndex < tableCount; tableIndex++)
                {
                    dtFinal = new DataTable();
                    dtFinal = response_LocalSQLData.LocalSQLData.Tables[tableIndex];
                    forIndex = 0;

                    if (dtFinal.Rows.Count > 0)
                    {

                        #region [ Remove Table Name Column from the DataTable ]
                        {
                            tableName = string.Empty;
                            globalTableName = string.Empty;
                            columnName = dtFinal.Columns[0].ColumnName;
                            tableName = dtFinal.Rows[0][columnName].ToString().Trim();
                            dtFinal.Columns.Remove(columnName);

                            if (dtFinal.Columns.Contains("GlobalTable_Name"))
                            {
                                globalTableName = dtFinal.Rows[0]["GlobalTable_Name"].ToString();
                                dtFinal.Columns.Remove("GlobalTable_Name");
                            }
                        }
                        #endregion [ Remove Table Name Column from the DataTable ]

                        if (tableName == "Table_PrimaryKey_Details")
                        {
                            dtPrimaryColumnDetails = response_LocalSQLData.LocalSQLData.Tables[tableIndex];
                            dtPrimaryColumnDetails.TableName = tableName;
                        }
                        else
                        {
                            startCount = 0;
                            rowCounts = 2;
                            endCount = Program.ConfigTopCount;

                            if (Program.Config_write_XML_Upload)
                            {
                                dtFinal.TableName = tableName;
                                this.Write_XML_File("Upload", dtFinal, MethodBase.GetCurrentMethod().Name);
                            }

                            #region [ Add Identity_Column Field to DataTable ]

                            DataTable dtReturnTable = dtFinal.Clone();
                            DataColumn identColumn = new DataColumn();
                            identColumn.ColumnName = "Identity_Column";
                            identColumn.DataType = Type.GetType("System.Int32");
                            identColumn.AutoIncrement = true;
                            identColumn.AutoIncrementSeed = 1;
                            identColumn.AutoIncrementStep = 1;

                            dtReturnTable.Columns.Add(identColumn);
                            dtReturnTable.AcceptChanges();

                            dtFinal.Select().CopyToDataTable(dtReturnTable, LoadOption.OverwriteChanges);
                            dtFinal = dtReturnTable;

                            #endregion [ Add Identity_Column Field to DataTable ]

                            Global.Global.WriteLog("Local2Live SQL : ", LogMessageType.Information, "Count for Insert/Update record " + dtFinal.Rows.Count.ToString() + " in table " + tableName);
                            for (forIndex = 0; forIndex <= rowCounts; forIndex++)
                            {
                                returnData = true;
                                dtUpload = dtFinal.Select(string.Format("Identity_Column > {0} and Identity_Column <= {1}", startCount.ToString(), endCount.ToString())).CopyToDataTable();

                                uploadRows = dtUpload.Rows.Count;

                                #region [ Set the Primary Keys to DataTable ]

                                tableName = tableName.ToString().Trim();
                                dtUpload.TableName = tableName;

                                {
                                    DataRow[] dtrXML = dtPrimaryColumnDetails.Select("Table_Name ='" + tableName.ToString() + "'");
                                    if (dtrXML.Length > 0)
                                    {
                                        DataColumn[] dataColumns = new DataColumn[dtrXML.Length + 1];
                                        for (int i = 0; i < dataColumns.Count() - 1; i++)
                                        {
                                            dataColumns[i] = dtUpload.Columns[dtrXML[i]["Column_Name"].ToString().Replace("[", string.Empty).Replace("]", string.Empty)];

                                        }

                                        dtUpload.PrimaryKey = dataColumns;
                                    }
                                }

                                #endregion [ Set the Primary Keys to DataTable ]

                                oldTableName = tableName;
                                #region [ Remove Identity_Column ]
                                tableName = tableName.ToLower();
                                dtUpload.Columns.Remove("Identity_Column");
                                if (
                                        tableName == "vitrag_batch_final"
                                        || tableName == "batch_dat_trans_final"
                                        || tableName == "batch_result_final"
                                        || tableName == "batch_transaction_final"
                                    )
                                {
                                    tableName = tableName.ToUpper().Replace("_FINAL", string.Empty);
                                }
                                else if (tableName == "batch_ver2_final")
                                {
                                    tableName = "Batch";
                                }

                                dtUpload.TableName = tableName;
                                dtUpload.AcceptChanges();
                                #endregion [ Remove Identity_Column ]

                                dsOutPut = new DataSet();
                                dsOutPut.Tables.Add(dtUpload);

                                #region [ Upload data to Live SQL Server ]
                                //using (Scada_Sync_WebService.Skada_Sync_WSSoapClient live_Client = new Scada_Sync_WebService.Skada_Sync_WSSoapClient())
                                //{
                                //    Scada_Sync_WebService.paramUploadData_en live_Parameters = new Scada_Sync_WebService.paramUploadData_en();
                                //    clientDateTime = DateTime.Now;
                                //    dsOutPut.ExtendedProperties["UTCDifference"] = TimeZone.CurrentTimeZone.GetUtcOffset(clientDateTime).Ticks.ToString();
                                //    live_Parameters.DataSetToUpload = dsOutPut;

                                //    Scada_Sync_WebService.TransactionHeader live_Transaction = new Scada_Sync_WebService.TransactionHeader();
                                //    live_Transaction = new Scada_Sync_WebService.TransactionHeader();
                                //    live_Transaction.DeviceID = Program.ConfigMachine_Id;
                                //    live_Transaction.Password = Program.ConfigService_Password;
                                //    live_Transaction.UserName = Program.ConfigService_LoginID;
                                //    live_Transaction.Config_App_Version = Program.ConfigAPP_Version;
                                //    live_Transaction.Config_App_Name = Program.ConfigAPP_Name;
                                //    live_Transaction.Win_Service_GUID = Program.Win_Service_GUID;
                                //    live_Transaction.Win_Service_Version = Program.Win_Service_Version;

                                //    live_Response = live_Client.UploadData(live_Transaction, live_Parameters);
                                //}


                                //if (live_Response.ErrorCode == 0)
                                //{
                                //    totalUploadRows += uploadRows;

                                //    #region [ Upload data in Replica to Local SQL Server ]
                                //    using (Scada_Sync_WebService.PNF_GM_Sync_WSSoapClient local_Client = new Scada_Sync_WebService.PNF_GM_Sync_WSSoapClient())
                                //    {
                                //        Scada_Sync_WebService.paramUploadData_en local_Parameters = new Scada_Sync_WebService.paramUploadData_en();

                                //        dsOutPut.Tables[tableName].TableName = oldTableName;
                                //        tableName = oldTableName;

                                //        local_Parameters.TableName = tableName;
                                //        local_Parameters.GlobalTableName = globalTableName;
                                //        clientDateTime = DateTime.Now;
                                //        dsOutPut.ExtendedProperties["UTCDifference"] = TimeZone.CurrentTimeZone.GetUtcOffset(clientDateTime).Ticks.ToString();
                                //        local_Parameters.DataSetToUpload = dsOutPut;

                                //        Scada_Sync_WebService.TransactionHeader local_Transaction = new Scada_Sync_WebService.TransactionHeader();
                                //        local_Transaction.DeviceID = Program.ConfigMachine_Id;
                                //        local_Transaction.Password = Program.ConfigService_Password;
                                //        local_Transaction.UserName = Program.ConfigService_LoginID;
                                //        local_Transaction.Config_App_Version = Program.ConfigAPP_Version;
                                //        local_Transaction.Config_App_Name = Program.ConfigAPP_Name;
                                //        local_Transaction.Win_Service_GUID = Program.Win_Service_GUID;
                                //        local_Transaction.Win_Service_Version = Program.Win_Service_Version;

                                //        local_Response = local_Client.Upload_Except_Data_Replica(local_Transaction, local_Parameters);
                                //        if (local_Response.ErrorCode == 0)
                                //        {
                                //            Global.Global.WriteLog("Local2Live SQL Row No. " + Convert.ToInt32(forIndex + 1).ToString(), LogMessageType.Information, string.Format("{0} ({1} row(s) affected)", tableName, uploadRows));
                                //        }
                                //        else
                                //        {
                                //            Global.Global.WriteLog("ULocalSSDToLiveSSDTW localObjectResponse:" + tableName, LogMessageType.Error, local_Response.ErrorMessage);
                                //        }
                                //    }
                                //    #endregion [ Upload data in Replica to Local SQL Server ]
                                //}
                                //else
                                //{
                                //    Global.Global.WriteLog("ULocalSSDToLiveSSDTW liveObjectResponse:{" + tableName + "}", LogMessageType.Error, live_Response.ErrorMessage);
                                //}
                                #endregion [ Upload data to Live SQL Server ]
                                dsOutPut = null;

                                startCount = endCount;
                                if (endCount < dtFinal.Rows.Count)
                                {
                                    rowCounts++;
                                }
                                else
                                {
                                    break;
                                }

                                endCount += Program.ConfigTopCount;
                            }
                        }
                    }
                }

                #endregion [ Upload data to Live SQL Server and Update in Replica Table(s) of Local Sql Server ]
            }
            catch (Exception ex)
            {
                Global.Global.WriteLog("ULocalSSDToLiveSSDTW Exception:", LogMessageType.Error, ex.Message);
            }

            return returnData;
        }

        #endregion [ Upload Local SQL Server to Live SQL Server Data ]

        #region [ Download Live SQL Server Data to Local SQL Server ]

        /// <summary>
        /// 
        /// </summary>
        /// <param name="execute"></param>
        /// <returns></returns>
        private bool DownloadLiveSQLServerDataToLocalSQLServerData(int execute)
        {
            bool returnData = false;
            try
            {
                DataSet dsOutPut = new DataSet();
                DataTable dtFinal;
                DataTable dtPrimaryColumnDetails;

                string tableName;
                string columnName;
                int tableIndex;

                #region [ Get Live SQL Server ]

                Scada_Sync_WebService.Response_LocalSQLData local_Response;
                using (Scada_Sync_WebService.PNF_GM_Sync_WSSoapClient local_Client = new Scada_Sync_WebService.PNF_GM_Sync_WSSoapClient())
                {
                    Scada_Sync_WebService.TransactionHeader local_Transaction = new Scada_Sync_WebService.TransactionHeader();
                    local_Transaction.DeviceID = Program.ConfigMachine_Id;
                    local_Transaction.Password = Program.ConfigService_Password;
                    local_Transaction.UserName = Program.ConfigService_LoginID;
                    local_Transaction.Config_App_Version = Program.ConfigAPP_Version;
                    local_Transaction.Config_App_Name = Program.ConfigAPP_Name;
                    local_Transaction.Win_Service_GUID = Program.Win_Service_GUID;
                    local_Transaction.Win_Service_Version = Program.Win_Service_Version;

                    local_Response = local_Client.Get_Except_TableList(local_Transaction, execute, 1);
                }

                #endregion [ Get Live SQL Server ]

                if (local_Response.ErrorCode == -1)
                {
                    Global.Global.WriteLog("DLSSDTLSS", LogMessageType.Error, local_Response.ErrorMessage);
                    returnData = false;
                    return returnData;
                }

                DataTable dtTableNames = new DataTable();
                dtPrimaryColumnDetails = new DataTable();
                if (execute == 3)
                {
                    tableIndex = 0;
                    dtFinal = new DataTable();
                    dtFinal = local_Response.LocalSQLData.Tables[tableIndex];

                    if (dtFinal.Rows.Count > 0)
                    {
                        #region [ Remove Table Name Column from the DataTable ]
                        {
                            tableName = string.Empty;
                            columnName = dtFinal.Columns[0].ColumnName;
                            tableName = dtFinal.Rows[0][columnName].ToString();
                            dtFinal.Columns.Remove(columnName);
                        }
                        #endregion [ Remove Table Name Column from the DataTable ]

                        if (tableName == "Table_PrimaryKey_Details")
                        {
                            string[] columnsName = new string[1];
                            columnsName[0] = "Table_Name";

                            dtTableNames = local_Response.LocalSQLData.Tables[tableIndex].DefaultView.ToTable(true, columnsName);
                        }
                    }

                    if (dtTableNames.Rows.Count > 0)
                    {
                        for (int tableNameIndex = 0; tableNameIndex < dtTableNames.Rows.Count; tableNameIndex++)
                        {
                            tableName = dtTableNames.Rows[tableNameIndex]["Table_Name"].ToString();
                            dtPrimaryColumnDetails = local_Response.LocalSQLData.Tables[tableIndex].Select("Table_Name = '" + tableName + "'").CopyToDataTable();
                            dtPrimaryColumnDetails.TableName = tableName;

                            try
                            {
                                returnData = this.DownloadLoopTransactionData(
                                        tableName,
                                        dtPrimaryColumnDetails
                                        );
                            }
                            catch (Exception)
                            {
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Global.Global.WriteLog("DLSSDTLSS Exception", LogMessageType.Error, ex.Message);
            }

            return returnData;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="dtPrimaryColumnDetails"></param>
        /// <returns></returns>
        private bool DownloadLoopTransactionData(
            string tableName,
            DataTable dtPrimaryColumnDetails)
        {
            bool returnData = false;
            Int64 rowValue = 0;
            int rowCounts = 0;
            int forIndex;
            string fieldName = string.Empty;

            if (string.IsNullOrEmpty(fieldName) == false)
            {
                rowValue = Convert.ToInt64(dtPrimaryColumnDetails.Rows[0]["Row_Count"].ToString());
                rowCounts = 1;
            }

            try
            {
                for (forIndex = 0; forIndex <= rowCounts; forIndex++)
                {
                    //Scada_Sync_WebService.Response_LocalSQLData live_Response;
                    //using (Scada_Sync_WebService.Skada_Sync_WSSoapClient live_Client = new Scada_Sync_WebService.Skada_Sync_WSSoapClient())
                    //{
                    //    Scada_Sync_WebService.TransactionHeader live_Transaction = new Scada_Sync_WebService.TransactionHeader();
                    //    live_Transaction.DeviceID = Program.ConfigMachine_Id;
                    //    live_Transaction.Password = Program.ConfigService_Password;
                    //    live_Transaction.UserName = Program.ConfigService_LoginID;
                    //    live_Transaction.Config_App_Version = Program.ConfigAPP_Version;
                    //    live_Transaction.Config_App_Name = Program.ConfigAPP_Name;
                    //    live_Transaction.Win_Service_GUID = Program.Win_Service_GUID;
                    //    live_Transaction.Win_Service_Version = Program.Win_Service_Version;

                    //    live_Response = live_Client.GetExceptData_TableName(live_Transaction, tableName, string.Empty);
                    //}

                    //if (live_Response.ErrorCode == 0)
                    //{
                    //    if (live_Response.LocalSQLData.Tables[0].Rows.Count > 0)
                    //    {
                    //        returnData = this.DownloadLiveSQLServerDataToLocalSQLServerDataTableWise(
                    //                live_Response,
                    //                dtPrimaryColumnDetails);
                    //    }
                    //    else
                    //    {
                    //        break;
                    //    }
                    //}
                }
            }
            catch (Exception ex)
            {
                Global.Global.WriteLog("DownloadLoopTransactionData Exception for " + tableName + ":", LogMessageType.Error, ex.Message);
                returnData = false;
            }

            return returnData;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="response_LiveSQLData"></param>
        /// <param name="dtPrimaryColumnDetails"></param>
        /// <returns></returns>
        private bool DownloadLiveSQLServerDataToLocalSQLServerDataTableWise(
            Scada_Sync_WebService.Response_LocalSQLData response_LiveSQLData,
            DataTable dtPrimaryColumnDetails)
        {
            bool returnData = false;
            #region [ Variables ]

            string oldTableName;

            DataSet dsOutPut;
            DataTable dtUpload;
            DataTable dtFinal;
            string columnName;
            string tableName;
            string globalTableName;

            int startCount;
            int rowCounts;
            int uploadRows;
            int totalUploadRows;
            int endCount;
            int forIndex;
            int tableIndex;
            int tableCount;

            DateTime clientDateTime;

            Scada_Sync_WebService.Response_UploadData live_Response;
            Scada_Sync_WebService.Response_UploadData local_Response;
            #endregion [ Variables ]

            tableCount = response_LiveSQLData.LocalSQLData.Tables.Count;
            try
            {
                #region [ Upload data to Local SQL Server and Update in Replica Table(s) of Live Sql Server ]
                uploadRows = 0;
                totalUploadRows = 0;
                for (tableIndex = 0; tableIndex < tableCount; tableIndex++)
                {
                    dtFinal = new DataTable();
                    dtFinal = response_LiveSQLData.LocalSQLData.Tables[tableIndex];
                    forIndex = 0;

                    if (dtFinal.Rows.Count > 0)
                    {
                        #region [ Remove Table Name Column from the DataTable ]
                        {
                            tableName = string.Empty;
                            globalTableName = string.Empty;
                            columnName = dtFinal.Columns[0].ColumnName;
                            tableName = dtFinal.Rows[0][columnName].ToString();
                            dtFinal.Columns.Remove(columnName);

                            if (dtFinal.Columns.Contains("GlobalTable_Name"))
                            {
                                globalTableName = dtFinal.Rows[0]["GlobalTable_Name"].ToString();
                                dtFinal.Columns.Remove("GlobalTable_Name");
                            }
                        }
                        #endregion [ Remove Table Name Column from the DataTable ]

                        if (tableName == "Table_PrimaryKey_Details")
                        {
                            dtPrimaryColumnDetails = response_LiveSQLData.LocalSQLData.Tables[tableIndex];
                            dtPrimaryColumnDetails.TableName = tableName;
                        }
                        else
                        {
                            startCount = 0;
                            rowCounts = 2;
                            endCount = Program.ConfigTopCount;

                            if (Program.Config_write_XML_Download)
                            {
                                dtFinal.TableName = tableName;
                                this.Write_XML_File("Download", dtFinal, MethodBase.GetCurrentMethod().Name);
                            }

                            #region [ Add Identity_Column Field to DataTable ]

                            DataTable dtReturnTable = dtFinal.Clone();
                            DataColumn identColumn = new DataColumn();
                            identColumn.ColumnName = "Identity_Column";
                            identColumn.DataType = Type.GetType("System.Int32");
                            identColumn.AutoIncrement = true;
                            identColumn.AutoIncrementSeed = 1;
                            identColumn.AutoIncrementStep = 1;

                            dtReturnTable.Columns.Add(identColumn);
                            dtReturnTable.AcceptChanges();

                            dtFinal.Select().CopyToDataTable(dtReturnTable, LoadOption.OverwriteChanges);
                            dtFinal = dtReturnTable;

                            #endregion [ Add Identity_Column Field to DataTable ]

                            bool uploadtoReplica = false;
                            Global.Global.WriteLog("Live2Local SQL : ", LogMessageType.Information, "Count for Insert/Update record " + dtFinal.Rows.Count.ToString() + " in table " + tableName);
                            for (forIndex = 0; forIndex <= rowCounts; forIndex++)
                            {
                                returnData = true;
                                dtUpload = dtFinal.Select(string.Format("Identity_Column > {0} and Identity_Column <= {1}", startCount.ToString(), endCount.ToString())).CopyToDataTable();

                                uploadRows = dtUpload.Rows.Count;

                                #region [ Set the Primary Keys to DataTable ]

                                dtUpload.TableName = tableName.ToString();
                                {
                                    DataRow[] dtrXML = dtPrimaryColumnDetails.Select("Table_Name ='" + tableName.ToString() + "'");
                                    if (dtrXML.Length > 0)
                                    {
                                        DataColumn[] dataColumns = new DataColumn[dtrXML.Length + 1];
                                        for (int i = 0; i < dataColumns.Count() - 1; i++)
                                        {
                                            dataColumns[i] = dtUpload.Columns[dtrXML[i]["Column_Name"].ToString().Replace("[", string.Empty).Replace("]", string.Empty)];
                                        }

                                        dtUpload.PrimaryKey = dataColumns;
                                    }
                                }
                                #endregion [ Set the Primary Keys to DataTable ]

                                oldTableName = tableName;
                                #region [ Remove Identity_Column ]

                                dtUpload.Columns.Remove("Identity_Column");
                                tableName = tableName.ToLower();
                                if (
                                        tableName == "vitrag_batch_final"
                                        || tableName == "batch_dat_trans_final"
                                        || tableName == "batch_result_final"
                                        || tableName == "batch_transaction_final"
                                    )
                                {
                                    tableName = tableName.ToUpper().Replace("_FINAL", string.Empty);
                                }
                                else if (tableName == "batch_ver2_final")
                                {
                                    tableName = "Batch";
                                }

                                dtUpload.TableName = tableName;
                                dtUpload.AcceptChanges();
                                #endregion [ Remove Identity_Column ]

                                dsOutPut = new DataSet();
                                dsOutPut.Tables.Add(dtUpload);

                                #region [ Upload data to Local SQL Server ]
                                using (Scada_Sync_WebService.PNF_GM_Sync_WSSoapClient local_Client = new Scada_Sync_WebService.PNF_GM_Sync_WSSoapClient())
                                {
                                    Scada_Sync_WebService.paramUploadData_en local_Parameters = new Scada_Sync_WebService.paramUploadData_en();
                                    clientDateTime = DateTime.Now;
                                    dsOutPut.ExtendedProperties["UTCDifference"] = TimeZone.CurrentTimeZone.GetUtcOffset(clientDateTime).Ticks.ToString();
                                    local_Parameters.DataSetToUpload = dsOutPut;

                                    Scada_Sync_WebService.TransactionHeader local_Transaction = new Scada_Sync_WebService.TransactionHeader();
                                    local_Transaction.DeviceID = Program.ConfigMachine_Id;
                                    local_Transaction.Password = Program.ConfigService_Password;
                                    local_Transaction.UserName = Program.ConfigService_LoginID;
                                    local_Transaction.Config_App_Version = Program.ConfigAPP_Version;
                                    local_Transaction.Config_App_Name = Program.ConfigAPP_Name;
                                    local_Transaction.Win_Service_GUID = Program.Win_Service_GUID;
                                    local_Transaction.Win_Service_Version = Program.Win_Service_Version;

                                    local_Response = local_Client.SendDatatoSQL(local_Transaction, local_Parameters);
                                }
                                #endregion [ Upload data to Local SQL Server ]

                                if (local_Response.ErrorCode == 0)
                                {
                                    uploadtoReplica = false;
                                    totalUploadRows += uploadRows;
                                    if (uploadtoReplica == false)
                                    {
                                        #region [ Upload data in Replica to Live SQL Server ]
                                        //using (Scada_Sync_WebService.Skada_Sync_WSSoapClient live_Client = new Scada_Sync_WebService.Skada_Sync_WSSoapClient())
                                        //{
                                        //    Scada_Sync_WebService.paramUploadData_en local_Parameters = new Scada_Sync_WebService.paramUploadData_en();

                                        //    dsOutPut.Tables[tableName].TableName = oldTableName;
                                        //    tableName = oldTableName;

                                        //    local_Parameters.TableName = tableName;
                                        //    local_Parameters.GlobalTableName = globalTableName;
                                        //    clientDateTime = DateTime.Now;
                                        //    dsOutPut.ExtendedProperties["UTCDifference"] = TimeZone.CurrentTimeZone.GetUtcOffset(clientDateTime).Ticks.ToString();
                                        //    local_Parameters.DataSetToUpload = dsOutPut;

                                        //    Scada_Sync_WebService.TransactionHeader live_Transaction = new Scada_Sync_WebService.TransactionHeader();
                                        //    live_Transaction.DeviceID = Program.ConfigMachine_Id;
                                        //    live_Transaction.Password = Program.ConfigService_Password;
                                        //    live_Transaction.UserName = Program.ConfigService_LoginID;
                                        //    live_Transaction.Config_App_Version = Program.ConfigAPP_Version;
                                        //    live_Transaction.Config_App_Name = Program.ConfigAPP_Name;
                                        //    live_Transaction.Win_Service_GUID = Program.Win_Service_GUID;
                                        //    live_Transaction.Win_Service_Version = Program.Win_Service_Version;

                                        //    live_Response = live_Client.Upload_Except_Data_Replica(live_Transaction, local_Parameters);
                                        //    if (live_Response.ErrorCode == 0)
                                        //    {
                                        //        Global.Global.WriteLog("Live2Local SQL Row No. " + Convert.ToInt32(forIndex + 1).ToString(), LogMessageType.Information, string.Format("{0} ({1} row(s) affected)", tableName, uploadRows));
                                        //    }
                                        //    else
                                        //    {
                                        //        Global.Global.WriteLog("DLiveSSDToLocalSSDTW liveObjectResponse:{" + tableName + "}", LogMessageType.Error, live_Response.ErrorMessage);
                                        //    }
                                        //}
                                        #endregion [ Upload data in Replica to Live SQL Server ]
                                    }
                                }
                                else
                                {
                                    Global.Global.WriteLog("DLiveSSDToLocalSSDTW LocalObjectResponse:{" + tableName + "}", LogMessageType.Error, local_Response.ErrorMessage);
                                }

                                dsOutPut = null;

                                startCount = endCount;
                                if (endCount < dtFinal.Rows.Count)
                                {
                                    rowCounts++;
                                }
                                else
                                {
                                    break;
                                }

                                endCount += Program.ConfigTopCount;
                            }
                        }
                    }
                }

                #endregion [ Upload data to Local SQL Server and Update in Replica Table(s) of Live Sql Server ]
            }
            catch (Exception ex)
            {
                Global.Global.WriteLog("DLiveSSDToLocalSSDTW Exception:", LogMessageType.Error, ex.Message);
            }

            return returnData;
        }
        #endregion [ Download Live SQL Server Data to Local SQL Server ]

        #endregion [ Upload/Download Data from Local/Live SQL Server]

        #region [ Asphalt New batches ]
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private bool GetAsphaltNewBatches(string sourceConnectionString, string targetConnectionString)
        {
            bool returnData = false;
            string sqlTableName;
            sqlTableName = string.Empty;

            try
            {

                Scada_Sync_WebService.TransactionHeader local_Transaction = new Scada_Sync_WebService.TransactionHeader();
                Scada_Sync_WebService.Response_AsphaltNewBatches local_Response;

                #region [ Get AsphaltNewBatches ]
                using (Scada_Sync_WebService.PNF_GM_Sync_WSSoapClient local_Client = new Scada_Sync_WebService.PNF_GM_Sync_WSSoapClient())
                {
                    local_Transaction.DeviceID = Program.ConfigMachine_Id;
                    local_Transaction.Password = Program.ConfigService_Password;
                    local_Transaction.UserName = Program.ConfigService_LoginID;
                    local_Transaction.Config_App_Version = Program.ConfigAPP_Version;
                    local_Transaction.Config_App_Name = Program.ConfigAPP_Name;
                    local_Transaction.Win_Service_GUID = Program.Win_Service_GUID;
                    local_Transaction.Win_Service_Version = Program.Win_Service_Version;

                    local_Response = local_Client.Fetch_Asphalt_Batchs(local_Transaction);
                }
                #endregion [ Get AsphaltNewBatches ]

                if (local_Response.Upgraded_Asphalt_Batchs == null || local_Response.Upgraded_Asphalt_Batchs.Tables[0] == null)
                {
                    return returnData;
                }

                DataTable dtAsphaltNewBatches = local_Response.Upgraded_Asphalt_Batchs.Tables[0];
                if (dtAsphaltNewBatches.Rows.Count == 0)
                {
                    return returnData;
                }


                string[] tableList = new string[local_Response.Upgraded_Asphalt_Batchs.Tables.Count];
                string accessTableName;
                int tableIndex = 0;

                foreach (DataTable item in local_Response.Upgraded_Asphalt_Batchs.Tables)
                {
                    sqlTableName = item.TableName;
                    if (dtTablesList.Select("TABLE_NAME = '" + sqlTableName + "'").Length <= 0)
                    {
                        accessTableName = string.Empty;
                        if (GetAccessTableName(sqlTableName, out accessTableName, Program.ConfigMAP_TABLE_FORMAT))
                        {
                            item.TableName = accessTableName;
                        }
                        else
                        {
                            tableList[tableIndex] = sqlTableName;
                            tableIndex++;
                        }
                    }
                }

                foreach (string tableName in tableList)
                {
                    if (!string.IsNullOrEmpty(tableName))
                    {
                        local_Response.Upgraded_Asphalt_Batchs.Tables.Remove(tableName);
                    }
                }

                if (Program.ConfigMAP_TABLE_FORMAT == false)
                {
                    DataRow[] dtrxmlData = xmlSync_Tables_Details.Select("XMLRelation ='" + dtAsphaltNewBatches.TableName + "' AND XMLName = 'KEY'");
                    string columnName = string.Empty;
                    if (dtrxmlData.Length > 0)
                    {
                        columnName = dtrxmlData[0]["XMLKey"].ToString();

                        if (dtAsphaltNewBatches.Columns.Contains(columnName))
                        {
                            dtAsphaltNewBatches.Columns.Remove(columnName);
                        }
                    }
                    dtrxmlData = null;
                }

                if (Program.ConfigMAP_TABLE_FORMAT)
                {
                    dtAsphaltNewBatches = this.RenameFieldsofDataTable(dtAsphaltNewBatches);
                }

                DataTable dAsphaltNewBatches;
                using (PNF.RI_syncUtility objupload = new PNF.RI_syncUtility())
                {
                    string UTCDifference = local_Response.Upgraded_Asphalt_Batchs.ExtendedProperties["UTCDifference"].ToString();

                    if (Program.Config_write_XML_Download)
                    {
                        this.Write_XML_File("Download", dtAsphaltNewBatches, MethodBase.GetCurrentMethod().Name + "_Ashpalt");
                    }

                    if (Program.ConfigMAP_TABLE_FORMAT == true
                        || Program.Config_Transfer_data_to_main_database == true
                        || Program.Config_Relcon_map_table_format == true
                        )
                    {
                        bool removeRowID_Column = false;
                        dtAsphaltNewBatches = objupload.RemoveUnwantedFields(dtAsphaltNewBatches, sourceConnectionString, removeRowID_Column);
                    }
                    else
                    {
                        dtAsphaltNewBatches = objupload.RemoveUnwantedFields(dtAsphaltNewBatches, sourceConnectionString);
                    }

                    dAsphaltNewBatches = objupload.InsertAsphaltNewBatches(
                        dtAsphaltNewBatches,
                        Program.PlantAppVersion,
                        UTCDifference,
                        sourceConnectionString);
                }

                if (dAsphaltNewBatches != null && dAsphaltNewBatches.Rows.Count > 0)
                {
                    dAsphaltNewBatches.TableName = sqlTableName;

                    Scada_Sync_WebService.paramAsphaltNewBatches local_Parameters = new Scada_Sync_WebService.paramAsphaltNewBatches();
                    local_Parameters.AsphaltNewBatchesData = dAsphaltNewBatches;

                    using (Scada_Sync_WebService.PNF_GM_Sync_WSSoapClient local_Client = new Scada_Sync_WebService.PNF_GM_Sync_WSSoapClient())
                    {
                        local_Response = local_Client.Update_Asphalt_SyncStaus(local_Transaction, local_Parameters);
                    }

                    if (local_Response.ErrorCode == 1007)
                    {
                        returnData = true;
                    }
                }


            }
            catch (Exception ex)
            {
                Global.Global.WriteLog("GetAsphaltNewBatches for table Name " + sqlTableName + " :", LogMessageType.Error, ex.Message);
            }

            return returnData;
        }
        #endregion [ Asphalt New batches ]

        #region  [ Re-Sync Master Table like Customer , Site , Truck , Truck_Devic_mapping ]

        /// <summary>
        /// 
        /// </summary>
        private void Resync_Masters_Data()
        {
            string tableName = string.Empty;

            try
            {
                DataTable dtResync_Masters_Data;
                Scada_Sync_WebService.TransactionHeader local_Transaction = new Scada_Sync_WebService.TransactionHeader();
                local_Transaction.DeviceID = Program.ConfigMachine_Id;
                local_Transaction.Password = Program.ConfigService_Password;
                local_Transaction.UserName = Program.ConfigService_LoginID;
                local_Transaction.Config_App_Version = Program.ConfigAPP_Version;
                local_Transaction.Config_App_Name = Program.ConfigAPP_Name;
                local_Transaction.Win_Service_GUID = Program.Win_Service_GUID;
                local_Transaction.Win_Service_Version = Program.Win_Service_Version;

                /*for variable local_Response*/
                {
                    Scada_Sync_WebService.Respone_ReSync_Masters_Data local_Response;
                    using (Scada_Sync_WebService.PNF_GM_Sync_WSSoapClient local_Client = new Scada_Sync_WebService.PNF_GM_Sync_WSSoapClient())
                    {
                        local_Response = local_Client.GetResync_Masters_Data(local_Transaction);
                    }

                    dtResync_Masters_Data = local_Response.ReSync_Masters_Data.Tables[0];
                }

                if (dtResync_Masters_Data == null)
                {
                    return;
                }

                if (dtResync_Masters_Data.Rows.Count == 0)
                {
                    return;
                }

                Scada_Sync_WebService.Response_DownloadedSyncStaus live_Response;
                Scada_Sync_WebService.TransactionHeader live_Transaction = new Scada_Sync_WebService.TransactionHeader();
                live_Transaction.DeviceID = Program.ConfigMachine_Id;
                live_Transaction.Password = Program.ConfigService_Password;
                live_Transaction.UserName = Program.ConfigService_LoginID;
                live_Transaction.Config_App_Version = Program.ConfigAPP_Version;
                live_Transaction.Config_App_Name = Program.ConfigAPP_Name;
                live_Transaction.Win_Service_GUID = Program.Win_Service_GUID;
                live_Transaction.Win_Service_Version = Program.Win_Service_Version;

                bool Set_to_download = false;
                bool Delete_replica_live = false;
                bool Data_downloaded = false;
                bool Set_to_upload = false;
                bool IS_Sync_completed = false;

                for (int i = 0; i < dtResync_Masters_Data.Rows.Count; i++)
                {
                    tableName = dtResync_Masters_Data.Rows[i]["TABLE_NAME"].ToString();

                    #region [ Set_to_download ]

                    try
                    {
                        Set_to_download = ConvertToBoolean(dtResync_Masters_Data.Rows[i]["SET_TO_DOWNLOAD"].ToString());
                        if (Set_to_download == true)
                        {
                            Scada_Sync_WebService.Response_DownloadedSyncStaus local_Response;
                            using (Scada_Sync_WebService.PNF_GM_Sync_WSSoapClient local_Client = new Scada_Sync_WebService.PNF_GM_Sync_WSSoapClient())
                            {
                                local_Response = local_Client.Resync_Masters_set_table_to_download_or_upload(local_Transaction, tableName, true);
                            }

                            if (local_Response.ErrorCode == 0 || local_Response.ErrorCode == 1007)
                            {
                                this.Update_Resync_Masters_Data(local_Transaction, tableName, "SET_TO_DOWNLOAD", "DELETE_REPLICA_LIVE", "1");

                                //using (Scada_Sync_WebService.Skada_Sync_WSSoapClient live_Client = new Scada_Sync_WebService.Skada_Sync_WSSoapClient())
                                //{
                                //    live_Response = live_Client.Delete_Replica_Resync_Master_Data(live_Transaction, tableName);
                                //}

                                //if (live_Response.ErrorCode == 0 || live_Response.ErrorCode == 1007)
                                //{
                                //    this.Update_Resync_Masters_Data(local_Transaction, tableName, "DELETE_REPLICA_LIVE", "DATA_DOWNLOADED", "1");
                                //}
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Global.Global.WriteLog("Resync_Masters_Data Exception at Set_to_download for Table_name " + tableName, LogMessageType.Information, ex.Message);
                    }

                    #endregion [ Set_to_download ]

                    #region [ Delete_replica_live ]
                    try
                    {
                        Delete_replica_live = ConvertToBoolean(dtResync_Masters_Data.Rows[i]["DELETE_REPLICA_LIVE"].ToString());
                        if (Delete_replica_live == true)
                        {
                            //using (Scada_Sync_WebService.Skada_Sync_WSSoapClient live_Client = new Scada_Sync_WebService.Skada_Sync_WSSoapClient())
                            //{
                            //    live_Response = live_Client.Delete_Replica_Resync_Master_Data(live_Transaction, tableName);
                            //}

                            //if (live_Response.ErrorCode == 0 || live_Response.ErrorCode == 1007)
                            //{
                            //    this.Update_Resync_Masters_Data(local_Transaction, tableName, "DELETE_REPLICA_LIVE", "DATA_DOWNLOADED", "1");
                            //}
                        }
                    }
                    catch (Exception ex)
                    {
                        Global.Global.WriteLog("Resync_Masters_Data Exception at Delete_replica_live for Table_name " + tableName, LogMessageType.Information, ex.Message);
                    }
                    #endregion [ Delete_replica_live ]

                    #region [ Data_downloaded ]
                    try
                    {
                        Data_downloaded = ConvertToBoolean(dtResync_Masters_Data.Rows[i]["DATA_DOWNLOADED"].ToString());
                        if (Data_downloaded == true)
                        {
                            //Scada_Sync_WebService.Response_LocalSQLData except_response;
                            //using (Scada_Sync_WebService.Skada_Sync_WSSoapClient live_Client = new Scada_Sync_WebService.Skada_Sync_WSSoapClient())
                            //{
                            //    except_response = live_Client.GetExceptData_TableName(live_Transaction, tableName, string.Empty);
                            //}

                            //if (except_response.ErrorCode == 0 || except_response.ErrorCode == 1007)
                            //{
                            //    if (except_response.LocalSQLData.Tables[0].Rows.Count == 0)
                            //    {
                            //        this.Update_Resync_Masters_Data(local_Transaction, tableName, "DATA_DOWNLOADED", "SET_TO_UPLOAD", "1");


                            //        Scada_Sync_WebService.Response_DownloadedSyncStaus local_Response;
                            //        using (Scada_Sync_WebService.PNF_GM_Sync_WSSoapClient local_Client = new Scada_Sync_WebService.PNF_GM_Sync_WSSoapClient())
                            //        {
                            //            local_Response = local_Client.Resync_Masters_set_table_to_download_or_upload(local_Transaction, tableName, false);
                            //        }

                            //        if (local_Response.ErrorCode == 0 || local_Response.ErrorCode == 1007)
                            //        {
                            //            this.Update_Resync_Masters_Data(local_Transaction, tableName, "SET_TO_UPLOAD", "IS_SYNC_COMPLETED", "1");
                            //        }
                            //    }
                            //}
                        }
                    }
                    catch (Exception ex)
                    {
                        Global.Global.WriteLog("Resync_Masters_Data Exception at Data_downloaded for Table_name " + tableName, LogMessageType.Information, ex.Message);
                    }

                    #endregion [ Data_downloaded ]

                    #region [ Set_to_upload ]
                    try
                    {
                        Set_to_upload = ConvertToBoolean(dtResync_Masters_Data.Rows[i]["SET_TO_UPLOAD"].ToString());
                        if (Set_to_upload == true)
                        {
                            Scada_Sync_WebService.Response_DownloadedSyncStaus local_Response;
                            using (Scada_Sync_WebService.PNF_GM_Sync_WSSoapClient local_Client = new Scada_Sync_WebService.PNF_GM_Sync_WSSoapClient())
                            {
                                local_Response = local_Client.Resync_Masters_set_table_to_download_or_upload(local_Transaction, tableName, false);
                            }

                            if (local_Response.ErrorCode == 0 || local_Response.ErrorCode == 1007)
                            {
                                this.Update_Resync_Masters_Data(local_Transaction, tableName, "SET_TO_UPLOAD", "IS_SYNC_COMPLETED", "1");
                            }
                        }

                    }
                    catch (Exception ex)
                    {
                        Global.Global.WriteLog("Resync_Masters_Data Exception at Set_to_upload for Table_name " + tableName, LogMessageType.Information, ex.Message);
                    }

                    #endregion [ Set_to_upload ]

                    IS_Sync_completed = ConvertToBoolean(dtResync_Masters_Data.Rows[i]["IS_SYNC_COMPLETED"].ToString());
                    if (IS_Sync_completed == true)
                    {
                        this.Update_Resync_Masters_Data(local_Transaction, tableName, "IS_SYNC_COMPLETED", "IS_SYNC_STARTED", "0");
                    }
                }

            }
            catch (Exception ex)
            {
                Global.Global.WriteLog("Resync_Masters_Data Exception : " + tableName, LogMessageType.Information, ex.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="local_Transaction"></param>
        /// <param name="tableName"></param>
        /// <param name="field_Name_1"></param>
        /// <param name="field_Name_2"></param>
        /// <param name="field_Value_2"></param>
        private void Update_Resync_Masters_Data(Scada_Sync_WebService.TransactionHeader local_Transaction, string tableName, string field_Name_1, string field_Name_2, string field_Value_2)
        {
            Scada_Sync_WebService.Response_DownloadedSyncStaus local_Response;
            using (Scada_Sync_WebService.PNF_GM_Sync_WSSoapClient local_Client = new Scada_Sync_WebService.PNF_GM_Sync_WSSoapClient())
            {
                local_Response = local_Client.Update_Resync_Masters_Data(local_Transaction, tableName, field_Name_1, "0", field_Name_2, field_Value_2);
            }
        }
        #endregion  [ Re-Sync Master Table like Customer , Site , Truck , Truck_Devic_mapping ]

        #endregion [if (Program.ConfigApplication_Version == "1")]

        #region [ Desilting Plant ]

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sourceConnectionString"></param>
        /// <param name="targetConnectionString"></param>
        /// <returns></returns>
        private bool GetMastersData(string sourceConnectionString, string targetConnectionString)
        {
            bool returnData = false;

            if (xmlSync_Tables_Details.Select("XmlRelation = 'ORGNIZATION'").Length > 0)
            {
                returnData = GetMastersData(sourceConnectionString, targetConnectionString, "ORGNIZATION");
            }

            if (xmlSync_Tables_Details.Select("XmlRelation = 'ORGANIZATION_SETTINGS'").Length > 0)
            {
                returnData = GetMastersData(sourceConnectionString, targetConnectionString, "ORGANIZATION_SETTINGS");
            }

            if (xmlSync_Tables_Details.Select("XmlRelation = 'Users'").Length > 0)
            {
                returnData = GetMastersData(sourceConnectionString, targetConnectionString, "Users");
            }

            if (xmlSync_Tables_Details.Select("XmlRelation = 'Truck_Master'").Length > 0)
            {
                returnData = GetMastersData(sourceConnectionString, targetConnectionString, "Truck_Master");
            }

            if (xmlSync_Tables_Details.Select("XmlRelation = 'TRUCK_DEVICE_MAPPING'").Length > 0)
            {
                returnData = GetMastersData(sourceConnectionString, targetConnectionString, "TRUCK_DEVICE_MAPPING");
            }

            return returnData;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sourceConnectionString"></param>
        /// <param name="targetConnectionString"></param>
        /// <param name="parameterTableName"></param>
        /// <returns></returns>
        private bool GetMastersData(string sourceConnectionString, string targetConnectionString, string parameterTableName)
        {
            bool returnData = false;
            string returnQuery = string.Empty;

            try
            {
                string errorMessage = string.Empty;
                DataSet dtResponseModified = null;

                Scada_Sync_WebService.TransactionHeader live_Transaction = new Scada_Sync_WebService.TransactionHeader();
                live_Transaction.DeviceID = Program.ConfigMachine_Id;
                live_Transaction.Password = Program.ConfigService_Password;
                live_Transaction.UserName = Program.ConfigService_LoginID;
                live_Transaction.Config_App_Version = Program.ConfigAPP_Version;
                live_Transaction.Config_App_Name = Program.ConfigAPP_Name;
                live_Transaction.Win_Service_GUID = Program.Win_Service_GUID;
                live_Transaction.Win_Service_Version = Program.Win_Service_Version;

                //Scada_Sync_WebService.Response_TripsData local_Response;

                //using (Scada_Sync_WebService.Skada_Sync_WSSoapClient local_Client = new Scada_Sync_WebService.Skada_Sync_WSSoapClient())
                //{
                //    local_Response = local_Client.GetMastersData(live_Transaction, parameterTableName);
                //}

                //dtResponseModified = local_Response.Trips_Data;

                //if (dtResponseModified.Tables.Count == 0)
                //{
                //    return returnData;
                //}

                string[] tableList = new string[dtResponseModified.Tables.Count];
                int tableIndex = 0;
                string sqlTableName;
                string accessTableName;
                foreach (DataTable item in dtResponseModified.Tables)
                {
                    sqlTableName = item.TableName;
                    if (dtTablesList.Select("TABLE_NAME = '" + sqlTableName + "'").Length <= 0)
                    {
                        accessTableName = string.Empty;
                        if (GetAccessTableName(sqlTableName, out accessTableName))
                        {
                            item.TableName = accessTableName;
                        }
                        else
                        {
                            tableList[tableIndex] = sqlTableName;
                            tableIndex++;
                        }
                    }
                }

                foreach (string tableName in tableList)
                {
                    if (!string.IsNullOrEmpty(tableName))
                    {
                        dtResponseModified.Tables.Remove(tableName);
                    }
                }

                Global.Global.WriteLog("Master data", LogMessageType.Information, "Sync Data found.");
                try
                {
                    string UTCDifference = dtResponseModified.ExtendedProperties["UTCDifference"].ToString();
                    DataTable dtResponse4WebService = new DataTable();
                    DataTable downloadDataTable = new DataTable();
                    using (PNF.RI_syncUtility objupload = new PNF.RI_syncUtility())
                    {
                        foreach (DataTable dtDownloadedMain in dtResponseModified.Tables)
                        {
                            string tableName = dtDownloadedMain.TableName;

                            #region [ Other Tables ]

                            DataTable dtBatch_nos = new DataTable();
                            downloadDataTable = objupload.RemoveUnwantedFields(dtDownloadedMain, sourceConnectionString);
                            downloadDataTable.AcceptChanges();

                            dtResponse4WebService = objupload.UpdateAccessDatabaseWithDownloadedData(
                                downloadDataTable,
                                Program.PlantAppVersion,
                                UTCDifference,
                                sourceConnectionString,
                                targetConnectionString,
                                true,
                                out returnQuery
                                );

                            if (dtResponse4WebService.Rows.Count > 0)
                            {
                                Global.Global.WriteLog("Master data", LogMessageType.Information, "Sync Data update successfully in Source and Target Database.");

                                Scada_Sync_WebService.paramDownloadSyncStatus local_Parameters = new Scada_Sync_WebService.paramDownloadSyncStatus();
                                local_Parameters.DownloadedSyncedData = dtResponse4WebService;
                                //local_Parameters.SyncType = Scada_Sync_WebService.SyncDataType.MasterSyncData;
                                //Scada_Sync_WebService.Response_DownloadedSyncStaus local_Respose;
                                //using (Scada_Sync_WebService.Skada_Sync_WSSoapClient local_Client = new Scada_Sync_WebService.Skada_Sync_WSSoapClient())
                                //{
                                //    local_Respose = local_Client.DownloadedSyncStaus(live_Transaction, local_Parameters);
                                //    errorMessage = local_Respose.ErrorMessage.ToString();
                                //}
                                Global.Global.WriteLog("Master data", LogMessageType.Information, errorMessage);
                                returnData = true;
                            }
                            else
                            {
                                Global.Global.WriteLog("Master data" + tableName, LogMessageType.Error, "Sync Data not update.");
                            }
                            #endregion [ Other Tables ]
                        }
                    }
                }
                catch (Exception ex)
                {
                    Global.Global.WriteLog("Master data", LogMessageType.Error, ex.Message);
                    Global.Global.WriteLog("Master data", LogMessageType.Information, returnQuery);
                }
            }
            catch (CommunicationException ex)
            {
                Global.Global.WriteLog("Master data", LogMessageType.Error, ex.Message.ToString());
            }
            catch (Exception Modifiedex)
            {
                Global.Global.WriteLog("Master data", LogMessageType.DownloadError, Modifiedex.Message.ToString());
            }

            return returnData;
        }

        /// <summary>
        /// Download the Data from Live SQL Server
        /// </summary>
        /// <param name="sourceConnectionString"></param>
        /// <param name="targetConnectionString"></param>
        /// <returns></returns>
        private bool Get_DesiltingTrips(string sourceConnectionString, string targetConnectionString)
        {
            bool returnData = false;
            if (xmlSync_Tables_Details.Select("XmlRelation = 'DESILTING_TRIPS'").Length > 0)
            {
                returnData = Get_DesiltingTrips(sourceConnectionString, targetConnectionString, true);
            }
            return returnData;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sourceConnectionString"></param>
        /// <param name="targetConnectionString"></param>
        /// <param name="isInsert"></param>
        /// <returns></returns>
        private bool Get_DesiltingTrips(string sourceConnectionString, string targetConnectionString, bool isInsert)
        {
            bool returnData = false;
            string returnQuery = string.Empty;

            try
            {
                string errorMessage = string.Empty;
                DataSet dtResponseModified = null;

                Scada_Sync_WebService.TransactionHeader live_Transaction = new Scada_Sync_WebService.TransactionHeader();
                live_Transaction.DeviceID = Program.ConfigMachine_Id;
                live_Transaction.Password = Program.ConfigService_Password;
                live_Transaction.UserName = Program.ConfigService_LoginID;
                live_Transaction.Config_App_Version = Program.ConfigAPP_Version;
                live_Transaction.Config_App_Name = Program.ConfigAPP_Name;
                live_Transaction.Win_Service_GUID = Program.Win_Service_GUID;
                live_Transaction.Win_Service_Version = Program.Win_Service_Version;

                //Scada_Sync_WebService.Response_TripsData local_Response;

                //using (Scada_Sync_WebService.Skada_Sync_WSSoapClient local_Client = new Scada_Sync_WebService.Skada_Sync_WSSoapClient())
                //{
                //    local_Response = local_Client.GetDesilting_Trips(live_Transaction, isInsert);
                //}

                //dtResponseModified = local_Response.Trips_Data;

                //if (dtResponseModified.Tables.Count == 0)
                //{
                //    return returnData;
                //}

                string[] tableList = new string[dtResponseModified.Tables.Count];
                int tableIndex = 0;
                string sqlTableName;
                string accessTableName;
                foreach (DataTable item in dtResponseModified.Tables)
                {
                    sqlTableName = item.TableName;
                    if (dtTablesList.Select("TABLE_NAME = '" + sqlTableName + "'").Length <= 0)
                    {
                        accessTableName = string.Empty;
                        if (GetAccessTableName(sqlTableName, out accessTableName))
                        {
                            item.TableName = accessTableName;
                        }
                        else
                        {
                            tableList[tableIndex] = sqlTableName;
                            tableIndex++;
                        }
                    }
                }

                foreach (string tableName in tableList)
                {
                    if (!string.IsNullOrEmpty(tableName))
                    {
                        dtResponseModified.Tables.Remove(tableName);
                    }
                }


                Global.Global.WriteLog("DesiltingTrips", LogMessageType.Information, "Sync Data found.");
                try
                {
                    string UTCDifference = dtResponseModified.ExtendedProperties["UTCDifference"].ToString();
                    DataTable dtResponse4WebService = new DataTable();
                    DataTable dtResponse4WebService1 = new DataTable();
                    DataTable downloadDataTable = new DataTable();
                    PNF.RI_syncUtility objupload = new PNF.RI_syncUtility();

                    foreach (DataTable dtDownloadedMain in dtResponseModified.Tables)
                    {
                        string tableName = dtDownloadedMain.TableName;
                        DataTable dtUploadData;
                        DataRow[] dtrSelectedRows = dtDownloadedMain.Select("UpdateInsert='INSERT'");

                        if (dtrSelectedRows != null)
                        {
                            if (dtrSelectedRows.Length > 0)
                            {
                                dtUploadData = dtrSelectedRows.CopyToDataTable();
                                dtUploadData.TableName = tableName;

                                DataTable dtBatch_nos = new DataTable();
                                downloadDataTable = objupload.RemoveUnwantedFields(dtUploadData, sourceConnectionString);
                                downloadDataTable.AcceptChanges();

                                int rowIndex = 0;
                                DataColumn[] dtcPrimaryColumns_MCGM;

                                downloadDataTable.PrimaryKey = null;
                                dtcPrimaryColumns_MCGM = new DataColumn[dtDownloadedMain.PrimaryKey.Length];

                                for (rowIndex = 0; rowIndex < dtcPrimaryColumns_MCGM.Count(); rowIndex++)
                                {
                                    dtcPrimaryColumns_MCGM[rowIndex] = downloadDataTable.Columns[dtDownloadedMain.PrimaryKey[rowIndex].ColumnName];
                                }

                                downloadDataTable.PrimaryKey = dtcPrimaryColumns_MCGM;

                                dtResponse4WebService = objupload.UpdateAccessDatabaseWithDownloadedData(
                                    downloadDataTable,
                                    Program.PlantAppVersion,
                                    UTCDifference,
                                    sourceConnectionString,
                                    targetConnectionString,
                                    true,
                                    out returnQuery
                                    );
                            }
                        }

                        dtrSelectedRows = dtDownloadedMain.Select("UpdateInsert='UPDATE'");
                        if (dtrSelectedRows != null)
                        {
                            if (dtrSelectedRows.Length > 0)
                            {
                                dtUploadData = dtrSelectedRows.CopyToDataTable();
                                dtUploadData.TableName = tableName;

                                DataTable dtBatch_nos = new DataTable();
                                downloadDataTable = objupload.RemoveUnwantedFields(dtUploadData, sourceConnectionString);
                                downloadDataTable.AcceptChanges();

                                int rowIndex = 0;
                                DataColumn[] dtcPrimaryColumns_MCGM;

                                downloadDataTable.PrimaryKey = null;
                                dtcPrimaryColumns_MCGM = new DataColumn[dtDownloadedMain.PrimaryKey.Length];

                                for (rowIndex = 0; rowIndex < dtcPrimaryColumns_MCGM.Count(); rowIndex++)
                                {
                                    dtcPrimaryColumns_MCGM[rowIndex] = downloadDataTable.Columns[dtDownloadedMain.PrimaryKey[rowIndex].ColumnName];
                                }

                                downloadDataTable.PrimaryKey = dtcPrimaryColumns_MCGM;

                                dtResponse4WebService1 = objupload.UpdateAccessDatabaseWithDownloadedData(
                                    downloadDataTable,
                                    Program.PlantAppVersion,
                                    UTCDifference,
                                    sourceConnectionString,
                                    targetConnectionString,
                                    false,
                                    out returnQuery
                                    );
                            }
                        }

                        if (dtResponse4WebService.Rows.Count > 0)
                        {
                            if (dtResponse4WebService1.Rows.Count > 0)
                            {
                                dtResponse4WebService.Merge(dtResponse4WebService1);
                            }
                        }
                        else
                        {
                            if (dtResponse4WebService1.Rows.Count > 0)
                            {
                                dtResponse4WebService = dtResponse4WebService1;
                            }
                        }

                        #region [ Other Tables ]
                        if (dtResponse4WebService.Rows.Count > 0)
                        {
                            Global.Global.WriteLog("DesiltingTrips", LogMessageType.Information, "Sync Data update successfully in Source and Target Database.");

                            Scada_Sync_WebService.paramDownloadSyncStatus local_Parameters = new Scada_Sync_WebService.paramDownloadSyncStatus();
                            local_Parameters.DownloadedSyncedData = dtResponse4WebService;
                            //local_Parameters.SyncType = Scada_Sync_WebService.SyncDataType.DesiltingSyncData;
                            //Scada_Sync_WebService.Response_DownloadedSyncStaus local_Respose;
                            //using (Scada_Sync_WebService.Skada_Sync_WSSoapClient local_Client = new Scada_Sync_WebService.Skada_Sync_WSSoapClient())
                            //{
                            //    local_Respose = local_Client.DownloadedSyncStaus(live_Transaction, local_Parameters);
                            //    errorMessage = local_Respose.ErrorMessage.ToString();
                            //}

                            Global.Global.WriteLog("DesiltingTrips", LogMessageType.Information, errorMessage);
                            returnData = true;
                        }
                        else
                        {
                            Global.Global.WriteLog("DesiltingTrips" + tableName, LogMessageType.Error, "Sync Data not update.");
                        }

                        #endregion [ Other Tables ]
                    }

                }
                catch (Exception ex)
                {
                    Global.Global.WriteLog("DesiltingTrips", LogMessageType.Error, ex.Message);
                    Global.Global.WriteLog("DesiltingTrips", LogMessageType.Information, returnQuery);
                }
            }
            catch (CommunicationException ex)
            {
                Global.Global.WriteLog("DesiltingTrips", LogMessageType.Error, ex.Message.ToString());
            }
            catch (Exception Modifiedex)
            {
                Global.Global.WriteLog("DesiltingTrips", LogMessageType.DownloadError, Modifiedex.Message.ToString());
            }

            return returnData;
        }

        #endregion [ Desilting Plant ]

        #region [ MCGM LIVE WORKS RELATED METHODS ]

        #region [ Asphalt ]

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private bool GetMCGM_HTML_ASPHALT_DATA()
        {
            bool returnData = false;
            DataSet dtset;

            int row_Index;
            int row_Count;
            string tableName;
            string file_Date;
            string dtStart_Date;
            string dtEnd_Date;
            string htmlDetails;

            DateTime dateWise_Index;
            DateTime dateWise_Count;
            DataTable dtDateWise_Data;
            DataTable dtTarget_MCGM;
            DataRow[] dtrDateWise_Data;
            string line_no = "0";

            try
            {
                //using (Scada_Sync_WebService.Skada_Sync_WSSoapClient live_Client = new Scada_Sync_WebService.Skada_Sync_WSSoapClient())
                //{
                //    line_no = "1";
                //    Scada_Sync_WebService.TransactionHeader live_Transaction = new Scada_Sync_WebService.TransactionHeader();
                //    live_Transaction.DeviceID = Program.ConfigMachine_Id;
                //    live_Transaction.Password = Program.ConfigService_Password;
                //    live_Transaction.UserName = Program.ConfigService_LoginID;
                //    live_Transaction.Config_App_Version = Program.ConfigAPP_Version;
                //    live_Transaction.Config_App_Name = Program.ConfigAPP_Name;
                //    live_Transaction.Win_Service_GUID = Program.Win_Service_GUID;
                //    live_Transaction.Win_Service_Version = Program.Win_Service_Version;
                //    Scada_Sync_WebService.paramUploadMCGMStruture pUploadData = new Scada_Sync_WebService.paramUploadMCGMStruture();
                //    pUploadData.Batch_No = 0;
                //    line_no = "1.1";
                //    Scada_Sync_WebService.Response_LocalSQLData live_Response = live_Client.GetMCGM_HTML_ASPHALT_DATA(live_Transaction, pUploadData);
                //    line_no = "2";
                //    dtset = live_Response.LocalSQLData;
                //    if (dtset.Tables.Count == 0)
                //    {
                //        return returnData;
                //    }

                //    dtTarget_MCGM = dtset.Tables[0];
                //    if (dtTarget_MCGM.Rows.Count == 0)
                //    {
                //        return returnData;
                //    }

                //    try
                //    {
                //        line_no = "3";
                //        Global.Global.WriteLog("HTML_ASPHALT_DATA", LogMessageType.Information, "Asphalt HTML File Creation Starts");
                //        tableName = dtTarget_MCGM.TableName;
                //        file_Date = string.Empty;
                //        dateWise_Index = Convert.ToDateTime(Convert.ToDateTime(dtTarget_MCGM.Compute("MIN(DATETIME)", "").ToString()).ToShortDateString());
                //        dateWise_Count = Convert.ToDateTime(Convert.ToDateTime(dtTarget_MCGM.Compute("MAX(DATETIME)", "").ToString()).ToShortDateString());

                //        while (dateWise_Index <= dateWise_Count)
                //        {
                //            file_Date = dateWise_Index.ToString("yyy-MM-dd");
                //            dtStart_Date = file_Date + " 00:00:01";
                //            dtEnd_Date = file_Date + " 23:59:59";
                //            dtDateWise_Data = new DataTable();

                //            dtrDateWise_Data = dtTarget_MCGM.Select("DATETIME >= '" + dtStart_Date + "' and DATETIME <= '" + dtEnd_Date + "'");
                //            if (dtrDateWise_Data.Length > 0)
                //            {
                //                dtDateWise_Data = dtrDateWise_Data.CopyToDataTable();
                //                if (dtDateWise_Data.Rows.Count > 0)
                //                {
                //                    dtDateWise_Data.TableName = tableName;
                //                    try
                //                    {
                //                        htmlDetails = this.Create_Asphalt_HTML_File(dtDateWise_Data, file_Date);
                //                        this.Write_HTML_File(htmlDetails, false, string.Empty, file_Date);
                //                        Global.Global.WriteLog("HTML_ASPHALT_DATA", LogMessageType.Information, "File Created for Date : " + file_Date);
                //                    }
                //                    catch (Exception ex)
                //                    {
                //                        Global.Global.WriteLog("HTML_ASPHALT_DATA", LogMessageType.Error, "Error while File Creating for Date : " + file_Date + Environment.NewLine + ex.Message);
                //                    }
                //                }
                //            }

                //            dateWise_Index = dateWise_Index.AddDays(1);
                //        }

                //        string[] columnsList = new string[1];
                //        columnsList[0] = "LOAD_NO";
                //        dtTarget_MCGM = dtTarget_MCGM.DefaultView.ToTable(true, columnsList);

                //        row_Index = 0;
                //        row_Count = dtTarget_MCGM.Rows.Count;
                //        for (row_Index = 0; row_Index < row_Count; row_Index++)
                //        {
                //            pUploadData.Batch_No = Convert.ToInt64(dtTarget_MCGM.Rows[row_Index]["LOAD_NO"].ToString());
                //            live_Client.Upload_MCGM_HTML_ASPHALT_DATA(live_Transaction, pUploadData);
                //        }
                //        Global.Global.WriteLog("HTML_ASPHALT_DATA", LogMessageType.Information, "Asphalt HTML File Creation Completes");
                //        returnData = true;
                //    }
                //    catch (Exception ex)
                //    {
                //        Global.Global.WriteLog("HTML_ASPHALT_DATA", LogMessageType.Error, ex.Message);
                //    }
                //}
            }
            catch (CommunicationException ex)
            {
                Global.Global.WriteLog("M N HTML_ASPHALT_DATA CM Except line_no = " + line_no, LogMessageType.Error, ex.Message.ToString());
            }
            catch (Exception exDe)
            {
                Global.Global.WriteLog("M N HTML_ASPHALT_DATA Except line_no = " + line_no, LogMessageType.Error, exDe.Message.ToString());
            }

            return returnData;
        }

        #region [ Asphalt HTML ]

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dtAsphalt"></param>
        /// <returns></returns>
        private string Create_Asphalt_HTML_File(DataTable dtAsphalt, string file_Date)
        {
            string htmlDetails = string.Empty;
            string tab_1 = "\t";
            string tab_2 = "\t\t";
            string tab_3 = "\t\t\t";
            string tab_4 = "\t\t\t\t";
            string tab_5 = "\t\t\t\t\t";
            string td_Open = "<td>";
            string td_Close = "</td>";

            StringBuilder sb = new StringBuilder();
            try
            {
                htmlDetails = this.Create_Asphalt_XML_File(dtAsphalt, file_Date);
                ////string head_Name = dtBatch_Dat_Trans.Rows[0]["ORGANIZATION_NAME"].ToString() == string.Empty ? Program.Organization_Name : dtBatch_Dat_Trans.Rows[0]["ORGANIZATION_NAME"].ToString() + " SCADA Application";
                string head_Name = Program.Organization_Name + " SCADA Application";

                #region [ HTML file format and tags ]
                sb.AppendLine("<html>");
                {
                    sb.AppendLine(tab_1 + "<head><title>" + DateTime.Now.ToString("yyyy-MM-dd") + "</title></head>");
                    sb.AppendLine(tab_1 + "<body>");
                    {
                        sb.AppendLine(tab_2 + "<div align='left'>");
                        {
                            sb.AppendLine(tab_3 + "<p align='center'><font face='MS Sans Serif'><strong>" + head_Name + "</strong></font></p>");
                            sb.AppendLine(tab_3 + "<p align='center'><strong><u>BATCH REPORT</u></strong></p>");

                            sb.AppendLine(tab_3 + "<div align='left'>");
                            {
                                sb.AppendLine(tab_4 + "<table border='1' cellspacing='0' width='100%'>");
                                sb.AppendLine(tab_5 + "<tr style='padding-top:10px;'>" + td_Open + "<strong></strong>" + td_Close + "</tr>");
                                sb.AppendLine(tab_4 + "</table>");
                            }
                            sb.AppendLine(tab_3 + "</div>");

                            sb.AppendLine(tab_3 + "<div align='left' style='padding-top:10px;'>");
                            sb.AppendLine(tab_4 + "<table border='1' cellspacing='1' width='80%'>");
                            sb.Append(htmlDetails);
                            sb.AppendLine(tab_4 + "</table>");
                            sb.AppendLine(tab_3 + "</div>");
                        }
                        sb.AppendLine(tab_2 + "</div>");
                    }
                    sb.AppendLine(tab_1 + "</body>");
                }
                sb.AppendLine("</html>");
                #endregion [ HTML file format and tags ]
            }
            catch (Exception ex)
            {
                Global.Global.WriteLog("HTML_Table Exception", LogMessageType.Error, ex.Message);
            }

            return sb.ToString();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dtAsphalt"></param>
        /// <returns></returns>
        private string Create_Asphalt_XML_File(DataTable dtAsphalt, string file_Date)
        {
            string returnData = string.Empty;
            try
            {
                string file_Name = string.Empty;
                string xml_Path = Program.ConfigXML_FILE_FOLDER_PATH.ToUpper();
                string directoryPath = string.IsNullOrEmpty(xml_Path) ? Program.CurrentApplicationPath : xml_Path;

                if (string.IsNullOrEmpty(xml_Path))
                {
                    directoryPath = Path.Combine(directoryPath, "XML", "Data");
                    if (!Directory.Exists(directoryPath))
                    {
                        Directory.CreateDirectory(directoryPath);
                    }
                }

                if (!Directory.Exists(directoryPath))
                {
                    Directory.CreateDirectory(directoryPath);
                }

                if (string.IsNullOrEmpty(file_Date))
                {
                    file_Name = directoryPath + "\\" + DateTime.Now.ToString("yyyy_MM_dd") + ".xml";
                }
                else
                {
                    file_Name = directoryPath + "\\" + Convert.ToDateTime(file_Date).ToString("yyyy_MM_dd") + ".xml";
                }

                if (File.Exists(file_Name))
                {
                    DataSet dsRead_XML = new DataSet();
                    dsRead_XML.ReadXml(file_Name);
                    dtAsphalt.Merge(dsRead_XML.Tables[0]);

                    File.Delete(file_Name);
                }

                dtAsphalt.WriteXml(file_Name);

                returnData = this.Convert_Asphalt_DataTable_HTML_Table(dtAsphalt);
            }
            catch (Exception ex)
            {
                Global.Global.WriteLog("XML_Table Exception", LogMessageType.Error, ex.Message);
            }

            return returnData;

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dtAsphalt"></param>
        /// <returns></returns>
        private string Convert_Asphalt_DataTable_HTML_Table(DataTable dtAsphalt)
        {
            return "\n<tr>" +
                string.Join(string.Empty, dtAsphalt.Columns.Cast<DataColumn>().Select(dc => "<td>" + dc.ColumnName + "</td>"))
                + "</tr>\n"
                + "<tr>"
                + string.Join("</tr>\n<tr>", dtAsphalt.AsEnumerable().Select(row => "<td>" + string.Join("</td><td>", row.ItemArray) + "</td>").ToArray())
                + "</tr>";
        }

        #endregion [ Asphalt HTML ]

        /// <summary>
        /// 
        /// </summary>
        /// <param name="htmlDetails"></param>
        private void Write_HTML_File(
            string htmlDetails,
            bool is_RMC,
            string batch_No,
            string file_Date
            )
        {

            string file_Name = string.Empty;
            string html_Path = Program.ConfigHTML_FILE_FOLDER_PATH.ToUpper();
            string directoryPath = string.IsNullOrEmpty(html_Path) ? Program.CurrentApplicationPath : html_Path;

            if (string.IsNullOrEmpty(html_Path))
            {
                directoryPath = Path.Combine(directoryPath, "HTML", "Data");
                if (!Directory.Exists(directoryPath))
                {
                    Directory.CreateDirectory(directoryPath);
                }
            }

            if (!Directory.Exists(directoryPath))
            {
                Directory.CreateDirectory(directoryPath);
            }

            if (is_RMC)
            {
                file_Name = directoryPath + "\\" + batch_No + ".html";
            }
            else
            {
                if (string.IsNullOrEmpty(file_Date))
                {
                    file_Name = directoryPath + "\\" + DateTime.Now.ToString("yyyy_MM_dd") + ".html";
                }
                else
                {
                    file_Name = directoryPath + "\\" + Convert.ToDateTime(file_Date).ToString("yyyy_MM_dd") + ".html";
                }
            }

            if (File.Exists(file_Name))
            {
                File.Delete(file_Name);
            }

            File.WriteAllText(file_Name, htmlDetails, Encoding.ASCII);
        }

        #endregion [ Asphalt ]

        #region [ Avoid Multiple Update's logic ]

        /// <summary>
        /// 
        /// </summary>
        /// <param name="is_Insert"></param>
        /// <param name="update_html_BDT_BR_BT">-1 for html ,0 for update_count</param>
        /// <param name="sourceConnectionString"></param>
        /// <param name="batch_No_Or_Row_ID"></param>
        /// <param name="batch_Index"></param>
        /// <param name="batch_Date"></param>
        /// <param name="batch_Time"></param>
        private void Insert_Update_Batch_Transaction_Upload_Status(
            bool is_Insert,
            int update_html_BDT_BR_BT,
            string sourceConnectionString,
            string batch_No_Or_Row_ID,
            string batch_Index,
            string batch_Date,
            string batch_Time
            )
        {

            string access_Query = string.Empty;
            if (is_Insert == true)
            {
                access_Query = "INSERT INTO BATCH_TRANSACTION_UPLOAD_STATUS(BATCH_NO ,BATCH_INDEX ,BATCH_DATE ,BATCH_TIME ,UPDATE_COUNT ,IS_HTML_FILE_CREATED ,FILE_CREATED_COUNT)VALUES(";
                access_Query += batch_No_Or_Row_ID;
                if (string.IsNullOrEmpty(batch_Index) == false)
                {
                    access_Query += "," + batch_Index; /*BATCH_INDEX*/
                }
                else
                {
                    access_Query += ",'0'"; /*BATCH_INDEX*/
                }
                if (string.IsNullOrEmpty(batch_Date) == false)
                {
                    access_Query += ",#" + batch_Date + "#"; /*BATCH_DATE*/
                }
                else
                {
                    access_Query += ",NULL"; /*BATCH_DATE*/
                }
                if (string.IsNullOrEmpty(batch_Time) == false)
                {
                    access_Query += ",#" + batch_Time + "#"; /*BATCH_TIME*/
                }
                else
                {
                    access_Query += ",NULL"; /*BATCH_TIME*/
                }
                access_Query += ",0"; /*UPDATE_COUNT*/
                access_Query += ",0"; /*IS_HTML_FILE_CREATED*/
                access_Query += ",0"; /*FILE_CREATED_COUNT*/
                access_Query += ")";
            }
            else
            {
                access_Query = "UPDATE BATCH_TRANSACTION_UPLOAD_STATUS SET ";
                if (update_html_BDT_BR_BT == -1)
                {
                    access_Query += "IS_HTML_FILE_CREATED = 1";
                    access_Query += ",FILE_CREATED_COUNT = 1";
                    access_Query += " WHERE BATCH_NO = " + batch_No_Or_Row_ID;
                }
                else if (update_html_BDT_BR_BT == 0)
                {
                    access_Query += "UPDATE_COUNT = UPDATE_COUNT + 1";
                    access_Query += " WHERE BATCH_NO = " + batch_No_Or_Row_ID;

                    if (string.IsNullOrEmpty(batch_Index) == false)
                    {
                        access_Query += " AND BATCH_INDEX = " + batch_Index; /*BATCH_INDEX*/
                    }

                    if (string.IsNullOrEmpty(batch_Date) == false)
                    {
                        access_Query += " AND BATCH_DATE = #" + batch_Date + "#"; /*BATCH_DATE*/
                    }

                    if (string.IsNullOrEmpty(batch_Time) == false)
                    {
                        access_Query += " AND BATCH_TIME = #" + batch_Time + "#"; /*BATCH_DATE*/
                    }
                }
                else if (update_html_BDT_BR_BT == 1)
                {
                    access_Query += "UPDATE_COUNT = 0";
                    access_Query += " WHERE BATCH_NO = " + batch_No_Or_Row_ID;

                    if (string.IsNullOrEmpty(batch_Index) == false)
                    {
                        access_Query += " AND BATCH_INDEX = " + batch_Index; /*BATCH_INDEX*/
                    }

                    if (string.IsNullOrEmpty(batch_Date) == false)
                    {
                        access_Query += " AND BATCH_DATE = #" + batch_Date + "#"; /*BATCH_DATE*/
                    }

                    if (string.IsNullOrEmpty(batch_Time) == false)
                    {
                        access_Query += " AND BATCH_TIME = #" + batch_Time + "#"; /*BATCH_DATE*/
                    }
                }
                else if (update_html_BDT_BR_BT == 2)
                {
                    access_Query += "IS_MCGM_BATCH = 1";
                    access_Query += " WHERE BATCH_NO = " + batch_No_Or_Row_ID;

                    if (string.IsNullOrEmpty(batch_Index) == false)
                    {
                        access_Query += " AND BATCH_INDEX = " + batch_Index; /*BATCH_INDEX*/
                    }

                    if (string.IsNullOrEmpty(batch_Date) == false)
                    {
                        access_Query += " AND BATCH_DATE = #" + batch_Date + "#"; /*BATCH_DATE*/
                    }

                    if (string.IsNullOrEmpty(batch_Time) == false)
                    {
                        access_Query += " AND BATCH_TIME = #" + batch_Time + "#"; /*BATCH_DATE*/
                    }
                }
            }

            int recordsAffected = 0;
            try
            {
                OleDbConnection ole_Source_Connection;
                using (ole_Source_Connection = new OleDbConnection(sourceConnectionString))
                {
                    ole_Source_Connection.Open();
                    using (OleDbCommand accessCommand = new OleDbCommand())
                    {
                        accessCommand.Connection = ole_Source_Connection;
                        accessCommand.CommandText = access_Query;
                        recordsAffected = accessCommand.ExecuteNonQuery();
                    }
                }

                if (recordsAffected == 0)
                {
                    access_Query = "Insert into BATCH_TRANSACTION_UPLOAD_STATUS(BATCH_NO ,BATCH_INDEX ,BATCH_DATE ,BATCH_TIME ,UPDATE_COUNT ,IS_HTML_FILE_CREATED ,FILE_CREATED_COUNT)VALUES(";
                    access_Query += batch_No_Or_Row_ID;
                    if (string.IsNullOrEmpty(batch_Index) == false)
                    {
                        access_Query += "," + batch_Index; /*BATCH_INDEX*/
                    }
                    else
                    {
                        access_Query += ",'0'"; /*BATCH_INDEX*/
                    }
                    if (string.IsNullOrEmpty(batch_Date) == false)
                    {
                        access_Query += ",#" + batch_Date + "#"; /*BATCH_DATE*/
                    }
                    else
                    {
                        access_Query += ",NULL"; /*BATCH_DATE*/
                    }
                    if (string.IsNullOrEmpty(batch_Time) == false)
                    {
                        access_Query += ",#" + batch_Time + "#"; /*BATCH_TIME*/
                    }
                    else
                    {
                        access_Query += ",NULL"; /*BATCH_DATE*/
                    }
                    access_Query += ",1"; /*UPDATE_COUNT*/
                    access_Query += ",0"; /*IS_HTML_FILE_CREATED*/
                    access_Query += ",0"; /*FILE_CREATED_COUNT*/
                    access_Query += ")";

                    using (ole_Source_Connection = new OleDbConnection(sourceConnectionString))
                    {
                        ole_Source_Connection.Open();
                        using (OleDbCommand accessCommand = new OleDbCommand())
                        {
                            accessCommand.Connection = ole_Source_Connection;
                            accessCommand.CommandText = access_Query;
                            recordsAffected = accessCommand.ExecuteNonQuery();
                        }
                    }
                }
            }
            catch (Exception)
            {
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sourceConnectionString"></param>
        /// <param name="table_Name"></param>
        /// <param name="field_Name"></param>
        /// <returns></returns>
        private bool FieldExists(string sourceConnectionString, string table_Name, string field_Name, string dataType, string defaultValue)
        {
            int recordsAffected = 0;
            OleDbConnection ole_Source_Connection;
            DataTable dtField_Exists = new DataTable();
            string access_Query = string.Empty;
            try
            {
                access_Query = "SELECT " + field_Name + " FROM " + table_Name + " WHERE 1 = 2 ";
                using (ole_Source_Connection = new OleDbConnection(sourceConnectionString))
                {
                    ole_Source_Connection.Open();
                    dtField_Exists = FillDatatableOLE(access_Query, ole_Source_Connection);
                }
            }
            catch (Exception)
            {
                access_Query = "ALTER TABLE " + table_Name + " ADD " + field_Name + " " + dataType;
                try
                {
                    using (ole_Source_Connection = new OleDbConnection(sourceConnectionString))
                    {
                        ole_Source_Connection.Open();
                        using (OleDbCommand accessCommand = new OleDbCommand())
                        {
                            accessCommand.Connection = ole_Source_Connection;
                            accessCommand.CommandText = access_Query;
                            recordsAffected = accessCommand.ExecuteNonQuery();

                            if (recordsAffected <= 0)
                            {
                                accessCommand.CommandText = "UPDATE " + table_Name + " SET " + field_Name + " = " + defaultValue;
                                recordsAffected = accessCommand.ExecuteNonQuery();
                            }

                        }

                    }
                }
                catch (Exception)
                {
                    using (ole_Source_Connection = new OleDbConnection(sourceConnectionString))
                    {
                        ole_Source_Connection.Open();
                        using (OleDbCommand accessCommand = new OleDbCommand())
                        {
                            accessCommand.Connection = ole_Source_Connection;
                            accessCommand.CommandText = "UPDATE " + table_Name + " SET " + field_Name + " = " + defaultValue;
                            recordsAffected = accessCommand.ExecuteNonQuery();
                        }
                    }
                }
            }

            return false;

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="is_Insert"></param>
        /// <param name="update_html_BDT_BR_BT">-1 for html ,0 for Bdt ,1 for br ,2 for BT</param>
        /// <param name="sourceConnectionString"></param>
        /// <param name="batch_No_Or_Row_ID"></param>
        private void Insert_Update_Batch_Master_Upload_Status(
            bool is_Insert,
            int update_html_BDT_BR_BT,
            string sourceConnectionString,
            string batch_No_Or_Row_ID
            )
        {
            string access_Query = string.Empty;
            if (is_Insert == true)
            {
                access_Query = "Insert into BATCH_MASTER_UPLOAD_STATUS(BATCH_NO ,BDT_UPDATE_COUNT ,BR_UPDATE_COUNT ,BT_UPDATE_COUNT ,IS_HTML_FILE_CREATED ,FILE_CREATED_COUNT)VALUES(";
                access_Query += batch_No_Or_Row_ID + ",0,0,0,0,0)";
            }
            else
            {
                access_Query = "UPDATE BATCH_MASTER_UPLOAD_STATUS SET ";
                if (update_html_BDT_BR_BT == -1)
                {
                    access_Query += "IS_HTML_FILE_CREATED = 1";
                    access_Query += ",FILE_CREATED_COUNT = 1";
                }
                else if (update_html_BDT_BR_BT == 0)
                {
                    access_Query += "BDT_UPDATE_COUNT = BDT_UPDATE_COUNT + 1";
                    access_Query += ",BR_UPDATE_COUNT = BR_UPDATE_COUNT + 1";
                }
                else if (update_html_BDT_BR_BT == 1)
                {
                    access_Query += "BDT_UPDATE_COUNT = BDT_UPDATE_COUNT + 1";
                    access_Query += ",BR_UPDATE_COUNT = BR_UPDATE_COUNT + 1";
                }
                else if (update_html_BDT_BR_BT == 2)
                {
                    access_Query += "BT_UPDATE_COUNT = BT_UPDATE_COUNT + 1";
                }

                access_Query += " WHERE BATCH_NO = " + batch_No_Or_Row_ID;
            }

            int recordsAffected = 0;
            try
            {
                OleDbConnection ole_Source_Connection;
                using (ole_Source_Connection = new OleDbConnection(sourceConnectionString))
                {
                    ole_Source_Connection.Open();
                    using (OleDbCommand accessCommand = new OleDbCommand())
                    {
                        accessCommand.Connection = ole_Source_Connection;
                        accessCommand.CommandText = access_Query;
                        recordsAffected = accessCommand.ExecuteNonQuery();
                    }
                }

                if (recordsAffected == 0)
                {
                    access_Query = "Insert into BATCH_MASTER_UPLOAD_STATUS(BATCH_NO ,BDT_UPDATE_COUNT ,BR_UPDATE_COUNT ,BT_UPDATE_COUNT ,IS_HTML_FILE_CREATED ,FILE_CREATED_COUNT)VALUES(";
                    access_Query += batch_No_Or_Row_ID + ",1,1,0,0,0)";

                    using (ole_Source_Connection = new OleDbConnection(sourceConnectionString))
                    {
                        ole_Source_Connection.Open();
                        using (OleDbCommand accessCommand = new OleDbCommand())
                        {
                            accessCommand.Connection = ole_Source_Connection;
                            accessCommand.CommandText = access_Query;
                            recordsAffected = accessCommand.ExecuteNonQuery();
                        }
                    }
                }

            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dtFinal"></param>
        /// <returns></returns>
        private string Return_Field_Value_with_Comma_Seperated(DataTable dtFinal, string columnName)
        {
            string returnData = string.Empty;
            returnData = string.Join(",", dtFinal.AsEnumerable().Select(row => string.Join(string.Empty, row.ItemArray[dtFinal.Columns.IndexOf(columnName)])).ToArray());
            return returnData;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tableName"></param>
        /// <returns></returns>
        private string GetField_Name(string tableName)
        {
            string returnData = string.Empty;
            tableName = tableName.ToLower();
            if (
                tableName == "batch_transaction"
                || tableName == "batch_result"
                || tableName == "batch_dat_trans"
                || tableName == "vitrag_batch"
                )
            {
                returnData = "Batch_no";
            }
            else if (
                tableName == "readrecord"
                )
            {
                returnData = "Row_Id";
            }
            else if (
                tableName == "bitumen"
                )
            {
                returnData = "Row_Id";
            }
            else if (
                tableName == "desilting_trips"
                )
            {
                returnData = "Id";
            }
            else if (tableName == "tb_batch")
            {
                returnData = "Id";
            }
            else if (tableName == "batch")
            {
                returnData = "BatchNo";
            }
            else if (tableName == "asphalt_batch")
            {
            }

            return returnData;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tableName"></param>
        /// <returns></returns>
        private bool Check_For_MCGM_Tables(string tableName)
        {
            tableName = tableName.ToLower();

            if (
                tableName == "tb_batch"
                || tableName == "readrecord"
                || tableName == "bitumen"

                || tableName == "batch"
                || tableName == "vitrag_batch"
                || tableName == "asphalt_batch"

                || tableName == "batch_transaction"
                || tableName == "batch_dat_trans"
                || tableName == "batch_result"

                || tableName == "desilting_trips"

                )
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sourceConnectionString"></param>
        /// <param name="dtFinal"></param>
        /// <param name="is_Table_Exist"></param>
        /// <returns></returns>
        private DataTable CheckWith_Batch_Upload_Status(
            string sourceConnectionString,
            DataTable dtFinal,
            bool is_Table_Exist
            )
        {
            int Config_Update_Count = 1;

            Config_Update_Count = Program.ConfigUpdate_Count;

            OleDbConnection ole_Source_Connection;
            DataTable dtBatch_upload_Status = null;
            DataTable returnData = dtFinal.Copy();
            string column_Name = "BDT_UPDATE_COUNT";
            string tableName = dtFinal.TableName.ToLower();
            string field_Name = GetField_Name(tableName);
            string access_Query = string.Empty;

            try
            {
                if (tableName == "batch_dat_trans")
                {
                    column_Name = "BDT_UPDATE_COUNT";
                }
                else if (tableName == "batch_result")
                {
                    column_Name = "BR_UPDATE_COUNT";
                }
                else if (
                    tableName == "batch_transaction"
                    || tableName == "readrecord"
                    || tableName == "tb_batch"
                    || tableName == "vitrag_batch"
                    || tableName == "desilting_trips"
                    )
                {
                    column_Name = "UPDATE_COUNT";
                }

                if (
                    tableName == "batch_dat_trans"
                    || tableName == "batch_result"
                    )
                {
                    access_Query = "SELECT ROW_INDEX ,BATCH_NO ,BDT_UPDATE_COUNT ,BR_UPDATE_COUNT ,BT_UPDATE_COUNT ,IS_HTML_FILE_CREATED FROM BATCH_MASTER_UPLOAD_STATUS ";
                }
                else
                {
                    access_Query = "SELECT ROW_INDEX ,BATCH_NO ,BATCH_INDEX ,BATCH_DATE ,BATCH_TIME ,UPDATE_COUNT ,IS_HTML_FILE_CREATED FROM BATCH_TRANSACTION_UPLOAD_STATUS";
                }

                if (is_Table_Exist)
                {
                    access_Query += " WHERE 1 = 0";
                }
                else
                {

                    if (tableName == "batch_dat_trans"
                        || tableName == "batch_result"
                        )
                    {
                        this.FieldExists(sourceConnectionString, "BATCH_MASTER_UPLOAD_STATUS", "FILE_CREATED_COUNT", "NUMBER", "0");
                        this.FieldExists(sourceConnectionString, "BATCH_MASTER_UPLOAD_STATUS", "IS_MCGM_BATCH", "NUMBER", "0");

                        this.FieldExists(sourceConnectionString, "BATCH_MASTER_UPLOAD_STATUS", "IS_MANUAL_CREATED", "NUMBER", "0");
                        this.FieldExists(sourceConnectionString, "BATCH_MASTER_UPLOAD_STATUS", "IS_MANUAL_CREATED_DATE", "DATETIME", "NULL");

                    }
                    else if (
                        tableName == "batch_transaction"
                        || tableName == "batch"
                        )
                    {
                        this.FieldExists(sourceConnectionString, "BATCH_TRANSACTION_UPLOAD_STATUS", "FILE_CREATED_COUNT", "NUMBER", "0");
                        this.FieldExists(sourceConnectionString, "BATCH_TRANSACTION_UPLOAD_STATUS", "IS_MCGM_BATCH", "NUMBER", "0");

                        this.FieldExists(sourceConnectionString, "BATCH_TRANSACTION_UPLOAD_STATUS", "IS_MANUAL_CREATED", "NUMBER", "0");
                        this.FieldExists(sourceConnectionString, "BATCH_TRANSACTION_UPLOAD_STATUS", "IS_MANUAL_CREATED_DATE", "DATETIME", "NULL");
                    }

                    //if (dtFinal.Rows.Count <= 10000)
                    //{
                    //    access_Query += " WHERE BATCH_NO in (" + Return_Field_Value_with_Comma_Seperated(dtFinal, field_Name) + ")";
                    //}
                    //else
                    {
                        access_Query += " WHERE BATCH_NO in (SELECT " + field_Name + " FROM " + tableName + ")";
                    }
                }
                using (ole_Source_Connection = new OleDbConnection(sourceConnectionString))
                {
                    ole_Source_Connection.Open();
                    dtBatch_upload_Status = this.FillDatatableOLE(access_Query, ole_Source_Connection);
                    ole_Source_Connection.Close();
                    ole_Source_Connection.Dispose();
                }
            }
            catch (Exception ex)
            {
                Global.Global.WriteLog("CWBUS : ", LogMessageType.Error, ex.Message + Environment.NewLine + access_Query);

                try
                {
                    using (ole_Source_Connection = new OleDbConnection(sourceConnectionString))
                    {
                        ole_Source_Connection.Open();
                        using (OleDbCommand accessCommand = new OleDbCommand())
                        {
                            accessCommand.Connection = ole_Source_Connection;

                            if (
                                tableName == "batch_dat_trans"
                                || tableName == "batch_result"
                                )
                            {
                                accessCommand.CommandText = @"
CREATE TABLE BATCH_MASTER_UPLOAD_STATUS (
ROW_INDEX AUTOINCREMENT,
BATCH_NO NUMBER,
BDT_UPDATE_COUNT NUMBER,
BR_UPDATE_COUNT NUMBER,
BT_UPDATE_COUNT NUMBER,
IS_HTML_FILE_CREATED NUMBER,
FILE_CREATED_COUNT NUMBER,
IS_MCGM_BATCH NUMBER
)";
                            }
                            else if (tableName == "batch_transaction")
                            {
                                accessCommand.CommandText = @"
CREATE TABLE BATCH_TRANSACTION_UPLOAD_STATUS (
ROW_INDEX AUTOINCREMENT,
BATCH_NO NUMBER,
BATCH_INDEX NUMBER,
BATCH_DATE DATETIME,
BATCH_TIME DATETIME,
UPDATE_COUNT NUMBER,
IS_HTML_FILE_CREATED NUMBER,
FILE_CREATED_COUNT NUMBER,
IS_MCGM_BATCH NUMBER
)";
                            }
                            accessCommand.ExecuteNonQuery();
                        }
                    }
                }
                catch (Exception)
                {
                }
            }

            if (dtBatch_upload_Status == null)
            {
                return returnData;
            }

            if (dtBatch_upload_Status.Rows.Count == 0)
            {
                return returnData;
            }

            DataRow[] dtrBatch;
            int row_Index = 0;
            int t_Row_Index = 0;
            string dtr_Query;
            int condition_Count = 0;
            //int br_condition_Count = 0;

            try
            {

                Global.Global.WriteLog("CWBUS ", LogMessageType.Information, "Removing Uploaded Records Starts for table = " + tableName);
                for (row_Index = 0; row_Index < dtFinal.Rows.Count; row_Index++)
                {


                    if (
                        tableName == "batch_dat_trans"
                        || tableName == "batch_result"
                        )
                    {
                        dtr_Query = "Batch_No = " + dtFinal.Rows[row_Index][field_Name].ToString();

                        //if (dtFinal.Rows[row_Index][field_Name].ToString() == "70825")
                        //{
                        //    DebugPrint(dtFinal.Rows[row_Index][field_Name].ToString());
                        //}
                    }
                    else if (tableName == "batch_transaction")
                    {
                        dtr_Query = "Batch_No = " + dtFinal.Rows[row_Index][field_Name].ToString() + " AND Batch_Index = " + dtFinal.Rows[row_Index]["Batch_Index"].ToString();
                    }
                    else if (
                        tableName == "readrecord"
                        || tableName == "tb_batch"
                        || tableName == "bitumen"
                        || tableName == "vitrag_batch"
                        || tableName == "desilting_trips"
                        )
                    {
                        dtr_Query = "Batch_No = " + dtFinal.Rows[row_Index][field_Name].ToString() + string.Empty;
                    }
                    else
                    {
                        dtr_Query = "1 = 0";
                    }

                    dtrBatch = dtBatch_upload_Status.Select(dtr_Query);
                    //Debug.Print(dtr_Query);


                    for (t_Row_Index = 0; t_Row_Index < dtrBatch.Length; t_Row_Index++)
                    {
                        if (
                            tableName == "batch_dat_trans"
                            || tableName == "batch_result"
                            )
                        {
                            condition_Count = (ConvertToInt32(dtrBatch[t_Row_Index]["BT_Update_Count"].ToString()) + Config_Update_Count);
                            if (
                                ConvertToInt32(dtrBatch[t_Row_Index][column_Name].ToString()) >= condition_Count
                                || ConvertToInt32(dtrBatch[t_Row_Index]["BR_Update_Count"].ToString()) >= condition_Count
                                )
                            {
                                if (
                                    tableName == "readrecord"
                                    || tableName == "tb_batch"
                                    || tableName == "bitumen"
                                    || tableName == "desilting_trips"
                                    )
                                {
                                    dtr_Query = field_Name + " = " + dtFinal.Rows[row_Index][field_Name].ToString() + string.Empty;
                                }

                                try
                                {
                                    DataRow[] dtrRemove = returnData.Select(dtr_Query);
                                    if (dtrRemove.Length > 0)
                                    {
                                        int remove_Row_Index = 0;
                                        int remove_Rows_Count = dtrRemove.Length;
                                        for (remove_Row_Index = 0; remove_Row_Index < remove_Rows_Count; remove_Row_Index++)
                                        {
                                            returnData.Rows.Remove(dtrRemove[remove_Row_Index]);
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    Global.Global.WriteLog("CWBUS Query : " + dtr_Query, LogMessageType.Error, ex.ToString());
                                }
                            }
                        }
                        else
                        {
                            condition_Count = Config_Update_Count;

                            if (ConvertToInt32(dtrBatch[t_Row_Index][column_Name].ToString()) >= condition_Count)
                            {
                                if (
                                    tableName == "readrecord"
                                    || tableName == "tb_batch"
                                    || tableName == "bitumen"
                                    || tableName == "desilting_trips"
                                    )
                                {
                                    dtr_Query = field_Name + " = " + dtFinal.Rows[row_Index][field_Name].ToString() + string.Empty;
                                }

                                try
                                {
                                    DataRow[] dtrRemove = returnData.Select(dtr_Query);
                                    if (dtrRemove.Length > 0)
                                    {
                                        int remove_Row_Index = 0;
                                        int remove_Rows_Count = dtrRemove.Length;
                                        for (remove_Row_Index = 0; remove_Row_Index < remove_Rows_Count; remove_Row_Index++)
                                        {
                                            returnData.Rows.Remove(dtrRemove[remove_Row_Index]);
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    Global.Global.WriteLog("CWBUS Query : " + dtr_Query, LogMessageType.Error, ex.ToString());
                                }
                            }
                        }
                    }
                }

                Global.Global.WriteLog("CWBUS ", LogMessageType.Information, "Removing Uploaded Records Completes for table = " + tableName);
            }
            catch (Exception ex)
            {
                Global.Global.WriteLog("CWBUS ", LogMessageType.Error, ex.ToString());
            }

            returnData.AcceptChanges();
            return returnData;
        }

        #endregion [ Avoid Multiple Update's logic ]

        #region [ RMC ]

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sourceConnectionString"></param>
        /// <returns></returns>
        private bool GetMCGM_HTML_RMC_DATA(string sourceConnectionString)
        {
            bool returnData = false;
            DataTable dtBatch_upload_Status = null;

            try
            {
                OleDbConnection ole_Source_Connection;
                string access_Query = string.Empty;
                access_Query = "SELECT DISTINCT BATCH_NO FROM BATCH_TRANSACTION_UPLOAD_STATUS WHERE IS_HTML_FILE_CREATED = 0 AND IS_MCGM_BATCH = 1";

                using (ole_Source_Connection = new OleDbConnection(sourceConnectionString))
                {
                    ole_Source_Connection.Open();
                    dtBatch_upload_Status = this.FillDatatableOLE(access_Query, ole_Source_Connection);
                    ole_Source_Connection.Close();
                    ole_Source_Connection.Dispose();
                }
            }
            catch (Exception)
            {
            }

            if (dtBatch_upload_Status == null)
            {
                return returnData;
            }

            if (dtBatch_upload_Status.Rows.Count == 0)
            {
                return returnData;
            }

            DataSet dsReturn_Value;
            string batch_No = string.Empty;
            int row_Index = 0;
            int row_Count = dtBatch_upload_Status.Rows.Count;
            string htmlDetails = string.Empty;

            Scada_Sync_WebService.TransactionHeader live_Transaction = new Scada_Sync_WebService.TransactionHeader();
            live_Transaction.DeviceID = Program.ConfigMachine_Id;
            live_Transaction.Password = Program.ConfigService_Password;
            live_Transaction.UserName = Program.ConfigService_LoginID;
            live_Transaction.Config_App_Version = Program.ConfigAPP_Version;
            live_Transaction.Config_App_Name = Program.ConfigAPP_Name;
            live_Transaction.Win_Service_GUID = Program.Win_Service_GUID;
            live_Transaction.Win_Service_Version = Program.Win_Service_Version;


            Scada_Sync_WebService.TransactionHeader local_Transaction = new Scada_Sync_WebService.TransactionHeader();
            local_Transaction.DeviceID = Program.ConfigMachine_Id;
            local_Transaction.Password = Program.ConfigService_Password;
            local_Transaction.UserName = Program.ConfigService_LoginID;
            local_Transaction.Config_App_Version = Program.ConfigAPP_Version;
            local_Transaction.Config_App_Name = Program.ConfigAPP_Name;
            local_Transaction.Win_Service_GUID = Program.Win_Service_GUID;
            local_Transaction.Win_Service_Version = Program.Win_Service_Version;

            try
            {
                dsReturn_Value = new DataSet();
                Global.Global.WriteLog("HTML_RMC_DATA", LogMessageType.Information, "RMC HTML File Creation Starts = " + row_Count);
                try
                {
                    for (row_Index = 0; row_Index < row_Count; row_Index++)
                    {
                        batch_No = dtBatch_upload_Status.Rows[row_Index]["Batch_No"].ToString();
                        if (Program.ConfigFile_Type == "0")
                        {
                            //using (Scada_Sync_WebService.Skada_Sync_WSSoapClient live_Client = new Scada_Sync_WebService.Skada_Sync_WSSoapClient())
                            //{
                            //    Scada_Sync_WebService.paramUploadMCGMStruture pUploadData = new Scada_Sync_WebService.paramUploadMCGMStruture();
                            //    pUploadData.Batch_No = ConvertToInt32(batch_No);
                            //    Scada_Sync_WebService.Response_LocalSQLData live_Response = live_Client.GetMCGM_HTML_RMC_DATA(live_Transaction, pUploadData);
                            //    dsReturn_Value = live_Response.LocalSQLData;
                            //}
                        }
                        else
                        {
                            using (Scada_Sync_WebService.PNF_GM_Sync_WSSoapClient local_Client = new Scada_Sync_WebService.PNF_GM_Sync_WSSoapClient())
                            {
                                Scada_Sync_WebService.paramUploadMCGMStruture pUploadData = new Scada_Sync_WebService.paramUploadMCGMStruture();
                                pUploadData.Batch_No = ConvertToInt32(batch_No);
                                Scada_Sync_WebService.Response_LocalSQLData live_Response = local_Client.GetMCGM_HTML_RMC_DATA(local_Transaction, pUploadData);
                                dsReturn_Value = live_Response.LocalSQLData;
                            }
                        }

                        if (dsReturn_Value.Tables.Count > 0)
                        {
                            if (dsReturn_Value.Tables.Count == 1)
                            {
                                Global.Global.WriteLog("HTML_RMC_DATA", LogMessageType.Information, " For " + batch_No + " " + dsReturn_Value.Tables[0].Rows[0]["ERROR_DESCRIPTION"].ToString());
                            }
                            else if (dsReturn_Value.Tables[0].Rows.Count > 0)
                            {
                                try
                                {
                                    htmlDetails = this.Create_RMC_HMTL_File(dsReturn_Value, batch_No);
                                    this.Write_HTML_File(htmlDetails, true, batch_No, string.Empty);

                                    Global.Global.WriteLog("HTML_RMC_DATA", LogMessageType.Information, " File created for Batch(s) : " + batch_No);
                                    this.Insert_Update_Batch_Transaction_Upload_Status(false, -1, sourceConnectionString,
                                        batch_No, string.Empty, string.Empty, string.Empty);
                                    returnData = true;
                                }
                                catch (Exception ex)
                                {
                                    Global.Global.WriteLog("M N HTML_RMC_DATA ", LogMessageType.Error, "Error while creating HTML file for Batch No(s) : " + batch_No + Environment.NewLine + ex.Message.ToString());
                                }
                            }
                        }

                        if ((row_Index % 10 == 0 && row_Index > 0)
                            || row_Index == row_Count)
                        {
                            Global.Global.WriteLog("HTML_RMC_DATA", LogMessageType.Information, " Total No. of Batch(s) : " + row_Count + " ,No. of Batch file has created : " + row_Index);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Global.Global.WriteLog("M N HTML_RMC_DATA ", LogMessageType.Error, ex.Message.ToString());
                }

                ////Global.Global.WriteLog("HTML_RMC_DATA", LogMessageType.Information, " Total No. of Batch(s) : " + row_Count + " ,No. of Batch file has created : " + row_Index);
                Global.Global.WriteLog("HTML_RMC_DATA", LogMessageType.Information, "RMC HTML File Creation Completed");
            }
            catch (CommunicationException ex)
            {
                Global.Global.WriteLog("M N HTML_RMC_DATA ", LogMessageType.Error, ex.Message.ToString());
            }
            catch (Exception exDe)
            {
                Global.Global.WriteLog("M N HTML_RMC_DATA ", LogMessageType.DownloadError, exDe.Message.ToString());
            }

            return returnData;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dtRMC"></param>
        /// <returns></returns>
        private string Create_RMC_HMTL_File(DataSet dtRMC, string batch_No)
        {
            string htmlDetails = string.Empty;
            string tab_1 = "\t";
            string tab_2 = "\t\t";
            string tab_3 = "\t\t\t";
            string tab_4 = "\t\t\t\t";
            string tab_5 = "\t\t\t\t\t";

            StringBuilder sb = new StringBuilder();

            DataTable dtBatch_Dat_Trans = dtRMC.Tables[0];
            DataTable dtBatch_Transaction = dtRMC.Tables[1];

            string trHeight_Open = "<tr height='25px' class='trBgColor'>";
            string tr_Close = "</tr>";
            string tdHeader_Open_Less_Than = "<td width='25%' id='";
            string td_Open_Less_Than = "<td id='";
            string td_Open_Greater_Than = "'>";
            string td_Close = "</td>";

            try
            {
                sb.AppendLine("<html>");
                {
                    sb.AppendLine(tab_1 + "<head><title>" + DateTime.Now.ToString("yyyy-MM-dd") + " " + batch_No + "</title></head>");
                    sb.AppendLine(tab_1 + "<style>.trBgColor{background-color:#FFFFFF;}</style>");

                    #region [ Body ]
                    sb.AppendLine(tab_1 + "<body>");
                    {
                        string head_Name = dtBatch_Dat_Trans.Rows[0]["ORGANIZATION_NAME"].ToString() == string.Empty
                            ? Program.Organization_Name
                            : dtBatch_Dat_Trans.Rows[0]["ORGANIZATION_NAME"].ToString() + " SCADA Application";

                        sb.AppendLine(tab_2 + "<div align='left'>");
                        sb.AppendLine(tab_3 + "<p align='center'><font face='MS Sans Serif'><strong>" + head_Name + "</strong></font></p>");
                        sb.AppendLine(tab_3 + "<p align='center'><strong><u>Autographic/Batch Report/Delivery Note</u></strong></p>");
                        sb.AppendLine(tab_2 + "</div>");

                        #region [ Header Div ]
                        sb.AppendLine(tab_2 + "<div align='left' style='padding-top:0px;background-color:#000;'>");
                        {
                            sb.AppendLine(tab_3 + "<table border='0' cellspacing='1' width='100%'>");
                            {
                                sb.AppendLine(tab_4 + trHeight_Open);
                                {
                                    sb.AppendLine(tab_5
                                        + tdHeader_Open_Less_Than + "Batch_Date_Header" + td_Open_Greater_Than + "Batch Date:" + td_Close
                                        + tdHeader_Open_Less_Than + "Batch_Date_Value" + td_Open_Greater_Than + Convert.ToDateTime(dtBatch_Dat_Trans.Rows[0]["Batch_Date"].ToString()).ToString("dd/MM/yyyy") + td_Close);

                                    sb.AppendLine(tab_5
                                        + tdHeader_Open_Less_Than + "Plant_Serial_No_Header" + td_Open_Greater_Than + "Plant Serial No:" + td_Close
                                        + tdHeader_Open_Less_Than + "Plant_Serial_No_Value" + td_Open_Greater_Than + dtBatch_Dat_Trans.Rows[0]["PlantSlNO"].ToString() + td_Close);
                                }
                                sb.AppendLine(tab_4 + tr_Close);

                                sb.AppendLine(tab_4 + trHeight_Open);
                                {
                                    sb.AppendLine(tab_5
                                       + tdHeader_Open_Less_Than + "Batch_Start_Time_Header" + td_Open_Greater_Than + "Batch Start Time:" + td_Close
                                       + tdHeader_Open_Less_Than + "Batch_Start_Time_Value" + td_Open_Greater_Than + dtBatch_Dat_Trans.Rows[0]["Batch_Start_Time"].ToString() + td_Close);
                                    sb.AppendLine(tab_5
                                        + tdHeader_Open_Less_Than + "Batch_End_Time_Header" + td_Open_Greater_Than + "Batch End Time:" + td_Close
                                        + tdHeader_Open_Less_Than + "Batch_End_Time_Value" + td_Open_Greater_Than + dtBatch_Dat_Trans.Rows[0]["Batch_End_Time"].ToString() + td_Close);
                                }
                                sb.AppendLine(tab_4 + tr_Close);

                                sb.AppendLine(tab_4 + trHeight_Open);
                                {
                                    string plant_App_Version = Program.PlantAppVersion;

                                    if (
                                        plant_App_Version == "3"
                                        || plant_App_Version == "3.0"
                                        || plant_App_Version == "6"
                                        || plant_App_Version == "6.0"
                                        || plant_App_Version == "6.2"
                                        )
                                    {
                                        plant_App_Version = "3";
                                    }

                                    sb.AppendLine(tab_5
                                    + tdHeader_Open_Less_Than + "Version_Header" + td_Open_Greater_Than + "Version:" + td_Close
                                    + tdHeader_Open_Less_Than + "Version_Value" + td_Open_Greater_Than + plant_App_Version + td_Close);
                                    sb.AppendLine(tab_5
                                        + tdHeader_Open_Less_Than + "Batcher_Name_Header" + td_Open_Greater_Than + "Batcher Name:" + td_Close
                                        + tdHeader_Open_Less_Than + "Batcher_Name_Value" + td_Open_Greater_Than + dtBatch_Dat_Trans.Rows[0]["Batcher_Name"].ToString() + td_Close);
                                }
                                sb.AppendLine(tab_4 + tr_Close);

                                sb.AppendLine(tab_4 + trHeight_Open);
                                {
                                    sb.AppendLine(tab_5
                                        + tdHeader_Open_Less_Than + "Batch_No_Header" + td_Open_Greater_Than + "Batch No:" + td_Close
                                        + tdHeader_Open_Less_Than + "Batch_No_Value" + td_Open_Greater_Than + batch_No + td_Close);

                                    sb.AppendLine(tab_5
                                        + tdHeader_Open_Less_Than + "Truck_Driver_Header" + td_Open_Greater_Than + "Truck Driver:" + td_Close
                                        + tdHeader_Open_Less_Than + "Truck_Driver_Value" + td_Open_Greater_Than + dtBatch_Dat_Trans.Rows[0]["Truck_Driver"].ToString() + td_Close);
                                }
                                sb.AppendLine(tab_4 + tr_Close);

                                sb.AppendLine(tab_4 + trHeight_Open);
                                {
                                    sb.AppendLine(tab_5
                                        + tdHeader_Open_Less_Than + "_Header" + td_Open_Greater_Than + "Customer:" + td_Close
                                        + tdHeader_Open_Less_Than + "_Value" + td_Open_Greater_Than + dtBatch_Dat_Trans.Rows[0]["CUSTOMER_NAME"].ToString() + td_Close);
                                    sb.AppendLine(tab_5
                                        + tdHeader_Open_Less_Than + "Production_Header" + td_Open_Greater_Than + "Production:" + td_Close
                                        + tdHeader_Open_Less_Than + "Production_Value" + td_Open_Greater_Than + dtBatch_Dat_Trans.Rows[0]["Production_Qty"].ToString() + td_Close);
                                }
                                sb.AppendLine(tab_4 + tr_Close);

                                sb.AppendLine(tab_4 + trHeight_Open);
                                {
                                    sb.AppendLine(tab_5
                                        + tdHeader_Open_Less_Than + "Site_Address_Header" + td_Open_Greater_Than + "Site Address:" + td_Close
                                        + tdHeader_Open_Less_Than + "Site_Address_Value" + td_Open_Greater_Than + dtBatch_Dat_Trans.Rows[0]["Site"].ToString() + td_Close);
                                    sb.AppendLine(tab_5
                                        + tdHeader_Open_Less_Than + "Returned_Header" + td_Open_Greater_Than + "Returned:" + td_Close
                                        + tdHeader_Open_Less_Than + "Returned_Value" + td_Open_Greater_Than + dtBatch_Dat_Trans.Rows[0]["Returned_Qty"].ToString() + td_Close);
                                }
                                sb.AppendLine(tab_4 + tr_Close);

                                sb.AppendLine(tab_4 + trHeight_Open);
                                {
                                    sb.AppendLine(tab_5
                                        + tdHeader_Open_Less_Than + "Recipe_Code_Header" + td_Open_Greater_Than + "Recipe Code:" + td_Close
                                        + tdHeader_Open_Less_Than + "Recipe_Code_Value" + td_Open_Greater_Than + dtBatch_Dat_Trans.Rows[0]["Recipe_Code"].ToString() + td_Close);
                                    sb.AppendLine(tab_5
                                        + tdHeader_Open_Less_Than + "Total_Order_Header" + td_Open_Greater_Than + "Total Order:" + td_Close
                                        + tdHeader_Open_Less_Than + "Total_Order_Value" + td_Open_Greater_Than + dtBatch_Dat_Trans.Rows[0]["Ordered_Qty"].ToString() + td_Close);
                                }
                                sb.AppendLine(tab_4 + tr_Close);

                                sb.AppendLine(tab_4 + trHeight_Open);
                                {
                                    sb.AppendLine(tab_5
                                    + tdHeader_Open_Less_Than + "Recipe_Name_Header" + td_Open_Greater_Than + "Recipe Name:" + td_Close
                                    + tdHeader_Open_Less_Than + "Recipe_Name_Value" + td_Open_Greater_Than + dtBatch_Dat_Trans.Rows[0]["Recipe_Name"].ToString() + td_Close);
                                    sb.AppendLine(tab_5
                                        + tdHeader_Open_Less_Than + "With_This_Load_Header" + td_Open_Greater_Than + "With This Load:" + td_Close
                                        + tdHeader_Open_Less_Than + "With_This_Load_Value" + td_Open_Greater_Than + dtBatch_Dat_Trans.Rows[0]["WithThisLoad"].ToString() + td_Close);
                                }
                                sb.AppendLine(tab_4 + tr_Close);

                                sb.AppendLine(tab_4 + trHeight_Open);
                                {
                                    sb.AppendLine(tab_5
                                        + tdHeader_Open_Less_Than + "Truck_No_Header" + td_Open_Greater_Than + "Truck No:" + td_Close
                                        + tdHeader_Open_Less_Than + "Truck_No_Value" + td_Open_Greater_Than + dtBatch_Dat_Trans.Rows[0]["Truck_No"].ToString() + td_Close);
                                    sb.AppendLine(tab_5
                                        + tdHeader_Open_Less_Than + "Batch_Size_Header" + td_Open_Greater_Than + "Batch Size:" + td_Close
                                        + tdHeader_Open_Less_Than + "Batch_Size_Value" + td_Open_Greater_Than + dtBatch_Dat_Trans.Rows[0]["Batch_Size"].ToString() + td_Close);
                                }
                                sb.AppendLine(tab_4 + tr_Close);
                            }
                            sb.AppendLine(tab_3 + "</table>");
                        }
                        sb.AppendLine(tab_2 + "</div>");

                        #endregion [ Header Div ]

                        sb.AppendLine(tab_2 + "<br></br>");

                        #region [ Listing Div ]
                        sb.AppendLine(tab_2 + "<div align='left' style='padding-top:0px;background-color:#000;'>");
                        {
                            sb.AppendLine(tab_3 + "<table border='0' cellspacing='1' width='100%'>");
                            {
                                int row_Index = 0;
                                int row_Count = dtBatch_Transaction.Rows.Count;
                                int column_Index = 0;
                                int column_Count = dtBatch_Transaction.Columns.Count;
                                int column_Start_Index = 0;

                                for (row_Index = 0; row_Index < row_Count; row_Index++)
                                {
                                    sb.AppendLine(tab_4 + trHeight_Open);

                                    if (row_Index == 0)
                                    {
                                        sb.AppendLine(tab_5 + td_Open_Less_Than + dtBatch_Transaction.Columns[0].ColumnName + "_" + row_Index + "_0" + td_Open_Greater_Than + string.Empty + td_Close);
                                    }
                                    else
                                    {
                                        sb.AppendLine(tab_5 + td_Open_Less_Than + dtBatch_Transaction.Columns[0].ColumnName + "_" + row_Index + "_0" + td_Open_Greater_Than + dtBatch_Transaction.Rows[row_Index][0] + td_Close);
                                    }
                                    column_Start_Index = 2;

                                    for (column_Index = column_Start_Index; column_Index < column_Count; column_Index++)
                                    {
                                        sb.AppendLine(tab_5 + td_Open_Less_Than + dtBatch_Transaction.Columns[column_Index].ColumnName + "_" + row_Index + "_" + column_Index + td_Open_Greater_Than + dtBatch_Transaction.Rows[row_Index][column_Index].ToString() + td_Close);
                                    }

                                    sb.AppendLine(tab_4 + tr_Close);
                                }
                            }

                            sb.AppendLine(tab_3 + "</table>");
                        }
                        sb.AppendLine(tab_2 + "</div>");
                        #endregion [ Listing Div ]
                    }
                    sb.AppendLine(tab_1 + "</body>");

                    #endregion [ Body ]

                }
                sb.AppendLine("</html>");

            }
            catch (Exception ex)
            {
                Global.Global.WriteLog("Create_RMC_HMTL_File Exception", LogMessageType.Error, ex.Message);
            }

            return sb.ToString();
        }

        #endregion [ RMC ]

        #endregion [ MCGM LIVE WORKS RELATED METHODS ]

        #region [ Private Methods ]

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dt_xml_Data"></param>
        private void Write_XML_File(string folder_Name, DataTable dt_xml_Data, string additional_File_Name)
        {
            Global.Global.Write_XML_File(folder_Name, dt_xml_Data, additional_File_Name);
        }

        /// <summary>
        /// Remove Duplicates Rows
        /// </summary>
        /// <param name="dtMainSource"></param>
        /// <param name="keyColumns"></param>
        private void RemoveDuplicatesRows(DataTable dtMainSource, List<string> keyColumns)
        {
            try
            {
                Dictionary<string, string> uniquenessDistinct = new Dictionary<string, string>(dtMainSource.Rows.Count);
                StringBuilder sb = null;
                int rowIndex = 0;
                DataRow currentRow;
                DataRowCollection dtRows = dtMainSource.Rows;

                while (rowIndex < dtRows.Count - 1)
                {
                    currentRow = dtRows[rowIndex];

                    sb = new StringBuilder();
                    foreach (string colname in keyColumns)
                    {
                        sb.Append(currentRow[colname].ToString().ToUpper());
                    }

                    if (uniquenessDistinct.ContainsKey(sb.ToString().ToUpper()))
                    {
                        dtRows.Remove(currentRow);
                    }
                    else
                    {
                        uniquenessDistinct.Add(sb.ToString(), string.Empty);
                        rowIndex++;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Get the Table Columns list
        /// </summary>
        /// <param name="tableName">Table Name</param>
        /// <param name="connectionString">Connection string</param>
        /// <returns></returns>
        private DataTable GetTableColumns(string tableName, string connectionString)
        {
            DataTable returnData = new DataTable();

            if (dtTablesList.Select("TABLE_NAME = '" + tableName + "'").Length > 0)
            {
                using (OleDbConnection oleConnection = new OleDbConnection(connectionString))
                {
                    oleConnection.Open();
                    using (OleDbDataAdapter objAdpter = new OleDbDataAdapter())
                    {
                        objAdpter.SelectCommand = new OleDbCommand(" SELECT * FROM " + tableName + " WHERE 1 = 2 ", oleConnection);
                        objAdpter.SelectCommand.CommandType = CommandType.Text;

                        using (DataTable dtDetails = new DataTable())
                        {
                            objAdpter.Fill(dtDetails);
                            dtDetails.TableName = "executeQuery";
                            returnData = dtDetails.Clone();
                        }
                    }

                    oleConnection.Close();
                    oleConnection.Dispose();
                }

                returnData.TableName = tableName;
            }

            return returnData;
        }

        /// <summary>
        /// Fill Datatable from Access Connection
        /// </summary>
        /// <param name="selectQuery">Select Query</param>
        /// <param name="connection">Connection object</param>
        /// <returns>Datatable</returns>
        private DataTable FillDatatableOLE(string selectQuery, OleDbConnection connection)
        {
            try
            {
                if (connection.State != ConnectionState.Open)
                {
                    connection.Open();
                }

                DataTable returnData = new DataTable();
                using (OleDbDataAdapter adapter = new OleDbDataAdapter())
                {
                    adapter.SelectCommand = new OleDbCommand(selectQuery, connection);
                    adapter.SelectCommand.CommandType = CommandType.Text;
                    adapter.Fill(returnData);
                }

                return returnData;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        /// <summary>
        /// Compare the Access Database by linq logic
        /// </summary>
        /// <param name="dtFirstTable">first DataTable</param>
        /// <param name="dtSecondTable">Second DataTable</param>
        /// <param name="tableName">table name</param>
        /// <returns>DataTable</returns>
        private DataTable CompareAccessDatabasebyLinq(DataTable dtFirstTable, DataTable dtSecondTable, string tableName = "Difference", bool localAccessDatabase = false)
        {
            try
            {
                int loopRowIndex;
                string columnName;
                string columnDataType;
                string[] arrayColumn;
                DataColumn columnAddnew;
                DataRow[] dtrXML;
                DataTable returnData = new DataTable(tableName);
                var varMismatch = dtFirstTable.AsEnumerable().Except(dtSecondTable.AsEnumerable(), DataRowComparer.Default);
                if (varMismatch != null && varMismatch.Count() > 0)
                {
                    #region [ Mismatch Find Logic ]

                    DataTable dtMismatch = (from a in varMismatch.AsEnumerable() select a).CopyToDataTable();
                    {
                        DataTable dtMisMatch_Data_Update = new DataTable();
                        DataTable dtMisMatch_Data_Insert = new DataTable();

                        dtrXML = xmlSync_Tables_Details.Select("XmlRelation ='" + tableName.ToString() + "' and XmlName='Key'");
                        if (dtrXML.Length > 0 && dtSecondTable.Rows.Count > 0)
                        {
                            if (dtrXML.Length == 1)
                            {
                                #region [ One Primary Column Logic ]

                                var qry1 = dtMismatch.AsEnumerable().Select(a => new
                                {
                                    Id1 = a[dtrXML[0]["XmlKey"].ToString()].ToString()
                                });

                                var qry2 = dtSecondTable.AsEnumerable().Select(b => new
                                {
                                    Id1 = b[dtrXML[0]["XmlKey"].ToString()].ToString()
                                });

                                var exceptTable = qry1.Except(qry2);
                                if (exceptTable != null && exceptTable.Count() > 0)
                                {
                                    var varMisMatch_Data_Insert = (from a in dtMismatch.AsEnumerable()
                                                                   join ab in exceptTable on a[dtrXML[0]["XmlKey"].ToString()].ToString() equals ab.Id1
                                                                   select a);

                                    if (varMisMatch_Data_Insert != null || varMisMatch_Data_Insert.Count() > 0)
                                    {
                                        dtMisMatch_Data_Insert = (from a in varMisMatch_Data_Insert.AsEnumerable()   /*inserted*/
                                                                  select a).CopyToDataTable();
                                    }
                                }

                                if (Program.Config_Insert_Only == false || localAccessDatabase == false)
                                {
                                    var Intersect = qry1.Intersect(qry2);
                                    if (Intersect != null && Intersect.Count() > 0)
                                    {
                                        var varMisMatch_Data_Update = (from a in dtMismatch.AsEnumerable()
                                                                       join ab in Intersect on a[dtrXML[0]["XmlKey"].ToString()].ToString() equals ab.Id1
                                                                       select a);


                                        if (varMisMatch_Data_Update != null && varMisMatch_Data_Update.Count() > 0)
                                        {
                                            dtMisMatch_Data_Update = (from a in varMisMatch_Data_Update.AsEnumerable() select a).CopyToDataTable();
                                        }
                                    }
                                }
                                #endregion [ One Primary Column Logic ]
                            }
                            else if (dtrXML.Length == 2)
                            {
                                #region [ Two Primary Column Logic ]
                                var qry1 = dtMismatch.AsEnumerable().Select(a => new
                                {
                                    Id1 = a[dtrXML[0]["XmlKey"].ToString()].ToString(),
                                    Id2 = a[dtrXML[1]["XmlKey"].ToString()].ToString()
                                });

                                var qry2 = dtSecondTable.AsEnumerable().Select(b => new
                                {
                                    Id1 = b[dtrXML[0]["XmlKey"].ToString()].ToString(),
                                    Id2 = b[dtrXML[1]["XmlKey"].ToString()].ToString()
                                });

                                var execptab = qry1.Except(qry2);

                                if (execptab != null && execptab.Count() > 0)
                                {
                                    var varMisMatch_Data_Insert = (from a in dtMismatch.AsEnumerable()
                                                                   join ab in execptab on a[dtrXML[0]["XmlKey"].ToString()].ToString() equals ab.Id1
                                                                   where a[dtrXML[1]["XmlKey"].ToString()].ToString() == ab.Id2
                                                                   select a);

                                    if (varMisMatch_Data_Insert != null && varMisMatch_Data_Insert.Count() > 0)
                                    {
                                        dtMisMatch_Data_Insert = (from a in varMisMatch_Data_Insert.AsEnumerable() select a).CopyToDataTable(); /*inserted*/
                                    }
                                }

                                if (Program.Config_Insert_Only == false || localAccessDatabase == false)
                                {
                                    var Intersect = qry1.Intersect(qry2);
                                    if (Intersect != null && Intersect.Count() > 0)
                                    {
                                        var varMisUpdate = (from a in dtMismatch.AsEnumerable()
                                                            join ab in Intersect on a[dtrXML[0]["XmlKey"].ToString()].ToString() equals ab.Id1
                                                            where a[dtrXML[1]["XmlKey"].ToString()].ToString() == ab.Id2
                                                            select a);

                                        if (varMisUpdate != null && varMisUpdate.Count() > 0)
                                        {
                                            dtMisMatch_Data_Update = (from a in varMisUpdate.AsEnumerable() select a).CopyToDataTable();
                                        }
                                    }
                                }
                                #endregion [ Two Primary Column Logic ]
                            }
                            else if (dtrXML.Length == 3)
                            {
                                #region [ Three Primary Column Logic ]

                                var qry1 = dtMismatch.AsEnumerable().Select(a => new
                                {
                                    Id1 = a[dtrXML[0]["XmlKey"].ToString()].ToString(),
                                    Id2 = a[dtrXML[1]["XmlKey"].ToString()].ToString(),
                                    Id3 = a[dtrXML[2]["XmlKey"].ToString()].ToString()
                                });

                                var qry2 = dtSecondTable.AsEnumerable().Select(b => new
                                {
                                    Id1 = b[dtrXML[0]["XmlKey"].ToString()].ToString(),
                                    Id2 = b[dtrXML[1]["XmlKey"].ToString()].ToString(),
                                    Id3 = b[dtrXML[2]["XmlKey"].ToString()].ToString()
                                });

                                var execptab = qry1.Except(qry2);

                                if (execptab != null && execptab.Count() > 0)
                                {
                                    var varMisMatch_Data_Insert = (from a in dtMismatch.AsEnumerable()
                                                                   join ab in execptab on a[dtrXML[0]["XmlKey"].ToString()].ToString() equals ab.Id1
                                                                   where
                                                                   a[dtrXML[1]["XmlKey"].ToString()].ToString() == ab.Id2 &&
                                                                   a[dtrXML[2]["XmlKey"].ToString()].ToString() == ab.Id3
                                                                   select a);
                                    if (varMisMatch_Data_Insert != null && varMisMatch_Data_Insert.Count() > 0)
                                    {
                                        dtMisMatch_Data_Insert = (from a in varMisMatch_Data_Insert.AsEnumerable() select a).CopyToDataTable();          /*inserted*/
                                    }
                                }

                                if (Program.Config_Insert_Only == false || localAccessDatabase == false)
                                {
                                    var Intersect = qry1.Intersect(qry2);
                                    if (Intersect != null && Intersect.Count() > 0)
                                    {
                                        var varMisUpdate = (from a in dtMismatch.AsEnumerable()
                                                            join ab in Intersect on a[dtrXML[0]["XmlKey"].ToString()].ToString() equals ab.Id1
                                                            where
                                                            a[dtrXML[1]["XmlKey"].ToString()].ToString() == ab.Id2 &&
                                                            a[dtrXML[2]["XmlKey"].ToString()].ToString() == ab.Id3
                                                            select a);
                                        if (varMisUpdate != null && varMisUpdate.Count() > 0)
                                        {
                                            dtMisMatch_Data_Update = (from a in varMisUpdate.AsEnumerable() select a).CopyToDataTable();
                                        }
                                    }
                                }
                                #endregion [ Three Primary Column Logic ]
                            }
                            else /*if (dtrXML.Length == 4)*/
                            {
                                #region [ Four Primary Column Logic ]

                                var qry1 = dtMismatch.AsEnumerable().Select(a => new
                                {
                                    Id1 = a[dtrXML[0]["XmlKey"].ToString()].ToString(),
                                    Id2 = a[dtrXML[1]["XmlKey"].ToString()].ToString(),
                                    Id3 = a[dtrXML[2]["XmlKey"].ToString()].ToString(),
                                    Id4 = a[dtrXML[3]["XmlKey"].ToString()].ToString()
                                });

                                var qry2 = dtSecondTable.AsEnumerable().Select(b => new
                                {
                                    Id1 = b[dtrXML[0]["XmlKey"].ToString()].ToString(),
                                    Id2 = b[dtrXML[1]["XmlKey"].ToString()].ToString(),
                                    Id3 = b[dtrXML[2]["XmlKey"].ToString()].ToString(),
                                    Id4 = b[dtrXML[3]["XmlKey"].ToString()].ToString()
                                });

                                var execptab = qry1.Except(qry2);

                                if (execptab != null && execptab.Count() > 0)
                                {
                                    var varMisMatch_Data_Insert = (from a in dtMismatch.AsEnumerable()
                                                                   join ab in execptab
                                                                   on a[dtrXML[0]["XmlKey"].ToString()].ToString() equals ab.Id1
                                                                   where
                                                                   a[dtrXML[1]["XmlKey"].ToString()].ToString() == ab.Id2 &&
                                                                   a[dtrXML[2]["XmlKey"].ToString()].ToString() == ab.Id3 &&
                                                                   a[dtrXML[3]["XmlKey"].ToString()].ToString() == ab.Id4
                                                                   select a);

                                    if (varMisMatch_Data_Insert != null && varMisMatch_Data_Insert.Count() > 0)
                                    {
                                        dtMisMatch_Data_Insert = (from a in varMisMatch_Data_Insert.AsEnumerable() select a).CopyToDataTable();          /*inserted*/
                                    }
                                }

                                if (Program.Config_Insert_Only == false || localAccessDatabase == false)
                                {
                                    var Intersect = qry1.Intersect(qry2);
                                    if (Intersect != null && Intersect.Count() > 0)
                                    {
                                        var varMisUpdate = (from a in dtMismatch.AsEnumerable()
                                                            join ab in Intersect on a[dtrXML[0]["XmlKey"].ToString()].ToString() equals ab.Id1
                                                            where
                                                            a[dtrXML[1]["XmlKey"].ToString()].ToString() == ab.Id2 &&
                                                            a[dtrXML[2]["XmlKey"].ToString()].ToString() == ab.Id3 &&
                                                            a[dtrXML[3]["XmlKey"].ToString()].ToString() == ab.Id4
                                                            select a);

                                        if (varMisUpdate != null && varMisUpdate.Count() > 0)
                                        {
                                            dtMisMatch_Data_Update = (from a in varMisUpdate.AsEnumerable() select a).CopyToDataTable();
                                        }
                                    }
                                }

                                #endregion [ Four Primary Column Logic ]
                            }

                            if (dtMisMatch_Data_Update.Rows.Count > 0)
                            {
                                DataColumn columnUpdate = new DataColumn("UpdateInsert", typeof(string));
                                columnUpdate.DefaultValue = "Update";
                                dtMisMatch_Data_Update.Columns.Add(columnUpdate);
                                returnData = dtMisMatch_Data_Update;

                                Global.Global.WriteLog("CAD :byLinq : ", LogMessageType.Information, "Count for update record " + dtMisMatch_Data_Update.Rows.Count.ToString() + " in table " + tableName);
                            }

                            if (dtMisMatch_Data_Insert.Rows.Count > 0)
                            {
                                DataColumn columnInsert = new DataColumn("UpdateInsert", typeof(string));
                                columnInsert.DefaultValue = "Insert";
                                dtMisMatch_Data_Insert.Columns.Add(columnInsert);
                                if (returnData.Rows.Count > 0)
                                {
                                    returnData.Merge(dtMisMatch_Data_Insert);
                                }
                                else
                                {
                                    returnData = dtMisMatch_Data_Insert;
                                }

                                Global.Global.WriteLog("CAD :byLinq : ", LogMessageType.Information, "Count for insert record " + dtMisMatch_Data_Insert.Rows.Count.ToString() + " in table " + tableName);
                            }
                        }
                        else
                        {
                            if (dtMismatch.Rows.Count > 0)
                            {
                                DataColumn columnUpdate = new DataColumn("UpdateInsert", typeof(string));
                                columnUpdate.DefaultValue = "Insert";
                                dtMismatch.Columns.Add(columnUpdate);
                                returnData = dtMismatch;

                                Global.Global.WriteLog("CAD :byLinq : ", LogMessageType.Information, "Count for insert record " + dtMismatch.Rows.Count.ToString() + " in table " + tableName);
                            }
                        }

                        if (dtMisMatch_Data_Update != null)
                        {
                            dtMisMatch_Data_Update.Dispose();
                        }

                        if (dtMisMatch_Data_Insert != null)
                        {
                            dtMisMatch_Data_Insert.Dispose();
                        }
                    }

                    #endregion [ Mismatch Find Logic ]
                }
                else
                {
                    return returnData;
                }

                if (Program.writeFile)
                {
                    this.CreateQueriesinfile(returnData, dtFirstTable, tableName);
                }

                #region [ Add Org_Id ,Identity_Column Field to DataTable ]

                #region [ Add Identity_Column Field to DataTable ]
                DataTable dtReturnTable = returnData.Clone();
                DataColumn identColumn = new DataColumn();
                identColumn.ColumnName = "Identity_Column";
                identColumn.DataType = Type.GetType("System.Int32");
                identColumn.AutoIncrement = true;
                identColumn.AutoIncrementSeed = 1;
                identColumn.AutoIncrementStep = 1;

                dtReturnTable.Columns.Add(identColumn);
                dtReturnTable.AcceptChanges();

                returnData.Select().CopyToDataTable(dtReturnTable, LoadOption.OverwriteChanges);
                returnData = dtReturnTable;
                #endregion [ Add Identity_Column Field to DataTable ]

                #region [ Add Org_Id Field to DataTable ]

                DataRow[] dtrXMLAddRows;
                /* Add new column in datatable which define in syncs table xml file*/
                dtrXMLAddRows = xmlSync_Tables_Details.Select("XmlRelation ='" + tableName.ToString() + "' and XmlName='AddKey'");
                if (dtrXMLAddRows.Length > 0)
                {
                    for (loopRowIndex = 0; loopRowIndex < dtrXMLAddRows.Length; loopRowIndex++)
                    {
                        columnAddnew = new DataColumn();
                        arrayColumn = null;
                        columnName = string.Empty;
                        columnDataType = string.Empty;
                        arrayColumn = dtrXMLAddRows[loopRowIndex]["XmlKey"].ToString().Split(new string[] { "," }, StringSplitOptions.None);
                        if (arrayColumn.Count() == 2)
                        {
                            columnName = arrayColumn[0].ToString();
                            columnDataType = arrayColumn[1].ToString().ToLower();
                            if (!returnData.Columns.Contains(columnName))
                            {
                                if (columnDataType == "int")
                                {
                                    columnAddnew = new DataColumn(columnName, typeof(Int32));
                                }
                                else if (columnDataType == "decimal")
                                {
                                    columnAddnew = new DataColumn(columnName, typeof(decimal));
                                }
                                else if (columnDataType == "double")
                                {
                                    columnAddnew = new DataColumn(columnName, typeof(double));
                                }
                                else if (columnDataType == "string")
                                {
                                    columnAddnew = new DataColumn(columnName, typeof(string));
                                }
                                else if (columnDataType == "date")
                                {
                                    columnAddnew = new DataColumn(columnName, typeof(DateTime));
                                }
                                else /*(columnDataType == "string")*/
                                {
                                    columnAddnew = new DataColumn(columnName, typeof(string));
                                }

                                columnAddnew.DefaultValue = Program.OrganizationId.ToString();
                                returnData.Columns.Add(columnAddnew);
                            }
                        }
                    }
                }

                #endregion [ Add Org_Id Field to DataTable ]

                #endregion [ Add Org_Id ,Identity_Column Field to DataTable ]

                return returnData;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
            finally
            {
                GC.Collect();
            }
        }

        /// <summary>
        /// Generate the Queries to File
        /// </summary>
        /// <param name="dtFinal">Final DataTable</param>
        /// <param name="dtSource"></param>
        /// <param name="tableName"></param>
        private void CreateQueriesinfile(DataTable dtFinal, DataTable dtSource, string tableName = "Difference")
        {
            try
            {
                if (dtSource == null)
                {
                    return;
                }

                string directoryPath = Path.Combine(Program.CurrentApplicationPath, "QueryOutput");
                if (!Directory.Exists(directoryPath))
                {
                    Directory.CreateDirectory(directoryPath);
                }

                string fileName = directoryPath + "\\Query_" + DateTime.Now.ToString("YYY_MM_dd") + ".txt";
                string commaValue;
                string Query = string.Empty;
                string strwhere = string.Empty;
                string result;

                DataRow dtrFinal;
                int loopRowIndex;
                int loopColumnIndex;

                for (loopRowIndex = 0; loopRowIndex < dtFinal.Rows.Count; loopRowIndex++)
                {
                    dtrFinal = dtFinal.Rows[loopRowIndex];
                    strwhere = string.Empty;
                    Query = string.Empty;
                    commaValue = string.Empty;
                    result = string.Empty;

                    if (dtrFinal["UpdateInsert"].ToString().ToLower() == "update")
                    {
                        strwhere = this.BuildWhereCondition(dtrFinal, tableName);
                        for (loopColumnIndex = 0; loopColumnIndex < dtSource.Columns.Count; loopColumnIndex++)
                        {
                            result = this.BuildResultFields(dtSource.Columns[loopColumnIndex], dtrFinal[loopColumnIndex].ToString(), tableName);
                            Query += (result.Length > 0 ? commaValue + result : string.Empty);
                            commaValue = ",";
                        }

                        strwhere = ((Query.Length > 0) ? " WHERE " : string.Empty) + strwhere;
                        Query = @"Update " + tableName + " SET " + Query + strwhere + string.Empty;
                    }
                    else
                    {
                        for (loopColumnIndex = 0; loopColumnIndex < dtSource.Columns.Count; loopColumnIndex++)
                        {
                            result = this.BuildResultFields(dtSource.Columns[loopColumnIndex], dtrFinal[loopColumnIndex].ToString(), string.Empty);
                            Query += (result.Length > 0 ? commaValue + result : string.Empty);
                            commaValue = ",";
                        }

                        Query = @"INSERT INTO " + tableName + " VALUES(" + Query + ")";
                    }

                    using (StreamWriter strBuildQuery = new StreamWriter(fileName, true))
                    {
                        strBuildQuery.WriteLine(Query);
                    }

                }
            }
            catch (Exception ex)
            {
                Global.Global.WriteLog("InsertUpdate Query file created:", LogMessageType.Error, ex.Message.ToString());
            }
        }

        /// <summary>
        /// Build Where Condition
        /// </summary>
        /// <param name="dtrDetails">datarow</param>
        /// <param name="tableName">table name</param>
        /// <returns>where condition string</returns>
        private string BuildWhereCondition(DataRow dtrDetails, string tableName)
        {
            try
            {
                string _strwhere = string.Empty;
                DataRow[] _dtxmlrow = xmlSync_Tables_Details.Select("XmlRelation ='" + tableName.ToString() + "' and XmlName='Key'");
                if (_dtxmlrow.Length > 0)
                {
                    for (int rowno = 0; rowno < _dtxmlrow.Length; rowno++)
                    {
                        string strc = _dtxmlrow[rowno]["XmlKey"].ToString();
                        _strwhere = _strwhere + ((_strwhere.ToString() == string.Empty) ? string.Empty : " And ") + _dtxmlrow[rowno]["XmlKey"].ToString() + " = '" + dtrDetails[strc].ToString() + "'";
                    }
                }
                return _strwhere;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dataColumn"></param>
        /// <param name="strvalue"></param>
        /// <param name="tableName"></param>
        /// <returns></returns>
        private string BuildResultFields(DataColumn dataColumn, string strvalue, string tableName)
        {
            string columnName = dataColumn.ColumnName;
            string dataType = dataColumn.DataType.Name.ToString();
            string returnData = string.Empty;

            if (string.IsNullOrEmpty(columnName.Trim()) == false && string.IsNullOrEmpty(tableName.Trim()) == false)
            {
                DataRow[] _dtxmlrow = xmlSync_Tables_Details.Select("XmlRelation ='" + tableName.ToString() + "' and XmlKey='" + columnName + "'");
                if (_dtxmlrow.Length > 0)
                {
                    return returnData;
                }
            }

            if (strvalue == DBNull.Value.ToString())
            {
                returnData = "null";
            }
            else
            {
                if (dataType == "String")
                {
                    returnData = string.Format("'{0}'", strvalue);
                }
                else if (dataType == "DateTime")
                {
                    returnData = string.Format("#{0}#", strvalue);
                }
                else
                {
                    returnData = (string.IsNullOrEmpty(strvalue) ? "0" : strvalue);
                }
            }

            if (string.IsNullOrEmpty(columnName.Trim()) == false && string.IsNullOrEmpty(tableName.Trim()) == false)
            {
                returnData = columnName + " = " + returnData;
            }

            return returnData;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dtRenameTable"></param>
        /// <returns></returns>
        private DataTable RenameFieldsofDataTable(DataTable dtRenameTable)
        {
            string tableNameasFileName = string.Empty;

            try
            {
                tableNameasFileName = dtRenameTable.Rows[1]["TableName"].ToString();
            }
            catch (Exception)
            {
            }

            if (string.IsNullOrEmpty(tableNameasFileName))
            {
                tableNameasFileName = dtRenameTable.TableName;
            }

            string newcolumn_Name = string.Empty;
            string column_Name = string.Empty;

            for (int i = 0; i < dtRenameTable.Columns.Count; i++)
            {
                column_Name = dtRenameTable.Columns[i].ColumnName;

                DataRow[] dtrColumnName = xmlCSV_Tables_Details.Select("TableName = '" + tableNameasFileName + "' and colName = '" + column_Name + "'");

                if (dtrColumnName.Length > 0)
                {
                    if (Program.ConfigMAP_TABLE_FORMAT)
                    {
                        if (dtrColumnName[0]["COLNAME"].ToString().ToLower() == "batch_time")
                        {
                            dtRenameTable.Columns.Add("Time", typeof(System.DateTime));
                            dtRenameTable.Columns.Add("Year", typeof(System.String));
                            dtRenameTable.Columns.Add("Month", typeof(System.String));

                            for (int rowIndex = 0; rowIndex < dtRenameTable.Rows.Count; rowIndex++)
                            {
                                dtRenameTable.Rows[rowIndex]["Time"] = dtRenameTable.Rows[rowIndex][i];
                                dtRenameTable.Rows[rowIndex]["Year"] = Convert.ToDateTime(dtRenameTable.Rows[rowIndex][i]).Year;
                                dtRenameTable.Rows[rowIndex]["Month"] = Convert.ToDateTime(dtRenameTable.Rows[rowIndex][i]).Month;
                            }
                        }
                    }

                    newcolumn_Name = dtrColumnName[0]["Value"].ToString();
                }

                if (string.IsNullOrEmpty(newcolumn_Name) == false)
                {
                    dtRenameTable.Columns[i].ColumnName = newcolumn_Name;
                }

                newcolumn_Name = string.Empty;
            }

            return dtRenameTable;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sqlTableName"></param>
        /// <param name="accessTableName"></param>
        /// <param name="map_table_format"></param>
        /// <returns></returns>
        private bool GetAccessTableName(string sqlTableName, out  string accessTableName, bool map_table_format)
        {
            bool returnData = false;
            try
            {
                accessTableName = string.Empty;

                if (map_table_format == false)
                {
                    DataRow[] dtrxmlData = xmlSync_Tables_Details.Select("XMLKey ='" + sqlTableName + "'");
                    if (dtrxmlData.Length > 0)
                    {
                        accessTableName = dtrxmlData[0]["XMLRelation"].ToString();
                        returnData = true;
                    }
                }
                else
                {
                    DataRow[] dtrxmlData = xmlCSV_Tables_Details.Select("TableName ='" + sqlTableName + "'");
                    if (dtrxmlData.Length > 0)
                    {
                        accessTableName = dtrxmlData[0]["Value"].ToString();
                        returnData = true;
                    }
                }

            }
            catch (Exception)
            {
                throw;
            }
            return returnData;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sqlTableName"></param>
        /// <param name="accessTableName"></param>
        /// <returns></returns>
        private bool GetAccessTableName(string sqlTableName, out  string accessTableName)
        {
            return GetAccessTableName(sqlTableName, out accessTableName, false);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        private bool ConvertToBoolean(string value)
        {
            bool returnData = false;
            if (bool.TryParse(value.ToString(), out returnData) == false)
            {
                returnData = false;
            }

            return returnData;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        private double ConvertToDouble(string value)
        {
            double returnData = 0;
            if (double.TryParse(value.ToString(), out returnData) == false)
            {
                returnData = 0;
            }
            return returnData;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        private int ConvertToInt32(object value)
        {
            int returnData = 0;
            if (int.TryParse(value.ToString(), out returnData) == false)
            {
                returnData = 0;
            }
            return returnData;
        }

        /// <summary>
        /// Debug print method
        /// </summary>
        /// <param name="printMessage">message to print</param>
        private void DebugPrint(string printMessage)
        {
            Debug.Print(printMessage);
        }

        #endregion [ Private Methods ]

        #region [ Dispose Method ]

        // Pointer to an external unmanaged resource. 
        private IntPtr handle;

        // Other managed resource this class uses. 
        private Component component = new Component();

        // Track whether Dispose has been called. 
        private bool disposed = false;

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Dispose(bool disposing) executes in two distinct scenarios. 
        /// If disposing equals true, the method has been called directly 
        /// or indirectly by a user's code. Managed and unmanaged resources 
        /// can be disposed. 
        /// If disposing equals false, the method has been called by the 
        /// runtime from inside the finalizer and you should not reference 
        /// other objects. Only unmanaged resources can be disposed. 
        /// </summary>
        /// <param name="disposing"></param>
        protected virtual void Dispose(bool disposing)
        {
            // Check to see if Dispose has already been called. 
            if (!this.disposed)
            {
                // If disposing equals true, dispose all managed 
                // and unmanaged resources. 
                if (disposing)
                {
                    // Dispose managed resources.
                    component.Dispose();
                }

                // Call the appropriate methods to clean up 
                // unmanaged resources here. 
                // If disposing is false, 
                // only the following code is executed.
                CloseHandle(handle);
                handle = IntPtr.Zero;

                // Note disposing has been done.
                disposed = true;

            }
        }

        /// <summary>
        /// Use interop to call the method necessary 
        /// to clean up the unmanaged resource.
        /// </summary>
        /// <param name="handle"></param>
        /// <returns></returns>
        [System.Runtime.InteropServices.DllImport("Kernel32")]
        private extern static Boolean CloseHandle(IntPtr handle);

        /// <summary>
        /// Use C# destructor syntax for finalization code. 
        /// This destructor will run only if the Dispose method 
        /// does not get called. 
        /// It gives your base class the opportunity to finalize. 
        /// Do not provide destructors in types derived from this class.
        /// </summary>
        ~CompareDatabase()
        {
            // Do not re-create Dispose clean-up code here. 
            // Calling Dispose(false) is optimal in terms of 
            // readability and maintainability.
            Dispose(false);
        }

        #endregion [ Dispose Method ]
    }
    #endregion [ Class ]
}
