﻿using System;
using System.Diagnostics;
using System.Management;
using System.Windows.Forms;
using ScadaKing_Data_App.Class;
using System.Net;

namespace ScadaKing_Data_App
{
    static class Program
    {

        public static string ConnetionXML_FileName_Path;

        #region [ Variables ]

        ////internal static bool StartService;

        /// <summary>
        /// 
        /// </summary>
        /// 
        internal static string Round_Digits;

        /// <summary>
        /// 
        /// </summary>
        internal static string rdoMCGM;

        /// <summary>
        /// 
        /// </summary>
        internal static string rdoNonMCGM;

        /// <summary>
        /// 
        /// </summary>
        internal static string hideDateFromRecipe;

        /// <summary>
        /// 
        /// </summary>
        internal static bool syncStop;

        /// <summary>
        /// 
        /// </summary>
        internal static bool writeFile;

        /// <summary>
        /// 
        /// </summary>
        internal static string gsz_PLC_SourceConnectionString;

        /// <summary>
        /// 
        /// </summary>
        internal static string gszConnectionString;

        /// <summary>
        /// 
        /// </summary>
        internal static string gszTargetConnectionString_live;

        /// <summary>
        /// 
        /// </summary>
        internal static string gszTargetConnectionString_MCGM;

        /// <summary>
        /// 
        /// </summary>
        internal static string CurrentApplicationPath;

        /// <summary>
        /// 
        /// </summary>
        ////internal static int OrganizationId;

        /// <summary>
        /// 
        /// </summary>
        internal static string Organization_Name;

        /// <summary>
        /// 
        /// </summary>
        internal static string PlantAppVersion;

        /// <summary>
        /// Connect With Local Server
        /// </summary>


        /// <summary>
        /// 
        /// </summary>
        internal static string ConfigMachine_Id;

        internal static string GATE4_TARGET; 

        /// <summary>
        /// 
        /// </summary>
        internal static string ConfigService_LoginID;

        /// <summary>
        /// 
        /// </summary>
        internal static string ConfigService_Password;

        /// <summary>
        /// 
        /// </summary>
        internal static string ConfigAPP_Version;

        /// <summary>
        /// 
        /// </summary>
        internal static string ConfigAPP_Name;

        /// <summary>
        /// 

        //internal static bool ConfigMaintain_Log;

        /// <summary>
        /// 
        /// </summary>

        //internal static bool Config_Transfer_data_to_main_database;

        /// </summary>

        /// APPLICATION_VERSION 
        /// </summary>
        internal static string Win_Service_Version;
        /// <summary>
        /// APPLICATION_GUID
        /// </summary>
        internal static string Win_Service_GUID;
        /// <summary>
        /// 
        /// </summary>
        //internal static string ConfigApplication_Type;

        /// <summary>
        /// FILE_TYPE
        /// </summary>
        //internal static string ConfigFile_Type;

        /// <summary>
        /// IS_CREATE_HTML_FILE
        /// </summary>
        internal static bool ConfigIS_CREATE_HTML_FILE;
        /// <summary>
        /// HTML_FILE_FOLDER_PATH
        /// </summary>
        internal static string ConfigHTML_FILE_FOLDER_PATH;

        /// <summary>
        /// XML_FILE_FOLDER_PATH
        /// </summary>
        internal static string ConfigXML_FILE_FOLDER_PATH;

        /// <summary>
        /// APPLICATION_Type
        /// </summary>
        ////internal static bool ConfigUse_Config_Mac_ID;

        /// <summary>
        ///
        /// </summary>
        internal static string Date_Format;

        /// <summary>
        ///
        /// </summary>
        internal static string Time_Format;

        /// <summary>
        /// 
        /// </summary>
        internal static string DateTime_Format; 

        #endregion [ Variables ]

        /// <summary>
        /// The main entry point for the application.
        /// </summary>

        #region
        private static void InitializeConfigVariables()
        {
            Program.PlantAppVersion = GetConfigValue("Plant_App_Version").ToLower();

            switch (Program.PlantAppVersion)
            {
                case "1.0":
                    Program.PlantAppVersion = "1";
                    break;
                case "2.0":
                    Program.PlantAppVersion = "2";
                    break;
                case "3.0":
                    Program.PlantAppVersion = "3";
                    break;
                default:
                    break;
            }
            Program.Organization_Name = GetConfigValue("Org_Name").ToLower();
            Program.Round_Digits = GetConfigValue("round_Digits").ToLower();

            Program.rdoMCGM = GetConfigValue("rdoMCGM").ToString();
            Program.rdoNonMCGM = GetConfigValue("rdoNonMCGM").ToString();
            Program.hideDateFromRecipe = GetConfigValue("hideDateFromRecipe").ToString();

            try
            {
                Program.ConfigIS_CREATE_HTML_FILE = ((Convert.ToString(GetConfigValue("IS_CREATE_HTML_FILE")) == "1") ? true : false);
            }
            catch (Exception)
            {
                Program.ConfigIS_CREATE_HTML_FILE = false;
            }
            try
            {
                Program.ConfigXML_FILE_FOLDER_PATH = GetConfigValue("XML_FILE_FOLDER_PATH").ToLower();
            }
            catch (Exception)
            {
                Program.ConfigXML_FILE_FOLDER_PATH = "";
            }

            try
            {
                Program.ConfigHTML_FILE_FOLDER_PATH = GetConfigValue("HTML_FILE_FOLDER_PATH").ToLower();
            }
            catch (Exception)
            {
                Program.ConfigHTML_FILE_FOLDER_PATH = "";
            }

            Program.Win_Service_Version = GetService_Version();

            Program.Win_Service_GUID = GetService_GUID();

            try
            {
                Program.ConfigMachine_Id = GetConfigValue("MacID").ToLower();
            }
            catch (Exception)
            {
                Program.ConfigMachine_Id = string.Empty;
            }

            Program.ConfigMachine_Id = Program.ConfigMachine_Id.Trim();

            Program.GATE4_TARGET = Program.GetConfigValue("GATE4_TARGET").ToLower();

            Program.ConfigService_LoginID = GetConfigValue("SERVICE_LOGIID");
            Program.ConfigService_Password = GetConfigValue("SERVICE_PASSWORD");

            Program.ConfigAPP_Name = GetConfigValue("APP_NAME");
            Program.ConfigAPP_Version = GetConfigValue("APP_VERSION");

            //Added BY Makarand... - 24-Jan-2018
            Program.Date_Format = GetConfigValue("Date_Format");
            Program.Time_Format = GetConfigValue("Time_Format");
            Program.DateTime_Format = GetConfigValue("DateTime_Format");
            //... End
        }

        internal static string GetConfigValue(string keyValue)
        {
            //return ConfigurationManager.AppSettings[keyValue].ToString(); //ConfigurationSettings.AppSettings[keyValue].ToString();
            return General.GetConfigFileValue(keyValue);
        }

        [STAThread]
        private static string GetService_Version()
        {
            string returnData = string.Empty;
            foreach (var item in System.Reflection.Assembly.GetExecutingAssembly().GetCustomAttributes(false))
            {
                Debug.Print(item.ToString());
                if (item is System.Reflection.AssemblyFileVersionAttribute)
                {
                    returnData = ((System.Reflection.AssemblyFileVersionAttribute)item).Version;
                    break;
                }
            }
            return returnData;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private static string GetService_GUID()
        {
            string returnData = string.Empty;
            foreach (var item in System.Reflection.Assembly.GetExecutingAssembly().GetCustomAttributes(false))
            {
                Debug.Print(item.ToString());
                if (item is System.Runtime.InteropServices.GuidAttribute)
                {
                    returnData = ((System.Runtime.InteropServices.GuidAttribute)item).Value;
                    break;
                }
            }
            return returnData;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        private static int ConvertToInt32(string value)
        {
            int returnData = 0;
            if (int.TryParse(value.ToString(), out returnData) == false)
            {
                returnData = 0;
            }
            return returnData;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private static string GetMachineId()
        {
            string strmachineId = string.Empty;
            try
            {
                {
                    strmachineId = GetHardDiskSerialNo();
                }
                return strmachineId;
            }
            catch (Exception ex)
            {
                Global.WriteLog("GetMachineId :", LogMessageType.Error, ex.Message.ToString());
                return GetHardDiskSerialNo();
            }
        }

        private static string GetHardDiskSerialNo()
        {
            string returnData = string.Empty;
            try
            {
                ManagementObjectSearcher searcher = new ManagementObjectSearcher("SELECT * FROM Win32_DiskDrive");
                foreach (ManagementObject managementObject in searcher.Get())
                {
                    if (managementObject["DeviceId"].ToString() == @"\\.\PHYSICALDRIVE1")
                    {
                        returnData = managementObject["SerialNumber"].ToString();
                        break;
                    }
                }
                return returnData;
            }
            catch (Exception ex)
            {
                Global.WriteLog("GetMachineId :", LogMessageType.Error, ex.Message.ToString());
                return string.Empty;
            }
        }
        #endregion

        /// <summary>
        /// 
        /// </summary>
        [STAThread]
        static void Main()
        {
            int line_No = 0;
            Global.FlushMemory();
            General.SetColorsVariables();

            CurrentApplicationPath = Application.StartupPath;

            InitializeConfigVariables();
            if (Global.GetConnectionString() == false)
            {
                General.ShowErrorMessage("Connection XML file not found", "");
                string configSerialNo = General.GetConfigFileValue("HardDiskSerialNo");
                _hardDiskSerialNo = General.GetHardDiskSerialNo();
                if (_hardDiskSerialNo != configSerialNo)
                {
                    return;
                }
            }
            else
            {
                Global.WriteLog("GetConnectionString:", LogMessageType.Information, "Source is connected");
            }
            Global.WriteLog("GetConnectionString:", LogMessageType.Information, "line_No = 1;");
            line_No = 1;
            #region Check Service Is Connected
            ScadaKing_Web_Service.ScadaKing_Web_ServiceSoapClient objService = new ScadaKing_Web_Service.ScadaKing_Web_ServiceSoapClient();
            Global.WriteLog("GetConnectionString:", LogMessageType.Information, "line_No = 2;");
            line_No = 2;
            string url = string.Empty;
            bool IsServiceConected = false;
            line_No = 3;
            Global.WriteLog("GetConnectionString:", LogMessageType.Information, "line_No = 3;");

            try
            {
                url = objService.Endpoint.Address.ToString();
                line_No = 4;
                Global.WriteLog("GetConnectionString:", LogMessageType.Information, "line_No = 4;");
                var myRequest = (HttpWebRequest)WebRequest.Create(url);
                line_No = 5;
                Global.WriteLog("GetConnectionString:", LogMessageType.Information, "line_No = 5;");
                var response = (HttpWebResponse)myRequest.GetResponse();
                line_No = 6;
                Global.WriteLog("GetConnectionString:", LogMessageType.Information, "line_No = 6;");
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    IsServiceConected = true;
                    line_No = 7;
                    Global.WriteLog("GetConnectionString:", LogMessageType.Information, "line_No = 7;");
                    //ShowMessage(string.Format("{0} Available", url));
                }
                else
                {
                    IsServiceConected = false;
                    General.ShowMessage(string.Format("Service Returned, but with status: {0}", response.StatusDescription), "");
                    line_No = 8;
                    Global.WriteLog("GetConnectionString:", LogMessageType.Information, "line_No = 8;");
                }
            }
            catch (Exception ex)
            {
                line_No = 9;
                Global.WriteLog("GetConnectionString:", LogMessageType.Information, "line_No = 9;");
                IsServiceConected = false;
                //General.ShowMessage(string.Format("Service unavailable: {0}", ex.Message), "");
                General.ShowMessage(string.Format("Service unavailable: {0}", ex.Message), "MCGM_Transfered_Batch() FOR Line No [" + line_No + "] Exception: ");
                throw ex;
            }
            #endregion Check Service Is Connected
            line_No = 10;
            Global.WriteLog("GetConnectionString:", LogMessageType.Information, "line_No = 10;");
            if (IsServiceConected == true)
            {
                try
                {
                    line_No = 11;
                    Global.WriteLog("GetConnectionString:", LogMessageType.Information, "line_No = 11;");
                    frmlogin form = new frmlogin();
                    line_No = 12;
                    Global.WriteLog("GetConnectionString:", LogMessageType.Information, "line_No = 12;");
                    if (form.ShowDialog() == DialogResult.OK)
                    {
                        form.Dispose();
                        Application.Run(new frmMain());
                        line_No = 13;
                        Global.WriteLog("GetConnectionString:", LogMessageType.Information, "line_No = 13;");
                    }
                    else
                    {
                        line_No = 14;
                        Global.WriteLog("GetConnectionString:", LogMessageType.Information, "line_No = 14;");
                        Application.Exit();
                        Application.ExitThread();
                        line_No = 15;
                        Global.WriteLog("GetConnectionString:", LogMessageType.Information, "line_No = 15;");
                    }
                }
                catch (Exception ex)
                {
                    General.ShowMessage(string.Format("Falied to load form: {0}", ex.Message), "MCGM_Transfered_Batch() FOR Line No [" + line_No + "] Exception: ");
                    throw ex;
                }

            }

        }
        public static string _hardDiskSerialNo { get; set; }
    }
}
