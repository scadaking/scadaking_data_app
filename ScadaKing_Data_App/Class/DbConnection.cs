﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.OleDb;
namespace ScadaKing_Data_App
{
   internal static class DbConnection
    {
       private static OleDbConnection _OleDbConnection = new OleDbConnection();
       internal static OleDbConnection Open(string ConnectionString)
       {
           try
           {
               if (_OleDbConnection.State == System.Data.ConnectionState.Broken || _OleDbConnection.State == System.Data.ConnectionState.Closed)
               {
                   _OleDbConnection = new OleDbConnection(ConnectionString);
                   _OleDbConnection.Open();
               }
               return _OleDbConnection;
           }
           catch (Exception ex)
           {
               throw ex;
           }
       }
       internal static void Close()
       {
           try
           {
               if (_OleDbConnection.State == System.Data.ConnectionState.Open)
               {
                   _OleDbConnection.Close();
               }
           }
           catch (Exception ex)
           {
               throw ex;
           }
       }
    }
}
