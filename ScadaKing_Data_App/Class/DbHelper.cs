﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.OleDb;
using System.Data;
using System.Data.SqlClient;
namespace ScadaKing_Data_App
{
    public class DbHelper
    {

        private static OleDbCommand _OleDBCommand;

        private static OleDbDataAdapter _OleDbDataAdapter;

        internal static bool ExecuteNonQuery(string QueryString, string ConnectionString)
        {
            bool ReturnResult = false;
            try
            {
                _OleDBCommand = new OleDbCommand();
                _OleDBCommand.CommandText = QueryString;
                _OleDBCommand.CommandType = System.Data.CommandType.Text;
                _OleDBCommand.Connection = DbConnection.Open(ConnectionString);
                int iReturnCode = _OleDBCommand.ExecuteNonQuery();
                ReturnResult = iReturnCode >= 1 ? true : false;
            }
            catch (Exception ex)
            {
                ReturnResult = false;
                throw ex;
            }
            finally
            {
                DbConnection.Close();
                _OleDBCommand.Dispose();
            }
            return ReturnResult;
        }

        internal static DataTable FillDataTable(string QueryString, string ConnectionString)
        {
            DataTable dtResult = new DataTable();
            try
            {
                _OleDBCommand = new OleDbCommand();
                _OleDBCommand.CommandText = QueryString;
                _OleDBCommand.CommandType = System.Data.CommandType.Text;
                _OleDBCommand.Connection = DbConnection.Open(ConnectionString);
                _OleDbDataAdapter = new OleDbDataAdapter();
                _OleDbDataAdapter.SelectCommand = _OleDBCommand;
                _OleDbDataAdapter.Fill(dtResult);
                _OleDbDataAdapter.Dispose();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                DbConnection.Close();
                _OleDBCommand.Dispose();
            }
            return dtResult;
        }

        internal static object ExecuteScalar(string QueryString, string ConnectionString)
        {
            object ReturnResult;
            try
            {
                _OleDBCommand = new OleDbCommand();
                _OleDBCommand.CommandText = QueryString;
                _OleDBCommand.CommandType = System.Data.CommandType.Text;
                _OleDBCommand.Connection = DbConnection.Open(ConnectionString);
                ReturnResult = _OleDBCommand.ExecuteScalar();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                DbConnection.Close();
                _OleDBCommand.Dispose();
            }
            return ReturnResult;
        }
    }
}
