﻿#region [ Using ]
using System;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Management;
using System.Security.Cryptography;
using System.Text;
using System.Windows.Forms;
using System.Xml;
#endregion [ Using ]

namespace ScadaKing_Data_App.Class
{
    public static class General
    {

        #region [ Variables for Object Color ]

        /// <summary>
        /// object Back Color 
        /// </summary>
        internal static Color objectBackColor;

        /// <summary>
        /// object Fore Color
        /// </summary>
        internal static Color objectForeColor;

        /// <summary>
        /// 
        /// </summary>
        public static Color oBackColor;

        /// <summary>
        /// 
        /// </summary>
        public static Color oForeColor;

        /// <summary>
        /// 
        /// </summary>
        public static Color BackColorWhite;

        /// <summary>
        /// 
        /// </summary>
        public static Color ForeColorBlack;

        /// <summary>
        /// 
        /// </summary>
        public static Color GridBackgroundColor;

        /// <summary>
        /// 
        /// </summary>
        public static Color GridBackGroundForeColor;

        /// <summary>
        /// 
        /// </summary>
        public static Color GridSelectionForeColor;

        /// <summary>
        /// 
        /// </summary>
        public static Color GridSelectionBackColor;

        /// <summary>
        /// 
        /// </summary>
        public static Color GridColumnHeadersBackColor;

        /// <summary>
        /// 
        /// </summary>
        public static Color GridColumnHeadersForeColor;

        /// <summary>
        /// 
        /// </summary>
        public static long orgId;

        /// <summary>
        /// 
        /// </summary>
        public static string OrganizationName;

        /// <summary>
        /// 
        /// </summary>
        public static string User_Name;

        /// <summary>
        /// 
        /// </summary>
        public static long UserId;

        /// <summary>
        /// 
        /// </summary>
        public static bool IsUpgradeBatches;

        /// <summary>
        /// 
        /// </summary>
        public static double PlantAppVersion;

        /// <summary>
        /// 
        /// </summary>
        public static double PlantCapacity;
        /// <summary>
        /// 
        /// </summary>
        public static string _ConfigMachine_id;

        public static string ConfigMachine_Id
        {
            get
            {
                return _ConfigMachine_id;
            }
            set
            {
                _ConfigMachine_id = value;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public static string _ConfigService_Password;

        public static string ConfigService_Password
        {
            get
            {
                return _ConfigService_Password;
            }
            set
            {
                _ConfigService_Password = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public static string _ConfigAPP_Version;

        public static string ConfigAPP_Version
        {
            get
            {
                return _ConfigAPP_Version;
            }
            set
            {
                _ConfigAPP_Version = value;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public static string _ConfigAPP_Name;
        public static string ConfigAPP_Name
        {
            get
            {
                return _ConfigAPP_Name;
            }
            set
            {
                _ConfigAPP_Name = value;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public static string _Win_Service_GUID;
        public static string Win_Service_GUID
        {
            get
            {
                return _Win_Service_GUID;
            }
            set
            {
                _Win_Service_GUID = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public static string _Win_Service_Version;
        public static string Win_Service_Version
        {
            get
            {
                return _Win_Service_Version;
            }
            set
            {
                _Win_Service_Version = value;
            }
        }
        public static long ModuleId { get; set; }

        public static ScadaKing_Data_App.Code.UserType User_Type { get; set; }

        #endregion [ Variables for Object Color ]

        #region [ 'Convert To' Events ]

        /// <summary>
        /// 
        /// </summary>
        /// <param name="keyValue"></param>
        /// <returns></returns>
        internal static byte ConvertToByte(string keyValue)
        {
            byte returnData = 0;
            if (byte.TryParse(keyValue, out returnData))
            {
                return returnData;
            }
            else
            {
                return 0;
            }
        }

        internal static void ClearControls(Control localControl, string notToClearControlName)
        {
            if (localControl.Name.ToUpper() == notToClearControlName.ToUpper())
            {
                return;
            }

            if (localControl.Controls.Count > 0)
            {
                int indexCount;
                for (indexCount = 0; indexCount < localControl.Controls.Count; indexCount++)
                {
                    ClearControls(localControl.Controls[indexCount], notToClearControlName);
                }
            }

            if (localControl is TextBox)
            {
                ((TextBox)localControl).Clear();
            }
            else if (localControl is RichTextBox)
            {
                ((RichTextBox)localControl).Clear();
            }
            else if (localControl is DateTimePicker)
            {
                ((DateTimePicker)localControl).Value = DateTime.Now;

                if (((DateTimePicker)localControl).ShowCheckBox == true)
                {
                    ((DateTimePicker)localControl).Checked = false;
                }
            }
            else if (localControl is ComboBox)
            {
                ((ComboBox)localControl).SelectedIndex = -1;
            }
            else if (localControl is ListBox)
            {
                ((ListBox)localControl).SelectedIndex = -1;
            }
            else if (localControl is DataGridView)
            {
                ((DataGridView)localControl).DataSource = null;
            }
            else if (localControl is DataGrid)
            {
                ((DataGrid)localControl).DataSource = null;
            }
            else if (localControl is TabControl)
            {
                ((TabControl)localControl).SelectedTab = ((TabControl)localControl).TabPages[0];
            }
            else if (localControl is CheckBox)
            {
                ((CheckBox)localControl).CheckState = CheckState.Unchecked;
            }
            else if (localControl is RadioButton)
            {
                ((RadioButton)localControl).Checked = false;
            }
        }


        internal static void ChangeObjectColor(object sender, bool enterFocus)
        {
            Color backColor = Color.White;
            Color foreColor = Color.Black;

            if (enterFocus)
            {
                backColor = objectBackColor;
                foreColor = objectForeColor;
            }

            if (sender is TextBox)
            {
                ((TextBox)sender).BackColor = backColor;
                ((TextBox)sender).ForeColor = foreColor;
                ((TextBox)sender).SelectAll();
            }
            else if (sender is ComboBox)
            {
                ((ComboBox)sender).BackColor = backColor;
                ((ComboBox)sender).ForeColor = foreColor;
                ((ComboBox)sender).SelectAll();
            }
        }



        /// <summary>
        /// 
        /// </summary>
        /// <param name="keyValue"></param>
        /// <returns></returns>
        internal static bool ConvertToBool(string keyValue)
        {
            bool returnData = false;
            if (bool.TryParse(keyValue, out returnData))
            {
                return returnData;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        internal static int ConvertToInteger(string value)
        {
            string val = string.Empty;
            int returnData = 0;
            if (value.Contains("."))
            {
                val = value.Split('.')[0].ToString();
            }
            else
            {
                val = value;
            }

            if (int.TryParse(val, out returnData))
            {
                return returnData;
            }
            else
            {
                return 0;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        internal static double ConvertToDouble(string value)
        {
            double returnData = 0;
            if (double.TryParse(value, out returnData))
            {
                return returnData;
            }
            else
            {
                return 0;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="keyValue"></param>
        /// <returns></returns>
        internal static decimal ConvertToDecimal(string keyValue)
        {
            decimal returnData = 0;
            if (decimal.TryParse(keyValue, out returnData))
            {
                return returnData;
            }
            else
            {
                return 0;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        internal static string ConvertToUpper(string value)
        {
            return string.IsNullOrEmpty(value) ? string.Empty : value.ToUpper();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        internal static string ConvertToLower(string value)
        {
            return string.IsNullOrEmpty(value) ? string.Empty : value.ToLower();
        }
        #endregion [ 'Convert To' Events ]


        private static byte[] key = { };

        /// <summary>
        /// Decrypt matrix
        /// </summary>
        private static byte[] IV = { 18, 52, 86, 120, 144, 171, 205, 239 };

        internal static void SetGridDefaults(
            DataGridView dataGridViewObject,
            string freezeColumn,
            bool fontBold,
            string fontName,
            float fontSize,
            bool readOnly,
            float columnHeaderFontSize,
            bool columnHeaderFontBold,
            bool visibleColumns,
            int rowHeight)
        {
            try
            {
                dataGridViewObject.SuspendLayout();
                if (string.IsNullOrEmpty(fontName))
                {
                    fontName = "Tahoma";
                }

                if (fontSize <= 0)
                {
                    fontSize = 8;
                }

                #region [ Column Header Style ]
                DataGridViewCellStyle ocolumnHeadersDefaultCellStyle = new DataGridViewCellStyle();
                {
                    ocolumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
                    ocolumnHeadersDefaultCellStyle.Font = new Font(fontName, columnHeaderFontSize, columnHeaderFontBold ? FontStyle.Bold : FontStyle.Regular, GraphicsUnit.Point);
                    if (dataGridViewObject.Name == "dgvSearch_MCGM")
                    {
                        ocolumnHeadersDefaultCellStyle.BackColor = System.Drawing.ColorTranslator.FromHtml("#395C9C"); //General.GridColumnHeadersBackColor;
                        ocolumnHeadersDefaultCellStyle.ForeColor = System.Drawing.ColorTranslator.FromHtml("#FFFFFF");// General.GridColumnHeadersForeColor;
                    }
                    else
                    {
                        ocolumnHeadersDefaultCellStyle.BackColor = System.Drawing.ColorTranslator.FromHtml("#2A8AD4");
                        ocolumnHeadersDefaultCellStyle.ForeColor = System.Drawing.ColorTranslator.FromHtml("#FFFFFF");
                    }
                    ocolumnHeadersDefaultCellStyle.SelectionBackColor = SystemColors.Highlight;
                    ocolumnHeadersDefaultCellStyle.SelectionForeColor = SystemColors.HighlightText;
                    ocolumnHeadersDefaultCellStyle.WrapMode = DataGridViewTriState.True;
                    dataGridViewObject.ColumnHeadersDefaultCellStyle = ocolumnHeadersDefaultCellStyle;
                }
                #endregion [ Column Header Style ]

                #region [ Default Cell Style ]
                DataGridViewCellStyle odefaultCellStyle = new DataGridViewCellStyle();
                {
                    odefaultCellStyle.DataSourceNullValue = string.Empty;
                    odefaultCellStyle.Font = new System.Drawing.Font(fontName, fontSize, fontBold ? FontStyle.Bold : FontStyle.Regular, GraphicsUnit.Point);
                    odefaultCellStyle.ForeColor = General.GridBackGroundForeColor;
                    odefaultCellStyle.NullValue = string.Empty;
                    odefaultCellStyle.SelectionBackColor = General.GridSelectionBackColor;
                    odefaultCellStyle.SelectionForeColor = General.GridSelectionForeColor;
                    odefaultCellStyle.BackColor = General.GridBackgroundColor;
                    odefaultCellStyle.WrapMode = DataGridViewTriState.True;
                    dataGridViewObject.RowTemplate.DefaultCellStyle = odefaultCellStyle;
                }
                #endregion [ Default Cell Style ]

                dataGridViewObject.AllowUserToOrderColumns = true;
                dataGridViewObject.BackgroundColor = General.GridBackgroundColor;
                dataGridViewObject.BorderStyle = BorderStyle.FixedSingle;
                dataGridViewObject.GridColor = Color.FromKnownColor(KnownColor.Highlight);
                dataGridViewObject.BackColor = Color.Cornsilk;
                dataGridViewObject.ColumnHeadersHeight = 32;
                dataGridViewObject.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
                dataGridViewObject.EnableHeadersVisualStyles = false;
                dataGridViewObject.AllowUserToAddRows = readOnly;
                dataGridViewObject.AllowUserToDeleteRows = false;
                dataGridViewObject.AllowUserToResizeRows = false;
                dataGridViewObject.ReadOnly = readOnly;
                dataGridViewObject.ShowCellToolTips = true;
                dataGridViewObject.RowTemplate.Height = rowHeight;
                dataGridViewObject.RowTemplate.ReadOnly = false;
                dataGridViewObject.MultiSelect = false;
                dataGridViewObject.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
                dataGridViewObject.AutoGenerateColumns = false;

                if (dataGridViewObject.DataSource != null)
                {
                    dataGridViewObject.AllowUserToAddRows = false;
                    dataGridViewObject.AllowUserToDeleteRows = false;
                    dataGridViewObject.Refresh();

                    SetVisibleGridColumn(dataGridViewObject, visibleColumns);
                    if (!string.IsNullOrEmpty(freezeColumn))
                    {
                        dataGridViewObject.Columns[freezeColumn].DisplayIndex = 0;
                        dataGridViewObject.Columns[freezeColumn].Frozen = true;
                    }
                }

                dataGridViewObject.ResumeLayout(false);
            }
            catch (Exception)
            {

            }
        }

        internal static void SetVisibleGridColumn(DataGridView dataGridViewObject, bool value)
        {
            int colCount = dataGridViewObject.Columns.Count;
            int loopIndex = 0;
            for (loopIndex = 0; loopIndex < colCount; loopIndex++)
            {
                dataGridViewObject.Columns[loopIndex].Visible = value;
            }
        }


        internal static void ShowMessage(string messageString, string messageBoxTitle)
        {
            if (string.IsNullOrEmpty(messageString))
            {
                return;
            }

            MessageBox.Show(messageString, messageBoxTitle, MessageBoxButtons.OK, MessageBoxIcon.Information);
        }


        internal static void ShowErrorMessage(string messageString)
        {
            if (string.IsNullOrEmpty(messageString))
            {
                return;
            }

            MessageBox.Show(messageString, "", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        internal static void ShowErrorMessage(string messageString, string messageBoxTitle)
        {
            if (string.IsNullOrEmpty(messageString))
            {
                return;
            }

            MessageBox.Show(messageString, messageBoxTitle, MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        internal static DialogResult ShowYesNoMessage(string messageString, string messageBoxTitle)
        {
            if (string.IsNullOrEmpty(messageString))
            {
                return DialogResult.No;
            }

            return MessageBox.Show(messageString, messageBoxTitle, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
        }
    
        private static string GetConnectionString(string xmlFilePath)
        {
            string returnData = string.Empty;
            XmlDocument objDocument = new XmlDocument();

            if (string.IsNullOrEmpty(xmlFilePath))
            {
                return string.Empty;
            }

            objDocument.Load(xmlFilePath);
            XmlElement rootxml;
            rootxml = objDocument.DocumentElement;
            string serverName = string.Empty;
            string userId = string.Empty;
            string password = string.Empty;
            string databaseName = string.Empty;

            foreach (XmlElement xmlchildNode in rootxml.ChildNodes)
            {
                if (xmlchildNode.Name.ToLower() == "server")
                {
                    serverName = Decrypt(Convert.ToString(xmlchildNode.InnerText));
                }
                if (xmlchildNode.Name.ToLower() == "userid" || xmlchildNode.Name.ToLower() == "uid")
                {
                    userId = Decrypt(Convert.ToString(xmlchildNode.InnerText));
                }
                if (xmlchildNode.Name.ToLower() == "password" || xmlchildNode.Name.ToLower() == "pwd")
                {
                    password = Decrypt(Convert.ToString(xmlchildNode.InnerText));
                }
                if (xmlchildNode.Name.ToLower() == "database")
                {
                    databaseName = Decrypt(Convert.ToString(xmlchildNode.InnerText));
                }
            }


            returnData = "Data Source=" + serverName + "; Initial Catalog =" + databaseName + "; User=" + userId + "; Password=" + password + "; MultipleActiveResultSets=True;";

            return returnData;
        }


        public static string Decrypt(string stringToDecrypt)
        {
            byte[] inputByteArray = new byte[stringToDecrypt.Length];
            try
            {

                key = System.Text.Encoding.UTF8.GetBytes("ABCDEFGH");
                DESCryptoServiceProvider desCryptoServiceProvider = new DESCryptoServiceProvider();
                inputByteArray = Convert.FromBase64String(stringToDecrypt);
                MemoryStream memoryStream = new MemoryStream();
                CryptoStream cryptoStream = new CryptoStream(memoryStream, desCryptoServiceProvider.CreateDecryptor(key, IV), CryptoStreamMode.Write);
                cryptoStream.Write(inputByteArray, 0, inputByteArray.Length);
                cryptoStream.FlushFinalBlock();

                Encoding encoding = Encoding.UTF8;
                return encoding.GetString(memoryStream.ToArray());
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public static string GetConnectionString()
        {
            return GetConnectionString(Program.ConnetionXML_FileName_Path);
        }

        internal static string GetConfigFileValue(string value)
        {
            try
            {
                return string.IsNullOrEmpty(ConfigurationManager.AppSettings[value.ToString()]) ? string.Empty : ConfigurationManager.AppSettings[value.ToString()];
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }
        
        internal static void SetColorsVariables()
        {
            Program.ConnetionXML_FileName_Path = GetConfigFileValue("XML_FileName_Path");
            objectBackColor = Color.FromName(GetConfigFileValue("TextBackColor"));
            objectForeColor = Color.FromName(GetConfigFileValue("TextForeColor"));
            oBackColor = Color.FromName(GetConfigFileValue("TextBackColor")); //// Color.GreenYellow;
            oForeColor = Color.FromName(GetConfigFileValue("TextForeColor")); //// Color.Blue;
            BackColorWhite = Color.FromName(GetConfigFileValue("TextBackColorWhite")); //// Color.White;
            ForeColorBlack = Color.FromName(GetConfigFileValue("TextForeColorBlack")); //// Color.Black;
            GridBackgroundColor = Color.FromName(GetConfigFileValue("GridBackGroundColor"));
            GridBackGroundForeColor = Color.FromName(GetConfigFileValue("GridBackGroundForeColor"));
            GridSelectionForeColor = Color.FromName(GetConfigFileValue("GridSelectionForeColor"));
            GridSelectionBackColor = Color.FromName(GetConfigFileValue("GridSelectionBackColor"));
            GridColumnHeadersBackColor = Color.FromName(GetConfigFileValue("GridColumnHeadersBackColor"));
            GridColumnHeadersForeColor = Color.FromName(GetConfigFileValue("GridColumnHeadersForeColor"));
        }

        /// <summary>
        /// get HardDisk SerialNo
        /// </summary>
        /// <returns>Serial Number</returns>
        internal static string GetHardDiskSerialNo()
        {
            string returnData = string.Empty;
            try
            {
                ManagementObjectSearcher searcher = new ManagementObjectSearcher("SELECT * FROM Win32_DiskDrive");
                foreach (ManagementObject wmi_HD in searcher.Get())
                {
                    returnData = wmi_HD["SerialNumber"].ToString();
                    break;
                }
            }
            catch (Exception)
            {
            }

            return returnData;
        }

        internal static void SetColumnDefaults(DataGridView dataGridViewObject, DataGridViewContentAlignment dataGridViewContentAlignment, ref int displayIndex, string columnName, string headerText, int columnWidth)
        {
            SetColumnDefaults(dataGridViewObject, dataGridViewContentAlignment, ref displayIndex, columnName, headerText, columnWidth, string.Empty);
        }

        internal static void SetColumnDefaults(DataGridView dataGridViewObject, DataGridViewContentAlignment dataGridViewContentAlignment, ref int displayIndex, string columnName, string headerText, int columnWidth, string formatString)
        {
            SetColumnDefaults(dataGridViewObject, dataGridViewContentAlignment, ref displayIndex, columnName, headerText, columnWidth, formatString, false, true);
        }


        internal static void SetColumnDefaults(
            DataGridView dataGridViewObject,
            DataGridViewContentAlignment dataGridViewContentAlignment,
            ref int displayIndex,
            string columnName,
            string headerText,
            int columnWidth,
            string formatString,
            bool expression,
            bool visible)
        {
            try
            {
                if (dataGridViewObject.Columns.Count > 0)
                {
                    dataGridViewObject.Columns[columnName].HeaderText = string.IsNullOrEmpty(headerText) == false ? headerText : columnName;
                    dataGridViewObject.Columns[columnName].Width = columnWidth;
                    dataGridViewObject.Columns[columnName].Visible = visible;
                    dataGridViewObject.Columns[columnName].DefaultCellStyle.Alignment = dataGridViewContentAlignment;
                    dataGridViewObject.Columns[columnName].DisplayIndex = displayIndex;

                    if (string.IsNullOrEmpty(formatString) == false)
                    {
                        dataGridViewObject.Columns[columnName].DefaultCellStyle.Format = formatString;
                    }

                    if (expression == false)
                    {
                        if (headerText == "IsMCGM")
                        {
                            dataGridViewObject.Columns[columnName].ReadOnly = false;
                        }
                        else
                        {
                            dataGridViewObject.Columns[columnName].ReadOnly = true;
                        }
                    }

                    displayIndex++;
                }
            }
            catch (Exception)
            {
            }
        }
    }
}
