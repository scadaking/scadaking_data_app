﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ScadaKing_Data_App.Class;
using System.Data.OleDb;

namespace ScadaKing_Data_App
{
    public partial class frmChangeTruckNumber : Form
    {
        DataTable dtTruckNumber = new DataTable();
        private string TruckNo = string.Empty;
        string Batch_No = string.Empty;
        public frmChangeTruckNumber(string Truck_No, DataTable dtValue, string BatchNo)
        {
            InitializeComponent();
            try
            {
                TruckNo = Truck_No;
                txtLocalTrucks.Text = TruckNo;
                dtTruckNumber = dtValue;
                Batch_No = BatchNo;
                BindCombo();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void BindCombo()
        {
            try
            {
                if (dtTruckNumber.Rows.Count > 0)
                {
                    cbLiveTrucks.ValueMember = "TRUCK_NO";
                    cbLiveTrucks.DisplayMember = "TRUCK_NO";
                    cbLiveTrucks.DataSource = dtTruckNumber;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                if (cbLiveTrucks.Text.Trim().TrimEnd().TrimStart().Length != 0)
                {
                    if (this.Chanage_Truck_Number() == true)
                    {
                        this.DialogResult = System.Windows.Forms.DialogResult.OK;
                        this.Close();
                    }
                    else
                    {
                        this.DialogResult = System.Windows.Forms.DialogResult.No;
                        this.Close();
                    }
                }
                else
                {
                    General.ShowMessage(" Truck is not found...Please select other truck...!!!", this.Text);
                }
            }
            catch (Exception ex)
            {
                General.ShowErrorMessage(ex.Message, this.Text);
            }
        }

        public bool Chanage_Truck_Number()
        {
            string strQuery = string.Empty;
            string Batch_TruckNo = string.Empty;
            object objTruckNo = "";
            bool returnResult = false;
            try
            {
                if (Program.PlantAppVersion == "3")
                {
                    strQuery = @"SELECT TRUCK_NO FROM BATCH_DAT_TRANS WHERE BATCH_NO = " + Batch_No + " ";
                    objTruckNo = DbHelper.ExecuteScalar(strQuery, dataConnectionConfiguration.Source_Connection_String);
                    if (objTruckNo != null || string.IsNullOrEmpty(objTruckNo.ToString()) == false)
                    {
                        strQuery = @"UPDATE BATCH_DAT_TRANS SET TRUCK_NO = '" + cbLiveTrucks.Text + "'  WHERE TRUCK_NO = '" + objTruckNo + "'";
                        objTruckNo = DbHelper.ExecuteScalar(strQuery, dataConnectionConfiguration.Source_Connection_String);
                        objTruckNo = DbHelper.ExecuteScalar(strQuery, dataConnectionConfiguration.Target_Connection_String);
                        strQuery = @"UPDATE TRUCK_MASTER SET TRUCK_NO = '" + cbLiveTrucks.Text + "', TNO = '" + cbLiveTrucks.Text + "' WHERE TRUCK_NO = '" + TruckNo + "'";
                        objTruckNo = DbHelper.ExecuteScalar(strQuery, dataConnectionConfiguration.Source_Connection_String);
                        objTruckNo = DbHelper.ExecuteScalar(strQuery, dataConnectionConfiguration.Target_Connection_String);
                        General.ShowMessage("Truck Number is Updated Successfully...!!!", this.Text);
                        TruckNo = cbLiveTrucks.Text;
                        returnResult = true;
                    }
                    else
                    {
                        General.ShowMessage("truck number not found on current batch..please select another batch...!!!", this.Text);
                        returnResult = false;
                    }
                }
                else if (Program.PlantAppVersion == "2")
                {
                    strQuery = @"SELECT TRUCK_NO FROM BATCH WHERE batchno = " + Batch_No + "";
                    objTruckNo = DbHelper.ExecuteScalar(strQuery, dataConnectionConfiguration.Source_Connection_String);
                    if (objTruckNo != null || string.IsNullOrEmpty(objTruckNo.ToString()) == false)
                    {
                        strQuery = @"UPDATE BATCH SET TRUCK_NO = '" + cbLiveTrucks.Text + "' WHERE TRUCK_NO = '" + objTruckNo + "'";
                        objTruckNo = DbHelper.ExecuteScalar(strQuery, dataConnectionConfiguration.Source_Connection_String);
                        objTruckNo = DbHelper.ExecuteScalar(strQuery, dataConnectionConfiguration.Target_Connection_String);
                        strQuery = @"UPDATE TRUCKMAS SET TRUCK_NO = '" + cbLiveTrucks.Text + "' WHERE TRUCK_NO = '" + TruckNo + "'";
                        objTruckNo = DbHelper.ExecuteScalar(strQuery, dataConnectionConfiguration.Source_Connection_String);
                        objTruckNo = DbHelper.ExecuteScalar(strQuery, dataConnectionConfiguration.Target_Connection_String);
                        General.ShowMessage("Truck Number is Updated Successfully...!!!", this.Text);
                        returnResult = true;
                    }
                    else
                    {
                        General.ShowMessage("truck number not found on current batch..please select another batch...!!!", this.Text);
                        returnResult = false;
                    }
                }
                else if (Program.PlantAppVersion == "1")
                {
                    strQuery = @"SELECT TRUCK_NO FROM VITRAG_BATCH WHERE BATCH_NO = " + Batch_No + " ";
                    objTruckNo = DbHelper.ExecuteScalar(strQuery, dataConnectionConfiguration.Source_Connection_String);
                    if (objTruckNo != null || string.IsNullOrEmpty(objTruckNo.ToString()) == false)
                    {
                        strQuery = @"UPDATE VITRAG_BATCH SET TRUCK_NO =  '" + cbLiveTrucks.Text + "' WHERE TRUCK_NO = '" + objTruckNo + "'";
                        objTruckNo = DbHelper.ExecuteScalar(strQuery, dataConnectionConfiguration.Source_Connection_String);
                        objTruckNo = DbHelper.ExecuteScalar(strQuery, dataConnectionConfiguration.Target_Connection_String);
                        strQuery = @"UPDATE TRUCK_MASTER SET TRUCK_NO = '" + cbLiveTrucks.Text + "', TNO = '" + cbLiveTrucks.Text + "' WHERE TRUCK_NO = '" + TruckNo + "'";
                        objTruckNo = DbHelper.ExecuteScalar(strQuery, dataConnectionConfiguration.Source_Connection_String);
                        objTruckNo = DbHelper.ExecuteScalar(strQuery, dataConnectionConfiguration.Target_Connection_String);
                        General.ShowMessage("Truck Number is Updated Successfully...!!!", this.Text);
                        returnResult = true;
                    }
                    else
                    {
                        General.ShowMessage("truck number not found on current batch..please select another batch...!!!", this.Text);
                    }
                }
                else
                {
                    General.ShowMessage(" Truck is not found...Please select other truck...!!!", this.Text);
                }
            }
            catch (Exception ex)
            {
                returnResult = false;
                throw ex;
            }
            return returnResult;
        }

        private void frmChangeTruckNumber_Load(object sender, EventArgs e)
        {

        }
    }
}

