﻿using System;
using System.Data;
namespace ScadaKing_Data_App.Controller
{
    internal static class BatchDataController
    {
        private static ScadaKing_Data_App.BusinessLayer.Factory.BatchDatTransRawFactory batchDatTransRawFactory = null;

        internal static DataTable GetOriginalBatchList(long orgID, long fromBatchNo, long toBatchNo, string customerCode, string fromBatchDate, string toBatchDate, int batchType, string Connection_String)
        {
            DataTable dtOriginalBatchList = new DataTable();
            string strFilter = string.Empty;

            try
            {
                //strFilter = strFilter + "  Batch_No > " + 0;
                if (fromBatchNo > 0)
                {
                    if (Program.PlantAppVersion == "2")
                    {
                        if (string.IsNullOrEmpty(strFilter) == false)
                        {
                            strFilter = strFilter + " AND CDBL(BatchNo) >= " + fromBatchNo.ToString() + "";
                        }
                        else
                        {
                            strFilter = strFilter + "  CDBL(BatchNo) >= " + fromBatchNo.ToString() + "";
                        }
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(strFilter) == false)
                        {
                            strFilter = strFilter + " AND CDBL(Batch_No) >= " + fromBatchNo.ToString() + "";
                        }
                        else
                        {
                            strFilter = strFilter + "  CDBL(Batch_No) >= " + fromBatchNo.ToString() + "";
                        }
                    }
                }
                if (toBatchNo > 0)
                {
                    if (Program.PlantAppVersion == "2")
                    {
                        if (string.IsNullOrEmpty(strFilter) == false)
                        {
                            strFilter = strFilter + " AND CDBL(BatchNo) <= " + toBatchNo.ToString() + "";
                        }
                        else
                        {
                            strFilter = strFilter + "  CDBL(BatchNo) <= " + toBatchNo.ToString() + "";
                        }
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(strFilter) == false)
                        {
                            strFilter = strFilter + " AND CDBL(Batch_No) <= " + toBatchNo.ToString() + "";
                        }
                        else
                        {
                            strFilter = strFilter + "  CDBL(Batch_No) <= " + toBatchNo.ToString() + "";
                        }
                    }
                }
                if (string.IsNullOrEmpty(customerCode) == false)
                {
                    if (Program.PlantAppVersion == "2")
                    {
                        if (string.IsNullOrEmpty(strFilter) == false)
                        {
                            strFilter = strFilter + " AND BDT.cust_no = '" + customerCode.ToString() + "'";
                        }
                        else
                        {
                            strFilter = strFilter + "  BDT.cust_no = '" + customerCode.ToString() + "'";
                        }
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(strFilter) == false)
                        {
                            strFilter = strFilter + " AND BDT.CUSTOMER_CODE = '" + customerCode.ToString() + "'";
                        }
                        else
                        {
                            strFilter = strFilter + "  BDT.CUSTOMER_CODE = '" + customerCode.ToString() + "'";
                        }
                    }
                }
                if (string.IsNullOrEmpty(fromBatchDate) == false)
                {
                    if (Program.PlantAppVersion == "2")
                    {
                        if (string.IsNullOrEmpty(strFilter) == false)
                        {
                            strFilter = strFilter + " AND  FORMAT(BDT.Date,'YYYY-MM-DD') >= Format('" + fromBatchDate.ToString() + "','yyyy-MM-dd')";
                        }
                        else
                        {
                            strFilter = strFilter + "   FORMAT(BDT.Date,'YYYY-MM-DD') >= Format('" + fromBatchDate.ToString() + "','yyyy-MM-dd')";
                        }
                    }
                    else if (Program.PlantAppVersion == "3.1")
                    {
                        if (string.IsNullOrEmpty(strFilter) == false)
                        {
                            strFilter = strFilter + " AND  FORMAT(BDT.BATCH_START_TIME,'YYYY-MM-DD') >= Format('" + fromBatchDate.ToString() + "','yyyy-MM-dd')";
                        }
                        else
                        {
                            strFilter = strFilter + "   FORMAT(BDT.BATCH_START_TIME,'YYYY-MM-DD') >= Format('" + fromBatchDate.ToString() + "','yyyy-MM-dd')";
                        }
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(strFilter) == false)
                        {
                            strFilter = strFilter + " AND  FORMAT(BDT.BATCH_DATE,'YYYY-MM-DD') >= Format('" + fromBatchDate.ToString() + "','yyyy-MM-dd')";
                        }
                        else
                        {
                            strFilter = strFilter + "   FORMAT(BDT.BATCH_DATE,'YYYY-MM-DD') >= Format('" + fromBatchDate.ToString() + "','yyyy-MM-dd')";
                        }
                    }
                }
                if (string.IsNullOrEmpty(toBatchDate) == false)
                {
                    if (Program.PlantAppVersion == "2")
                    {
                        if (string.IsNullOrEmpty(strFilter) == false)
                        {
                            strFilter = strFilter + " AND  FORMAT(BDT.Date,'YYYY-MM-DD') <= Format('" + toBatchDate.ToString() + "','yyyy-MM-dd')";
                        }
                        else
                        {
                            strFilter = strFilter + "   FORMAT(BDT.Date,'YYYY-MM-DD') <= Format('" + toBatchDate.ToString() + "','yyyy-MM-dd')";
                        }
                    }
                    else if (Program.PlantAppVersion == "3.1")
                    {
                        if (string.IsNullOrEmpty(strFilter) == false)
                        {
                            strFilter = strFilter + " AND  FORMAT(BDT.BATCH_START_TIME,'YYYY-MM-DD') <= Format('" + toBatchDate.ToString() + "','yyyy-MM-dd')";
                        }
                        else
                        {
                            strFilter = strFilter + "   FORMAT(BDT.BATCH_START_TIME,'YYYY-MM-DD') <= Format('" + toBatchDate.ToString() + "','yyyy-MM-dd')";
                        }
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(strFilter) == false)
                        {
                            strFilter = strFilter + " AND  FORMAT(BDT.BATCH_DATE,'YYYY-MM-DD') <= Format('" + toBatchDate.ToString() + "','yyyy-MM-dd')";
                        }
                        else
                        {
                            strFilter = strFilter + "   FORMAT(BDT.BATCH_DATE,'YYYY-MM-DD') <= Format('" + toBatchDate.ToString() + "','yyyy-MM-dd')";
                        }
                    }
                }
                //if (string.IsNullOrEmpty(strFilter) == false)
                //{
                //    strFilter = " where " + strFilter;
                //}

                string strQuery = string.Empty;
                //Added New Implementation for New Setup....25-Sept-2017... -Makarand.
                if (Program.PlantAppVersion == "4.0")
                {
                    strQuery = @"SELECT 
	                            BDT.BATCH_NO AS BatchNo
                                ,FORMAT(BDT.BATCH_DATE, '" + Program.Date_Format + @"') AS BatchDate
	                            ,BDT.BATCH_STARTTIME AS BatchStartTime
	                            ,BDT.BATCH_ENDTIME AS BatchEndTime
	                            ,BDT.SITE_ADR
	                            ,BDT.TRUCK_NO AS TruckNo 
	                            ,BDT.CUST_NAME AS CustomerName
                            FROM
	                            BATCH_DETAIL AS BDT
                                WHERE CDBL(BDT.BATCH_NO) > 0  ";
                }


                if (Program.PlantAppVersion == "3")
                {
                    strQuery = @"SELECT 
	                            BDT.BATCH_NO AS BatchNo
	                            ,FORMAT(BDT.BATCH_DATE, '" + Program.Date_Format + @"') AS BATCHDATE
	                            ,BDT.BATCH_START_TIME AS BatchStartTime
	                            ,BDT.BATCH_END_TIME AS BatchEndTime
	                            ,SITE
	                            ,TRUCK_NO AS TruckNo 
	                            ,CM.CUSTOMER_NAME AS CustomerName
                            FROM
	                            ((BATCH_DAT_TRANS AS BDT LEFT JOIN CUSTOMER_MASTER AS CM ON BDT.CUSTOMER_CODE = CM.CUSTOMER_CODE)) 
                                WHERE CDBL(BDT.BATCH_NO) > 0 ";
                }
                if (Program.PlantAppVersion == "3.1")
                {
                    strQuery = @"SELECT 
	                            BDT.BATCH_NO AS BATCHNO
	                            ,FORMAT(BDT.BATCH_START_TIME, '" + Program.Date_Format + @"') AS BATCHDATE
	                            ,BDT.BATCH_START_TIME AS BATCHSTARTTIME
	                            ,BDT.BATCH_END_TIME AS BATCHENDTIME
	                            ,SITE
	                            ,TRUCK_NO AS TRUCKNO 
	                            ,CM.NAME AS CUSTOMERNAME
                            FROM
	                            ((DOCKET_HEADER AS BDT LEFT JOIN CUSTOMER_MASTER AS CM ON BDT.CUSTOMER_CODE = CM.CODE)) 
                                WHERE CDBL(BDT.BATCH_NO) > 0 ";
                }

                if (Program.PlantAppVersion == "1")
                {
                    strQuery = @"SELECT 
	                            BDT.BATCH_NO AS BATCHNO
                                ,FORMAT(BDT.BATCH_DATE, '" + Program.Date_Format + @"') AS BATCHDATE
	                            ,FORMAT(MIN(BDT.BATCH_TIME) ,'HH:MM:SS') AS BATCHSTARTTIME
	                            ,FORMAT(MAX(BDT.BATCH_TIME) ,'HH:MM:SS') AS BATCHENDTIME
	                            ,IIF(ISNULL(SM.SITE_ADDRESS),BDT.SITE_CODE,SM.SITE_ADDRESS) AS SITE
	                            ,TM.TRUCK_NO AS TRUCKNO	
	                            ,CM.CUSTOMER_NAME AS CUSTOMERNAME	
                            FROM
	                            ((VITRAG_BATCH AS BDT LEFT JOIN CUSTOMER_MASTER AS CM ON (BDT.CUSTOMER_CODE = CM.CUSTOMER_CODE OR BDT.CUSTOMER_CODE = CM.CUSTOMER_NAME))
	                            LEFT JOIN SITE_MASTER AS SM ON ((BDT.SITE_CODE = SM.SITE OR BDT.SITE_CODE = SM.SITE_ADDRESS)) )
	                            LEFT JOIN TRUCK_MASTER AS TM ON (BDT.TRUCK_NO = TM.TRUCK_NO OR BDT.TRUCK_NO = TM.TNO)
                            WHERE
                             CDBL(BDT.BATCH_NO) > 0  ";
                }
                if (Program.PlantAppVersion == "2")
                {
                    strQuery = @"SELECT
	                            BDT.BATCHNO AS BatchNo
                                ,FORMAT(BDT.DATE, '" + Program.Date_Format + @"') AS BatchDate           
	                            ,format(MIN(BDT.TIME),'hh:mm:ss') AS BatchStartTime
                                ,format(MAX(BDT.TIME) ,'hh:mm:ss') AS BatchEndTime	
	                            ,BDT.JOB_SITE AS SITE
	                            ,BDT.TRUCK_NO AS TruckNo 
	                            ,BDT.CUST_NAME AS CustomerName
                            FROM BATCH AS BDT 
                            WHERE CDBL(BDT.BatchNo) > 0 ";
                }
                if (string.IsNullOrEmpty(strFilter) == false)
                {
                    strQuery = strQuery + " AND " + strFilter;
                    if (Program.PlantAppVersion == "1")
                    {
                        strQuery =
                        strQuery + @" GROUP BY BDT.BATCH_NO,BDT.BATCH_DATE,CM.CUSTOMER_NAME,TM.TRUCK_NO,SM.SITE_ADDRESS ,BDT.SITE_CODE ";
                    }
                    if (Program.PlantAppVersion == "2")
                    {
                        strQuery =
                        strQuery + @" group by
	                            BDT.BATCHNO
	                            ,BDT.DATE
	                            ,BDT.JOB_SITE
	                            ,BDT.TRUCK_NO
	                            ,BDT.CUST_NAME ";
                    }
                }
                dtOriginalBatchList = DbHelper.FillDataTable(strQuery, Connection_String);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dtOriginalBatchList;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="orgID"></param>
        /// <param name="fromBatchNo"></param>
        /// <param name="toBatchNo"></param>
        /// <param name="customerCode"></param>
        /// <param name="fromBatchDate"></param>
        /// <param name="toBatchDate"></param>
        /// <param name="batchType"></param>
        /// <returns></returns>
        /*
       public static MCGM_BATCH.BusinessLayer.Model.BatchDatTransRaw[] GetOriginalBatchList(long orgID, long fromBatchNo, long toBatchNo, string customerCode, DateTime? fromBatchDate, DateTime? toBatchDate, int batchType)
       {
           batchDatTransRawFactory = new MCGM_BATCH.BusinessLayer.Factory.BatchDatTransRawFactory();

           MCGM_BATCH.BusinessLayer.Model.BatchDatTransRaw[] returnData;
            
           ScadaKing_Web_Service.Data_Trans_Field[] returnValue = batchDatTransRawFactory.GetOriginalBatchList(orgID, customerCode, fromBatchNo, toBatchNo, fromBatchDate, toBatchDate, batchType);
           #region
           returnData = new MCGM_BATCH.BusinessLayer.Model.BatchDatTransRaw[returnValue.Length];
           int listIndex = 0;
           for (listIndex = 0; listIndex < returnValue.Length; listIndex++)
           {
               returnData[listIndex] = new MCGM_BATCH.BusinessLayer.Model.BatchDatTransRaw();
               returnData[listIndex].AppVersion = returnValue[listIndex].AppVersion;
               returnData[listIndex].BatchDate = returnValue[listIndex].BatchDate;
               returnData[listIndex].BatchEndTime = returnValue[listIndex].BatchEndTime;

               returnData[listIndex].BatchEndTimeFloat = returnValue[listIndex].BatchEndTimeFloat;
               returnData[listIndex].BatcherName = returnValue[listIndex].BatcherName;
               returnData[listIndex].BatcherUserLevel = returnValue[listIndex].BatcherUserLevel;

               returnData[listIndex].BatchNo = returnValue[listIndex].BatchNo;
               returnData[listIndex].BatchSize = returnValue[listIndex].BatchSize;
               returnData[listIndex].BatchStartTime = returnValue[listIndex].BatchStartTime;


               returnData[listIndex].BatchStartTimeFloat = returnValue[listIndex].BatchStartTimeFloat;
               returnData[listIndex].BatchTime = returnValue[listIndex].BatchTime;
               returnData[listIndex].BatchTimeText = returnValue[listIndex].BatchTimeText;

               returnData[listIndex].BatchYear = returnValue[listIndex].BatchYear;
               ////returnData[listIndex].BrokenRulesList = returnValue[listIndex].
               returnData[listIndex].Cem1SetWt = returnValue[listIndex].Cem1SetWt;
               returnData[listIndex].CustomerCode = returnValue[listIndex].CustomerCode;
               returnData[listIndex].CustomerName = returnValue[listIndex].CustomerName;
               returnData[listIndex].IsMCGM = returnValue[listIndex].IsMCGM;

               ////returndata[listindex].isvalid = returnvalue[listindex].isvalid;
               returnData[listIndex].LoadingDateTime = returnValue[listIndex].LoadingDateTime;
               returnData[listIndex].MixerCapacity = returnValue[listIndex].MixerCapacity;
               returnData[listIndex].MixingTime = returnValue[listIndex].MixingTime;
               returnData[listIndex].OrderedQty = returnValue[listIndex].OrderedQty;
               returnData[listIndex].OrderNo = returnValue[listIndex].OrderNo;
               ////returndata[listindex].organizationname = returnvalue[listindex].organizationname;
               returnData[listIndex].OrgId = returnValue[listIndex].OrgId;
               returnData[listIndex].ProductionQty = returnValue[listIndex].ProductionQty;
               returnData[listIndex].RecipeCode = returnValue[listIndex].RecipeCode;
               returnData[listIndex].RecipeName = returnValue[listIndex].RecipeName;
               returnData[listIndex].ReplayStatus = returnValue[listIndex].ReplayStatus;
               returnData[listIndex].ReturnedQty = returnValue[listIndex].ReturnedQty;
               returnData[listIndex].RouteId = returnValue[listIndex].RouteId;
               returnData[listIndex].ScheduleId = returnValue[listIndex].ScheduleId;
               returnData[listIndex].Site = returnValue[listIndex].Site;
               returnData[listIndex].Strength = returnValue[listIndex].Strength;
               returnData[listIndex].TruckDriver = returnValue[listIndex].TruckDriver;
               returnData[listIndex].TruckNo = returnValue[listIndex].TruckNo;
               returnData[listIndex].UnLoadingDateTime = returnValue[listIndex].UnLoadingDateTime;
               returnData[listIndex].Valid = returnValue[listIndex].Valid;
               ////returnData[listIndex].ValidationRules = returnValue[listIndex].ValidationRules;

               returnData[listIndex].Withthisload = returnValue[listIndex].Withthisload;

           }
           #endregion
           return returnData;
           
       }
        */

        public static DataSet MCGM_Transfered_Batch(long BatchID, string Connection_String)
        {
            int line_No = 0;
            DataTable dtBatch_Dat_Trans = new DataTable();
            DataTable dtBatch_Transaction = new DataTable();
            DataSet dsReturnTables = new DataSet();
            try
            {
                string strQuery = string.Empty;

                //Added For New Implementation... - Makarand.
                if (Program.PlantAppVersion == "4.0")
                {
                    strQuery = @"SELECT TOP 1 BDT.BATCH_NO
	                            ,'' AS ORGANIZATION_NAME
	                            ,BDT.BATCH_DATE
	                            ,'' AS PlantSlNO
	                            ,format(BDT.BATCH_STARTTIME,'HH:mm:ss') AS BATCH_START_TIME
	                            ,format(BDT.BATCH_ENDTIME,'HH:mm:ss') AS BATCH_END_TIME
	                            ,BDT.SITE_ADR AS SITE
	                            ,BDT.TRUCK_NO
                                ,'' AS Batcher_Name
	                            ,'' AS TRUCK_DRIVER
	                            ,BDT.CUST_NAME AS CUSTOMER_NAME                      
	                            ,ROUND(IIF(ISNULL(BDT.PRODUCTION_QTY),0,CDBL(VAL(BDT.PRODUCTION_QTY))),2) AS Production_Qty	
	                            ,'0' AS Returned_Qty
	                            ,BDT.GRADE  AS Recipe_Code
	                            ,'0' AS Ordered_Qty
	                            ,BDT.GRADE AS Recipe_Name
	                            ,'0' AS WithThisLoad	
	                            ,BDT.BATCH_SIZE	 
                                FROM BATCH_DETAIL AS BDT 
                                WHERE BDT.BATCH_NO = " + BatchID;
					line_No = 1;
				}
                else if (Program.PlantAppVersion == "3")
                {
                    strQuery = @"SELECT TOP 1 BDT.BATCH_NO
	                            ,'' AS ORGANIZATION_NAME
	                            ,FORMAT(BDT.BATCH_DATE, '" + Program.Date_Format + @"') AS BATCH_DATE
	                            ,'' AS PlantSlNO
	                            ,format(BDT.BATCH_START_TIME,'" + Program.Time_Format + @"') AS BATCH_START_TIME
	                            ,format(BDT.BATCH_END_TIME,'" + Program.Time_Format + @"') AS BATCH_END_TIME
	                            ,SITE
	                            ,TRUCK_NO
                                ,IIF(ISNULL(BDT.Batcher_Name),'USER',BDT.Batcher_Name) AS Batcher_Name
	                            ,BDT.TRUCK_DRIVER
	                            ,CM.CUSTOMER_NAME	                        
	                            ,ROUND(IIF(ISNULL(BDT.PRODUCTION_QTY),0,CDBL(VAL(BDT.PRODUCTION_QTY))),2) AS Production_Qty	
	                            ,BDT.Returned_Qty
	                            ,BDT.Recipe_Code
	                            ,BDT.Ordered_Qty
	                            ,BDT.Recipe_Name
	                            ,BDT.WithThisLoad	
	                            ,BDT.Batch_Size	 
                                FROM ((BATCH_DAT_TRANS AS BDT LEFT JOIN CUSTOMER_MASTER AS CM ON BDT.CUSTOMER_CODE = CM.CUSTOMER_CODE)) 
                                WHERE BDT.BATCH_NO = " + BatchID;

                    line_No = 2;
                }
                else if (Program.PlantAppVersion == "3.1")
                {
                    strQuery = @"SELECT TOP 1 BDT.BATCH_NO
	                            ,'' AS ORGANIZATION_NAME
	                            ,FORMAT(BDT.BATCH_START_TIME, '" + Program.Date_Format + @"') AS BATCH_DATE
	                            ,'' AS PLANTSLNO
	                            ,FORMAT(BDT.BATCH_START_TIME,'" + Program.Time_Format + @"') AS BATCH_START_TIME
	                            ,FORMAT(BDT.BATCH_END_TIME,'" + Program.Time_Format + @"') AS BATCH_END_TIME
	                            ,BDT.SITE
	                            ,BDT.TRUCK_NO
	                            ,IIF(ISNULL(BDT.BATCHER_NAME),'USER',BDT.BATCHER_NAME) AS BATCHER_NAME
	                            ,BDT.TRUCK_DRIVER
	                            ,CM.NAME AS CUSTOMER_NAME
	                            ,ROUND(IIF(ISNULL(BDT.PRODUCEDQTY),0,CDBL(VAL(BDT.PRODUCEDQTY))),2) AS PRODUCTION_QTY	
	                            ,BDT.RETURNEDQTY AS Returned_Qty
	                            ,BDT.RECIPECODE AS Recipe_Code
                                ,BDT.ORDEREDQTY AS Ordered_Qty
	                            ,BDT.RECIPENAME AS Recipe_Name
	                            ,BDT.WITHTHISLOAD
	                            ,BDT.BATCH_SIZE	 
                                FROM ((DOCKET_HEADER AS BDT LEFT JOIN CUSTOMER_MASTER AS CM ON BDT.CUSTOMER_CODE = CM.CODE)) 
                                WHERE BDT.BATCH_NO = " + BatchID;

                    line_No = 3;
					#region Create NAMESETUP table if not exist in db
					DataTable dtFindTable = new DataTable();
					line_No = 4;
					string strAccessQuery = string.Empty;
					try
					{
						strAccessQuery = @"SELECT TOP 1 * FROM NAMESETUP";
						dtFindTable = DbHelper.FillDataTable(strAccessQuery, Connection_String);
					}
					catch
					{
						strAccessQuery = string.Empty;
						strAccessQuery = @"CREATE TABLE NAMESETUP(
	                                    GATE1 String
										,GATE2 String
										,GATE3 String
										,GATE4 String
										,GATE5 String
										,GATE6 String
										,CEMENT1 String
										,CEMENT2 String
										,CEMENT3 String
										,CEMENT4 String
										,FILLER String
										,WATER1 String
										,WATER2 String
										,ADDMIX1 String
										,ADDMIX2 String
										,ADDMIX3 String
										,ADDMIX4 String
										,SILICA String
										,SLURRY String
										,PIGMENT String
                                    )";
						DbHelper.ExecuteNonQuery(strAccessQuery, Connection_String);

						strAccessQuery = string.Empty;
						strAccessQuery = @"insert into NAMESETUP values('GATE1','GATE2','GATE3','GATE4','GATE5','GATE6','CEMENT1','CEMENT2','CEMENT3','CEMENT4','FILLER','WATER1','WATER2','Admix1','Admix2','Admix3','Admix4','SILICA','SLURRY','PIGMENT')";
						DbHelper.ExecuteNonQuery(strAccessQuery, Connection_String);
						line_No = 5;
					}
					#endregion
				}
				else if (Program.PlantAppVersion == "1")
                {
                    strQuery = @" SELECT TOP 1
	                                BDT.BATCH_NO
	                                ,'' AS ORGANIZATION_NAME
	                                ,BDT.BATCH_DATE
	                                ,'' AS PLANTSLNO
	                                ,format(MIN(BDT.BATCH_TIME),'hh:mm:ss') AS BATCH_START_TIME
	                                ,format(MAX(BDT.BATCH_TIME) ,'hh:mm:ss') AS BATCH_END_TIME	
	                                ,TM.TRUCK_NO
	                                ,'USER' AS BATCHER_NAME
	                                ,TM.TRUCK_DRIVER
	                                ,CM.CUSTOMER_NAME	
	                                ,(SELECT ROUND(SUM(IIF(ISNULL(VB.M3_QTY),0,CDBL(VAL(VB.M3_QTY)))),2) FROM VITRAG_BATCH AS VB WHERE VB.BATCH_NO = BDT.BATCH_NO) AS PRODUCTION_QTY
	                                ,IIF(ISNULL(SM.SITE_ADDRESS),BDT.SITE_CODE,SM.SITE_ADDRESS) AS SITE
	                                ,0 AS RETURNED_QTY
	                                ,IIF(ISNULL(BDT.FORMULA_NAME),BDT.FORMULA_CODE,BDT.FORMULA_NAME) AS RECIPE_CODE
	                                ,0 AS ORDERED_QTY
	                                ,IIF(ISNULL(BDT.FORMULA_NAME),BDT.FORMULA_CODE,BDT.FORMULA_NAME) AS RECIPE_NAME
	                                ,'' AS WITHTHISLOAD	
	                                ,ROUND(IIF(ISNULL(BDT.M3_QTY),0,CDBL(VAL(BDT.M3_QTY))),2) AS BATCH_SIZE
                                    FROM
	                                ((VITRAG_BATCH AS BDT LEFT JOIN CUSTOMER_MASTER AS CM ON (BDT.CUSTOMER_CODE = CM.CUSTOMER_CODE OR BDT.CUSTOMER_CODE = CM.CUSTOMER_NAME))
	                                LEFT JOIN SITE_MASTER AS SM ON ((BDT.SITE_CODE = SM.SITE OR BDT.SITE_CODE = SM.SITE_ADDRESS)) )
	                                LEFT JOIN TRUCK_MASTER AS TM ON (BDT.TRUCK_NO = TM.TRUCK_NO OR BDT.TRUCK_NO = TM.TNO)
                                    WHERE  BDT.BATCH_NO =" + BatchID
                                                             + @" GROUP BY BDT.BATCH_NO,BDT.BATCH_DATE,TM.TRUCK_NO,TM.TRUCK_DRIVER,BDT.M3_QTY,CM.CUSTOMER_NAME,SM.SITE_ADDRESS,BDT.SITE_CODE,BDT.FORMULA_CODE,BDT.FORMULA_NAME ";
					line_No = 6;
				}
                else if (Program.PlantAppVersion == "2")
                {
                    strQuery = @"SELECT TOP 1 
	                        B.BATCHNO AS BATCH_NO
	                        ,'' AS ORGANIZATION_NAME
	                        ,B.DATE AS BATCH_DATE
	                        ,'' AS PLANTSLNO	                       
                            ,format(MIN(B.TIME),'hh:mm:ss') AS BATCH_START_TIME
	                        ,format(MAX(B.TIME) ,'hh:mm:ss') AS BATCH_END_TIME	
	                        ,B.TRUCK_NO
	                        ,'USER' AS BATCHER_NAME
	                        ,'' AS TRUCK_DRIVER
	                        ,B.CUST_NAME AS CUSTOMER_NAME	                        
                            ,ROUND(IIF(ISNULL(B.M3_QTY),0,CDBL(VAL(B.M3_QTY))),2) AS PRODUCTION_QTY
	                        ,B.JOB_SITE AS SITE
	                        ,0 AS RETURNED_QTY
	                        ,IIF(ISNULL(designmix),'',designmix) AS RECIPE_CODE
	                        ,0 AS ORDERED_QTY
	                        ,IIF(ISNULL(designmix),'',designmix) AS RECIPE_NAME
	                        ,'' AS WITHTHISLOAD	
	                       ,ROUND(IIF(ISNULL(B.M3_QTY),0,CDBL(VAL(B.M3_QTY))) / COUNT(B.BATCHNO) ,2) AS BATCH_SIZE
                        FROM BATCH AS B
                        WHERE B.BATCHNO =" + BatchID
                        + @" GROUP BY
	                            B.BATCHNO,B.DATE,B.TRUCK_NO,B.CUST_NAME,B.M3_QTY,B.JOB_SITE,designmix";

                    #region Create NAMESETUP table if not exist in db
                    DataTable dtFindTable = new DataTable();
                    line_No = 7;
                    string strAccessQuery = string.Empty;
                    try
                    {
                        strAccessQuery = @"SELECT TOP 1 * FROM NAMESETUP";
                        dtFindTable = DbHelper.FillDataTable(strAccessQuery, Connection_String);
                    }
                    catch
                    {
                        strAccessQuery = string.Empty;
                        strAccessQuery = @"CREATE TABLE NAMESETUP(
	                                    AGG1 String
	                                    ,AGG2 String
	                                    ,AGG3 String
	                                    ,AGG4 String
	                                    ,CEMENT String
	                                    ,FILLER1 String
	                                    ,FILLER2 String
	                                    ,WATER1 String
	                                    ,WATER2 String
	                                    ,WATER3 String
	                                    ,ADDMIX1 String
	                                    ,ADDMIX2 String
                                    )";
                        DbHelper.ExecuteNonQuery(strAccessQuery, Connection_String);

                        strAccessQuery = string.Empty;
                        strAccessQuery = @"insert into NAMESETUP values('R SAND','V S I','10MM','20 MM','CEMENT 1','Cem5','FILLER2','WATER1','WATER2','WATER3','Admix1.1','Admix1.2')";
                        DbHelper.ExecuteNonQuery(strAccessQuery, Connection_String);
                        line_No = 8;
                    }
                    #endregion
                }
                dtBatch_Dat_Trans = DbHelper.FillDataTable(strQuery, Connection_String);
                dtBatch_Dat_Trans.TableName = "HTML_HEADER";

                string Batch_Date = string.Empty;
                string Date = string.Empty;
                foreach (DataRow dr in dtBatch_Dat_Trans.Rows)
                {
                    DateTime BatchDt;
                    Batch_Date = Convert.ToString(dr["BATCH_DATE"]);//Convert.ToDateTime(dr["BATCH_DATE"]).ToString(Program.Date_Format);

                    //Batch_Date =  DateTime.ParseExact(Batch_Date, "dd/MM/yyyy", CultureInfo.InvariantCulture)
                    //    .ToString("MM/dd/yyyy", CultureInfo.InvariantCulture);

                    //Batch_Date = Convert.ToString(DateTime.ParseExact(Batch_Date, "dd/MM/yyyy", CultureInfo.InvariantCulture).ToString("MM/dd/yyyy", CultureInfo.InvariantCulture));

                    //BatchDt = Convert.ToDateTime(dr["BATCH_DATE"]);
                    //BatchDt = Convert.ToDateTime(Batch_Date);
                    BatchDt = DateTime.ParseExact(Batch_Date, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                    Batch_Date = Convert.ToDateTime(BatchDt).ToString(Program.Date_Format);
                    Date = Batch_Date.Replace("-", "/");
                    //try
                    //{
                        dtBatch_Dat_Trans.Rows[0]["Batch_Date"] = Date;
                    //}
                    //catch (Exception)
                    //{
                    //    //dtBatch_Dat_Trans.Rows[0]["Batch_Date"] = Convert.ToDateTime(Date);
                    //}
                }

                dsReturnTables.Tables.Add(dtBatch_Dat_Trans);
                line_No = 9;
                strQuery = Batch_Transation_Query(BatchID, Connection_String);
                line_No = 10;
                dtBatch_Transaction = DbHelper.FillDataTable(strQuery, Connection_String);
                line_No = 11;
                dtBatch_Transaction.TableName = "HTML_DETAILS";
                dsReturnTables.Tables.Add(dtBatch_Transaction);
                line_No = 12;
            }
            catch (Exception ex)
            {
                Global.WriteLog("MCGM_Transfered_Batch() FOR Line No [" + line_No + "] Exception: ", LogMessageType.Error, ex.Message);
                throw ex;
            }
            return dsReturnTables;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="BatchID"></param>
        /// <returns></returns>
        /*
        public static MCGM_BATCH.ScadaKing_Web_Service.Response_LocalSQLData MCGM_Transfered_Batch(long BatchID, bool show_Raw_Data)
        {
            batchDatTransRawFactory = new MCGM_BATCH.BusinessLayer.Factory.BatchDatTransRawFactory();

            return batchDatTransRawFactory.MCGM_Transfered_Batch(BatchID, show_Raw_Data);
        }
        */

        internal static bool Update_HTML_File_Creation_Status(string batch_No, string file_Created_Date)
        {
            batchDatTransRawFactory = new ScadaKing_Data_App.BusinessLayer.Factory.BatchDatTransRawFactory();
            return batchDatTransRawFactory.Update_HTML_File_Creation_Status(batch_No, file_Created_Date);
        }

        private static string Batch_Transation_Query(long BatchNo, string strConnection)
        {
            //bool flag = false;
            string QueryResult = string.Empty;
            int round_Digits = 3;
            int line_No = 0;
            try
            {
                //Added For New Plant Installation
                if (Program.PlantAppVersion == "4.0")
                {
                    #region Version 4.0
                    QueryResult = @"SELECT DISTINCT TOP 1 '' AS FLAG ,1 AS ORDERNO,SWITCH(AGR1 = '', 'AGG1' ,AGR1 <> '',AGR1 )AS PROD1
                                        ,SWITCH(AGR2 = '', 'AGG2' ,AGR2 <> '',AGR2) AS PROD2
                                        ,SWITCH(AGR3 = '', 'AGG3' ,AGR3 <> '',AGR3) AS PROD3
                                        ,SWITCH(AGR4 = '', 'AGG4' ,AGR4 <> '',AGR4) AS PROD4
                                        ,SWITCH(AGR5 = '', 'AGG5' ,AGR5 <> '',AGR5) AS PROD5
                                        ,SWITCH(CEM1 = '', 'CEM1' ,CEM1 <> '',CEM1) AS CEMENT1
                                        ,SWITCH(CEM2 = '', 'CEM2' ,CEM2 <> '',CEM2) AS CEMENT2
                                        ,SWITCH(CEM3 = '', 'CEM3' ,CEM3 <> '',CEM3) AS CEMENT3
                                        ,SWITCH(WAT1 = '', 'WATER1' ,WAT1<> '',WAT1) AS WATER1
                                        ,SWITCH(ADD1 = '', 'ADD1' ,ADD1<> '',ADD1) AS ADD1
                                        ,SWITCH(ADD2 = '', 'ADD2' ,ADD2<> '',ADD2) AS ADD2
                                   FROM  NAMESETUP  "
                                                     + @" union
                                        SELECT TOP 1
			                            'Recipe' AS FLAG
			                            ,2 AS OrderNo
		                           ,ROUND(VAL(IIF(ISNULL(BDT.AGR1_SET),0,CDBL(VAL(BDT.AGR1_SET)))),0) AS [Prod1]
		                            ,ROUND(VAL(IIF(ISNULL(BDT.AGR2_SET),0,CDBL(VAL(BDT.AGR2_SET)))),0) AS [Prod2]
		                            ,ROUND(VAL(IIF(ISNULL(BDT.AGR3_SET),0,CDBL(VAL(BDT.AGR3_SET)))),0) AS [Prod3]
		                            ,ROUND(VAL(IIF(ISNULL(BDT.AGR4_SET),0,CDBL(VAL(BDT.AGR4_SET)))),0) AS [Prod4]
		                            ,ROUND(VAL(IIF(ISNULL(BDT.AGR5_SET),0,CDBL(VAL(BDT.AGR5_SET)))),0) AS [Prod5]
		                            ,ROUND(VAL(IIF(ISNULL(BDT.CEM1_SET),0,CDBL(VAL(BDT.CEM1_SET)))),0) AS [Cement1]
		                            ,ROUND(VAL(IIF(ISNULL(BDT.CEM2_SET),0,CDBL(VAL(BDT.CEM2_SET)))),0) AS [Cement2]
		                            ,ROUND(VAL(IIF(ISNULL(BDT.CEM3_SET),0,CDBL(VAL(BDT.CEM3_SET)))),0) AS [Cement3]	
		                            ,ROUND(VAL(IIF(ISNULL(BDT.WAT1_SET),0,CDBL(VAL(BDT.WAT1_SET)))),0) AS [Water1]
		                            ,ROUND(VAL(IIF(ISNULL(BDT.ADD1_SET),0,CDBL(VAL(BDT.ADD1_SET)))),4) AS [Admixture1]
		                            ,ROUND(VAL(IIF(ISNULL(BDT.ADD2_SET),0,CDBL(VAL(BDT.ADD2_SET)))),4) AS [Admixture3]
                            FROM BATCHES AS BT
                            ,BATCH_DETAIL AS BDT
                            WHERE  BDT.BATCH_NO  = BT.BATCH_NO 
                            AND BT.BATCH_NO =" + BatchNo + @" union 
	                               SELECT TOP 1        
		                            'Target' AS flag 
		                            ,3 AS OrderNo 
		                            ,ROUND(VAL(IIF(ISNULL(BT.AGR1_ACT),0,CDBL(VAL(BT.AGR1_ACT)))),0) AS [Prod1]
		                            ,ROUND(VAL(IIF(ISNULL(BT.AGR2_ACT),0,CDBL(VAL(BT.AGR2_ACT)))),0) AS [Prod2]
		                            ,ROUND(VAL(IIF(ISNULL(BT.AGR3_ACT),0,CDBL(VAL(BT.AGR3_ACT)))),0) AS [Prod3]
		                            ,ROUND(VAL(IIF(ISNULL(BT.AGR4_ACT),0,CDBL(VAL(BT.AGR4_ACT)))),0) AS [Prod4]
		                            ,ROUND(VAL(IIF(ISNULL(BT.AGR5_ACT),0,CDBL(VAL(BT.AGR5_ACT)))),0) AS [Prod5]
		                            ,ROUND(VAL(IIF(ISNULL(BT.CEM1_ACT),0,CDBL(VAL(BT.CEM1_ACT)))),0) AS [Cement1]
		                            ,ROUND(VAL(IIF(ISNULL(BT.CEM2_ACT),0,CDBL(VAL(BT.CEM2_ACT)))),0) AS [Cement2]
		                            ,ROUND(VAL(IIF(ISNULL(BT.CEM3_ACT),0,CDBL(VAL(BT.CEM3_ACT)))),0) AS [Cement3]	
		                            ,ROUND(VAL(IIF(ISNULL(BT.WAT1_ACT),0,CDBL(VAL(BT.WAT1_ACT)))),0) AS [Water1]
		                            ,ROUND(VAL(IIF(ISNULL(BT.ADD1_ACT),0,CDBL(VAL(BT.ADD1_ACT)))),4) AS [Admixture1]
		                            ,ROUND(VAL(IIF(ISNULL(BT.ADD2_ACT),0,CDBL(VAL(BT.ADD2_ACT)))),4) AS [Admixture3]
                            FROM BATCHES AS BT
                            ,BATCH_DETAIL AS BDT
                            WHERE BT.BATCH_NO = BDT.BATCH_NO 
                            AND BT.BATCH_NO =" + BatchNo + @" union all 
                            SELECT format(BATCHX_ENDTIME ,'hh:mm:ss') AS flag,4 AS OrderNo
		                           ,ROUND(AGR1_ACT,0) AS [Prod1]
		                            ,ROUND(AGR2_ACT,0) AS [Prod2]
		                            ,ROUND(AGR3_ACT,0) AS [Prod3]
		                            ,ROUND(AGR4_ACT,0) AS [Prod4]
		                            ,ROUND(AGR5_ACT,0) AS [Prod5]
		                            ,ROUND(CEM1_ACT,0) AS [Cement1]
		                            ,ROUND(CEM2_ACT,0) AS [Cement2]
		                            ,ROUND(CEM3_ACT,0) AS [Cement3]
		                            ,ROUND(WAT1_ACT,0) AS [Water1]
		                            ,ROUND(ADD1_ACT,4) AS [Admixture1]
		                            ,ROUND(ADD2_ACT,4) AS [Admixture2]
                            FROM BATCHES where batch_no = " + BatchNo + @" union all 
                                SELECT TOP 1  'Act' AS flag,5 AS OrderNo
	                                ,ROUND(sum(AGR1_ACT),0) AS [Prod1]
	                                ,ROUND(sum(AGR2_ACT),0) AS [Prod2]
	                                ,ROUND(sum(AGR3_ACT),0) AS [Prod3]
	                                ,ROUND(sum(AGR4_ACT),0) AS [Prod4]
	                                ,ROUND(sum(AGR5_ACT),0) AS [Prod5]
	                                ,ROUND(sum(CEM1_ACT),0) AS [Cement1]
	                                ,ROUND(sum(CEM2_ACT),0) AS [Cement2]
	                                ,ROUND(sum(CEM3_ACT),0) AS [Cement3]
	                                ,ROUND(sum(WAT1_ACT),0) AS [Water1]
	                                ,ROUND(sum(ADD1_ACT),4) AS [Admixture1]
	                                ,ROUND(sum(ADD2_ACT),4) AS [Admixture2]
                                FROM BATCHES where batch_no =" + BatchNo + @" union all 
                                SELECT TOP 1 'Tar' AS flag,6 AS OrderNo
	                                ,ROUND(SUM(AGR1_SET * PRODUCTION_QTY),0) AS [Prod1]	
	                                ,ROUND(SUM(AGR2_SET * PRODUCTION_QTY),0) AS [Prod2]
	                                ,ROUND(SUM(AGR3_SET * PRODUCTION_QTY),0) AS [Prod3]
	                                ,ROUND(SUM(AGR4_SET * PRODUCTION_QTY),0) AS [Prod4]
	                                ,ROUND(SUM(AGR5_SET * PRODUCTION_QTY),0) AS [Prod5]
	                                ,ROUND(SUM(CEM1_SET * PRODUCTION_QTY),0) AS [Cement1]
	                                ,ROUND(SUM(CEM2_SET * PRODUCTION_QTY),0) AS [Cement2]
	                                ,ROUND(SUM(CEM3_SET * PRODUCTION_QTY),0) AS [Cement3]
	                                ,ROUND(SUM(WAT1_SET * PRODUCTION_QTY),0) AS [Water1]
	                                ,ROUND(SUM(ADD1_SET * PRODUCTION_QTY),4) AS [Admixture1]
	                                ,ROUND(SUM(ADD2_SET * PRODUCTION_QTY),4) AS [Admixture2]
                                FROM BATCH_DETAIL where batch_no =" + BatchNo;
                    #endregion
                }

                if (Program.PlantAppVersion == "3")
                {
                    if (Program.GATE4_TARGET == "1")
                    {
                        #region Version3 New [ Gate4_Target Value ] ....

                        QueryResult = @"SELECT distinct TOP 1 '' AS flag ,1 AS OrderNo,Switch(Gate1Name = '', 'AGG1' ,Gate1Name <> '',Gate1Name )AS Prod1
	                                                ,Switch(Gate2Name = '', 'AGG2' ,Gate2Name <> '',Gate2Name) AS Prod2
	                                                ,Switch(Gate3Name = '', 'AGG3' ,Gate3Name <> '',Gate3Name) AS Prod3
	                                                ,Switch(Gate4Name = '', 'AGG4' ,Gate4Name <> '',Gate4Name) AS Prod4
	                                                ,Switch(Gate5Name = '', 'AGG5' ,Gate5Name <> '',Gate5Name) AS Prod5
	                                                ,Switch(Gate6Name = '', 'AGG6' ,Gate6Name <> '',Gate6Name) AS Prod6
	                                                ,Switch(Cem1Name = '', 'CEM1' ,Cem1Name <> '',Cem1Name) AS Cement1
	                                                ,Switch(Cem2Name = '', 'CEM2' ,Cem2Name <> '',Cem2Name) AS Cement2
	                                                ,Switch(Cem3Name = '', 'CEM3' ,Cem3Name <> '',Cem3Name) AS Cement3
                                                    ,Switch(Cem4Name = '', 'CEM4' ,Cem4Name <> '',Cem4Name) AS Cement4
	                                                ,Switch(FillName = '', 'FILLER' ,FillName<> '',FillName) AS Fill
	                                                ,Switch(Wtr1Name = '', 'WATER1' ,Wtr1Name<> '',Wtr1Name) AS Water1
	                                                ,Switch(Wtr2Name = '', 'WATER2' ,Wtr2Name<> '',Wtr2Name) AS Water2
	                                                ,Switch(Admix1Name = '', 'Admix1.1' ,Admix1Name<> '',Admix1Name) AS Admixture1
	                                                ,Switch(Admix12Name = '', 'Admix1.2' ,Admix12Name<> '',Admix12Name) AS Admixture2
	                                                ,Switch(Admix2Name = '', 'Admix2.1' ,Admix2Name<> '',Admix2Name) AS Admixture3
	                                                ,Switch(Admix22Name = '', 'Admix2.2' ,Admix22Name<> '',Admix22Name) AS Admixture4
	                                                ,Switch(SilicaName = '', 'ADD1' ,SilicaName<> '',SilicaName) AS Add1
	                                                ,Switch(SlurryName = '', 'ADD2' ,SlurryName<> '',SlurryName) AS Add2
                                                 FROM  NameSetUp "
                                                         + @" union
                                                      SELECT TOP 1
	                                                    'Recipe' AS FLAG
	                                                    ,2 AS OrderNo
	                                                ,ROUND(VAL(IIF(ISNULL(BT.Gate1_Rec),0,CDBL(VAL(BT.Gate1_Rec)))),0) AS [Prod1]
	                                                ,ROUND(VAL(IIF(ISNULL(BT.Gate2_Rec),0,CDBL(VAL(BT.Gate2_Rec)))),0) AS [Prod2]
	                                                ,ROUND(VAL(IIF(ISNULL(BT.Gate3_Rec),0,CDBL(VAL(BT.Gate3_Rec)))),0) AS [Prod3]
	                                                ,ROUND(VAL(IIF(ISNULL(BT.Gate4_Rec),0,CDBL(VAL(BT.Gate4_Rec)))),0) AS [Prod4]
	                                                ,ROUND(VAL(IIF(ISNULL(BT.Gate5_Rec),0,CDBL(VAL(BT.Gate5_Rec)))),0) AS [Prod5]
	                                                ,ROUND(VAL(IIF(ISNULL(BT.Gate6_Rec),0,CDBL(VAL(BT.Gate6_Rec)))),0) AS [Prod6]
	                                                ,ROUND(VAL(IIF(ISNULL(BT.Cem1_Rec),0,CDBL(VAL(BT.Cem1_Rec)))),0) AS [Cement1]
	                                                ,ROUND(VAL(IIF(ISNULL(BT.Cem2_Rec),0,CDBL(VAL(BT.Cem2_Rec)))),0) AS [Cement2]
	                                                ,ROUND(VAL(IIF(ISNULL(BT.Cem3_Rec),0,CDBL(VAL(BT.Cem3_Rec)))),0) AS [Cement3]		
                                                    ,ROUND(VAL(IIF(ISNULL(BT.Cem4_Rec),0,CDBL(VAL(BT.Cem4_Rec)))),0) AS [Cement4]
	                                                ,ROUND(VAL(IIF(ISNULL(BDT.Filler1_Target),0,CDBL(VAL(BDT.Filler1_Target)))),0) AS [Fill]		
	                                                ,ROUND(VAL(IIF(ISNULL(BT.Wtr1_Rec),0,CDBL(VAL(BT.Wtr1_Rec)))),0) AS [Water1]
	                                                ,ROUND(VAL(IIF(ISNULL(BT.Wtr2_Rec),0,CDBL(VAL(BT.Wtr2_Rec)))),0) AS [Water2]	
	                                                ,ROUND(VAL(IIF(ISNULL(BT.Adm1_Rec),0,CDBL(VAL(BT.Adm1_Rec)))),2) AS [Admixture1]	
	                                                ,ROUND(VAL(IIF(ISNULL(BT.Adm2_Rec),0,CDBL(VAL(BT.Adm2_Rec)))),2) AS [Admixture2]
	                                                ,ROUND(VAL(IIF(ISNULL(BDT.Adm2_Target1),0,CDBL(VAL(BDT.Adm2_Target1)))),2) AS [Admixture3]	
	                                                ,ROUND(VAL(IIF(ISNULL(BDT.Adm2_Target2),0,CDBL(VAL(BDT.Adm2_Target2)))),2) AS [Admixture4]	
	                                                ,ROUND(VAL(IIF(ISNULL(BDT.Silica_Target),0,CDBL(VAL(BDT.Silica_Target)))),0) AS [Add1]	
	                                                ,ROUND(VAL(IIF(ISNULL(BDT.Slurry_Target),0,CDBL(VAL(BDT.Slurry_Target)))),0) AS [Add2]	
                                                FROM BATCH_DAT_TRANS AS BT
                                                ,BATCH_TRANSACTION AS BDT
                                                WHERE BT.BATCH_NO = BDT.BATCH_NO 
                                            AND BT.BATCH_NO =" + BatchNo
                                                        + @" union  
                                               SELECT TOP 1        
	                                            'Target' AS flag 
	                                            ,3 AS OrderNo ";

                        string strTragetQuery = string.Empty;
                        string strQuery = "SELECT TOP 1 * FROM BATCH_DAT_TRANS WHERE BATCH_NO =  -2";
                        DataTable dtColumns = DbHelper.FillDataTable(strQuery, strConnection);
                        if (dtColumns.Columns.Contains("Gate1_Target"))
                        {
                            strTragetQuery = @" ,ROUND(VAL(IIF(ISNULL(BT.Gate1_Target),0,CDBL(VAL(BT.Gate1_Target)))),0) AS [Prod1]
	                                            ,ROUND(VAL(IIF(ISNULL(BT.Gate2_Target),0,CDBL(VAL(BT.Gate2_Target)))),0) AS [Prod2]
		                                        ,ROUND(VAL(IIF(ISNULL(BT.Gate3_Target),0,CDBL(VAL(BT.Gate3_Target)))),0) AS [Prod3]
	                                            ,ROUND(VAL(IIF(ISNULL(BT.Gate4_Target),0,CDBL(VAL(BT.Gate4_Target)))),0) AS [Prod4]
	                                            ,ROUND(VAL(IIF(ISNULL(BT.Gate5_Target),0,CDBL(VAL(BT.Gate5_Target)))),0) AS [Prod5]
	                                            ,ROUND(VAL(IIF(ISNULL(BT.Gate6_Target),0,CDBL(VAL(BT.Gate6_Target)))),0) AS [Prod6]	
		                                        ,ROUND(VAL(IIF(ISNULL(BT.Cement1_Target),0,CDBL(VAL(BT.Cement1_Target)))),0) AS [Cement1]
	                                            ,ROUND(VAL(IIF(ISNULL(BT.Cement2_Target),0,CDBL(VAL(BT.Cement2_Target)))),0) AS [Cement2]
	                                            ,ROUND(VAL(IIF(ISNULL(BT.Cement3_Target),0,CDBL(VAL(BT.Cement3_Target)))),0) AS [Cement3]	
                                                ,ROUND(VAL(IIF(ISNULL(BT.Cement4_Target),0,CDBL(VAL(BT.Cement4_Target)))),0) AS [Cement4]	
		                                        ,ROUND(VAL(IIF(ISNULL(BT.Filler1_Target),0,CDBL(VAL(BT.Filler1_Target)))),0) AS [Fill]		
	                                            ,ROUND(VAL(IIF(ISNULL(BT.Water1_Target),0,CDBL(VAL(BT.Water1_Target)))),0) AS [Water1]
	                                            ,ROUND(VAL(IIF(ISNULL(BT.Water2_Target),0,CDBL(VAL(BT.Water2_Target)))),0) AS [Water2]	
		                                        ,ROUND(VAL(IIF(ISNULL(BT.Adm1_Target1),0,CDBL(VAL(BT.Adm1_Target1))))," + round_Digits + @") AS [Admixture1]	
		                                        ,ROUND(VAL(IIF(ISNULL(BT.Adm1_Target2),0,CDBL(VAL(BT.Adm1_Target2))))," + round_Digits + @") AS [Admixture2]
	                                            ,ROUND(VAL(IIF(ISNULL(BT.Adm2_Target1),0,CDBL(VAL(BT.Adm2_Target1))))," + round_Digits + @") AS [Admixture3]	
		                                        ,ROUND(VAL(IIF(ISNULL(BT.Adm2_Target2),0,CDBL(VAL(BT.Adm2_Target2))))," + round_Digits + @") AS [Admixture4]	
	                                            ,ROUND(VAL(IIF(ISNULL(BT.Silica_Target),0,CDBL(VAL(BT.Silica_Target)))),0) AS [Add1]	
	                                            ,ROUND(VAL(IIF(ISNULL(BT.Slurry_Target),0,CDBL(VAL(BT.Slurry_Target)))),0) AS [Add2]";
                        }
                        else
                        {
                            strTragetQuery = @" ,ROUND(VAL(IIF(ISNULL(BT.Gate1_Target),0,CDBL(VAL(BT.Gate1_Target)))),0) AS [Prod1]
	                                            ,ROUND(VAL(IIF(ISNULL(BT.Gate2_Target),0,CDBL(VAL(BT.Gate2_Target)))),0) AS [Prod2]
		                                        ,ROUND(VAL(IIF(ISNULL(BT.Gate3_Target),0,CDBL(VAL(BT.Gate3_Target)))),0) AS [Prod3]
	                                            ,ROUND(VAL(IIF(ISNULL(BT.Gate4_Target),0,CDBL(VAL(BT.Gate4_Target)))),0) AS [Prod4]
	                                            ,ROUND(VAL(IIF(ISNULL(BT.Gate5_Target),0,CDBL(VAL(BT.Gate5_Target)))),0) AS [Prod5]
	                                            ,ROUND(VAL(IIF(ISNULL(BT.Gate6_Target),0,CDBL(VAL(BT.Gate6_Target)))),0) AS [Prod6]	
		                                        ,ROUND(VAL(IIF(ISNULL(BT.Cement1_Target),0,CDBL(VAL(BT.Cement1_Target)))),0) AS [Cement1]
	                                            ,ROUND(VAL(IIF(ISNULL(BT.Cement2_Target),0,CDBL(VAL(BT.Cement2_Target)))),0) AS [Cement2]
	                                            ,ROUND(VAL(IIF(ISNULL(BT.Cement3_Target),0,CDBL(VAL(BT.Cement3_Target)))),0) AS [Cement3]	
                                                ,ROUND(VAL(IIF(ISNULL(BT.Cement4_Target),0,CDBL(VAL(BT.Cement4_Target)))),0) AS [Cement4]	
		                                        ,ROUND(VAL(IIF(ISNULL(BT.Filler1_Target),0,CDBL(VAL(BT.Filler1_Target)))),0) AS [Fill]		
	                                            ,ROUND(VAL(IIF(ISNULL(BT.Water1_Target),0,CDBL(VAL(BT.Water1_Target)))),0) AS [Water1]
	                                            ,ROUND(VAL(IIF(ISNULL(BT.Water2_Target),0,CDBL(VAL(BT.Water2_Target)))),0) AS [Water2]	
		                                        ,ROUND(VAL(IIF(ISNULL(BT.Adm1_Target1),0,CDBL(VAL(BT.Adm1_Target1))))," + round_Digits + @") AS [Admixture1]	
		                                        ,ROUND(VAL(IIF(ISNULL(BT.Adm1_Target2),0,CDBL(VAL(BT.Adm1_Target2))))," + round_Digits + @") AS [Admixture2]
	                                            ,ROUND(VAL(IIF(ISNULL(BT.Adm2_Target1),0,CDBL(VAL(BT.Adm2_Target1))))," + round_Digits + @") AS [Admixture3]	
		                                        ,ROUND(VAL(IIF(ISNULL(BT.Adm2_Target2),0,CDBL(VAL(BT.Adm2_Target2))))," + round_Digits + @") AS [Admixture4]	
	                                            ,ROUND(VAL(IIF(ISNULL(BT.Silica_Target),0,CDBL(VAL(BT.Silica_Target)))),0) AS [Add1]	
	                                            ,ROUND(VAL(IIF(ISNULL(BT.Slurry_Target),0,CDBL(VAL(BT.Slurry_Target)))),0) AS [Add2]";
                        }
                        QueryResult = QueryResult + " " + strTragetQuery + @" FROM BATCH_TRANSACTION AS BT
                                            ,BATCH_DAT_TRANS AS BDT
                                            WHERE BDT.BATCH_NO = BT.BATCH_NO 
                                            AND BT.BATCH_NO  =" + BatchNo
                                + @" union all 
                                                SELECT format(Batch_Time ,'hh:mm:ss') AS flag,4 AS OrderNo
                                                        ,ROUND(Gate1_Actual,0) AS [Prod1]
                                                        ,ROUND(Gate2_Actual,0) AS [Prod2]
                                                        ,ROUND(Gate3_Actual,0) AS [Prod3]
                                                        ,ROUND(Gate4_Actual,0) AS [Prod4]
                                                        ,ROUND(Gate5_Actual,0) AS [Prod5]
                                                        ,ROUND(Gate6_Actual,0) AS [Prod6]
                                                        ,ROUND(Cement1_Actual,0) AS [Cement1]
                                                        ,ROUND(Cement2_Actual,0) AS [Cement2]
                                                        ,ROUND(Cement3_Actual,0) AS [Cement3]
                                                        ,ROUND(Cement4_Actual,0) AS [Cement4]
                                                        ,ROUND(Filler1_Actual,0) AS [Fill]
                                                        ,ROUND(Water1_Actual,0) AS [Water1]
                                                        ,ROUND(Water2_Actual,0) AS [Water2]
                                                        ,ROUND(Adm1_Actual1," + round_Digits + @") AS [Admixture1]
                                                        ,ROUND(Adm1_Actual2," + round_Digits + @") AS [Admixture2]
                                                        ,ROUND(Adm2_Actual1," + round_Digits + @") AS [Admixture3]
                                                        ,ROUND(Adm2_Actual2," + round_Digits + @") AS [Admixture4]
                                                        ,ROUND(Silica_Actual,0) AS [Add1]
                                                        ,ROUND(Slurry_Actual,0) AS [Add2] 
                                                FROM BATCH_TRANSACTION where batch_no =" + BatchNo
                                + @" union all 
                                                SELECT TOP 1  'Act' AS flag,5 AS OrderNo
                                                    ,ROUND(sum(Gate1_Actual),0) AS [Prod1]
	                                                ,ROUND(sum(Gate2_Actual),0) AS [Prod2]
	                                                ,ROUND(sum(Gate3_Actual),0) AS [Prod3]
	                                                ,ROUND(sum(Gate4_Actual),0) AS [Prod4]
	                                                ,ROUND(sum(Gate5_Actual),0) AS [Prod5]
	                                                ,ROUND(sum(Gate6_Actual),0) AS [Prod6]
	                                                ,ROUND(sum(Cement1_Actual),0) AS [Cement1]
	                                                ,ROUND(sum(Cement2_Actual),0) AS [Cement2]
	                                                ,ROUND(sum(Cement3_Actual),0) AS [Cement3]
                                                    ,ROUND(sum(Cement4_Actual),0) AS [Cement4]  
	                                                ,ROUND(sum(Filler1_Actual),0) AS [Fill]
	                                                ,ROUND(sum(Water1_Actual),0) AS [Water1]
	                                                ,ROUND(sum(Water2_Actual),0) AS [Water2]
	                                                ,ROUND(sum(Adm1_Actual1)," + round_Digits + @") AS [Admixture1]
	                                                ,ROUND(sum(Adm1_Actual2)," + round_Digits + @") AS [Admixture2]
	                                                ,ROUND(sum(Adm2_Actual1)," + round_Digits + @") AS [Admixture3]
	                                                ,ROUND(sum(Adm2_Actual2)," + round_Digits + @") AS [Admixture4]
	                                                ,ROUND(sum(Silica_Actual),0) AS [Add1]
	                                                ,ROUND(sum(Slurry_Actual),0) AS [Add2] 
                                                FROM BATCH_TRANSACTION where batch_no =" + BatchNo
                                + @" union all 
                                                SELECT TOP 1 'Tar' AS flag,6 AS OrderNo
                                                    ,ROUND(SUM(Gate1_Target),0) AS [Prod1]	
	                                                ,ROUND(SUM(Gate2_Target),0) AS [Prod2]
	                                                ,ROUND(SUM(Gate3_Target),0) AS [Prod3]
	                                                ,ROUND(SUM(Gate4_Target),0) AS [Prod4]
	                                                ,ROUND(SUM(Gate5_Target),0) AS [Prod5]
	                                                ,ROUND(SUM(Gate6_Target),0) AS [Prod6]		
	                                                ,ROUND(SUM(Cement1_Target),0) AS [Cement1]
	                                                ,ROUND(SUM(Cement2_Target),0) AS [Cement2]
	                                                ,ROUND(SUM(Cement3_Target),0) AS [Cement3]
                                                    ,ROUND(SUM(Cement4_Target),0) AS [Cement4]
	                                                ,ROUND(SUM(Filler1_Target),0) AS [Fill]
	                                                ,ROUND(SUM(Water1_Target),0) AS [Water1]
	                                                ,ROUND(SUM(Water2_Target),0) AS [Water2]
	                                                ,ROUND(SUM(Adm1_Target1)," + round_Digits + @") AS [Admixture1]
	                                                ,ROUND(SUM(Adm1_Target2)," + round_Digits + @") AS [Admixture2]
	                                                ,ROUND(SUM(Adm2_Target1)," + round_Digits + @") AS [Admixture3]
	                                                ,ROUND(SUM(Adm2_Target2)," + round_Digits + @") AS [Admixture4]
	                                                ,ROUND(SUM(Silica_Target),0) AS [Add1]
	                                                ,ROUND(SUM(Slurry_Target),0) AS [Add2]
                                                FROM BATCH_TRANSACTION where batch_no =" + BatchNo;
                        #endregion
                    }
                    else
                    {
                        #region Version3 Original/Old....

                        QueryResult = @"SELECT distinct TOP 1 '' AS flag ,1 AS OrderNo,Switch(Gate1Name = '', 'AGG1' ,Gate1Name <> '',Gate1Name )AS Prod1
	                                                ,Switch(Gate2Name = '', 'AGG2' ,Gate2Name <> '',Gate2Name) AS Prod2
	                                                ,Switch(Gate3Name = '', 'AGG3' ,Gate3Name <> '',Gate3Name) AS Prod3
	                                                ,Switch(Gate4Name = '', 'AGG4' ,Gate4Name <> '',Gate4Name) AS Prod4
	                                                ,Switch(Gate5Name = '', 'AGG5' ,Gate5Name <> '',Gate5Name) AS Prod5
	                                                ,Switch(Gate6Name = '', 'AGG6' ,Gate6Name <> '',Gate6Name) AS Prod6
	                                                ,Switch(Cem1Name = '', 'CEM1' ,Cem1Name <> '',Cem1Name) AS Cement1
	                                                ,Switch(Cem2Name = '', 'CEM2' ,Cem2Name <> '',Cem2Name) AS Cement2
	                                                ,Switch(Cem3Name = '', 'CEM3' ,Cem3Name <> '',Cem3Name) AS Cement3
	                                                ,Switch(FillName = '', 'FILLER' ,FillName<> '',FillName) AS Fill
	                                                ,Switch(Wtr1Name = '', 'WATER1' ,Wtr1Name<> '',Wtr1Name) AS Water1
	                                                ,Switch(Wtr2Name = '', 'WATER2' ,Wtr2Name<> '',Wtr2Name) AS Water2
	                                                ,Switch(Admix1Name = '', 'Admix1.1' ,Admix1Name<> '',Admix1Name) AS Admixture1
	                                                ,Switch(Admix12Name = '', 'Admix1.2' ,Admix12Name<> '',Admix12Name) AS Admixture2
	                                                ,Switch(Admix2Name = '', 'Admix2.1' ,Admix2Name<> '',Admix2Name) AS Admixture3
	                                                ,Switch(Admix22Name = '', 'Admix2.2' ,Admix22Name<> '',Admix22Name) AS Admixture4
	                                                ,Switch(SilicaName = '', 'ADD1' ,SilicaName<> '',SilicaName) AS Add1
	                                                ,Switch(SlurryName = '', 'ADD2' ,SlurryName<> '',SlurryName) AS Add2
                                                 FROM  NameSetUp "
                                                         + @" union
                                                  SELECT TOP 1
	                                                'Recipe' AS FLAG
	                                                ,2 AS OrderNo
	                                            ,ROUND(SUM(VAL(IIF(ISNULL(BT.Gate1_Target),0,CDBL(VAL(BT.Gate1_Target))))) / IIF( IIF(ISNULL(BDT.BATCH_SIZE),0,CDBL(VAL(BDT.BATCH_SIZE))) > 1 ,COUNT(BT.BATCH_NO) ,IIF(ISNULL(BDT.PRODUCTION_QTY),1,CDBL(VAL(BDT.PRODUCTION_QTY)))),0) AS [Prod1]
	                                            ,ROUND(SUM(VAL(IIF(ISNULL(BT.Gate2_Target),0,CDBL(VAL(BT.Gate2_Target))))) / IIF( IIF(ISNULL(BDT.BATCH_SIZE),0,CDBL(VAL(BDT.BATCH_SIZE))) > 1 ,COUNT(BT.BATCH_NO) ,IIF(ISNULL(BDT.PRODUCTION_QTY),1,CDBL(VAL(BDT.PRODUCTION_QTY)))),0) AS [Prod2]
	                                            ,ROUND(SUM(VAL(IIF(ISNULL(BT.Gate3_Target),0,CDBL(VAL(BT.Gate3_Target))))) / IIF( IIF(ISNULL(BDT.BATCH_SIZE),0,CDBL(VAL(BDT.BATCH_SIZE))) > 1 ,COUNT(BT.BATCH_NO) ,IIF(ISNULL(BDT.PRODUCTION_QTY),1,CDBL(VAL(BDT.PRODUCTION_QTY)))),0) AS [Prod3]
	                                            ,ROUND(SUM(VAL(IIF(ISNULL(BT.Gate4_Target),0,CDBL(VAL(BT.Gate4_Target))))) / IIF( IIF(ISNULL(BDT.BATCH_SIZE),0,CDBL(VAL(BDT.BATCH_SIZE))) > 1 ,COUNT(BT.BATCH_NO) ,IIF(ISNULL(BDT.PRODUCTION_QTY),1,CDBL(VAL(BDT.PRODUCTION_QTY)))),0) AS [Prod4]
	                                            ,ROUND(SUM(VAL(IIF(ISNULL(BT.Gate5_Target),0,CDBL(VAL(BT.Gate5_Target))))) / IIF( IIF(ISNULL(BDT.BATCH_SIZE),0,CDBL(VAL(BDT.BATCH_SIZE))) > 1 ,COUNT(BT.BATCH_NO) ,IIF(ISNULL(BDT.PRODUCTION_QTY),1,CDBL(VAL(BDT.PRODUCTION_QTY)))),0) AS [Prod5]
	                                            ,ROUND(SUM(VAL(IIF(ISNULL(BT.Gate6_Target),0,CDBL(VAL(BT.Gate6_Target))))) / IIF( IIF(ISNULL(BDT.BATCH_SIZE),0,CDBL(VAL(BDT.BATCH_SIZE))) > 1 ,COUNT(BT.BATCH_NO) ,IIF(ISNULL(BDT.PRODUCTION_QTY),1,CDBL(VAL(BDT.PRODUCTION_QTY)))),0) AS [Prod6]	
	                                            ,ROUND(SUM(VAL(IIF(ISNULL(BT.Cement1_Target),0,CDBL(VAL(BT.Cement1_Target))))) / IIF( IIF(ISNULL(BDT.BATCH_SIZE),0,CDBL(VAL(BDT.BATCH_SIZE))) > 1 ,COUNT(BT.BATCH_NO) ,IIF(ISNULL(BDT.PRODUCTION_QTY),1,CDBL(VAL(BDT.PRODUCTION_QTY)))),0) AS [Cement1]
	                                            ,ROUND(SUM(VAL(IIF(ISNULL(BT.Cement2_Target),0,CDBL(VAL(BT.Cement2_Target))))) / IIF( IIF(ISNULL(BDT.BATCH_SIZE),0,CDBL(VAL(BDT.BATCH_SIZE))) > 1 ,COUNT(BT.BATCH_NO) ,IIF(ISNULL(BDT.PRODUCTION_QTY),1,CDBL(VAL(BDT.PRODUCTION_QTY)))),0) AS [Cement2]
	                                            ,ROUND(SUM(VAL(IIF(ISNULL(BT.Cement3_Target),0,CDBL(VAL(BT.Cement3_Target))))) / IIF( IIF(ISNULL(BDT.BATCH_SIZE),0,CDBL(VAL(BDT.BATCH_SIZE))) > 1 ,COUNT(BT.BATCH_NO) ,IIF(ISNULL(BDT.PRODUCTION_QTY),1,CDBL(VAL(BDT.PRODUCTION_QTY)))),0) AS [Cement3]	
	                                            ,ROUND(SUM(VAL(IIF(ISNULL(BT.Filler1_Target),0,CDBL(VAL(BT.Filler1_Target))))) / IIF( IIF(ISNULL(BDT.BATCH_SIZE),0,CDBL(VAL(BDT.BATCH_SIZE))) > 1 ,COUNT(BT.BATCH_NO) ,IIF(ISNULL(BDT.PRODUCTION_QTY),1,CDBL(VAL(BDT.PRODUCTION_QTY)))),0) AS [Fill]		
	                                            ,ROUND(SUM(VAL(IIF(ISNULL(BT.Water1_Target),0,CDBL(VAL(BT.Water1_Target))))) / IIF( IIF(ISNULL(BDT.BATCH_SIZE),0,CDBL(VAL(BDT.BATCH_SIZE))) > 1 ,COUNT(BT.BATCH_NO) ,IIF(ISNULL(BDT.PRODUCTION_QTY),1,CDBL(VAL(BDT.PRODUCTION_QTY)))),0) AS [Water1]
	                                            ,ROUND(SUM(VAL(IIF(ISNULL(BT.Water2_Target),0,CDBL(VAL(BT.Water2_Target))))) / IIF( IIF(ISNULL(BDT.BATCH_SIZE),0,CDBL(VAL(BDT.BATCH_SIZE))) > 1 ,COUNT(BT.BATCH_NO) ,IIF(ISNULL(BDT.PRODUCTION_QTY),1,CDBL(VAL(BDT.PRODUCTION_QTY)))),0) AS [Water2]	
	                                            ,ROUND(SUM(VAL(IIF(ISNULL(BT.Adm1_Target1),0,CDBL(VAL(BT.Adm1_Target1))))) / IIF( IIF(ISNULL(BDT.BATCH_SIZE),0,CDBL(VAL(BDT.BATCH_SIZE))) > 1 ,COUNT(BT.BATCH_NO) ,IIF(ISNULL(BDT.PRODUCTION_QTY),1,CDBL(VAL(BDT.PRODUCTION_QTY)))),2) AS [Admixture1]	
	                                            ,ROUND(SUM(VAL(IIF(ISNULL(BT.Adm1_Target2),0,CDBL(VAL(BT.Adm1_Target2))))) / IIF( IIF(ISNULL(BDT.BATCH_SIZE),0,CDBL(VAL(BDT.BATCH_SIZE))) > 1 ,COUNT(BT.BATCH_NO) ,IIF(ISNULL(BDT.PRODUCTION_QTY),1,CDBL(VAL(BDT.PRODUCTION_QTY)))),2) AS [Admixture2]
	                                            ,ROUND(SUM(VAL(IIF(ISNULL(BT.Adm2_Target1),0,CDBL(VAL(BT.Adm2_Target1))))) / IIF( IIF(ISNULL(BDT.BATCH_SIZE),0,CDBL(VAL(BDT.BATCH_SIZE))) > 1 ,COUNT(BT.BATCH_NO) ,IIF(ISNULL(BDT.PRODUCTION_QTY),1,CDBL(VAL(BDT.PRODUCTION_QTY)))),2) AS [Admixture3]	
	                                            ,ROUND(SUM(VAL(IIF(ISNULL(BT.Adm2_Target2),0,CDBL(VAL(BT.Adm2_Target2))))) / IIF( IIF(ISNULL(BDT.BATCH_SIZE),0,CDBL(VAL(BDT.BATCH_SIZE))) > 1 ,COUNT(BT.BATCH_NO) ,IIF(ISNULL(BDT.PRODUCTION_QTY),1,CDBL(VAL(BDT.PRODUCTION_QTY)))),2) AS [Admixture4]	
                                                ,ROUND(SUM(VAL(IIF(ISNULL(BT.Silica_Target),0,CDBL(VAL(BT.Silica_Target))))) / IIF( IIF(ISNULL(BDT.BATCH_SIZE),0,CDBL(VAL(BDT.BATCH_SIZE))) > 1 ,COUNT(BT.BATCH_NO) ,IIF(ISNULL(BDT.PRODUCTION_QTY),1,CDBL(VAL(BDT.PRODUCTION_QTY)))),0) AS [Add1]	
												,ROUND(SUM(VAL(IIF(ISNULL(BT.Slurry_Target),0,CDBL(VAL(BT.Slurry_Target))))) / IIF( IIF(ISNULL(BDT.BATCH_SIZE),0,CDBL(VAL(BDT.BATCH_SIZE))) > 1 ,COUNT(BT.BATCH_NO) ,IIF(ISNULL(BDT.PRODUCTION_QTY),1,CDBL(VAL(BDT.PRODUCTION_QTY)))),0) AS [Add2]
                                            FROM BATCH_TRANSACTION AS BT
                                            ,batch_dat_trans AS BDT
                                            WHERE BT.BATCH_NO = BDT.BATCH_NO 
                                        AND BT.BATCH_NO =" + BatchNo
                                                        + @" GROUP BY BDT.BATCH_SIZE
	                                                    ,BDT.PRODUCTION_QTY union  
                                               SELECT TOP 1        
	                                            'Target' AS flag 
	                                            ,3 AS OrderNo ";

                        string strTragetQuery = string.Empty;
                        string strQuery = "SELECT TOP 1 * FROM BATCH_DAT_TRANS WHERE BATCH_NO =  -2";
                        DataTable dtColumns = DbHelper.FillDataTable(strQuery, strConnection);
                        if (dtColumns.Columns.Contains("Gate1_Target"))
                        {
                            strTragetQuery = @" ,ROUND(VAL(IIF(ISNULL(BT.Gate1_Target),0,CDBL(VAL(BT.Gate1_Target)))),0) AS [Prod1]
	                                            ,ROUND(VAL(IIF(ISNULL(BT.Gate2_Target),0,CDBL(VAL(BT.Gate2_Target)))),0) AS [Prod2]
		                                        ,ROUND(VAL(IIF(ISNULL(BT.Gate3_Target),0,CDBL(VAL(BT.Gate3_Target)))),0) AS [Prod3]
	                                            ,ROUND(VAL(IIF(ISNULL(BT.Gate4_Target),0,CDBL(VAL(BT.Gate4_Target)))),0) AS [Prod4]
	                                            ,ROUND(VAL(IIF(ISNULL(BT.Gate5_Target),0,CDBL(VAL(BT.Gate5_Target)))),0) AS [Prod5]
	                                            ,ROUND(VAL(IIF(ISNULL(BT.Gate6_Target),0,CDBL(VAL(BT.Gate6_Target)))),0) AS [Prod6]	
		                                        ,ROUND(VAL(IIF(ISNULL(BT.Cement1_Target),0,CDBL(VAL(BT.Cement1_Target)))),0) AS [Cement1]
	                                            ,ROUND(VAL(IIF(ISNULL(BT.Cement2_Target),0,CDBL(VAL(BT.Cement2_Target)))),0) AS [Cement2]
	                                            ,ROUND(VAL(IIF(ISNULL(BT.Cement3_Target),0,CDBL(VAL(BT.Cement3_Target)))),0) AS [Cement3]	
		                                        ,ROUND(VAL(IIF(ISNULL(BT.Filler1_Target),0,CDBL(VAL(BT.Filler1_Target)))),0) AS [Fill]		
	                                            ,ROUND(VAL(IIF(ISNULL(BT.Water1_Target),0,CDBL(VAL(BT.Water1_Target)))),0) AS [Water1]
	                                            ,ROUND(VAL(IIF(ISNULL(BT.Water2_Target),0,CDBL(VAL(BT.Water2_Target)))),0) AS [Water2]	
		                                        ,ROUND(VAL(IIF(ISNULL(BT.Adm1_Target1),0,CDBL(VAL(BT.Adm1_Target1))))," + round_Digits + @") AS [Admixture1]	
		                                        ,ROUND(VAL(IIF(ISNULL(BT.Adm1_Target2),0,CDBL(VAL(BT.Adm1_Target2))))," + round_Digits + @") AS [Admixture2]
	                                            ,ROUND(VAL(IIF(ISNULL(BT.Adm2_Target1),0,CDBL(VAL(BT.Adm2_Target1))))," + round_Digits + @") AS [Admixture3]	
		                                        ,ROUND(VAL(IIF(ISNULL(BT.Adm2_Target2),0,CDBL(VAL(BT.Adm2_Target2))))," + round_Digits + @") AS [Admixture4]	
	                                            ,ROUND(VAL(IIF(ISNULL(BT.Silica_Target),0,CDBL(VAL(BT.Silica_Target)))),0) AS [Add1]	
	                                            ,ROUND(VAL(IIF(ISNULL(BT.Slurry_Target),0,CDBL(VAL(BT.Slurry_Target)))),0) AS [Add2]";
                        }
                        else
                        {
                            strTragetQuery = @" ,ROUND(VAL(IIF(ISNULL(BT.Gate1_Target),0,CDBL(VAL(BT.Gate1_Target)))),0) AS [Prod1]
	                                            ,ROUND(VAL(IIF(ISNULL(BT.Gate2_Target),0,CDBL(VAL(BT.Gate2_Target)))),0) AS [Prod2]
		                                        ,ROUND(VAL(IIF(ISNULL(BT.Gate3_Target),0,CDBL(VAL(BT.Gate3_Target)))),0) AS [Prod3]
	                                            ,ROUND(VAL(IIF(ISNULL(BT.Gate4_Target),0,CDBL(VAL(BT.Gate4_Target)))),0) AS [Prod4]
	                                            ,ROUND(VAL(IIF(ISNULL(BT.Gate5_Target),0,CDBL(VAL(BT.Gate5_Target)))),0) AS [Prod5]
	                                            ,ROUND(VAL(IIF(ISNULL(BT.Gate6_Target),0,CDBL(VAL(BT.Gate6_Target)))),0) AS [Prod6]	
		                                        ,ROUND(VAL(IIF(ISNULL(BT.Cement1_Target),0,CDBL(VAL(BT.Cement1_Target)))),0) AS [Cement1]
	                                            ,ROUND(VAL(IIF(ISNULL(BT.Cement2_Target),0,CDBL(VAL(BT.Cement2_Target)))),0) AS [Cement2]
	                                            ,ROUND(VAL(IIF(ISNULL(BT.Cement3_Target),0,CDBL(VAL(BT.Cement3_Target)))),0) AS [Cement3]	
		                                        ,ROUND(VAL(IIF(ISNULL(BT.Filler1_Target),0,CDBL(VAL(BT.Filler1_Target)))),0) AS [Fill]		
	                                            ,ROUND(VAL(IIF(ISNULL(BT.Water1_Target),0,CDBL(VAL(BT.Water1_Target)))),0) AS [Water1]
	                                            ,ROUND(VAL(IIF(ISNULL(BT.Water2_Target),0,CDBL(VAL(BT.Water2_Target)))),0) AS [Water2]	
		                                        ,ROUND(VAL(IIF(ISNULL(BT.Adm1_Target1),0,CDBL(VAL(BT.Adm1_Target1))))," + round_Digits + @") AS [Admixture1]	
		                                        ,ROUND(VAL(IIF(ISNULL(BT.Adm1_Target2),0,CDBL(VAL(BT.Adm1_Target2))))," + round_Digits + @") AS [Admixture2]
	                                            ,ROUND(VAL(IIF(ISNULL(BT.Adm2_Target1),0,CDBL(VAL(BT.Adm2_Target1))))," + round_Digits + @") AS [Admixture3]	
		                                        ,ROUND(VAL(IIF(ISNULL(BT.Adm2_Target2),0,CDBL(VAL(BT.Adm2_Target2))))," + round_Digits + @") AS [Admixture4]	
	                                            ,ROUND(VAL(IIF(ISNULL(BT.Silica_Target),0,CDBL(VAL(BT.Silica_Target)))),0) AS [Add1]	
	                                            ,ROUND(VAL(IIF(ISNULL(BT.Slurry_Target),0,CDBL(VAL(BT.Slurry_Target)))),0) AS [Add2]";
                        }
                        QueryResult = QueryResult + " " + strTragetQuery + @" FROM BATCH_TRANSACTION AS BT
                                            ,BATCH_DAT_TRANS AS BDT
                                            WHERE BDT.BATCH_NO = BT.BATCH_NO 
                                            AND BT.BATCH_NO  =" + BatchNo
                                + @" union all 
                                                SELECT format(Batch_Time ,'hh:mm:ss') AS flag,4 AS OrderNo
                                                        ,ROUND(Gate1_Actual,0) AS [Prod1]
                                                        ,ROUND(Gate2_Actual,0) AS [Prod2]
                                                        ,ROUND(Gate3_Actual,0) AS [Prod3]
                                                        ,ROUND(Gate4_Actual,0) AS [Prod4]
                                                        ,ROUND(Gate5_Actual,0) AS [Prod5]
                                                        ,ROUND(Gate6_Actual,0) AS [Prod6]
                                                        ,ROUND(Cement1_Actual,0) AS [Cement1]
                                                        ,ROUND(Cement2_Actual,0) AS [Cement2]
                                                        ,ROUND(Cement3_Actual,0) AS [Cement3]
                                                        ,ROUND(Filler1_Actual,0) AS [Fill]
                                                        ,ROUND(Water1_Actual,0) AS [Water1]
                                                        ,ROUND(Water2_Actual,0) AS [Water2]
                                                        ,ROUND(Adm1_Actual1," + round_Digits + @") AS [Admixture1]
                                                        ,ROUND(Adm1_Actual2," + round_Digits + @") AS [Admixture2]
                                                        ,ROUND(Adm2_Actual1," + round_Digits + @") AS [Admixture3]
                                                        ,ROUND(Adm2_Actual2," + round_Digits + @") AS [Admixture4]
                                                        ,ROUND(Silica_Actual,0) AS [Add1]
                                                        ,ROUND(Slurry_Actual,0) AS [Add2] 
                                                FROM BATCH_TRANSACTION where batch_no =" + BatchNo
                                + @" union all 
                                                SELECT TOP 1  'Act' AS flag,5 AS OrderNo
                                                    ,ROUND(sum(Gate1_Actual),0) AS [Prod1]
	                                                ,ROUND(sum(Gate2_Actual),0) AS [Prod2]
	                                                ,ROUND(sum(Gate3_Actual),0) AS [Prod3]
	                                                ,ROUND(sum(Gate4_Actual),0) AS [Prod4]
	                                                ,ROUND(sum(Gate5_Actual),0) AS [Prod5]
	                                                ,ROUND(sum(Gate6_Actual),0) AS [Prod6]
	                                                ,ROUND(sum(Cement1_Actual),0) AS [Cement1]
	                                                ,ROUND(sum(Cement2_Actual),0) AS [Cement2]
	                                                ,ROUND(sum(Cement3_Actual),0) AS [Cement3]
	                                                ,ROUND(sum(Filler1_Actual),0) AS [Fill]
	                                                ,ROUND(sum(Water1_Actual),0) AS [Water1]
	                                                ,ROUND(sum(Water2_Actual),0) AS [Water2]
	                                                ,ROUND(sum(Adm1_Actual1)," + round_Digits + @") AS [Admixture1]
	                                                ,ROUND(sum(Adm1_Actual2)," + round_Digits + @") AS [Admixture2]
	                                                ,ROUND(sum(Adm2_Actual1)," + round_Digits + @") AS [Admixture3]
	                                                ,ROUND(sum(Adm2_Actual2)," + round_Digits + @") AS [Admixture4]
	                                                ,ROUND(sum(Silica_Actual),0) AS [Add1]
	                                                ,ROUND(sum(Slurry_Actual),0) AS [Add2] 
                                                FROM BATCH_TRANSACTION where batch_no =" + BatchNo
                                + @" union all 
                                                SELECT TOP 1 'Tar' AS flag,6 AS OrderNo
                                                    ,ROUND(SUM(Gate1_Target),0) AS [Prod1]	
	                                                ,ROUND(SUM(Gate2_Target),0) AS [Prod2]
	                                                ,ROUND(SUM(Gate3_Target),0) AS [Prod3]
	                                                ,ROUND(SUM(Gate4_Target),0) AS [Prod4]
	                                                ,ROUND(SUM(Gate5_Target),0) AS [Prod5]
	                                                ,ROUND(SUM(Gate6_Target),0) AS [Prod6]		
	                                                ,ROUND(SUM(Cement1_Target),0) AS [Cement1]
	                                                ,ROUND(SUM(Cement2_Target),0) AS [Cement2]
	                                                ,ROUND(SUM(Cement3_Target),0) AS [Cement3]
	                                                ,ROUND(SUM(Filler1_Target),0) AS [Fill]
	                                                ,ROUND(SUM(Water1_Target),0) AS [Water1]
	                                                ,ROUND(SUM(Water2_Target),0) AS [Water2]
	                                                ,ROUND(SUM(Adm1_Target1)," + round_Digits + @") AS [Admixture1]
	                                                ,ROUND(SUM(Adm1_Target2)," + round_Digits + @") AS [Admixture2]
	                                                ,ROUND(SUM(Adm2_Target1)," + round_Digits + @") AS [Admixture3]
	                                                ,ROUND(SUM(Adm2_Target2)," + round_Digits + @") AS [Admixture4]
	                                                ,ROUND(SUM(Silica_Target),0) AS [Add1]
	                                                ,ROUND(SUM(Slurry_Target),0) AS [Add2]
                                                FROM BATCH_TRANSACTION where batch_no =" + BatchNo;
                        #endregion
                    }

                    line_No = 1;
                }
                if (Program.PlantAppVersion == "3.1")
                {
                    #region Version3.1 for bitcon
                    QueryResult = @" SELECT distinct TOP 1 '' AS flag ,1 AS OrderNo,Switch(GATE1 = '', 'AGG1' ,GATE1 <> '',GATE1 )AS Prod1
		,Switch(GATE2 = '', 'AGG2' ,GATE2 <> '',GATE2) AS Prod2
		,Switch(GATE3 = '', 'AGG3' ,GATE3 <> '',GATE3) AS Prod3
		,Switch(GATE4 = '', 'AGG4' ,GATE4 <> '',GATE4) AS Prod4
		,Switch(GATE5 = '', 'AGG5' ,GATE5 <> '',GATE5) AS Prod5
		,Switch(GATE6 = '', 'AGG6' ,GATE6 <> '',GATE6) AS Prod6
		,Switch(CEMENT1 = '', 'CEM1' ,CEMENT1 <> '',CEMENT1) AS Cement1
		,Switch(CEMENT2 = '', 'CEM2' ,CEMENT2 <> '',CEMENT2) AS Cement2
		,Switch(CEMENT3 = '', 'CEM3' ,CEMENT3 <> '',CEMENT3) AS Cement3
		,Switch(CEMENT4 = '', 'CEM4' ,CEMENT4 <> '',CEMENT4) AS Cement4
		,Switch(FILLER = '', 'FILLER' ,FILLER<> '',FILLER) AS Fill
		,Switch(WATER1 = '', 'WATER1' ,WATER1<> '',WATER1) AS Water1
		,Switch(WATER2 = '', 'WATER2' ,WATER2<> '',WATER2) AS Water2
		,Switch(ADDMIX1 = '', 'Admix1.1' ,ADDMIX1<> '',ADDMIX1) AS Admixture1
		,Switch(ADDMIX2 = '', 'Admix1.2' ,ADDMIX2<> '',ADDMIX2) AS Admixture2
		,Switch(ADDMIX3 = '', 'Admix2.1' ,ADDMIX3<> '',ADDMIX3) AS Admixture3
		,Switch(ADDMIX4 = '', 'Admix2.2' ,ADDMIX4<> '',ADDMIX4) AS Admixture4
		,Switch(Silica = '', 'Silica' ,SILICA<> '',SILICA) AS Silica
		,Switch(SLURRY = '', 'Slurry' ,SLURRY<> '',SLURRY) AS Slurry
		,Switch(PIGMENT = '', 'ADD2' ,PIGMENT<> '',PIGMENT) AS Pigment
	 FROM  NameSetUp   union all
SELECT TOP 1
				'Recipe' AS FLAG
				,2 AS OrderNo
		   ,ROUND(VAL(IIF(ISNULL(BDT.Gate1_Target),0,CDBL(VAL(BDT.Gate1_Target)))),0) AS [Prod1]
			,ROUND(VAL(IIF(ISNULL(BDT.Gate2_Target),0,CDBL(VAL(BDT.Gate2_Target)))),0) AS [Prod2]
			,ROUND(VAL(IIF(ISNULL(BDT.Gate3_Target),0,CDBL(VAL(BDT.Gate3_Target)))),0) AS [Prod3]
			,ROUND(VAL(IIF(ISNULL(BDT.Gate4_Target),0,CDBL(VAL(BDT.Gate4_Target)))),0) AS [Prod4]
			,ROUND(VAL(IIF(ISNULL(BDT.Gate5_Target),0,CDBL(VAL(BDT.Gate5_Target)))),0) AS [Prod5]
			,ROUND(VAL(IIF(ISNULL(BDT.Gate6_Target),0,CDBL(VAL(BDT.Gate6_Target)))),0) AS [Prod6]
			,ROUND(VAL(IIF(ISNULL(BDT.Cement1_Target),0,CDBL(VAL(BDT.Cement1_Target)))),0) AS [Cement1]
			,ROUND(VAL(IIF(ISNULL(BDT.Cement2_Target),0,CDBL(VAL(BDT.Cement2_Target)))),0) AS [Cement2]
			,ROUND(VAL(IIF(ISNULL(BDT.Cement3_Target),0,CDBL(VAL(BDT.Cement3_Target)))),0) AS [Cement3]	
			,ROUND(VAL(IIF(ISNULL(BDT.Cement4_Target),0,CDBL(VAL(BDT.Cement4_Target)))),0) AS [Cement4]	
			,ROUND(VAL(IIF(ISNULL(BDT.Filler1_Target),0,CDBL(VAL(BDT.Filler1_Target)))),0) AS [Fill]	
			,ROUND(VAL(IIF(ISNULL(BDT.Water1_Target),0,CDBL(VAL(BDT.Water1_Target)))),0) AS [Water1]
			,ROUND(VAL(IIF(ISNULL(BDT.Water2_Target),0,CDBL(VAL(BDT.Water2_Target)))),0) AS [Water2]
			,ROUND(VAL(IIF(ISNULL(BDT.Adm1_Target1),0,CDBL(VAL(BDT.Adm1_Target1)))),4) AS [Admixture1]
			,ROUND(VAL(IIF(ISNULL(BDT.Adm1_Target2),0,CDBL(VAL(BDT.Adm1_Target2)))),4) AS [Admixture2]
			,ROUND(VAL(IIF(ISNULL(BDT.Adm2_Target1),0,CDBL(VAL(BDT.Adm2_Target1)))),4) AS [Admixture3]
			,ROUND(VAL(IIF(ISNULL(BDT.Adm2_Target2),0,CDBL(VAL(BDT.Adm2_Target2)))),4) AS [Admixture4]
			,ROUND(VAL(IIF(ISNULL(BDT.Silica_Target),0,CDBL(VAL(BDT.Silica_Target)))),0) AS [Silica]
			,ROUND(VAL(IIF(ISNULL(BDT.Slurry_Target),0,CDBL(VAL(BDT.Slurry_Target)))),0) AS [Slurry]
			,ROUND(VAL(IIF(ISNULL(BDT.Pigment_Target),0,CDBL(VAL(BDT.Pigment_Target)))),0) AS [Pigment]
	FROM DOCKET_HEADER AS BT
	,DOCKET_DETAILS AS BDT
	WHERE  BDT.BATCH_NO  = BT.BATCH_NO 
	AND BT.BATCH_NO = " + BatchNo
                                + @" union all
SELECT TOP 1
				'Target' AS FLAG
				,3 AS OrderNo
		   ,ROUND(VAL(IIF(ISNULL(BDT.Gate1_Target),0,CDBL(VAL(BDT.Gate1_Target)))),0) AS [Prod1]
			,ROUND(VAL(IIF(ISNULL(BDT.Gate2_Target),0,CDBL(VAL(BDT.Gate2_Target)))),0) AS [Prod2]
			,ROUND(VAL(IIF(ISNULL(BDT.Gate3_Target),0,CDBL(VAL(BDT.Gate3_Target)))),0) AS [Prod3]
			,ROUND(VAL(IIF(ISNULL(BDT.Gate4_Target),0,CDBL(VAL(BDT.Gate4_Target)))),0) AS [Prod4]
			,ROUND(VAL(IIF(ISNULL(BDT.Gate5_Target),0,CDBL(VAL(BDT.Gate5_Target)))),0) AS [Prod5]
			,ROUND(VAL(IIF(ISNULL(BDT.Gate6_Target),0,CDBL(VAL(BDT.Gate6_Target)))),0) AS [Prod6]
			,ROUND(VAL(IIF(ISNULL(BDT.Cement1_Target),0,CDBL(VAL(BDT.Cement1_Target)))),0) AS [Cement1]
			,ROUND(VAL(IIF(ISNULL(BDT.Cement2_Target),0,CDBL(VAL(BDT.Cement2_Target)))),0) AS [Cement2]
			,ROUND(VAL(IIF(ISNULL(BDT.Cement3_Target),0,CDBL(VAL(BDT.Cement3_Target)))),0) AS [Cement3]	
			,ROUND(VAL(IIF(ISNULL(BDT.Cement4_Target),0,CDBL(VAL(BDT.Cement4_Target)))),0) AS [Cement4]	
			,ROUND(VAL(IIF(ISNULL(BDT.Filler1_Target),0,CDBL(VAL(BDT.Filler1_Target)))),0) AS [Fill]	
			,ROUND(VAL(IIF(ISNULL(BDT.Water1_Target),0,CDBL(VAL(BDT.Water1_Target)))),0) AS [Water1]
			,ROUND(VAL(IIF(ISNULL(BDT.Water2_Target),0,CDBL(VAL(BDT.Water2_Target)))),0) AS [Water2]
			,ROUND(VAL(IIF(ISNULL(BDT.Adm1_Target1),0,CDBL(VAL(BDT.Adm1_Target1)))),4) AS [Admixture1]
			,ROUND(VAL(IIF(ISNULL(BDT.Adm1_Target2),0,CDBL(VAL(BDT.Adm1_Target2)))),4) AS [Admixture2]
			,ROUND(VAL(IIF(ISNULL(BDT.Adm2_Target1),0,CDBL(VAL(BDT.Adm2_Target1)))),4) AS [Admixture3]
			,ROUND(VAL(IIF(ISNULL(BDT.Adm2_Target2),0,CDBL(VAL(BDT.Adm2_Target2)))),4) AS [Admixture4]
			,ROUND(VAL(IIF(ISNULL(BDT.Silica_Target),0,CDBL(VAL(BDT.Silica_Target)))),0) AS [Silica]
			,ROUND(VAL(IIF(ISNULL(BDT.Slurry_Target),0,CDBL(VAL(BDT.Slurry_Target)))),0) AS [Slurry]
			,ROUND(VAL(IIF(ISNULL(BDT.Pigment_Target),0,CDBL(VAL(BDT.Pigment_Target)))),0) AS [Pigment]
	FROM DOCKET_HEADER AS BT
	,DOCKET_DETAILS AS BDT
	WHERE  BDT.BATCH_NO  = BT.BATCH_NO 
	AND BT.BATCH_NO = " + BatchNo
                                + @" union all
SELECT format(BDT.Batch_Time ,'hh:mm:ss')  AS FLAG
				,4 AS OrderNo
		   ,ROUND(VAL(IIF(ISNULL(BDT.Gate1_Actual),0,CDBL(VAL(BDT.Gate1_Actual)))),0) AS [Prod1]
			,ROUND(VAL(IIF(ISNULL(BDT.Gate2_Actual),0,CDBL(VAL(BDT.Gate2_Actual)))),0) AS [Prod2]
			,ROUND(VAL(IIF(ISNULL(BDT.Gate3_Actual),0,CDBL(VAL(BDT.Gate3_Actual)))),0) AS [Prod3]
			,ROUND(VAL(IIF(ISNULL(BDT.Gate4_Actual),0,CDBL(VAL(BDT.Gate4_Actual)))),0) AS [Prod4]
			,ROUND(VAL(IIF(ISNULL(BDT.Gate5_Actual),0,CDBL(VAL(BDT.Gate5_Actual)))),0) AS [Prod5]
			,ROUND(VAL(IIF(ISNULL(BDT.Gate6_Actual),0,CDBL(VAL(BDT.Gate6_Actual)))),0) AS [Prod6]
			,ROUND(VAL(IIF(ISNULL(BDT.Cement1_Actual),0,CDBL(VAL(BDT.Cement1_Actual)))),0) AS [Cement1]
			,ROUND(VAL(IIF(ISNULL(BDT.Cement2_Actual),0,CDBL(VAL(BDT.Cement2_Actual)))),0) AS [Cement2]
			,ROUND(VAL(IIF(ISNULL(BDT.Cement3_Actual),0,CDBL(VAL(BDT.Cement3_Actual)))),0) AS [Cement3]	
			,ROUND(VAL(IIF(ISNULL(BDT.Cement4_Actual),0,CDBL(VAL(BDT.Cement4_Actual)))),0) AS [Cement4]	
			,ROUND(VAL(IIF(ISNULL(BDT.Filler1_Actual),0,CDBL(VAL(BDT.Filler1_Actual)))),0) AS [Fill]	
			,ROUND(VAL(IIF(ISNULL(BDT.Water1_Actual),0,CDBL(VAL(BDT.Water1_Actual)))),0) AS [Water1]
			,ROUND(VAL(IIF(ISNULL(BDT.Water2_Actual),0,CDBL(VAL(BDT.Water2_Actual)))),0) AS [Water2]
			,ROUND(VAL(IIF(ISNULL(BDT.Adm1_Actual1),0,CDBL(VAL(BDT.Adm1_Actual1)))),4) AS [Admixture1]
			,ROUND(VAL(IIF(ISNULL(BDT.Adm1_Actual2),0,CDBL(VAL(BDT.Adm1_Actual2)))),4) AS [Admixture2]
			,ROUND(VAL(IIF(ISNULL(BDT.Adm2_Actual1),0,CDBL(VAL(BDT.Adm2_Actual1)))),4) AS [Admixture3]
			,ROUND(VAL(IIF(ISNULL(BDT.Adm2_Actual2),0,CDBL(VAL(BDT.Adm2_Actual2)))),4) AS [Admixture4]
			,ROUND(VAL(IIF(ISNULL(BDT.Silica_Actual),0,CDBL(VAL(BDT.Silica_Actual)))),0) AS [Silica]
			,ROUND(VAL(IIF(ISNULL(BDT.Slurry_Actual),0,CDBL(VAL(BDT.Slurry_Actual)))),0) AS [Slurry]
			,ROUND(VAL(IIF(ISNULL(BDT.Pigment_Actual),0,CDBL(VAL(BDT.Pigment_Actual)))),0) AS [Pigment]
	FROM DOCKET_DETAILS AS BDT
	WHERE BDT.BATCH_NO = " + BatchNo
                                + @" union all
SELECT TOP 1 'Act'  AS FLAG
				,5 AS OrderNo
		   ,ROUND(SUM(VAL(IIF(ISNULL(BDT.Gate1_Actual),0,CDBL(VAL(BDT.Gate1_Actual))))),0) AS [Prod1]
			,ROUND(SUM(VAL(IIF(ISNULL(BDT.Gate2_Actual),0,CDBL(VAL(BDT.Gate2_Actual))))),0) AS [Prod2]
			,ROUND(SUM(VAL(IIF(ISNULL(BDT.Gate3_Actual),0,CDBL(VAL(BDT.Gate3_Actual))))),0) AS [Prod3]
			,ROUND(SUM(VAL(IIF(ISNULL(BDT.Gate4_Actual),0,CDBL(VAL(BDT.Gate4_Actual))))),0) AS [Prod4]
			,ROUND(SUM(VAL(IIF(ISNULL(BDT.Gate5_Actual),0,CDBL(VAL(BDT.Gate5_Actual))))),0) AS [Prod5]
			,ROUND(SUM(VAL(IIF(ISNULL(BDT.Gate6_Actual),0,CDBL(VAL(BDT.Gate6_Actual))))),0) AS [Prod6]
			,ROUND(SUM(VAL(IIF(ISNULL(BDT.Cement1_Actual),0,CDBL(VAL(BDT.Cement1_Actual))))),0) AS [Cement1]
			,ROUND(SUM(VAL(IIF(ISNULL(BDT.Cement2_Actual),0,CDBL(VAL(BDT.Cement2_Actual))))),0) AS [Cement2]
			,ROUND(SUM(VAL(IIF(ISNULL(BDT.Cement3_Actual),0,CDBL(VAL(BDT.Cement3_Actual))))),0) AS [Cement3]	
			,ROUND(SUM(VAL(IIF(ISNULL(BDT.Cement4_Actual),0,CDBL(VAL(BDT.Cement4_Actual))))),0) AS [Cement4]	
			,ROUND(SUM(VAL(IIF(ISNULL(BDT.Filler1_Actual),0,CDBL(VAL(BDT.Filler1_Actual))))),0) AS [Fill]	
			,ROUND(SUM(VAL(IIF(ISNULL(BDT.Water1_Actual),0,CDBL(VAL(BDT.Water1_Actual))))),0) AS [Water1]
			,ROUND(SUM(VAL(IIF(ISNULL(BDT.Water2_Actual),0,CDBL(VAL(BDT.Water2_Actual))))),0) AS [Water2]
			,ROUND(SUM(VAL(IIF(ISNULL(BDT.Adm1_Actual1),0,CDBL(VAL(BDT.Adm1_Actual1))))),4) AS [Admixture1]
			,ROUND(SUM(VAL(IIF(ISNULL(BDT.Adm1_Actual2),0,CDBL(VAL(BDT.Adm1_Actual2))))),4) AS [Admixture2]
			,ROUND(SUM(VAL(IIF(ISNULL(BDT.Adm2_Actual1),0,CDBL(VAL(BDT.Adm2_Actual1))))),4) AS [Admixture3]
			,ROUND(SUM(VAL(IIF(ISNULL(BDT.Adm2_Actual2),0,CDBL(VAL(BDT.Adm2_Actual2))))),4) AS [Admixture4]
			,ROUND(SUM(VAL(IIF(ISNULL(BDT.Silica_Actual),0,CDBL(VAL(BDT.Silica_Actual))))),0) AS [Silica]
			,ROUND(SUM(VAL(IIF(ISNULL(BDT.Slurry_Actual),0,CDBL(VAL(BDT.Slurry_Actual))))),0) AS [Slurry]
			,ROUND(SUM(VAL(IIF(ISNULL(BDT.Pigment_Actual),0,CDBL(VAL(BDT.Pigment_Actual))))),0) AS [Pigment]
	FROM DOCKET_DETAILS AS BDT
	WHERE BDT.BATCH_NO = " + BatchNo
                                + @" union all
SELECT TOP 1 'Tar'  AS FLAG
				,6 AS OrderNo
		    ,ROUND(SUM(VAL(IIF(ISNULL(BDT.Gate1_Target),0,CDBL(VAL(BDT.Gate1_Target))))),0) AS [Prod1]
			,ROUND(SUM(VAL(IIF(ISNULL(BDT.Gate2_Target),0,CDBL(VAL(BDT.Gate2_Target))))),0) AS [Prod2]
			,ROUND(SUM(VAL(IIF(ISNULL(BDT.Gate3_Target),0,CDBL(VAL(BDT.Gate3_Target))))),0) AS [Prod3]
			,ROUND(SUM(VAL(IIF(ISNULL(BDT.Gate4_Target),0,CDBL(VAL(BDT.Gate4_Target))))),0) AS [Prod4]
			,ROUND(SUM(VAL(IIF(ISNULL(BDT.Gate5_Target),0,CDBL(VAL(BDT.Gate5_Target))))),0) AS [Prod5]
			,ROUND(SUM(VAL(IIF(ISNULL(BDT.Gate6_Target),0,CDBL(VAL(BDT.Gate6_Target))))),0) AS [Prod6]
			,ROUND(SUM(VAL(IIF(ISNULL(BDT.Cement1_Target),0,CDBL(VAL(BDT.Cement1_Target))))),0) AS [Cement1]
			,ROUND(SUM(VAL(IIF(ISNULL(BDT.Cement2_Target),0,CDBL(VAL(BDT.Cement2_Target))))),0) AS [Cement2]
			,ROUND(SUM(VAL(IIF(ISNULL(BDT.Cement3_Target),0,CDBL(VAL(BDT.Cement3_Target))))),0) AS [Cement3]	
			,ROUND(SUM(VAL(IIF(ISNULL(BDT.Cement4_Target),0,CDBL(VAL(BDT.Cement4_Target))))),0) AS [Cement4]	
			,ROUND(SUM(VAL(IIF(ISNULL(BDT.Filler1_Target),0,CDBL(VAL(BDT.Filler1_Target))))),0) AS [Fill]	
			,ROUND(SUM(VAL(IIF(ISNULL(BDT.Water1_Target),0,CDBL(VAL(BDT.Water1_Target))))),0) AS [Water1]
			,ROUND(SUM(VAL(IIF(ISNULL(BDT.Water2_Target),0,CDBL(VAL(BDT.Water2_Target))))),0) AS [Water2]
			,ROUND(SUM(VAL(IIF(ISNULL(BDT.Adm1_Target1),0,CDBL(VAL(BDT.Adm1_Target1))))),4) AS [Admixture1]
			,ROUND(SUM(VAL(IIF(ISNULL(BDT.Adm1_Target2),0,CDBL(VAL(BDT.Adm1_Target2))))),4) AS [Admixture2]
			,ROUND(SUM(VAL(IIF(ISNULL(BDT.Adm2_Target1),0,CDBL(VAL(BDT.Adm2_Target1))))),4) AS [Admixture3]
			,ROUND(SUM(VAL(IIF(ISNULL(BDT.Adm2_Target2),0,CDBL(VAL(BDT.Adm2_Target2))))),4) AS [Admixture4]
			,ROUND(SUM(VAL(IIF(ISNULL(BDT.Silica_Target),0,CDBL(VAL(BDT.Silica_Target))))),0) AS [Silica]
			,ROUND(SUM(VAL(IIF(ISNULL(BDT.Slurry_Target),0,CDBL(VAL(BDT.Slurry_Target))))),0) AS [Slurry]
			,ROUND(SUM(VAL(IIF(ISNULL(BDT.Pigment_Target),0,CDBL(VAL(BDT.Pigment_Target))))),0) AS [Pigment]
	FROM DOCKET_DETAILS AS BDT
	WHERE BDT.BATCH_NO = " + BatchNo;
                    #endregion Version3.1 for bitcon
                }
                else if (Program.PlantAppVersion == "1")
                {
                    #region Version1


                    QueryResult = @"SELECT TOP 1
	'' AS flag 
	,1 AS OrderNo
	,Switch(IIF(ISNULL(Name_01),'',Name_01) = '' , 'AGG1' ,IIF(ISNULL(Name_01),'',Name_01) <> '',Name_01 )AS [Prod1]
	,Switch(IIF(ISNULL(Name_02),'',Name_02) = '' , 'AGG2' ,IIF(ISNULL(Name_02),'',Name_02) <> '',Name_02 )AS [Prod2]
	,Switch(IIF(ISNULL(Name_03),'',Name_03) = '' , 'AGG3' ,IIF(ISNULL(Name_03),'',Name_03) <> '',Name_03 )AS [Prod3]
	,Switch(IIF(ISNULL(Name_04),'',Name_04) = '' , 'AGG4' ,IIF(ISNULL(Name_04),'',Name_04) <> '',Name_04 )AS [Prod4]
	,Switch(IIF(ISNULL(Name_05),'',Name_05) = '' , 'CEM1' ,IIF(ISNULL(Name_05),'',Name_05) <> '',Name_05 )AS [Prod5]
	,Switch(IIF(ISNULL(Name_06),'',Name_06) = '' , 'WATER' ,IIF(ISNULL(Name_06),'',Name_06) <> '',Name_06 )AS [Prod6]
	,Switch(IIF(ISNULL(Name_07),'',Name_07) = '' , 'ADD 1' ,IIF(ISNULL(Name_07),'',Name_07) <> '',Name_07 )AS [Prod7]
	,Switch(IIF(ISNULL(Name_08),'',Name_08) = '' , 'ADD 2' ,IIF(ISNULL(Name_08),'',Name_08) <> '',Name_08 )AS [Prod8]
	,Switch(IIF(ISNULL(Name_09),'',Name_09) = '' , 'ADD 3' ,IIF(ISNULL(Name_09),'',Name_09) <> '',Name_09 )AS [Prod9]
FROM VITRAG_BATCH  where batch_no = " + BatchNo
+ @" GROUP BY 
	Name_01
	,Name_02
	,Name_03
	,Name_04
	,Name_05
	,Name_06
	,Name_07
	,Name_08
	,Name_09 	
UNION
SELECT  TOP 1
	'Recipe' AS flag
	,2 AS OrderNo
	,ROUND(IIF(ISNULL(Target_01),0,CDbl(VAL(Target_01))) ,0)  AS [Prod1]
	,ROUND(IIF(ISNULL(Target_02),0,CDbl(VAL(Target_02))) ,0)  AS [Prod2]
	,ROUND(IIF(ISNULL(Target_03),0,CDbl(VAL(Target_03))) ,0)  AS [Prod3]
	,ROUND(IIF(ISNULL(Target_04),0,CDbl(VAL(Target_04))) ,0)  AS [Prod4]
	,ROUND(IIF(ISNULL(Target_05),0,CDbl(VAL(Target_05))) ,0)  AS [Prod5]
	,ROUND(IIF(ISNULL(Target_06),0,CDbl(VAL(Target_06))) ,0)  AS [Prod6]	
	,ROUND(IIF(ISNULL(Target_07),0,CDbl(VAL(Target_07))) ,2)  AS [Prod7]
	,ROUND(IIF(ISNULL(Target_08),0,CDbl(VAL(Target_08))) ,0)  AS [Prod8]
	,ROUND(IIF(ISNULL(Target_09),0,CDbl(VAL(Target_09))) ,0)  AS [Prod9]	
FROM VITRAG_BATCH  WHERE batch_no = " + BatchNo
+ @" UNION 
SELECT  TOP 1
	'Target' AS flag
	,3 AS OrderNo
	,ROUND(IIF(ISNULL(Target_01),0,CDbl(VAL(Target_01))) ,0)  AS [Prod1]
	,ROUND(IIF(ISNULL(Target_02),0,CDbl(VAL(Target_02))) ,0)  AS [Prod2]
	,ROUND(IIF(ISNULL(Target_03),0,CDbl(VAL(Target_03))) ,0)  AS [Prod3]
	,ROUND(IIF(ISNULL(Target_04),0,CDbl(VAL(Target_04))) ,0)  AS [Prod4]
	,ROUND(IIF(ISNULL(Target_05),0,CDbl(VAL(Target_05))) ,0)  AS [Prod5]
	,ROUND(IIF(ISNULL(Target_06),0,CDbl(VAL(Target_06))) ,0)  AS [Prod6]	
	,ROUND(IIF(ISNULL(Target_07),0,CDbl(VAL(Target_07))) ,2)  AS [Prod7]
	,ROUND(IIF(ISNULL(Target_08),0,CDbl(VAL(Target_08))) ,0)  AS [Prod8]
	,ROUND(IIF(ISNULL(Target_09),0,CDbl(VAL(Target_09))) ,0)  AS [Prod9]	
FROM VITRAG_BATCH  WHERE batch_no = " + BatchNo
+ @" UNION ALL
SELECT 
	format(Batch_Time ,'hh:mm:ss') AS flag
	,4 AS OrderNo
	,ROUND(Actual_01,0) AS [Prod1]
	,ROUND(Actual_02,0) AS [Prod2]
	,ROUND(Actual_03,0) AS [Prod3]
	,ROUND(Actual_04,0) AS [Prod4]
	,ROUND(Actual_05,0) AS [Prod5]
	,ROUND(Actual_06,0) AS [Prod6]
	,ROUND(Actual_07,2) AS [Prod7]
	,ROUND(Actual_08,0) AS [Prod8]
	,ROUND(Actual_09,0) AS [Prod9]
FROM VITRAG_BATCH  where batch_no = " + BatchNo
+ @" UNION ALL
SELECT 
	'Act' AS flag
	,5 AS OrderNo	
	,ROUND(sum(Actual_01),0) AS [Prod1]
	,ROUND(sum(Actual_02),0) AS [Prod2]
	,ROUND(sum(Actual_03),0) AS [Prod3]
	,ROUND(sum(Actual_04),0) AS [Prod4]
	,ROUND(sum(Actual_05),0) AS [Prod5]
	,ROUND(sum(Actual_06),0) AS [Prod6]
	,ROUND(sum(Actual_07),2) AS [Prod7]
	,ROUND(sum(Actual_08),0) AS [Prod8]
	,ROUND(sum(Actual_09),0) AS [Prod9]	
FROM VITRAG_BATCH  where batch_no = " + BatchNo
+ @" GROUP BY
	Name_01
	,Name_02
	,Name_03
	,Name_04
	,Name_05
	,Name_06
	,Name_07
	,Name_08
	,Name_09
UNION ALL
SELECT
	'Tar' AS flag
	,6 AS OrderNo 		
	,ROUND(SUM(Target_01),0) AS [Prod1]
	,ROUND(SUM(Target_02),0) AS [Prod2]
	,ROUND(SUM(Target_03),0) AS [Prod3]
	,ROUND(SUM(Target_04),0) AS [Prod4]
	,ROUND(SUM(Target_05),0) AS [Prod5]
	,ROUND(SUM(Target_06),0) AS [Prod6]
	,ROUND(SUM(Target_07),2) AS [Prod7]
	,ROUND(SUM(Target_08),0) AS [Prod8]
	,ROUND(SUM(Target_09),0) AS [Prod9]
FROM VITRAG_BATCH  where batch_no = " + BatchNo + "";
                    #endregion
                }
                else if (Program.PlantAppVersion == "2")
                {
                    #region Version2


                    QueryResult = @"SELECT TOP 1
	'' AS flag 
	,1 AS OrderNo
	,[AGG1]
	,[AGG2]
	,[AGG3]
	,[AGG4]
	,[CEMENT]
	,[FILLER1]
	,[FILLER2]
	,[WATER1]
	,[WATER2]
	,[WATER3]
	,[ADDMIX1]
	,[ADDMIX2]
FROM NAMESETUP
UNION ALL
SELECT TOP 1
	'Recipe' AS flag
	,2 AS OrderNo
	,ROUND(IIF(ISNULL(AGG1NOM),0,CDBL(VAL(AGG1NOM)))/IIF(IIF(ISNULL(M3_QTY),0,CDBL(VAL(M3_QTY)))<=0,1,CDBL(VAL(M3_QTY))) ,0)  AS [AGG1]
	,ROUND(IIF(ISNULL(agg2nom),0,CDbl(VAL(agg2nom)))/IIF(IIF(ISNULL(M3_QTY),0,CDBL(VAL(M3_QTY)))<=0,1,CDBL(VAL(M3_QTY))) ,0)  AS [AGG2]
	,ROUND(IIF(ISNULL(agg3nom),0,CDbl(VAL(agg3nom)))/IIF(IIF(ISNULL(M3_QTY),0,CDBL(VAL(M3_QTY)))<=0,1,CDBL(VAL(M3_QTY))) ,0)  AS [AGG3]
	,ROUND(IIF(ISNULL(agg4nom),0,CDbl(VAL(agg4nom)))/IIF(IIF(ISNULL(M3_QTY),0,CDBL(VAL(M3_QTY)))<=0,1,CDBL(VAL(M3_QTY))) ,0)  AS [AGG4]
	,ROUND(IIF(ISNULL(cementnom),0,CDbl(VAL(cementnom)))/IIF(IIF(ISNULL(M3_QTY),0,CDBL(VAL(M3_QTY)))<=0,1,CDBL(VAL(M3_QTY))) ,0)  AS [CEMENT]
	,ROUND(IIF(ISNULL(filler1nom),0,CDbl(VAL(filler1nom)))/IIF(IIF(ISNULL(M3_QTY),0,CDBL(VAL(M3_QTY)))<=0,1,CDBL(VAL(M3_QTY))) ,0)  AS [FILLER1]	
	,ROUND(IIF(ISNULL(filler2nom),0,CDbl(VAL(filler2nom)))/IIF(IIF(ISNULL(M3_QTY),0,CDBL(VAL(M3_QTY)))<=0,1,CDBL(VAL(M3_QTY))) ,0)  AS [FILLER2]
	,ROUND(IIF(ISNULL(water1nom),0,CDbl(VAL(water1nom)))/IIF(IIF(ISNULL(M3_QTY),0,CDBL(VAL(M3_QTY)))<=0,1,CDBL(VAL(M3_QTY))) ,0)  AS [WATER1]
	,ROUND(IIF(ISNULL(water2nom),0,CDbl(VAL(water2nom)))/IIF(IIF(ISNULL(M3_QTY),0,CDBL(VAL(M3_QTY)))<=0,1,CDBL(VAL(M3_QTY))) ,0)  AS [WATER2]
	,ROUND(IIF(ISNULL(water3nom),0,CDbl(VAL(water3nom)))/IIF(IIF(ISNULL(M3_QTY),0,CDBL(VAL(M3_QTY)))<=0,1,CDBL(VAL(M3_QTY))) ,0)  AS [WATER3]
	,ROUND(IIF(ISNULL(addmix1nom),0,CDbl(VAL(addmix1nom)))/IIF(IIF(ISNULL(M3_QTY),0,CDBL(VAL(M3_QTY)))<=0,1,CDBL(VAL(M3_QTY))) ,2)  AS [ADDMIX1]
	,ROUND(IIF(ISNULL(addmix2nom),0,CDbl(VAL(addmix2nom)))/IIF(IIF(ISNULL(M3_QTY),0,CDBL(VAL(M3_QTY)))<=0,1,CDBL(VAL(M3_QTY))) ,2)  AS [ADDMIX2]	
FROM BATCH WHERE batchno =" + BatchNo
+ @" UNION ALL
SELECT TOP 1
	'Target' AS flag
	,3 AS OrderNo
	,ROUND(IIF(ISNULL(agg1nom),0,CDbl(VAL(agg1nom)))/IIF(IIF(ISNULL(M3_QTY),0,CDBL(VAL(M3_QTY)))<=0,1,CDBL(VAL(M3_QTY))) ,0)  AS [AGG1]
	,ROUND(IIF(ISNULL(agg2nom),0,CDbl(VAL(agg2nom)))/IIF(IIF(ISNULL(M3_QTY),0,CDBL(VAL(M3_QTY)))<=0,1,CDBL(VAL(M3_QTY))) ,0)  AS [AGG2]
	,ROUND(IIF(ISNULL(agg3nom),0,CDbl(VAL(agg3nom)))/IIF(IIF(ISNULL(M3_QTY),0,CDBL(VAL(M3_QTY)))<=0,1,CDBL(VAL(M3_QTY))) ,0)  AS [AGG3]
	,ROUND(IIF(ISNULL(agg4nom),0,CDbl(VAL(agg4nom)))/IIF(IIF(ISNULL(M3_QTY),0,CDBL(VAL(M3_QTY)))<=0,1,CDBL(VAL(M3_QTY))) ,0)  AS [AGG4]
	,ROUND(IIF(ISNULL(cementnom),0,CDbl(VAL(cementnom)))/IIF(IIF(ISNULL(M3_QTY),0,CDBL(VAL(M3_QTY)))<=0,1,CDBL(VAL(M3_QTY))),0)  AS [CEMENT]
	,ROUND(IIF(ISNULL(filler1nom),0,CDbl(VAL(filler1nom)))/IIF(IIF(ISNULL(M3_QTY),0,CDBL(VAL(M3_QTY)))<=0,1,CDBL(VAL(M3_QTY))) ,0)  AS [FILLER1]	
	,ROUND(IIF(ISNULL(filler2nom),0,CDbl(VAL(filler2nom)))/IIF(IIF(ISNULL(M3_QTY),0,CDBL(VAL(M3_QTY)))<=0,1,CDBL(VAL(M3_QTY))),0)  AS [FILLER2]
	,ROUND(IIF(ISNULL(water1nom),0,CDbl(VAL(water1nom)))/IIF(IIF(ISNULL(M3_QTY),0,CDBL(VAL(M3_QTY)))<=0,1,CDBL(VAL(M3_QTY))) ,0)  AS [WATER1]
	,ROUND(IIF(ISNULL(water2nom),0,CDbl(VAL(water2nom)))/IIF(IIF(ISNULL(M3_QTY),0,CDBL(VAL(M3_QTY)))<=0,1,CDBL(VAL(M3_QTY))) ,0)  AS [WATER2]
	,ROUND(IIF(ISNULL(water3nom),0,CDbl(VAL(water3nom)))/IIF(IIF(ISNULL(M3_QTY),0,CDBL(VAL(M3_QTY)))<=0,1,CDBL(VAL(M3_QTY))) ,0)  AS [WATER3]
	,ROUND(IIF(ISNULL(addmix1nom),0,CDbl(VAL(addmix1nom)))/IIF(IIF(ISNULL(M3_QTY),0,CDBL(VAL(M3_QTY)))<=0,1,CDBL(VAL(M3_QTY))) ,2)  AS [ADDMIX1]
	,ROUND(IIF(ISNULL(addmix2nom),0,CDbl(VAL(addmix2nom)))/IIF(IIF(ISNULL(M3_QTY),0,CDBL(VAL(M3_QTY)))<=0,1,CDBL(VAL(M3_QTY))) ,2)  AS [ADDMIX2]	
FROM BATCH WHERE batchno = " + BatchNo
+ @" UNION ALL
SELECT 
	format(Time ,'hh:mm:ss') AS flag
	,4 AS OrderNo
	,ROUND(agg1,0) AS [AGG1]  
	,ROUND(agg2,0) AS [AGG2]
	,ROUND(agg3,0) AS [AGG3]  
	,ROUND(agg4,0) AS [AGG4]  
	,ROUND(cement,0) AS [CEMENT]
	,ROUND(filler1,0) AS [FILLER1]
	,ROUND(filler2,0) AS [FILLER2]
	,ROUND(water1,0) AS [WATER1]
	,ROUND(water2,0) AS [WATER2]
	,ROUND(water3,0) AS [WATER3]
	,ROUND(addmix1,2) AS [ADDMIX1]
	,ROUND(addmix2,2) AS [ADDMIX2]
FROM BATCH  where batchno = " + BatchNo
+ @" UNION 
SELECT TOP 1
	'Act' AS flag
	,5 as OrderNo
	,ROUND(agg1act,0) AS [AGG1]
	,ROUND(agg2act,0) AS [AGG2]
	,ROUND(agg3act,0) AS [AGG3]
	,ROUND(agg4act,0) AS [AGG4]
	,ROUND(cementact,0) AS [CEMENT]
	,ROUND(filler1act,0) AS [FILLER1]
	,ROUND(filler2act,0) AS [FILLER2]
	,ROUND(water1act,0) AS [WATER1]
	,ROUND(water2act,0) AS [WATER2]
	,ROUND(water3act,0) AS [WATER3]
	,ROUND(addmix1act,2) AS [ADDMIX1]
	,ROUND(addmix2act,2) AS [ADDMIX2]
FROM BATCH WHERE batchno = " + BatchNo
+ @" UNION
SELECT TOP 1
	'Tar' AS flag
	,6 as OrderNo
	,ROUND(agg1nom,0) AS [AGG1]   
	,ROUND(agg2nom,0) AS [AGG2]   
	,ROUND(agg3nom,0) AS [AGG3]   
	,ROUND(agg4nom,0) AS [AGG4]   
	,ROUND(cementnom,0) AS [CEMENT] 
	,ROUND(filler1nom,0) AS [FILLER1]
	,ROUND(filler2nom,0) AS [FILLER2] 
	,ROUND(water1nom,0) AS [WATER1] 
	,ROUND(water2nom,0) AS [WATER2] 
	,ROUND(water3nom,0) AS [WATER3] 
	,ROUND(addmix1nom,2)  AS [ADDMIX1]
	,ROUND(addmix2nom,2) AS [ADDMIX2]
FROM BATCH WHERE batchno = " + BatchNo
+ @" ORDER BY OrderNo";
                    #endregion
                }
                line_No = 2;
            }
            catch (Exception ex)
            {
                Global.WriteLog("Batch_Transation_Query() FOR Line No [" + line_No + "] Exception: ", LogMessageType.Error, ex.Message);
                throw ex;
            }
            return QueryResult;
        }

        internal static void Create_Batch_Tables()
        {
            string strQuery = string.Empty;
            DataTable dtBATCH_MASTER_UPLOAD_STATUS = null;
            try
            {
                strQuery = string.Empty;
                strQuery = @"SELECT TOP 1 * FROM BATCH_MASTER_UPLOAD_STATUS WHERE ROW_INDEX = 0";
                dtBATCH_MASTER_UPLOAD_STATUS = DbHelper.FillDataTable(strQuery, dataConnectionConfiguration.Target_Connection_String);
            }
            catch (Exception)
            {

                strQuery = @"CREATE TABLE BATCH_MASTER_UPLOAD_STATUS(
                            ROW_INDEX Counter(1,1)
                            ,BATCH_NO	Double
                            ,BDT_UPDATE_COUNT	Double
                            ,BR_UPDATE_COUNT	Double
                            ,BT_UPDATE_COUNT	Double
                            ,IS_HTML_FILE_CREATED	Double
                            ,FILE_CREATED_COUNT	Double
                            ,IS_MCGM_BATCH	Double
                            ,IS_MANUAL_CREATED	Double
                            ,IS_MANUAL_CREATED_DATE	Datetime
                            ,BT_UPDATE_COUNT_LIVE	Double
                            ,FILE_CREATED_DATE	Datetime
                            ,FILE_CREATED_FLAG	Double
                            )";
                DbHelper.ExecuteNonQuery(strQuery, dataConnectionConfiguration.Target_Connection_String);
            }

            try
            {
                strQuery = string.Empty;
                strQuery = @"SELECT TOP 1 * FROM BATCH_TRANSACTION_UPLOAD_STATUS WHERE ROW_INDEX = 0";
                dtBATCH_MASTER_UPLOAD_STATUS = DbHelper.FillDataTable(strQuery, dataConnectionConfiguration.Target_Connection_String);

            }
            catch (Exception)
            {
                strQuery = @" CREATE TABLE BATCH_TRANSACTION_UPLOAD_STATUS(
                    ROW_INDEX	Counter(1,1)
                    ,BATCH_NO	Double
                    ,BATCH_INDEX	Double
                    ,BATCH_DATE	Datetime
                    ,BATCH_TIME	Datetime
                    ,UPDATE_COUNT	Double
                    ,IS_HTML_FILE_CREATED	Double
                    ,FILE_CREATED_COUNT	Double
                    ,IS_MCGM_BATCH	Double
                    ,IS_MANUAL_CREATED	Double
                    ,IS_MANUAL_CREATED_DATE	Datetime
                    ,UPDATE_COUNT_LIVE	Double
                    ,FILE_CREATED_DATE	Datetime
                    ,FILE_CREATED_FLAG	Double)";
                DbHelper.ExecuteNonQuery(strQuery, dataConnectionConfiguration.Target_Connection_String);
            }
        }
        internal static DataTable Fetch_Batch_Master_Upload_Status(string Connection_String, DataTable dtbatchRecordList)
        {
            string strQuery = string.Empty;
            DataTable dtFetch_Batch_Master_Upload_Status = new DataTable();
            try
            {
                string strFilterValues = string.Empty;
                foreach (DataRow item in dtbatchRecordList.Rows)
                {
                    if (string.IsNullOrEmpty(strFilterValues) == true)
                    {
                        strFilterValues = item["BatchNo"].ToString();
                    }
                    else
                    {
                        strFilterValues = strFilterValues + "," + item["BatchNo"].ToString();
                    }
                }
                strQuery = @"SELECT * FROM BATCH_MASTER_UPLOAD_STATUS WHERE CDBL(BATCH_NO) > 0 AND IS_MCGM_BATCH = 1";
                if (string.IsNullOrEmpty(strFilterValues) == false)
                {
                    strQuery = strQuery + " AND BATCH_NO IN(" + strFilterValues + ")";
                }
                dtFetch_Batch_Master_Upload_Status = DbHelper.FillDataTable(strQuery, Connection_String);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dtFetch_Batch_Master_Upload_Status;
        }
    }
}