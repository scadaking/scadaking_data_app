﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using ScadaKing_Data_App.Class;
using System.Linq;
namespace ScadaKing_Data_App
{
    public partial class frmListingBatch : Form
    {
        string from_Batch_No_MCGM = string.Empty;
        string to_Batch_No_MCGM = string.Empty;
        string from_Batch_Date_MCGM = string.Empty;
        string to_Batch_Date_MCGM = string.Empty;
        string customer_Code_MCGM = string.Empty;
        string from_Batch_No_Non_MCGM = string.Empty;
        string to_Batch_No_Non_MCGM = string.Empty;
        string from_Batch_Date_Non_MCGM = string.Empty;
        string to_Batch_Date_Non_MCGM = string.Empty;
        string customer_Code_Non_MCGM = string.Empty;

        #region start
        public frmListingBatch()
        {
            InitializeComponent();
            if (Program.PlantAppVersion == "2")
            {
                btnMaster.Visible = false;
            }
        }

        private void FillDropdowns()
        {

        }

        bool _bActivated;
        int _batchType;

        public int BatchType
        {
            get { return this._batchType; }
            set { this._batchType = value; }
        }

        private void btnFilter_Click(object sender, EventArgs e)
        {
            if (tabUpdate.SelectedTab.Text == tabUpdate.TabPages[0].Text)
            {
                BatchType = 1;
                BindGridData_Non_MCGM();
            }
            else if (tabUpdate.SelectedTab.Text == tabUpdate.TabPages[1].Text)
            {
                BatchType = 4;
                BindGridData_MCGM();
            }
        }

        DataTable _DtBatchRawDetails_Non_MCGM;
        public DataTable DtBatchRawDetails_Non_MCGM
        {
            get { return this._DtBatchRawDetails_Non_MCGM; }
            set { this._DtBatchRawDetails_Non_MCGM = value; }
        }
        ScadaKing_Data_App.BusinessLayer.Model.BatchDatTransRaw[] _batchDatTransRaw_Non_MCGM;
        public ScadaKing_Data_App.BusinessLayer.Model.BatchDatTransRaw[] BatchRawDetails_Non_MCGM
        {
            get { return this._batchDatTransRaw_Non_MCGM; }
            set { this._batchDatTransRaw_Non_MCGM = value; }
        }
        DataTable _DtbatchDatTransRaw_MCGM;
        public DataTable DtbatchDatTransRaw_MCGM
        {
            get { return this._DtbatchDatTransRaw_MCGM; }
            set { this._DtbatchDatTransRaw_MCGM = value; }
        }
        DataTable _dtFetch_Batch_Master_Upload_Status;
        public DataTable dtFetch_Batch_Master_Upload_Status
        {
            get { return this._dtFetch_Batch_Master_Upload_Status; }
            set { this._dtFetch_Batch_Master_Upload_Status = value; }
        }

        ScadaKing_Data_App.BusinessLayer.Model.BatchDatTransRaw[] _batchDatTransRaw_MCGM;
        public ScadaKing_Data_App.BusinessLayer.Model.BatchDatTransRaw[] BatchRawDetails_MCGM
        {

            get { return this._batchDatTransRaw_MCGM; }
            set { this._batchDatTransRaw_MCGM = value; }
        }

        private void frmListingBatch_Load(object sender, EventArgs e)
        {
            this.tabUpdate.Dock = DockStyle.Fill;
            this.WindowState = FormWindowState.Maximized;
            this.dgvSearch_MCGM.CellClick += new DataGridViewCellEventHandler(dgvSearch_Non_MCGM_CellClick);
            this.dgvSearch_Non_MCGM.CellClick += new DataGridViewCellEventHandler(dgvSearch_Non_MCGM_CellClick);
        }

        private void dgvSearch_Non_MCGM_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex <= 0 || e.RowIndex < 0)
            {
                return;
            }

            if (e.ColumnIndex == ((DataGridView)sender).Columns["cmdPreview"].Index)
            {
                frmPreview form = new frmPreview();
                DataGridViewRow objgridRow = new DataGridViewRow();
                objgridRow = ((DataGridView)sender).Rows[e.RowIndex];
                form.objGridRow = objgridRow;
                if (((DataGridView)sender).Name == this.dgvSearch_Non_MCGM.Name)
                {
                    form.ReGenerate_File = false;
                }
                else
                {
                    form.ReGenerate_File = true;
                    #region Check priview Batch No is MCGM OR NONMCGM
                    if (this.dtFetch_Batch_Master_Upload_Status.Rows.Count > 0)
                    {
                        string strBatchNo = objgridRow.Cells["BatchNo"].Value.ToString();
                        if (dtFetch_Batch_Master_Upload_Status.Select("Batch_No=" + strBatchNo).Length > 0)
                        {
                            form.Is_Government_Batch = true;
                        }
                    }
                    #endregion CHECK BATCH NO MCGM OR NONMCGM
                }
                if (form.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    this.btnRefresh_Click(sender, e);
                }
            }
        }

        private void frmListingBatch_Activated(object sender, EventArgs e)
        {
            if (this._bActivated == true)
            {
                return;
            }
            this._bActivated = true;
            this.GetData_Non_MCGM();
        }

        private void GetData_Non_MCGM()
        {
            try
            {
                DataTable dtOriginalBatchList = new DataTable();
                this.Cursor = Cursors.WaitCursor;
                //if (this._bActivated == false)
                //{
                //    dtOriginalBatchList = Controller.BatchDataController.GetOriginalBatchList(General.orgId, 0, 0, string.Empty, null, null, this._batchType, dataConnectionConfiguration.Source_Connection_String);
                //}
                //else
                {
                    string batch_From_DateTime = null;
                    string batch_To_DateTime = null;
                    if (this.dtpFromDate.Checked == true)
                    {
                        batch_From_DateTime = this.dtpFromDate.Value.ToShortDateString();
                        batch_To_DateTime = this.dtpFromDate.Value.ToShortDateString();
                    }
                    else
                    {
                        if (General.ConvertToInteger(this.txtFromBatchNo.Text) > 0 || General.ConvertToInteger(this.txtToBatchNo.Text) > 0)
                        {

                        }
                        else
                        {
                            batch_From_DateTime = DateTime.Today.ToShortDateString();
                            batch_To_DateTime = DateTime.Today.ToShortDateString();
                        }
                    }
                    string customer_code = string.Empty;

                    try
                    {
                        Global.FlushMemory();
                        if (this.txtToBatchNo.Text.Trim().TrimEnd().TrimStart().Length == 0 && this.txtFromBatchNo.Text.Trim().TrimEnd().TrimStart().Length > 0)
                        {
                            this.txtToBatchNo.Text = txtFromBatchNo.Text;
                        }
                        if (this.txtFromBatchNo.Text.Trim().TrimEnd().TrimStart().Length == 0 && this.txtToBatchNo.Text.Trim().TrimEnd().TrimStart().Length > 0)
                        {
                            this.txtFromBatchNo.Text = txtToBatchNo.Text;
                        }
                        dtOriginalBatchList = Controller.BatchDataController.GetOriginalBatchList(
                        General.orgId,
                        General.ConvertToInteger(this.txtFromBatchNo.Text),
                        General.ConvertToInteger(this.txtToBatchNo.Text),
                        customer_code,
                        batch_From_DateTime,
                        batch_To_DateTime,
                        this._batchType, dataConnectionConfiguration.Source_Connection_String);

                        this.DtbatchDatTransRaw_MCGM = Controller.BatchDataController.GetOriginalBatchList(
                        General.orgId,
                        General.ConvertToInteger(this.txtFromBatchNo.Text),
                        General.ConvertToInteger(this.txtToBatchNo.Text),
                        customer_code,
                        batch_From_DateTime,
                        batch_To_DateTime,
                        this._batchType, dataConnectionConfiguration.Target_Connection_String);
                    }
                    catch (Exception ex)
                    {
                        General.ShowErrorMessage(ex.Message);
                    }
                }
                DataView View = new DataView(dtOriginalBatchList.Clone());
                /* bool IsReplicaMatched = false;
                 foreach (DataRow rowOgBatchList in dtOriginalBatchList.Rows)
                 {
                     IsReplicaMatched = false;
                     foreach (DataRow rowReplica in this.DtbatchDatTransRaw_MCGM.Rows)
                     {
                         if (rowOgBatchList["BatchNo"].ToString() == rowReplica["BatchNo"].ToString())
                         {
                             IsReplicaMatched = true;
                         }
                     }
                     if (IsReplicaMatched == false)
                     {
                         View.Table.Rows.Add(rowOgBatchList["BatchNo"], rowOgBatchList["BatchDate"], rowOgBatchList["BatchStartTime"], rowOgBatchList["BatchEndTime"], rowOgBatchList["Site"], rowOgBatchList["TruckNo"], rowOgBatchList["CustomerName"]);
                     }
                 }*/

                //var varExceptUploadedBatch = dtOriginalBatchList.AsEnumerable().Except(DtbatchDatTransRaw_MCGM.AsEnumerable(), DataRowComparer.Default);
                var varExceptUploadedBatch = dtOriginalBatchList.AsEnumerable().Where(
    r => !DtbatchDatTransRaw_MCGM.AsEnumerable().Select(x => x["BatchNo"]).ToList().Contains(r["BatchNo"])).ToList();
                if (varExceptUploadedBatch != null && varExceptUploadedBatch.Count() > 0)
                {
                    DataTable dtExceptUploadedBatch = (from a in varExceptUploadedBatch.AsEnumerable() select a).CopyToDataTable();
                    View = new DataView(dtExceptUploadedBatch);
                }

                dtOriginalBatchList = View.ToTable();
                this.DtBatchRawDetails_Non_MCGM = dtOriginalBatchList;
                this.BindGridData_Non_MCGM();
            }
            catch (Exception ex)
            {
                General.ShowErrorMessage(ex.Message);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void GetData_MCGM()
        {
            try
            {
                DataTable dtbatchRecordList = new DataTable();
                this.Cursor = Cursors.WaitCursor;
                if (this._bActivated == false)
                {
                    //////batchRecordList = Controller.BatchDataController.GetOriginalBatchList(General.orgId, 0, 0, string.Empty, null, null, this._batchType);
                }
                else
                {
                    string batch_From_DateTime = null;
                    string batch_To_DateTime = null;
                    if (this.dtpFromDate.Checked == true)
                    {
                        batch_From_DateTime = this.dtpFromDate.Value.ToShortDateString();
                        batch_To_DateTime = this.dtpFromDate.Value.ToShortDateString();
                    }
                    else
                    {
                        if (General.ConvertToInteger(this.txtFromBatchNo.Text) > 0 || General.ConvertToInteger(this.txtToBatchNo.Text) > 0)
                        {

                        }
                        else
                        {
                            batch_From_DateTime = DateTime.Today.ToShortDateString();
                            batch_To_DateTime = DateTime.Today.ToShortDateString();
                        }
                    }
                    string customer_code = string.Empty;
                    dtbatchRecordList = Controller.BatchDataController.GetOriginalBatchList(
                        General.orgId,
                        General.ConvertToInteger(this.txtFromBatchNo.Text),
                        General.ConvertToInteger(this.txtToBatchNo.Text),
                        customer_code,
                        batch_From_DateTime,
                        batch_To_DateTime,
                        this._batchType, dataConnectionConfiguration.Target_Connection_String);
                }
                this.DtbatchDatTransRaw_MCGM = dtbatchRecordList;
                this.BindGridData_MCGM();

                #region Fetch Batch Master Upload Status for coloring grid row on the besis of mcgm or nonmcgm
                this.dtFetch_Batch_Master_Upload_Status = Controller.BatchDataController.Fetch_Batch_Master_Upload_Status(dataConnectionConfiguration.Source_Connection_String, dtbatchRecordList);
                #endregion Fetch Batch Master Upload Status for coloring grid row on the besis of mcgm or nonmcgm
            }
            catch (Exception ex)
            {
                General.ShowErrorMessage(ex.Message);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void BindGridData_Non_MCGM()
        {
            this.UserAccess(this.Controls);
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.dgvSearch_Non_MCGM.DataSource = DtBatchRawDetails_Non_MCGM;
                gridShowColumn(ref dgvSearch_Non_MCGM);
            }
            catch (Exception ex)
            {
                General.ShowErrorMessage(ex.Message);
            }
            finally
            {
                this.lblRecordCount.Text = string.Format(lblRecordCount.Tag.ToString(), this._DtBatchRawDetails_Non_MCGM.Rows.Count, dgvSearch_Non_MCGM.Rows.Count.ToString());
                this.Cursor = Cursors.Default;
            }
        }

        private void BindGridData_MCGM()
        {
            this.UserAccess(this.Controls);
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.dgvSearch_MCGM.DataSource = this.DtbatchDatTransRaw_MCGM;
                gridShowColumn(ref dgvSearch_MCGM);
            }
            catch (Exception ex)
            {
                General.ShowErrorMessage(ex.Message);
            }
            finally
            {
                this.lblRecordCount.Text = string.Format(lblRecordCount.Tag.ToString(), this._DtbatchDatTransRaw_MCGM.Rows.Count, dgvSearch_MCGM.Rows.Count.ToString());
                this.Cursor = Cursors.Default;
            }
        }

        void gridShowColumn(ref DataGridView gridView)
        {
            General.SetGridDefaults(gridView, string.Empty, false, "Tahoma", 9, false, 9, true, false, 20);
            int displayIndex = 0;
            gridView.AllowUserToResizeColumns = true;
            gridView.AllowUserToOrderColumns = false;
            DataGridViewContentAlignment odgvcalignment = DataGridViewContentAlignment.MiddleLeft;
            General.SetColumnDefaults(gridView, odgvcalignment, ref displayIndex, "BatchNo", "Sr.No.", 100);
            General.SetColumnDefaults(gridView, odgvcalignment, ref displayIndex, "BatchDate", "Date", 120, "dd/MM/yyyy");
            General.SetColumnDefaults(gridView, odgvcalignment, ref displayIndex, "BatchStartTime", "Start Time", 120);
            General.SetColumnDefaults(gridView, odgvcalignment, ref displayIndex, "BatchEndTime", "End Time", 120);
            General.SetColumnDefaults(gridView, odgvcalignment, ref displayIndex, "CustomerName", "Customer Name", 200);
            General.SetColumnDefaults(gridView, odgvcalignment, ref displayIndex, "Site", "Site Name", 150);
            General.SetColumnDefaults(gridView, odgvcalignment, ref displayIndex, "TruckNo", "Vehicle No", 100);
            DataGridViewButtonColumn previewColumn = new DataGridViewButtonColumn();
            previewColumn.Tag = "Preview";
            previewColumn.Name = "cmdPreview";
            previewColumn.Text = "Preview";
            if (gridView.Name == this.dgvSearch_Non_MCGM.Name)
            {
                previewColumn.DefaultCellStyle.NullValue = "Preview & Generate";
            }
            else
            {
                previewColumn.DefaultCellStyle.NullValue = "Preview & Re-Generated";
            }
            if (gridView.Columns["cmdPreview"] == null)
            {
                gridView.Columns.Insert(gridView.Columns.Count, previewColumn);
            }
            General.SetColumnDefaults(gridView, DataGridViewContentAlignment.MiddleCenter, ref displayIndex, "cmdPreview", "Preview", 200);
        }

        private void UserAccess(Control.ControlCollection controlCollection)
        {
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            try
            {
                this.Close();
            }
            catch (Exception)
            {
                throw;
            }
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            Global.FlushMemory();
            if (tabUpdate.SelectedTab.Text == tabUpdate.TabPages[0].Text)
            {
                pnlTbMcgmBatch.Visible = false;
                pnlFilterUploadedData.Visible = false;
                if (sender != null)
                {
                }
                else
                {
                    if (this.from_Batch_No_Non_MCGM == txtFromBatchNo.Text
                        && from_Batch_Date_Non_MCGM == Convert.ToString(dtpFromDate.Value.ToString())
                        )
                    {
                        this.lblRecordCount.Text = string.Format(lblRecordCount.Tag.ToString(), this._DtBatchRawDetails_Non_MCGM.Rows.Count, dgvSearch_Non_MCGM.Rows.Count.ToString());
                        return;
                    }
                }
                from_Batch_No_Non_MCGM = this.txtFromBatchNo.Text;
                to_Batch_No_Non_MCGM = this.txtFromBatchNo.Text;
                from_Batch_Date_Non_MCGM = dtpFromDate.Value.ToString();
                to_Batch_Date_Non_MCGM = dtpFromDate.Value.ToString();
                BatchType = 1;
                this.GetData_Non_MCGM();
            }
            else if (tabUpdate.SelectedTab.Text == tabUpdate.TabPages[1].Text)
            {
                pnlTbMcgmBatch.Visible = true;
                pnlFilterUploadedData.Visible = true;
                if (sender != null)
                {
                }
                else
                {
                    if (this.from_Batch_No_MCGM == txtFromBatchNo.Text
                        && from_Batch_Date_MCGM == Convert.ToString(dtpFromDate.Value.ToString())
                        )
                    {
                        this.lblRecordCount.Text = string.Format(lblRecordCount.Tag.ToString(), this._DtbatchDatTransRaw_MCGM.Rows.Count, dgvSearch_MCGM.Rows.Count.ToString());
                        return;
                    }
                }

                from_Batch_No_MCGM = this.txtFromBatchNo.Text;
                to_Batch_No_MCGM = this.txtFromBatchNo.Text;
                from_Batch_Date_MCGM = dtpFromDate.Value.ToString();
                to_Batch_Date_MCGM = dtpFromDate.Value.ToString();
                BatchType = 4;
                this.GetData_MCGM();
            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            General.ClearControls(this, string.Empty);
            lblRecordCount.Text = "No. of Records : 0 , Filtered Records : 0";
        }

        #endregion start
        private void tabUpdate_Selected(object sender, TabControlEventArgs e)
        {
            this.btnRefresh_Click(null, e);
        }

        private void dgvSearch_Non_MCGM_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            //try
            //{
            //    return;
            //    //string b = dgvSearch_Non_MCGM.Rows[e.RowIndex].Cells["Valid"].Value.ToString();
            //    //if (b == "True")
            //    //{
            //    //    dgvSearch_Non_MCGM.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.Green;
            //    //}
            //    //else
            //    //{
            //    //    dgvSearch_Non_MCGM.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.White;
            //    //}

            //}
            //catch (Exception)
            //{

            //    throw;
            //}
        }

        private void dgvSearch_Non_MCGM_CellMouseEnter(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.RowIndex >= 0 && e.ColumnIndex >= 0)
                {
                    if (((DataGridView)sender).Columns.Contains("cmdPreview") == true)
                    {
                        if (((DataGridView)sender).Columns["cmdPreview"].Index == e.ColumnIndex)
                        {
                            ((DataGridView)sender).Cursor = Cursors.Hand;
                        }
                        else
                        {
                            ((DataGridView)sender).Cursor = Cursors.Default;
                        }
                    }
                }
                else
                {
                    ((DataGridView)sender).Cursor = Cursors.Default;
                }
            }
            catch (Exception ex)
            {
                General.ShowErrorMessage(ex.Message);
            }
        }

        private void dgvSearch_MCGM_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            try
            {
                if (e.RowIndex > -1 && e.ColumnIndex > -1)
                {
                    string Batch_NO = string.Empty;
                    Batch_NO = dgvSearch_MCGM["BatchNo", e.RowIndex].Value.ToString();
                    if (this.dtFetch_Batch_Master_Upload_Status != null && this.dtFetch_Batch_Master_Upload_Status.Rows.Count > 0 && this.dtFetch_Batch_Master_Upload_Status.Select("Batch_No=" + Batch_NO).Length > 0)
                    {
                        dgvSearch_MCGM.Rows[e.RowIndex].DefaultCellStyle.BackColor = System.Drawing.ColorTranslator.FromHtml("#90EE90");
                        dgvSearch_MCGM.Rows[e.RowIndex].DefaultCellStyle.ForeColor = System.Drawing.ColorTranslator.FromHtml("#00008B");
                    }
                }
            }
            catch (Exception ex)
            {
                General.ShowErrorMessage(ex.Message);
            }
        }

        private void tabUpdate_DrawItem(object sender, DrawItemEventArgs e)
        {
            try
            {
                Font f;
                Brush backBrush;
                Brush foreBrush;

                if (e.Index == this.tabUpdate.SelectedIndex)
                {
                    f = new Font(e.Font, FontStyle.Bold | FontStyle.Bold);
                    f = new Font(e.Font, FontStyle.Bold);
                    //backBrush = new System.Drawing.SolidBrush(Color.DarkGray);
                    //foreBrush = Brushes.White;
                    backBrush = new System.Drawing.SolidBrush(System.Drawing.ColorTranslator.FromHtml("#FCD036"));
                    foreBrush = new System.Drawing.SolidBrush(System.Drawing.ColorTranslator.FromHtml("#2A2A2A"));
                }
                else
                {
                    f = e.Font;
                    if (this.tabUpdate.SelectedIndex == 0)
                    {
                        backBrush = new System.Drawing.SolidBrush(System.Drawing.ColorTranslator.FromHtml("#395C9C"));
                        foreBrush = new System.Drawing.SolidBrush(System.Drawing.ColorTranslator.FromHtml("#FFFFFF"));
                    }
                    else if (this.tabUpdate.SelectedIndex == 1)
                    {
                        backBrush = new System.Drawing.SolidBrush(System.Drawing.ColorTranslator.FromHtml("#2A8AD4"));
                        foreBrush = new System.Drawing.SolidBrush(System.Drawing.ColorTranslator.FromHtml("#FFFFFF"));
                    }
                    else
                    {
                        backBrush = new SolidBrush(e.BackColor);
                        foreBrush = new SolidBrush(e.ForeColor);
                    }
                }

                string tabName = this.tabUpdate.TabPages[e.Index].Text;
                StringFormat sf = new StringFormat();
                //sf.Alignment = StringAlignment.Center;

                //selected tabpage.
                e.Graphics.FillRectangle(backBrush, e.Bounds);
                Rectangle r = e.Bounds;
                r = new Rectangle(r.X, r.Y + 3, r.Width, r.Height - 3);
                e.Graphics.DrawString(tabName, f, foreBrush, r, sf);

                sf.Dispose();
                if (e.Index == this.tabUpdate.SelectedIndex)
                {
                    f.Dispose();
                    backBrush.Dispose();
                }
                else
                {
                    backBrush.Dispose();
                    foreBrush.Dispose();
                }
            }
            catch (Exception Ex)
            {
                General.ShowErrorMessage(Ex.Message.ToString() + "Error Occured", "ScadaKing Data App");
            }
        }

        private void tabUpdate_MouseMove(object sender, MouseEventArgs e)
        {
            try
            {
                Rectangle mouseRect = new Rectangle(e.X, e.Y, 1, 1);
                for (int i = 0; i < tabUpdate.TabCount; i++)
                {
                    if (tabUpdate.GetTabRect(i).IntersectsWith(mouseRect))
                    {
                        //tabUpdate.SelectedIndex = i;
                        tabUpdate.TabPages[i].Cursor = Cursors.Hand;
                        break;
                    }
                }
            }
            catch (Exception Ex)
            {
                General.ShowErrorMessage(Ex.Message.ToString() + "Error Occured", "ScadaKing Data App");
            }
        }

        private void FilterUploadedGridData(object sender, EventArgs e)
        {
            try
            {
                if (tabUpdate.SelectedTab.Text == tabUpdate.TabPages[1].Text)
                {
                    this.Cursor = Cursors.WaitCursor;
                    BatchType = 4;
                    if (sender is RadioButton)
                    {
                        if (((RadioButton)sender).Tag.ToString() == "1" || ((RadioButton)sender).Tag.ToString() == "2")
                        {
                            string strValues = string.Empty;
                            if (this.dtFetch_Batch_Master_Upload_Status != null && this.dtFetch_Batch_Master_Upload_Status.Rows.Count > 0)
                            {
                                foreach (DataRow item in this.dtFetch_Batch_Master_Upload_Status.Rows)
                                {
                                    if (string.IsNullOrEmpty(strValues) == true)
                                    {
                                        strValues = item["Batch_No"].ToString();
                                    }
                                    else
                                    {
                                        strValues = strValues + "," + item["Batch_No"].ToString();
                                    }
                                }

                                if (((RadioButton)sender).Tag.ToString() == "1")
                                {
                                    DataView dvDtbatchDatTransRaw_MCGM = new DataView(this.DtbatchDatTransRaw_MCGM);
                                    dvDtbatchDatTransRaw_MCGM.RowFilter = "BatchNo in(" + strValues + ")";

                                    this.dgvSearch_MCGM.DataSource = dvDtbatchDatTransRaw_MCGM.ToTable();
                                    gridShowColumn(ref dgvSearch_MCGM);
                                    this.lblRecordCount.Text = string.Format(lblRecordCount.Tag.ToString(), this._DtbatchDatTransRaw_MCGM.Rows.Count, dgvSearch_MCGM.Rows.Count.ToString());
                                }

                                if (((RadioButton)sender).Tag.ToString() == "2")
                                {
                                    DataView dvDtbatchDatTransRaw_MCGM = new DataView(this.DtbatchDatTransRaw_MCGM);
                                    dvDtbatchDatTransRaw_MCGM.RowFilter = "BatchNo Not in(" + strValues + ")";
                                    this.dgvSearch_MCGM.DataSource = dvDtbatchDatTransRaw_MCGM.ToTable();
                                    gridShowColumn(ref dgvSearch_MCGM);
                                    this.lblRecordCount.Text = string.Format(lblRecordCount.Tag.ToString(), this._DtbatchDatTransRaw_MCGM.Rows.Count, dgvSearch_MCGM.Rows.Count.ToString());
                                }
                            }
                        }
                        else if (((RadioButton)sender).Tag.ToString() == "3")
                        {
                            this.dgvSearch_MCGM.DataSource = this.DtbatchDatTransRaw_MCGM;
                            gridShowColumn(ref dgvSearch_MCGM);
                            this.lblRecordCount.Text = string.Format(lblRecordCount.Tag.ToString(), this._DtbatchDatTransRaw_MCGM.Rows.Count, dgvSearch_MCGM.Rows.Count.ToString());
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                General.ShowErrorMessage(ex.Message, "ScadaKing Data App");
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void btnMaster_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            try
            {
                frmMaster objFormMaster;
                if (tabUpdate.SelectedTab.Text == tabUpdate.TabPages[0].Text)
                {
                    objFormMaster = new frmMaster(dataConnectionConfiguration.Source_Connection_String);
                }
                else
                {
                    objFormMaster = new frmMaster(dataConnectionConfiguration.Target_Connection_String);
                }
                objFormMaster.ShowDialog();
                objFormMaster.Dispose();
            }
            catch (Exception ex)
            {
                General.ShowErrorMessage(ex.Message);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }
    }
}