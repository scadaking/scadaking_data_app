﻿using System;
using System.Windows.Forms;
using System.Data;
using ScadaKing_Data_App.Class;

namespace ScadaKing_Data_App
{
    public partial class frmHtmlPreview : Form
    {
        private string _batchNo = string.Empty;
        private string _connectionString = string.Empty;
        string htmlDetails = string.Empty;
        DataSet dsFetch_Data = new DataSet();
        string file_Created_Date = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss");

        public frmHtmlPreview(string BatchNo, string ConnectionString)
        {
            InitializeComponent();
            try
            {
                _batchNo = BatchNo;
                _connectionString = ConnectionString;
                Fetch_Data();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void Fetch_Data()
        {
            try
            {
                if (rdoLocalData.Checked == true)
                {
                    dsFetch_Data = new DataSet();
                    dsFetch_Data = ScadaKing_Data_App.Controller.BatchDataController.MCGM_Transfered_Batch(Convert.ToInt64(this._batchNo), dataConnectionConfiguration.Target_Connection_String);
                }
                else
                {
                    dsFetch_Data = new DataSet();
                    DataTable dtHeader = new DataTable();
                    DataTable dtDetails = new DataTable();
                    ScadaKing_Web_Service.TransactionHeader TransactionHeader = new ScadaKing_Web_Service.TransactionHeader();
                    TransactionHeader.UserName = General.User_Name;
                    TransactionHeader.Password = General.ConfigService_Password;
                    TransactionHeader.DeviceID = General.ConfigMachine_Id;
                    TransactionHeader.Config_App_Name = General.ConfigAPP_Name;
                    TransactionHeader.Config_App_Version = General.ConfigAPP_Version;
                    TransactionHeader.Win_Service_Version = General.Win_Service_Version;
                    TransactionHeader.Win_Service_GUID = General.Win_Service_GUID;
                    ScadaKing_Web_Service.Response_UploadData response = new ScadaKing_Web_Service.Response_UploadData();
                    ScadaKing_Web_Service.ScadaKing_Web_ServiceSoapClient objService = new ScadaKing_Web_Service.ScadaKing_Web_ServiceSoapClient();
                    response = objService.Fetch_HTML_DATA(TransactionHeader, Convert.ToInt64(General.orgId), Convert.ToInt64(this._batchNo));

                    if (response.SyncResponseData.Tables.Count > 0)
                    {
                        dtHeader = new DataTable();
                        dtHeader = response.SyncResponseData.Tables[0].Copy();
                        dtHeader.TableName = "HTML_HEADER";
                        dsFetch_Data.Tables.Add(dtHeader);
                    }
                    if (response.SyncResponseData.Tables.Count > 1)
                    {
                        dtDetails = new DataTable();
                        dtDetails = response.SyncResponseData.Tables[1].Copy();
                        dtDetails.TableName = "HTML_DETAILS";
                        dsFetch_Data.Tables.Add(dtDetails);
                    }
                }
                if (dsFetch_Data.Tables.Count > 0)
                {
                    if (dsFetch_Data.Tables.Count != 2)
                    {
                        if (rdoLocalData.Checked == true)
                        {
                            // General.ShowMessage("For Sr No. " + this._batchNo + " " + dsFetch_Data.Tables[0].Rows[0]["ERROR_DESCRIPTION"].ToString(), this.Text);    
                            General.ShowMessage("For Sr No.[" + this._batchNo + "] Failed to find html header and details data from local.", this.Text);
                        }
                        else
                        {
                            General.ShowMessage("For Sr No.[" + this._batchNo + "] Failed to download html header and details data from live.", this.Text);
                        }
                    }
                    else if (dsFetch_Data.Tables[0].Rows.Count > 0)
                    {
                        try
                        {
                            htmlDetails = Global.Create_RMC_HMTL_File(dsFetch_Data, this._batchNo.ToString());
                            wb1.DocumentText = htmlDetails;
                            btnGenerateFile.Enabled = true;
                        }
                        catch (Exception ex)
                        {
                            General.ShowMessage("Fetch_Data() M N Error while Generating file for Sr No(s) : " + Convert.ToString(this._batchNo) + Environment.NewLine + ex.Message.ToString(), this.Text);
                        }
                    }
                }
                else
                {
                    if (rdoLocalData.Checked == true)
                    {
                        General.ShowMessage("Data not found on local for Sr No.[" + Convert.ToString(this._batchNo) + "].", this.Text);
                    }
                    else
                    {
                        General.ShowMessage("Data not found on live for Sr No.[" + Convert.ToString(this._batchNo) + "].", this.Text);
                    }
                }
            }
            catch (Exception ex)
            {
                General.ShowErrorMessage(ex.Message);
            }
        }

        private void btnPreview_Batch_Click(object sender, EventArgs e)
        {
            try
            {
                wb1.DocumentText = null;
                Fetch_Data();
            }
            catch (Exception ex)
            {
                General.ShowErrorMessage(ex.Message);
            }
        }

        private void btnGenerateFile_Click(object sender, EventArgs e)
        {
            try
            {
                #region Create HTML File
                if (Program.ConfigIS_CREATE_HTML_FILE == true)
                {
                    if (General.ShowYesNoMessage("Do you want to Generate a file?", this.Text) == System.Windows.Forms.DialogResult.Yes)
                    {
                        Global.Write_HTML_File(htmlDetails, true, this._batchNo.ToString(), string.Empty);
                        General.ShowMessage("File Generated successfully...", this.Text);
                        this.DialogResult = System.Windows.Forms.DialogResult.OK;
                        btnGenerateFile.Enabled = false;
                    }
                }
                #endregion Create HTML File
            }
            catch (Exception ex)
            {
                General.ShowErrorMessage(ex.Message);
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            try
            {
                this.Close();
            }
            catch (Exception ex)
            {
                General.ShowErrorMessage(ex.Message);
            }
        }
    }
}
