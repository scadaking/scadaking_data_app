﻿namespace ScadaKing_Data_App
{
    partial class frmChangeTruckNumber
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbChangeTruckNumber = new System.Windows.Forms.GroupBox();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.lblLiveDbTruckNo = new System.Windows.Forms.Label();
            this.lblLocalDbTruckNo = new System.Windows.Forms.Label();
            this.txtLocalTrucks = new System.Windows.Forms.TextBox();
            this.cbLiveTrucks = new System.Windows.Forms.ComboBox();
            this.gbChangeTruckNumber.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbChangeTruckNumber
            // 
            this.gbChangeTruckNumber.Controls.Add(this.btnUpdate);
            this.gbChangeTruckNumber.Controls.Add(this.lblLiveDbTruckNo);
            this.gbChangeTruckNumber.Controls.Add(this.lblLocalDbTruckNo);
            this.gbChangeTruckNumber.Controls.Add(this.txtLocalTrucks);
            this.gbChangeTruckNumber.Controls.Add(this.cbLiveTrucks);
            this.gbChangeTruckNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbChangeTruckNumber.Location = new System.Drawing.Point(6, 3);
            this.gbChangeTruckNumber.Name = "gbChangeTruckNumber";
            this.gbChangeTruckNumber.Size = new System.Drawing.Size(371, 118);
            this.gbChangeTruckNumber.TabIndex = 0;
            this.gbChangeTruckNumber.TabStop = false;
            this.gbChangeTruckNumber.Text = "Truck No";
            // 
            // btnUpdate
            // 
            this.btnUpdate.Location = new System.Drawing.Point(277, 79);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(81, 33);
            this.btnUpdate.TabIndex = 1;
            this.btnUpdate.Text = "&Update";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // lblLiveDbTruckNo
            // 
            this.lblLiveDbTruckNo.AutoSize = true;
            this.lblLiveDbTruckNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLiveDbTruckNo.Location = new System.Drawing.Point(146, 25);
            this.lblLiveDbTruckNo.Name = "lblLiveDbTruckNo";
            this.lblLiveDbTruckNo.Size = new System.Drawing.Size(115, 13);
            this.lblLiveDbTruckNo.TabIndex = 3;
            this.lblLiveDbTruckNo.Text = "Live Truck Number";
            // 
            // lblLocalDbTruckNo
            // 
            this.lblLocalDbTruckNo.AutoSize = true;
            this.lblLocalDbTruckNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLocalDbTruckNo.Location = new System.Drawing.Point(8, 25);
            this.lblLocalDbTruckNo.Name = "lblLocalDbTruckNo";
            this.lblLocalDbTruckNo.Size = new System.Drawing.Size(122, 13);
            this.lblLocalDbTruckNo.TabIndex = 2;
            this.lblLocalDbTruckNo.Text = "Local Truck Number";
            // 
            // txtLocalTrucks
            // 
            this.txtLocalTrucks.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLocalTrucks.Location = new System.Drawing.Point(11, 53);
            this.txtLocalTrucks.Name = "txtLocalTrucks";
            this.txtLocalTrucks.ReadOnly = true;
            this.txtLocalTrucks.Size = new System.Drawing.Size(119, 20);
            this.txtLocalTrucks.TabIndex = 1;
            // 
            // cbLiveTrucks
            // 
            this.cbLiveTrucks.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbLiveTrucks.FormattingEnabled = true;
            this.cbLiveTrucks.Location = new System.Drawing.Point(149, 52);
            this.cbLiveTrucks.Name = "cbLiveTrucks";
            this.cbLiveTrucks.Size = new System.Drawing.Size(209, 21);
            this.cbLiveTrucks.TabIndex = 0;
            // 
            // frmChangeTruckNumber
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(384, 124);
            this.Controls.Add(this.gbChangeTruckNumber);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmChangeTruckNumber";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Change Truck Number";
            this.Load += new System.EventHandler(this.frmChangeTruckNumber_Load);
            this.gbChangeTruckNumber.ResumeLayout(false);
            this.gbChangeTruckNumber.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbChangeTruckNumber;
        private System.Windows.Forms.Label lblLiveDbTruckNo;
        private System.Windows.Forms.Label lblLocalDbTruckNo;
        private System.Windows.Forms.ComboBox cbLiveTrucks;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.TextBox txtLocalTrucks;
    }
}