﻿namespace ScadaKing_Data_App
{
    partial class frmListingBatch
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grpControls = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.pnlFilterUploadedData = new System.Windows.Forms.Panel();
            this.radioButton3 = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.pnlTbMcgmBatch = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.dtpFromDate = new System.Windows.Forms.DateTimePicker();
            this.lblFromDate = new System.Windows.Forms.Label();
            this.txtToBatchNo = new System.Windows.Forms.TextBox();
            this.lblToBatchNo = new System.Windows.Forms.Label();
            this.btnClear = new System.Windows.Forms.Button();
            this.txtFromBatchNo = new System.Windows.Forms.TextBox();
            this.lblFromBatchNo = new System.Windows.Forms.Label();
            this.lblRecordCount = new System.Windows.Forms.Label();
            this.btnRefresh = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.pnlGrid = new System.Windows.Forms.Panel();
            this.tabUpdate = new System.Windows.Forms.TabControl();
            this.tbNon_MCGM_Batch = new System.Windows.Forms.TabPage();
            this.dgvSearch_Non_MCGM = new System.Windows.Forms.DataGridView();
            this.tbMCGM_Batch = new System.Windows.Forms.TabPage();
            this.dgvSearch_MCGM = new System.Windows.Forms.DataGridView();
            this.btnMaster = new System.Windows.Forms.Button();
            this.grpControls.SuspendLayout();
            this.pnlFilterUploadedData.SuspendLayout();
            this.pnlTbMcgmBatch.SuspendLayout();
            this.pnlGrid.SuspendLayout();
            this.tabUpdate.SuspendLayout();
            this.tbNon_MCGM_Batch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSearch_Non_MCGM)).BeginInit();
            this.tbMCGM_Batch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSearch_MCGM)).BeginInit();
            this.SuspendLayout();
            // 
            // grpControls
            // 
            this.grpControls.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grpControls.Controls.Add(this.label2);
            this.grpControls.Controls.Add(this.pnlFilterUploadedData);
            this.grpControls.Controls.Add(this.pnlTbMcgmBatch);
            this.grpControls.Controls.Add(this.dtpFromDate);
            this.grpControls.Controls.Add(this.lblFromDate);
            this.grpControls.Controls.Add(this.txtToBatchNo);
            this.grpControls.Controls.Add(this.lblToBatchNo);
            this.grpControls.Controls.Add(this.btnClear);
            this.grpControls.Controls.Add(this.txtFromBatchNo);
            this.grpControls.Controls.Add(this.lblFromBatchNo);
            this.grpControls.Controls.Add(this.lblRecordCount);
            this.grpControls.Controls.Add(this.btnRefresh);
            this.grpControls.ForeColor = System.Drawing.Color.Black;
            this.grpControls.Location = new System.Drawing.Point(12, 12);
            this.grpControls.Name = "grpControls";
            this.grpControls.Size = new System.Drawing.Size(1126, 108);
            this.grpControls.TabIndex = 0;
            this.grpControls.TabStop = false;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.Highlight;
            this.label2.Location = new System.Drawing.Point(9, 88);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(840, 17);
            this.label2.TabIndex = 13;
            this.label2.Tag = "";
            this.label2.Text = "Note: If not any record found in filtered condition then current date\'s records w" +
    "ill be shown.";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pnlFilterUploadedData
            // 
            this.pnlFilterUploadedData.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlFilterUploadedData.Controls.Add(this.radioButton3);
            this.pnlFilterUploadedData.Controls.Add(this.radioButton2);
            this.pnlFilterUploadedData.Controls.Add(this.radioButton1);
            this.pnlFilterUploadedData.Location = new System.Drawing.Point(744, 12);
            this.pnlFilterUploadedData.Name = "pnlFilterUploadedData";
            this.pnlFilterUploadedData.Size = new System.Drawing.Size(163, 69);
            this.pnlFilterUploadedData.TabIndex = 12;
            this.pnlFilterUploadedData.Visible = false;
            // 
            // radioButton3
            // 
            this.radioButton3.AutoSize = true;
            this.radioButton3.Checked = true;
            this.radioButton3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.radioButton3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.radioButton3.Location = new System.Drawing.Point(3, 49);
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.Size = new System.Drawing.Size(102, 17);
            this.radioButton3.TabIndex = 2;
            this.radioButton3.TabStop = true;
            this.radioButton3.Tag = "3";
            this.radioButton3.Text = "Show All Data";
            this.radioButton3.UseVisualStyleBackColor = true;
            this.radioButton3.CheckedChanged += new System.EventHandler(this.FilterUploadedGridData);
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.radioButton2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.radioButton2.Location = new System.Drawing.Point(3, 29);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(129, 17);
            this.radioButton2.TabIndex = 1;
            this.radioButton2.TabStop = true;
            this.radioButton2.Tag = "2";
            this.radioButton2.Text = "Show Private Data";
            this.radioButton2.UseVisualStyleBackColor = true;
            this.radioButton2.CheckedChanged += new System.EventHandler(this.FilterUploadedGridData);
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.radioButton1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.radioButton1.Location = new System.Drawing.Point(3, 6);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(159, 17);
            this.radioButton1.TabIndex = 0;
            this.radioButton1.TabStop = true;
            this.radioButton1.Tag = "1";
            this.radioButton1.Text = "Show Government Data";
            this.radioButton1.UseVisualStyleBackColor = true;
            this.radioButton1.CheckedChanged += new System.EventHandler(this.FilterUploadedGridData);
            // 
            // pnlTbMcgmBatch
            // 
            this.pnlTbMcgmBatch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlTbMcgmBatch.Controls.Add(this.label4);
            this.pnlTbMcgmBatch.Controls.Add(this.label1);
            this.pnlTbMcgmBatch.Location = new System.Drawing.Point(912, 12);
            this.pnlTbMcgmBatch.Name = "pnlTbMcgmBatch";
            this.pnlTbMcgmBatch.Size = new System.Drawing.Size(208, 69);
            this.pnlTbMcgmBatch.TabIndex = 6;
            this.pnlTbMcgmBatch.Visible = false;
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.White;
            this.label4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.label4.Location = new System.Drawing.Point(3, 40);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(199, 21);
            this.label4.TabIndex = 1;
            this.label4.Text = "Data for Private Constructor";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.LightGreen;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(3, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(199, 21);
            this.label1.TabIndex = 0;
            this.label1.Text = "Data for Government Constructor";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // dtpFromDate
            // 
            this.dtpFromDate.CalendarFont = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpFromDate.CustomFormat = "dd/MM/yyyy";
            this.dtpFromDate.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpFromDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpFromDate.Location = new System.Drawing.Point(490, 18);
            this.dtpFromDate.Name = "dtpFromDate";
            this.dtpFromDate.ShowCheckBox = true;
            this.dtpFromDate.Size = new System.Drawing.Size(123, 21);
            this.dtpFromDate.TabIndex = 5;
            // 
            // lblFromDate
            // 
            this.lblFromDate.AutoSize = true;
            this.lblFromDate.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblFromDate.Location = new System.Drawing.Point(431, 22);
            this.lblFromDate.Name = "lblFromDate";
            this.lblFromDate.Size = new System.Drawing.Size(43, 13);
            this.lblFromDate.TabIndex = 4;
            this.lblFromDate.Text = "Date : ";
            // 
            // txtToBatchNo
            // 
            this.txtToBatchNo.Location = new System.Drawing.Point(327, 18);
            this.txtToBatchNo.Name = "txtToBatchNo";
            this.txtToBatchNo.Size = new System.Drawing.Size(88, 21);
            this.txtToBatchNo.TabIndex = 3;
            // 
            // lblToBatchNo
            // 
            this.lblToBatchNo.AutoSize = true;
            this.lblToBatchNo.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblToBatchNo.Location = new System.Drawing.Point(229, 22);
            this.lblToBatchNo.Name = "lblToBatchNo";
            this.lblToBatchNo.Size = new System.Drawing.Size(82, 13);
            this.lblToBatchNo.TabIndex = 2;
            this.lblToBatchNo.Text = "To Serial No. :";
            // 
            // btnClear
            // 
            this.btnClear.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.btnClear.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnClear.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClear.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnClear.Location = new System.Drawing.Point(134, 50);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(110, 36);
            this.btnClear.TabIndex = 9;
            this.btnClear.Text = "Clear";
            this.btnClear.UseVisualStyleBackColor = false;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // txtFromBatchNo
            // 
            this.txtFromBatchNo.Location = new System.Drawing.Point(125, 18);
            this.txtFromBatchNo.Name = "txtFromBatchNo";
            this.txtFromBatchNo.Size = new System.Drawing.Size(88, 21);
            this.txtFromBatchNo.TabIndex = 1;
            // 
            // lblFromBatchNo
            // 
            this.lblFromBatchNo.AutoSize = true;
            this.lblFromBatchNo.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFromBatchNo.Location = new System.Drawing.Point(9, 22);
            this.lblFromBatchNo.Name = "lblFromBatchNo";
            this.lblFromBatchNo.Size = new System.Drawing.Size(100, 13);
            this.lblFromBatchNo.TabIndex = 0;
            this.lblFromBatchNo.Text = "From Serial No. : ";
            // 
            // lblRecordCount
            // 
            this.lblRecordCount.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRecordCount.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.lblRecordCount.Location = new System.Drawing.Point(250, 57);
            this.lblRecordCount.Name = "lblRecordCount";
            this.lblRecordCount.Size = new System.Drawing.Size(363, 22);
            this.lblRecordCount.TabIndex = 11;
            this.lblRecordCount.Tag = "No. of Records : {0} , Filtered Records : {1}";
            this.lblRecordCount.Text = "No. of Records : {0} , Filtered Records : {1}";
            this.lblRecordCount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnRefresh
            // 
            this.btnRefresh.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.btnRefresh.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnRefresh.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRefresh.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnRefresh.Location = new System.Drawing.Point(9, 50);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(110, 36);
            this.btnRefresh.TabIndex = 8;
            this.btnRefresh.Text = "Fetch Data";
            this.btnRefresh.UseVisualStyleBackColor = false;
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // btnExit
            // 
            this.btnExit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExit.BackColor = System.Drawing.Color.Red;
            this.btnExit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnExit.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.ForeColor = System.Drawing.Color.White;
            this.btnExit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExit.Location = new System.Drawing.Point(1150, 15);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(110, 36);
            this.btnExit.TabIndex = 10;
            this.btnExit.Text = "G&o Back";
            this.btnExit.UseVisualStyleBackColor = false;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // pnlGrid
            // 
            this.pnlGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlGrid.Controls.Add(this.tabUpdate);
            this.pnlGrid.Location = new System.Drawing.Point(12, 124);
            this.pnlGrid.Name = "pnlGrid";
            this.pnlGrid.Size = new System.Drawing.Size(1244, 530);
            this.pnlGrid.TabIndex = 1;
            // 
            // tabUpdate
            // 
            this.tabUpdate.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabUpdate.Appearance = System.Windows.Forms.TabAppearance.FlatButtons;
            this.tabUpdate.Controls.Add(this.tbNon_MCGM_Batch);
            this.tabUpdate.Controls.Add(this.tbMCGM_Batch);
            this.tabUpdate.DrawMode = System.Windows.Forms.TabDrawMode.OwnerDrawFixed;
            this.tabUpdate.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabUpdate.ItemSize = new System.Drawing.Size(245, 28);
            this.tabUpdate.Location = new System.Drawing.Point(4, 10);
            this.tabUpdate.Multiline = true;
            this.tabUpdate.Name = "tabUpdate";
            this.tabUpdate.SelectedIndex = 0;
            this.tabUpdate.Size = new System.Drawing.Size(1212, 517);
            this.tabUpdate.TabIndex = 0;
            this.tabUpdate.Tag = "";
            this.tabUpdate.DrawItem += new System.Windows.Forms.DrawItemEventHandler(this.tabUpdate_DrawItem);
            this.tabUpdate.Selected += new System.Windows.Forms.TabControlEventHandler(this.tabUpdate_Selected);
            this.tabUpdate.MouseMove += new System.Windows.Forms.MouseEventHandler(this.tabUpdate_MouseMove);
            // 
            // tbNon_MCGM_Batch
            // 
            this.tbNon_MCGM_Batch.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(138)))), ((int)(((byte)(212)))));
            this.tbNon_MCGM_Batch.Controls.Add(this.dgvSearch_Non_MCGM);
            this.tbNon_MCGM_Batch.Cursor = System.Windows.Forms.Cursors.Default;
            this.tbNon_MCGM_Batch.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbNon_MCGM_Batch.ForeColor = System.Drawing.Color.White;
            this.tbNon_MCGM_Batch.Location = new System.Drawing.Point(4, 32);
            this.tbNon_MCGM_Batch.Margin = new System.Windows.Forms.Padding(3, 3, 10, 3);
            this.tbNon_MCGM_Batch.Name = "tbNon_MCGM_Batch";
            this.tbNon_MCGM_Batch.Padding = new System.Windows.Forms.Padding(3, 3, 10, 3);
            this.tbNon_MCGM_Batch.Size = new System.Drawing.Size(1204, 481);
            this.tbNon_MCGM_Batch.TabIndex = 0;
            this.tbNon_MCGM_Batch.Text = "                  TO BE UPLOAD                  ";
            // 
            // dgvSearch_Non_MCGM
            // 
            this.dgvSearch_Non_MCGM.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvSearch_Non_MCGM.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvSearch_Non_MCGM.Location = new System.Drawing.Point(1, 1);
            this.dgvSearch_Non_MCGM.Name = "dgvSearch_Non_MCGM";
            this.dgvSearch_Non_MCGM.Size = new System.Drawing.Size(1202, 479);
            this.dgvSearch_Non_MCGM.StandardTab = true;
            this.dgvSearch_Non_MCGM.TabIndex = 0;
            this.dgvSearch_Non_MCGM.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvSearch_Non_MCGM_CellClick);
            this.dgvSearch_Non_MCGM.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dgvSearch_Non_MCGM_CellFormatting);
            this.dgvSearch_Non_MCGM.CellMouseEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvSearch_Non_MCGM_CellMouseEnter);
            // 
            // tbMCGM_Batch
            // 
            this.tbMCGM_Batch.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(92)))), ((int)(((byte)(156)))));
            this.tbMCGM_Batch.Controls.Add(this.dgvSearch_MCGM);
            this.tbMCGM_Batch.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.tbMCGM_Batch.ForeColor = System.Drawing.Color.White;
            this.tbMCGM_Batch.Location = new System.Drawing.Point(4, 32);
            this.tbMCGM_Batch.Name = "tbMCGM_Batch";
            this.tbMCGM_Batch.Padding = new System.Windows.Forms.Padding(10, 3, 50, 3);
            this.tbMCGM_Batch.Size = new System.Drawing.Size(1204, 481);
            this.tbMCGM_Batch.TabIndex = 1;
            this.tbMCGM_Batch.Text = "                  UPLOADED                  ";
            // 
            // dgvSearch_MCGM
            // 
            this.dgvSearch_MCGM.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvSearch_MCGM.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvSearch_MCGM.Location = new System.Drawing.Point(1, 1);
            this.dgvSearch_MCGM.Name = "dgvSearch_MCGM";
            this.dgvSearch_MCGM.Size = new System.Drawing.Size(1202, 479);
            this.dgvSearch_MCGM.TabIndex = 0;
            this.dgvSearch_MCGM.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dgvSearch_MCGM_CellFormatting);
            this.dgvSearch_MCGM.CellMouseEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvSearch_Non_MCGM_CellMouseEnter);
            // 
            // btnMaster
            // 
            this.btnMaster.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnMaster.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(138)))), ((int)(((byte)(212)))));
            this.btnMaster.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnMaster.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.btnMaster.ForeColor = System.Drawing.Color.White;
            this.btnMaster.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnMaster.Location = new System.Drawing.Point(1150, 53);
            this.btnMaster.Name = "btnMaster";
            this.btnMaster.Size = new System.Drawing.Size(110, 36);
            this.btnMaster.TabIndex = 11;
            this.btnMaster.Tag = "";
            this.btnMaster.Text = "Show &Masters";
            this.btnMaster.UseVisualStyleBackColor = false;
            this.btnMaster.Click += new System.EventHandler(this.btnMaster_Click);
            // 
            // frmListingBatch
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1268, 659);
            this.Controls.Add(this.btnMaster);
            this.Controls.Add(this.pnlGrid);
            this.Controls.Add(this.grpControls);
            this.Controls.Add(this.btnExit);
            this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MinimizeBox = false;
            this.Name = "frmListingBatch";
            this.Text = "Record Listing";
            this.Activated += new System.EventHandler(this.frmListingBatch_Activated);
            this.Load += new System.EventHandler(this.frmListingBatch_Load);
            this.grpControls.ResumeLayout(false);
            this.grpControls.PerformLayout();
            this.pnlFilterUploadedData.ResumeLayout(false);
            this.pnlFilterUploadedData.PerformLayout();
            this.pnlTbMcgmBatch.ResumeLayout(false);
            this.pnlGrid.ResumeLayout(false);
            this.tabUpdate.ResumeLayout(false);
            this.tbNon_MCGM_Batch.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvSearch_Non_MCGM)).EndInit();
            this.tbMCGM_Batch.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvSearch_MCGM)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grpControls;
        private System.Windows.Forms.Button btnRefresh;
        private System.Windows.Forms.DateTimePicker dtpFromDate;
        private System.Windows.Forms.Label lblFromDate;
        private System.Windows.Forms.TextBox txtToBatchNo;
        private System.Windows.Forms.Label lblToBatchNo;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.TextBox txtFromBatchNo;
        private System.Windows.Forms.Label lblFromBatchNo;
        private System.Windows.Forms.Label lblRecordCount;
        private System.Windows.Forms.Panel pnlGrid;
        private System.Windows.Forms.TabControl tabUpdate;
        private System.Windows.Forms.TabPage tbNon_MCGM_Batch;
        private System.Windows.Forms.DataGridView dgvSearch_Non_MCGM;
        private System.Windows.Forms.TabPage tbMCGM_Batch;
        private System.Windows.Forms.DataGridView dgvSearch_MCGM;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel pnlTbMcgmBatch;
        private System.Windows.Forms.Panel pnlFilterUploadedData;
        private System.Windows.Forms.RadioButton radioButton3;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.Label label2;
        internal System.Windows.Forms.Button btnMaster;
    }
}