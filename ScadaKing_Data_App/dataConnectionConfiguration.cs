﻿namespace ScadaKing_Data_App
{
    #region [ Class ]
    /// <summary>
    /// dataConnectionConfiguration class
    /// </summary>
    public static class dataConnectionConfiguration
    {
        /// <summary>
        /// source Connection string Variable
        /// </summary>
        private static string _plc_Source_Connection_String;

        /// <summary>
        /// Source Connection string OLE Variable
        /// </summary>
        private static string _source_Connection_String;

        /// <summary>
        /// Target Connection string OLE Variable
        /// </summary>
        private static string _target_Connection_String;

        /// <summary>
        /// Target Connection string OLE Live Variable
        /// </summary>
        private static string _target_Connection_String_Live;

        /// <summary>
        /// Target Connection string OLE MCGM Variable
        /// </summary>
        private static string _target_Connection_String_MCGM;

        /// <summary>
        /// Gets or set the Source Connection string
        /// </summary>
        public static string PLC_Source_Connection_String
        {
            get
            {
                return string.IsNullOrEmpty(_plc_Source_Connection_String) ? string.Empty : _plc_Source_Connection_String;
            }

            set
            {
                _plc_Source_Connection_String = string.IsNullOrEmpty(value) ? string.Empty : value;
            }
        }

        /// <summary>
        /// Gets or set the Source Connection OLE string
        /// </summary>
        public static string Source_Connection_String
        {
            get
            {
                return string.IsNullOrEmpty(_source_Connection_String) ? string.Empty : _source_Connection_String;
            }

            set
            {
                _source_Connection_String = string.IsNullOrEmpty(value) ? string.Empty : value;
            }
        }

        /// <summary>
        /// Gets or set the Target Connection OLE string
        /// </summary>
        public static string Target_Connection_String
        {
            get
            {
                return string.IsNullOrEmpty(_target_Connection_String) ? string.Empty : _target_Connection_String;
            }

            set
            {
                _target_Connection_String = string.IsNullOrEmpty(value) ? string.Empty : value;
            }
        }

        /// <summary>
        /// Gets or set the Target Connection OLE Live string
        /// </summary>
        public static string Target_Connection_String_Live
        {
            get
            {
                return string.IsNullOrEmpty(_target_Connection_String_Live) ? string.Empty : _target_Connection_String_Live;
            }

            set
            {
                _target_Connection_String_Live = string.IsNullOrEmpty(value) ? string.Empty : value;
            }
        }

        /// <summary>
        /// Gets or set the Target Connection OLE MCGM string
        /// </summary>
        public static string Target_Connection_String_MCGM
        {
            get
            {
                return string.IsNullOrEmpty(_target_Connection_String_MCGM) ? string.Empty : _target_Connection_String_MCGM;
            }

            set
            {
                _target_Connection_String_MCGM = string.IsNullOrEmpty(value) ? string.Empty : value;
            }
        }

    }
    #endregion [ Class ]
}