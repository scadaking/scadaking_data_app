﻿#region [ Using's ]
using ScadaKing_Data_App.Class;
using System;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Windows.Forms;
using System.Xml;


#endregion [ Using's ]

namespace ScadaKing_Data_App
{

    #region [ Enum's ]
    /// <summary>
    /// 
    /// </summary>
    internal enum LogMessageType
    {
        /// <summary>
        /// 
        /// </summary>
        Information = 0,

        /// <summary>
        /// 
        /// </summary>
        Error = 1,

        /// <summary>
        /// 
        /// </summary>
        Warning = 2,

        /// <summary>
        /// 
        /// </summary>
        UploadError = 3,

        /// <summary>
        /// 
        /// </summary>
        DownloadError = 4,

        /// <summary>
        /// 
        /// </summary>
        UploadComplete = 5,

        /// <summary>
        /// 
        /// </summary>
        DownloadComplete = 6,

        /// <summary>
        /// 
        /// </summary>
        Ping = 7
    }
    #endregion [ Enum's ]

    #region [ Class ]

    public class Global
    {
        /// <summary>
        /// for Encrypting the password key
        /// </summary>
        private static byte[] key = { };

        /// <summary>
        /// for Encrypting the password IV
        /// </summary>
        private static byte[] IV = { 18, 52, 86, 120, 144, 171, 205, 239 };

        /// <summary>
        /// Read the Connection string value from Config file and build Connection String
        /// </summary>
        internal static bool GetConnectionString()
        {
            try
            {
                string xml_File_Path = Program.CurrentApplicationPath + @"\Connection.xml";
                if (File.Exists(xml_File_Path) == false)
                {
                    WriteLog("GetConnectionString:", LogMessageType.Error, "File Not found");
                    return false;
                }

                XmlDocument objDocument = new XmlDocument();
                objDocument.Load(xml_File_Path);

                XmlElement rootxml;
                rootxml = objDocument.DocumentElement;

                #region xml
                Program.gsz_PLC_SourceConnectionString = string.Empty;

                string xmlName = string.Empty;
                foreach (XmlElement xmlchildNode in rootxml)
                {
                    if (xmlchildNode.Name.ToLower() == "source")
                    {
                        foreach (XmlNode xmlsubchild in xmlchildNode.ChildNodes)
                        {
                            xmlName = xmlsubchild.Name.ToLower();
                            if (xmlName == "sourceconnectionstring")
                            {//
                                Program.gszConnectionString = Convert.ToString(xmlsubchild.InnerText.Trim());
                            }
                            if (xmlName == "targetconnectionstring")
                            {//
                                Program.gszConnectionString = Convert.ToString(xmlsubchild.InnerText.Trim());
                            }

                            //if (Program.Config_Transfer_data_to_main_database)
                            //{
                            //    if (xmlName == "plc_connectionstring")
                            //    {
                            //        Program.gsz_PLC_SourceConnectionString = Convert.ToString(xmlsubchild.InnerText.Trim());
                            //    }
                            //}
                        }

                        dataConnectionConfiguration.Source_Connection_String = Program.gszConnectionString;
                        //if (Program.Config_Transfer_data_to_main_database)
                        //{
                        //    dataConnectionConfiguration.PLC_Source_Connection_String = Program.gsz_PLC_SourceConnectionString;
                        //}
                    }


                    if (xmlchildNode.Name.ToLower() == "target")
                    {
                        xmlName = string.Empty;
                        foreach (XmlNode xmlsubchild in xmlchildNode.ChildNodes)
                        {
                            xmlName = xmlsubchild.Name.ToLower();
                            if (xmlName == "sourceconnectionstring")
                            {
                                Program.gszConnectionString = Convert.ToString(xmlsubchild.InnerText.Trim());
                            }

                            if (xmlName == "targetconnectionstring")
                            {//
                                Program.gszConnectionString = Convert.ToString(xmlsubchild.InnerText.Trim());
                            }

                            if (xmlName == "targetconnectionstring_live")
                            {
                                Program.gszTargetConnectionString_live = Convert.ToString(xmlsubchild.InnerText.Trim());
                            }

                            if (xmlName == "targetconnectionstring_mcgm")
                            {
                                Program.gszTargetConnectionString_MCGM = Convert.ToString(xmlsubchild.InnerText.Trim());
                            }
                        }

                        dataConnectionConfiguration.Target_Connection_String = Program.gszConnectionString;
                        dataConnectionConfiguration.Target_Connection_String_Live = Program.gszTargetConnectionString_live;
                        dataConnectionConfiguration.Target_Connection_String_MCGM = Program.gszTargetConnectionString_MCGM;
                    }

                    if (xmlchildNode.Name.ToLower() == "syncstop")
                    {
                        Program.syncStop = ((Convert.ToString(xmlchildNode.InnerText).ToLower() == "yes") ? true : false);
                    }

                    if (xmlchildNode.Name.ToLower() == "writefile")
                    {
                        Program.writeFile = ((Convert.ToString(xmlchildNode.InnerText).ToLower() == "yes") ? true : false);
                    }
                }
                #endregion

            }
            catch (Exception ex)
            {
                WriteLog("Connection Error", LogMessageType.Error, ex.Message.ToString());
            }

            return true;
        }

        /// <summary>
        /// De crypt the Password
        /// </summary>
        /// <param name="stringToDecrypt">encrypted string</param>
        /// <returns>returns decrypted string</returns>
        private static string Decrypt(string stringToDecrypt)
        {
            byte[] inputByteArray = new byte[stringToDecrypt.Length];
            try
            {
                key = System.Text.Encoding.UTF8.GetBytes("ABCDEFGH");
                DESCryptoServiceProvider des = new DESCryptoServiceProvider();
                inputByteArray = Convert.FromBase64String(stringToDecrypt);
                MemoryStream ms = new MemoryStream();
                CryptoStream cs = new CryptoStream(ms, des.CreateDecryptor(key, IV), CryptoStreamMode.Write);
                cs.Write(inputByteArray, 0, inputByteArray.Length);
                cs.FlushFinalBlock();
                System.Text.Encoding encoding = System.Text.Encoding.UTF8;

                return encoding.GetString(ms.ToArray());
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        /// <summary>
        /// Write the Log 
        /// </summary>
        /// <param name="eventName">Event Name</param>
        /// <param name="messageType">Message Type</param>
        /// <param name="writeMessage">expection String</param>
        internal static void WriteLog(string eventName, LogMessageType messageType, string writeMessage)
        {
            WriteLog(eventName, messageType, writeMessage, false);
        }

        /// <summary>
        /// Write the Log 
        /// </summary>
        /// <param name="eventName">Event Name</param>
        /// <param name="messageType">Message Type</param>
        /// <param name="writeMessage">expection String</param>
        internal static void WriteLog(string eventName, LogMessageType messageType, string writeMessage, bool is_MCGM)
        {
            writeMessage = eventName.Trim() + " - " + writeMessage.Trim();
            LogEvent(writeMessage, messageType, is_MCGM);
        }

        /// <summary>
        /// Log Events
        /// </summary>
        /// <param name="message">Message string</param>
        /// <param name="messageType">Message type</param>
        private static void LogEvent(string message, LogMessageType messageType, bool is_MCGM)
        {
            string directoryPath = Path.Combine(Program.CurrentApplicationPath, "MCGM_LogInfo");
            if (!Directory.Exists(directoryPath))
            {
                Directory.CreateDirectory(directoryPath);
            }

            message = message.Replace("Access Database", "(A DB)");
            message = message.Replace("database", "DB");
            string sLog = Environment.NewLine + DateTime.Now.ToString("HH:mm:ss") + " " + message;

            if (messageType == LogMessageType.Information
                || messageType == LogMessageType.DownloadComplete
                || messageType == LogMessageType.UploadComplete)
            {
                string infoFilePath = string.Empty;

                infoFilePath = directoryPath + "\\MCGM_" + DateTime.Now.ToString("yyyy_MM_dd");
                if (is_MCGM)
                {
                    infoFilePath += "_MCGM";
                }

                infoFilePath += "_Service_Info.txt";

                if (File.Exists(infoFilePath))
                {
                    File.AppendAllText(infoFilePath, sLog, Encoding.ASCII);
                }
                else
                {
                    sLog = DateTime.Now.ToString("HH:mm:ss") + " " + message;
                    File.WriteAllText(infoFilePath, sLog, Encoding.ASCII);
                }
            }
            else
            {
                string logFilePath = string.Empty;
                logFilePath = directoryPath + "\\MCGM_" + DateTime.Now.ToString("yyyy_MM_dd");
                if (is_MCGM)
                {
                    logFilePath += "_MCGM";
                }
                logFilePath += "_Error_Log.txt";

                if (File.Exists(logFilePath))
                {
                    File.AppendAllText(logFilePath, sLog, Encoding.ASCII);
                }
                else
                {
                    sLog = DateTime.Now.ToString("HH:mm:ss") + " " + message;
                    File.WriteAllText(logFilePath, sLog, Encoding.ASCII);
                }
            }
        }
        [System.Runtime.InteropServices.DllImport("kernel32.dll")]
        public static extern bool SetProcessWorkingSetSize(IntPtr proc, int min, int max);
        public static void FlushMemory()
        {
            try
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
                if (Environment.OSVersion.Platform == PlatformID.Win32NT)
                {
                    SetProcessWorkingSetSize(System.Diagnostics.Process.GetCurrentProcess().Handle, -1, -1);
                }
            }
            catch
            {
            }
        }

        #region [CRAETE HTML FILE]
        public static string Create_RMC_HMTL_File(DataSet dtRMC, string batch_No)
        {
            string htmlDetails = string.Empty;
            string tab_1 = "\t";
            string tab_2 = "\t\t";
            string tab_3 = "\t\t\t";
            string tab_4 = "\t\t\t\t";
            string tab_5 = "\t\t\t\t\t";

            StringBuilder sb = new StringBuilder();

            DataTable dtBatch_Dat_Trans = dtRMC.Tables[0];
            DataTable dtBatch_Transaction = dtRMC.Tables[1];
            object PlantSlNO = null;
            string PlantSlNO_Value = "PLANTSLNO";
            if (Program.PlantAppVersion == "3")
            {
                //PlantSlNO = DbHelper.ExecuteScalar("select PlantSlNO from NameSetup where batch_no =" + Convert.ToInt64(batch_No), dataConnectionConfiguration.Source_Connection_String);
                PlantSlNO = DbHelper.ExecuteScalar("select " + PlantSlNO_Value + " from NameSetup", dataConnectionConfiguration.Source_Connection_String);
            }
            else
            {
                PlantSlNO = dtBatch_Dat_Trans.Rows[0]["PlantSlNO"].ToString();
            }

            string trHeight_Open = "<tr height='25px' class='trBgColor'>";
            string tr_Close = "</tr>";
            string tdHeader_Open_Less_Than = "<td width='25%' id='";
            string td_Open_Less_Than = "<td id='";
            string td_Open_Greater_Than = "'>";
            string td_Close = "</td>";

            try
            {
                sb.AppendLine("<html>");
                {
                    sb.AppendLine(tab_1 + "<head><title>" + batch_No + "</title></head>");
                    sb.AppendLine(tab_1 + "<style>.trBgColor{background-color:#FFFFFF;}</style>");

                    #region [ Body ]
                    sb.AppendLine(tab_1 + "<body>");
                    {
                        string head_Name = dtBatch_Dat_Trans.Rows[0]["ORGANIZATION_NAME"].ToString() == string.Empty
                            ? Program.Organization_Name
                            : dtBatch_Dat_Trans.Rows[0]["ORGANIZATION_NAME"].ToString() + " SCADA Application";

                        sb.AppendLine(tab_2 + "<div align='left'>");
                        sb.AppendLine(tab_3 + "<p align='center'><font face='MS Sans Serif'><strong>" + head_Name + "</strong></font></p>");
                        sb.AppendLine(tab_3 + "<p align='center'><strong><u>Autographic/Batch Report/Delivery Note</u></strong></p>");
                        sb.AppendLine(tab_2 + "</div>");

                        #region [ Header Div ]
                        sb.AppendLine(tab_2 + "<div align='left' style='padding-top:0px;background-color:#000;'>");
                        {
                            sb.AppendLine(tab_3 + "<table border='0' cellspacing='1' width='100%'>");
                            {
                                sb.AppendLine(tab_4 + trHeight_Open);
                                {
                                    string[] formats = {"dd/MM/yyyy", "dd-MMM-yyyy", "yyyy-MM-dd","dd-MM-yyyy", "M/d/yyyy", "dd MMM yyyy"};
                                    
                                    string converted = DateTime.ParseExact(dtBatch_Dat_Trans.Rows[0]["Batch_Date"].ToString(), formats, System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None).ToString(Program.Date_Format);

                                    sb.AppendLine(tab_5
                                        + tdHeader_Open_Less_Than + "Batch_Date_Header" + td_Open_Greater_Than + "Batch Date:" + td_Close
                                         + tdHeader_Open_Less_Than + "Batch_Date_Value" + td_Open_Greater_Than + converted + td_Close);
                                    //+ tdHeader_Open_Less_Than + "Batch_Date_Value" + td_Open_Greater_Than + Convert.ToDateTime(dtBatch_Dat_Trans.Rows[0]["Batch_Date"]).ToString(Program.Date_Format) + td_Close); 

                                    //+ tdHeader_Open_Less_Than + "Batch_Date_Value" + td_Open_Greater_Than + dtBatch_Dat_Trans.Rows[0]["Batch_Date"].ToString() + td_Close); // ok

                                    //+ tdHeader_Open_Less_Than + "Batch_Date_Value" + td_Open_Greater_Than + converted + td_Close);

                                    //+ tdHeader_Open_Less_Than + "Batch_Date_Value" + td_Open_Greater_Than + Convert.ToDateTime(dtBatch_Dat_Trans.Rows[0]["Batch_Date"].ToString()).ToString("dd/MM/yyyy") + td_Close);

                                    sb.AppendLine(tab_5
                                        + tdHeader_Open_Less_Than + "Plant_Serial_No_Header" + td_Open_Greater_Than + "Plant Serial No:" + td_Close
                                        //+ tdHeader_Open_Less_Than + "Plant_Serial_No_Value" + td_Open_Greater_Than + dtBatch_Dat_Trans.Rows[0]["PlantSlNO"].ToString() + td_Close);
                                        + tdHeader_Open_Less_Than + "Plant_Serial_No_Value" + td_Open_Greater_Than + PlantSlNO + td_Close);
                                }
                                sb.AppendLine(tab_4 + tr_Close);

                                sb.AppendLine(tab_4 + trHeight_Open);
                                {
                                    sb.AppendLine(tab_5
                                       + tdHeader_Open_Less_Than + "Batch_Start_Time_Header" + td_Open_Greater_Than + "Batch Start Time:" + td_Close
                                       + tdHeader_Open_Less_Than + "Batch_Start_Time_Value" + td_Open_Greater_Than + dtBatch_Dat_Trans.Rows[0]["Batch_Start_Time"].ToString() + td_Close);
                                    sb.AppendLine(tab_5
                                        + tdHeader_Open_Less_Than + "Batch_End_Time_Header" + td_Open_Greater_Than + "Batch End Time:" + td_Close
                                        + tdHeader_Open_Less_Than + "Batch_End_Time_Value" + td_Open_Greater_Than + dtBatch_Dat_Trans.Rows[0]["Batch_End_Time"].ToString() + td_Close);
                                }
                                sb.AppendLine(tab_4 + tr_Close);

                                sb.AppendLine(tab_4 + trHeight_Open);
                                {
                                    string plant_App_Version = Program.PlantAppVersion;

                                    if (
                                        plant_App_Version == "3"
                                        || plant_App_Version == "3.0"
                                        || plant_App_Version == "4.0"
                                        || plant_App_Version == "6"
                                        || plant_App_Version == "6.0"
                                        || plant_App_Version == "6.2"
                                        )
                                    {
                                        plant_App_Version = "3";
                                    }

                                    sb.AppendLine(tab_5
                                    + tdHeader_Open_Less_Than + "Version_Header" + td_Open_Greater_Than + "Version:" + td_Close
                                    + tdHeader_Open_Less_Than + "Version_Value" + td_Open_Greater_Than + plant_App_Version + td_Close);
                                    sb.AppendLine(tab_5
                                        + tdHeader_Open_Less_Than + "Batcher_Name_Header" + td_Open_Greater_Than + "Batcher Name:" + td_Close
                                        + tdHeader_Open_Less_Than + "Batcher_Name_Value" + td_Open_Greater_Than + dtBatch_Dat_Trans.Rows[0]["Batcher_Name"].ToString() + td_Close);
                                }
                                sb.AppendLine(tab_4 + tr_Close);

                                sb.AppendLine(tab_4 + trHeight_Open);
                                {
                                    sb.AppendLine(tab_5
                                        + tdHeader_Open_Less_Than + "Batch_No_Header" + td_Open_Greater_Than + "Batch No:" + td_Close
                                        + tdHeader_Open_Less_Than + "Batch_No_Value" + td_Open_Greater_Than + batch_No + td_Close);

                                    sb.AppendLine(tab_5
                                        + tdHeader_Open_Less_Than + "Truck_Driver_Header" + td_Open_Greater_Than + "Truck Driver:" + td_Close
                                        + tdHeader_Open_Less_Than + "Truck_Driver_Value" + td_Open_Greater_Than + dtBatch_Dat_Trans.Rows[0]["Truck_Driver"].ToString() + td_Close);
                                }
                                sb.AppendLine(tab_4 + tr_Close);

                                sb.AppendLine(tab_4 + trHeight_Open);
                                {
                                    sb.AppendLine(tab_5
                                        + tdHeader_Open_Less_Than + "_Header" + td_Open_Greater_Than + "Customer:" + td_Close
                                        + tdHeader_Open_Less_Than + "_Value" + td_Open_Greater_Than + dtBatch_Dat_Trans.Rows[0]["CUSTOMER_NAME"].ToString() + td_Close);
                                    sb.AppendLine(tab_5
                                        + tdHeader_Open_Less_Than + "Production_Header" + td_Open_Greater_Than + "Production:" + td_Close
                                        + tdHeader_Open_Less_Than + "Production_Value" + td_Open_Greater_Than + dtBatch_Dat_Trans.Rows[0]["Production_Qty"].ToString() + td_Close);
                                }
                                sb.AppendLine(tab_4 + tr_Close);

                                sb.AppendLine(tab_4 + trHeight_Open);
                                {
                                    sb.AppendLine(tab_5
                                        + tdHeader_Open_Less_Than + "Site_Address_Header" + td_Open_Greater_Than + "Site Address:" + td_Close
                                        + tdHeader_Open_Less_Than + "Site_Address_Value" + td_Open_Greater_Than + dtBatch_Dat_Trans.Rows[0]["Site"].ToString() + td_Close);
                                    sb.AppendLine(tab_5
                                        + tdHeader_Open_Less_Than + "Returned_Header" + td_Open_Greater_Than + "Returned:" + td_Close
                                        + tdHeader_Open_Less_Than + "Returned_Value" + td_Open_Greater_Than + dtBatch_Dat_Trans.Rows[0]["Returned_Qty"].ToString() + td_Close);
                                }
                                sb.AppendLine(tab_4 + tr_Close);

                                sb.AppendLine(tab_4 + trHeight_Open);
                                {
                                    sb.AppendLine(tab_5
                                        + tdHeader_Open_Less_Than + "Recipe_Code_Header" + td_Open_Greater_Than + "Recipe Code:" + td_Close
                                        + tdHeader_Open_Less_Than + "Recipe_Code_Value" + td_Open_Greater_Than + dtBatch_Dat_Trans.Rows[0]["Recipe_Code"].ToString() + td_Close);
                                    sb.AppendLine(tab_5
                                        + tdHeader_Open_Less_Than + "Total_Order_Header" + td_Open_Greater_Than + "Total Order:" + td_Close
                                        + tdHeader_Open_Less_Than + "Total_Order_Value" + td_Open_Greater_Than + dtBatch_Dat_Trans.Rows[0]["Ordered_Qty"].ToString() + td_Close);
                                }
                                sb.AppendLine(tab_4 + tr_Close);

                                sb.AppendLine(tab_4 + trHeight_Open);
                                {
                                    sb.AppendLine(tab_5
                                    + tdHeader_Open_Less_Than + "Recipe_Name_Header" + td_Open_Greater_Than + "Recipe Name:" + td_Close
                                    + tdHeader_Open_Less_Than + "Recipe_Name_Value" + td_Open_Greater_Than + dtBatch_Dat_Trans.Rows[0]["Recipe_Name"].ToString() + td_Close);
                                    sb.AppendLine(tab_5
                                        + tdHeader_Open_Less_Than + "With_This_Load_Header" + td_Open_Greater_Than + "With This Load:" + td_Close
                                        + tdHeader_Open_Less_Than + "With_This_Load_Value" + td_Open_Greater_Than + dtBatch_Dat_Trans.Rows[0]["WithThisLoad"].ToString() + td_Close);
                                }
                                sb.AppendLine(tab_4 + tr_Close);

                                sb.AppendLine(tab_4 + trHeight_Open);
                                {
                                    sb.AppendLine(tab_5
                                        + tdHeader_Open_Less_Than + "Truck_No_Header" + td_Open_Greater_Than + "Truck No:" + td_Close
                                        + tdHeader_Open_Less_Than + "Truck_No_Value" + td_Open_Greater_Than + dtBatch_Dat_Trans.Rows[0]["Truck_No"].ToString() + td_Close);
                                    sb.AppendLine(tab_5
                                        + tdHeader_Open_Less_Than + "Batch_Size_Header" + td_Open_Greater_Than + "Batch Size:" + td_Close
                                        + tdHeader_Open_Less_Than + "Batch_Size_Value" + td_Open_Greater_Than + dtBatch_Dat_Trans.Rows[0]["Batch_Size"].ToString() + td_Close);
                                }
                                sb.AppendLine(tab_4 + tr_Close);
                            }
                            sb.AppendLine(tab_3 + "</table>");
                        }
                        sb.AppendLine(tab_2 + "</div>");

                        #endregion [ Header Div ]

                        sb.AppendLine(tab_2 + "<br></br>");

                        #region [ Listing Div ]
                        sb.AppendLine(tab_2 + "<div align='left' style='padding-top:0px;background-color:#000;'>");
                        {
                            sb.AppendLine(tab_3 + "<table border='0' cellspacing='1' width='100%'>");
                            {
                                int row_Index = 0;
                                int row_Count = dtBatch_Transaction.Rows.Count;
                                int column_Index = 0;
                                int column_Count = dtBatch_Transaction.Columns.Count;
                                int column_Start_Index = 0;

                                for (row_Index = 0; row_Index < row_Count; row_Index++)
                                {
                                    sb.AppendLine(tab_4 + trHeight_Open);

                                    if (row_Index == 0)
                                    {
                                        sb.AppendLine(tab_5 + td_Open_Less_Than + dtBatch_Transaction.Columns[0].ColumnName + "_" + row_Index + "_0" + td_Open_Greater_Than + string.Empty + td_Close);
                                    }
                                    else
                                    {
                                        if (Program.hideDateFromRecipe == "1")
                                        {
                                            if (dtBatch_Transaction.Rows[row_Index][0].ToString().Contains(":"))
                                            {
                                                sb.AppendLine(tab_5 + td_Open_Less_Than + dtBatch_Transaction.Columns[0].ColumnName + "_" + row_Index + "_0" + td_Open_Greater_Than + "" + td_Close);
                                            }
                                            else
                                            {
                                                sb.AppendLine(tab_5 + td_Open_Less_Than + dtBatch_Transaction.Columns[0].ColumnName + "_" + row_Index + "_0" + td_Open_Greater_Than + dtBatch_Transaction.Rows[row_Index][0] + td_Close);
                                            }
                                        }
                                        else
                                        {
                                            sb.AppendLine(tab_5 + td_Open_Less_Than + dtBatch_Transaction.Columns[0].ColumnName + "_" + row_Index + "_0" + td_Open_Greater_Than + dtBatch_Transaction.Rows[row_Index][0] + td_Close);
                                        }                                        
                                        //sb.AppendLine(tab_5 + td_Open_Less_Than + dtBatch_Transaction.Columns[0].ColumnName + "_" + row_Index + "_0" + td_Open_Greater_Than + dtBatch_Transaction.Rows[row_Index][0] + td_Close);

                                    }
                                    column_Start_Index = 2;

                                    for (column_Index = column_Start_Index; column_Index < column_Count; column_Index++)
                                    {
                                        string column_Value = "";


                                        if (
                                                dtBatch_Transaction.Columns[column_Index].ColumnName == "Admixture1"
                                            || dtBatch_Transaction.Columns[column_Index].ColumnName == "Admixture2"
                                            || dtBatch_Transaction.Columns[column_Index].ColumnName == "Admixture3"
                                            || dtBatch_Transaction.Columns[column_Index].ColumnName == "Admixture4")
                                        {
                                            if (row_Index > 0)
                                            {
                                                column_Value = Math.Round(
                                                    General.ConvertToDecimal(dtBatch_Transaction.Rows[row_Index][column_Index].ToString())
                                                    , 2).ToString(Program.Round_Digits);

                                                //column_Value = Math.Round(
                                                //    General.ConvertToDecimal(dtBatch_Transaction.Rows[row_Index][column_Index].ToString())
                                                //    , 2).ToString("0.00");

                                                //column_Value = Convert.ToString(Math.Round(Convert.ToDecimal(dtBatch_Transaction.Rows[row_Index][column_Index].ToString()), Convert.ToInt32(Program.Round_Digits)));
                                            }
                                            else
                                            {
                                                column_Value = dtBatch_Transaction.Rows[row_Index][column_Index].ToString();
                                            }
                                        }
                                        else
                                        {
                                            column_Value = dtBatch_Transaction.Rows[row_Index][column_Index].ToString();
                                        }

                                        sb.AppendLine(tab_5 + td_Open_Less_Than + dtBatch_Transaction.Columns[column_Index].ColumnName + "_" + row_Index + "_" + column_Index + td_Open_Greater_Than +
                                            column_Value
                                            + td_Close);
                                    }

                                    sb.AppendLine(tab_4 + tr_Close);
                                }
                            }

                            sb.AppendLine(tab_3 + "</table>");
                        }
                        sb.AppendLine(tab_2 + "</div>");
                        #endregion [ Listing Div ]
                    }
                    sb.AppendLine(tab_1 + "</body>");

                    #endregion [ Body ]

                }
                sb.AppendLine("</html>");
            }
            catch (Exception ex)
            {
                Global.WriteLog("Create_RMC_HMTL_File Exception", LogMessageType.Error, ex.Message);
            }

            return sb.ToString();
        }
        #endregion [CRAETE HTML FILE]
        #region [WRITE HTML FILE]
        /// <summary>
        /// 
        /// </summary>
        /// <param name="htmlDetails"></param>
        public static void Write_HTML_File(string htmlDetails, bool is_RMC, string batch_No, string file_Date)
        {
            string file_Name = string.Empty;
            file_Name = Application.StartupPath + @"\HTML\" + batch_No.ToString() + ".html";
            string file_Path = file_Name.Replace(Path.GetFileName(file_Name), "");
            if (!Directory.Exists(file_Path))
            {
                Directory.CreateDirectory(file_Path);
            }

            if (File.Exists(file_Name))
            {
                File.Delete(file_Name);
            }

            File.WriteAllText(file_Name, htmlDetails, Encoding.ASCII);

            string file_Name_Copy = string.Empty;
            string html_Path = Program.ConfigHTML_FILE_FOLDER_PATH.ToUpper();
            string directoryPath = string.IsNullOrEmpty(html_Path) ? Program.CurrentApplicationPath : html_Path;
            string directoryPath_Copy = directoryPath + "_Copy";

            if (string.IsNullOrEmpty(html_Path))
            {
                directoryPath = Path.Combine(directoryPath, "HTML", "Data");
                if (!Directory.Exists(directoryPath))
                {
                    Directory.CreateDirectory(directoryPath);
                }
            }

            if (!Directory.Exists(directoryPath))
            {
                Directory.CreateDirectory(directoryPath);
            }

            if (!Directory.Exists(directoryPath_Copy))
            {
                Directory.CreateDirectory(directoryPath_Copy);
            }

            if (is_RMC)
            {
                file_Name = directoryPath + "\\" + batch_No + ".html";
                file_Name_Copy = directoryPath_Copy + "\\" + batch_No + ".html";
            }
            else
            {
                if (string.IsNullOrEmpty(file_Date))
                {
                    file_Name = directoryPath + "\\" + DateTime.Now.ToString("yyyy_MM_dd") + ".html";
                    file_Name_Copy = directoryPath_Copy + "\\" + DateTime.Now.ToString("yyyy_MM_dd") + ".html";
                }
                else
                {
                    file_Name = directoryPath + "\\" + Convert.ToDateTime(file_Date).ToString("yyyy_MM_dd") + ".html";
                    file_Name_Copy = directoryPath_Copy + "\\" + Convert.ToDateTime(file_Date).ToString("yyyy_MM_dd") + ".html";
                }
            }

            if (File.Exists(file_Name_Copy))
            {
                File.Delete(file_Name_Copy);
            }

            if (File.Exists(file_Name))
            {
                File.Delete(file_Name);
            }

            File.WriteAllText(file_Name, htmlDetails, Encoding.ASCII);
            File.Copy(file_Name, file_Name_Copy);
            string file_Created_Date = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss");
            ScadaKing_Data_App.Controller.BatchDataController.Update_HTML_File_Creation_Status(batch_No, file_Created_Date);
        }
        #endregion [WRITE HTML FILE]

        /// <summary>
        /// 
        /// </summary>
        /// <param name="truck_Id"></param>
        #region [Truck Device Mapping Status]
        public static string DeviceRegistrationId = string.Empty;
        public static bool Validate_Device_Is_Mapped(string Truck_No)
        {
            bool returnResult = false;
            DeviceRegistrationId = string.Empty;
            try
            {
                if (string.IsNullOrEmpty(Truck_No) == true)
                {
                    string messageBoxTitle = "Alert!!!";
                    MessageBox.Show("Truck No" + Truck_No + " is not found.", messageBoxTitle);
                    returnResult = false;
                }
                else
                {
                    ScadaKing_Web_Service.TransactionHeader TransactionHeader = new ScadaKing_Web_Service.TransactionHeader();
                    TransactionHeader.UserName = Program.ConfigService_LoginID;
                    TransactionHeader.Password = Program.ConfigService_Password;
                    TransactionHeader.DeviceID = Program.ConfigMachine_Id;
                    TransactionHeader.Config_App_Name = Program.ConfigAPP_Name;
                    TransactionHeader.Config_App_Version = Program.ConfigAPP_Version;
                    TransactionHeader.Win_Service_Version = Program.Win_Service_Version;
                    TransactionHeader.Win_Service_GUID = Program.Win_Service_GUID;
                    ScadaKing_Web_Service.Response_UploadData response = new ScadaKing_Web_Service.Response_UploadData();
                    ScadaKing_Web_Service.ScadaKing_Web_ServiceSoapClient objService = new ScadaKing_Web_Service.ScadaKing_Web_ServiceSoapClient();
                    response = objService.Get_Device_Mapping_Records(TransactionHeader, Truck_No);
                    if (response.SyncResponseData.Tables.Count > 0)
                    {
                        DataTable dtValue = new DataTable();
                        dtValue = response.SyncResponseData.Tables[0];
                        if (dtValue.Rows.Count > 0)
                        {
                            if (Convert.ToBoolean(dtValue.Rows[0]["IS_ACTIVE"]) == true)
                            {
                                DeviceRegistrationId = dtValue.Rows[0]["DEVICE_REGISTRATION_ID"] == null ? "" : dtValue.Rows[0]["DEVICE_REGISTRATION_ID"].ToString();
                                returnResult = true;
                            }
                            else
                            {
                                Global.WriteLog("Global/Validate_Device_Is_Mapped", LogMessageType.Information, "Truck No:[" + Truck_No + "] is not active on live.");
                                string messageString = "Truck No:[" + Truck_No + "] is not active on Live...!!!";
                                string messageBoxTitle = "Scada King";
                                MessageBox.Show(messageString, messageBoxTitle);
                                returnResult = false;
                            }
                        }
                        else
                        {
                            string messageString = "Truck No:[" + Truck_No + "] is not mapped or found on Live...!!!";
                            string messageBoxTitle = "Scada King";
                            MessageBox.Show(messageString, messageBoxTitle);
                            returnResult = false;
                        }
                    }
                    else
                    {
                        Global.WriteLog("Validate_Device_Is_Mapped() Response Exception: ", LogMessageType.Error, response.ErrorMessage);
                        string messageString = "Truck No:" + Truck_No + " is not mapped or found on Live...!!!";
                        string messageBoxTitle = "Scada King";
                        MessageBox.Show(messageString, messageBoxTitle);
                        returnResult = false;
                    }
                }
            }
            catch (Exception ex)
            {
                returnResult = false;
                throw ex;
            }

            return returnResult;
        }
        #endregion[Truck Device Mapping Status]

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Truck_No"></param>
        /// <returns></returns>
        //#region [ Validate The Range(Radious of Vehicle) ]
        //public static bool Validate_Vehicle_Radius(string Truck_No, string BatchStartTime, string BatchEndTime)
        //{
        //    bool returnResult = false;
        //    try
        //    {
        //        if (string.IsNullOrEmpty(Truck_No) == true)
        //        {
        //            string messageBoxTitle = "Alert!!!";
        //            MessageBox.Show("Truck No" + Truck_No + " is not found.", messageBoxTitle);
        //        }
        //        else
        //        {
        //            ScadaKing_Web_Service.TransactionHeader TransactionHeader = new ScadaKing_Web_Service.TransactionHeader();
        //            TransactionHeader.UserName = Program.ConfigService_LoginID;
        //            TransactionHeader.Password = Program.ConfigService_Password;
        //            TransactionHeader.DeviceID = Program.ConfigMachine_Id;
        //            TransactionHeader.Config_App_Name = Program.ConfigAPP_Name;
        //            TransactionHeader.Config_App_Version = Program.ConfigAPP_Version;
        //            TransactionHeader.Win_Service_Version = Program.Win_Service_Version;
        //            TransactionHeader.Win_Service_GUID = Program.Win_Service_GUID;
        //            ScadaKing_Web_Service.Response_UploadData response = new ScadaKing_Web_Service.Response_UploadData();
        //            ScadaKing_Web_Service.ScadaKing_Web_ServiceSoapClient objService = new ScadaKing_Web_Service.ScadaKing_Web_ServiceSoapClient();
        //            response = objService.Validate_Radius_Vehicle(TransactionHeader, Truck_No, Convert.ToDateTime(BatchStartTime), Convert.ToDateTime(BatchEndTime));
        //            if (response.SyncResponseData.Tables.Count > 0)
        //            {
        //                DataTable dtValue = new DataTable();
        //                dtValue = response.SyncResponseData.Tables[0];
        //                if (dtValue.Rows.Count > 0)
        //                {
        //                    if (Convert.ToInt32(dtValue.Rows[0]["RESULT_CODE"]) == 100)
        //                    {
        //                        returnResult = true;
        //                    }
        //                    else
        //                    {
        //                        string Messagestring = "Truck No:[" + Truck_No + "]" + dtValue.Rows[0]["RETURN_MESSAGE"] + "";
        //                        string messageBoxTitle = "Scada King";
        //                        MessageBox.Show(Messagestring, messageBoxTitle);
        //                    }
        //                }
        //                else
        //                {
        //                    string Messagestring = "Truck No:" + Truck_No + " is not available or found in radius...!!!";
        //                    string messageBoxTitle = "Scada King";
        //                    MessageBox.Show(Messagestring, messageBoxTitle);
        //                }
        //            }
        //            else
        //            {
        //                Global.WriteLog("Validate_Vehicle_Radius() Response Exception: ", LogMessageType.Error, response.ErrorMessage);
        //                string Messagestring = "Truck No:" + Truck_No + " is not available or found in radius...!!!";
        //                string messageBoxTitle = "Scada King";
        //                MessageBox.Show(Messagestring, messageBoxTitle, MessageBoxButtons.OK, MessageBoxIcon.Error);
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        returnResult = false;
        //        throw ex;
        //    }
        //    return returnResult;
        //}
        //#endregion [ Validate The Range(Radious of Vehicle) ]

        #region [ Validate The Datetime Of Batch Date Time ]
        public static bool Validate_Batch_DateTime(string Truck_No, string BatchDateTime)
        {
            bool returnResult = true;
            try
            {
                if (string.IsNullOrEmpty(Truck_No) == true)
                {
                    string messageBoxTitle = "Alert!!!";
                    MessageBox.Show("Truck No" + Truck_No + " is not found.", messageBoxTitle);
                    returnResult = false;
                }
                else
                {
                    ScadaKing_Web_Service.TransactionHeader TransactionHeader = new ScadaKing_Web_Service.TransactionHeader();
                    TransactionHeader.UserName = Program.ConfigService_LoginID;
                    TransactionHeader.Password = Program.ConfigService_Password;
                    TransactionHeader.DeviceID = Program.ConfigMachine_Id;
                    TransactionHeader.Config_App_Name = Program.ConfigAPP_Name;
                    TransactionHeader.Config_App_Version = Program.ConfigAPP_Version;
                    TransactionHeader.Win_Service_Version = Program.Win_Service_Version;
                    TransactionHeader.Win_Service_GUID = Program.Win_Service_GUID;
                    ScadaKing_Web_Service.Response_UploadData response = new ScadaKing_Web_Service.Response_UploadData();
                    ScadaKing_Web_Service.ScadaKing_Web_ServiceSoapClient objService = new ScadaKing_Web_Service.ScadaKing_Web_ServiceSoapClient();
                    response = objService.Validate_Batch_Datetime(TransactionHeader, Truck_No, Program.PlantAppVersion, BatchDateTime);
                    if (response.SyncResponseData.Tables.Count > 0)
                    {
                        DataTable dtValue = new DataTable();
                        dtValue = response.SyncResponseData.Tables[0];
                        if (dtValue.Rows.Count > 0)
                        {
                            //Previous Batch Date Time
                            string PrevBatchDateTime = string.Empty;
                            string PrevMsgString = string.Empty;
                            string Prev_Batch_Date = dtValue.Rows[0]["PREV_BATCH_DATE"] == null ? string.Empty : dtValue.Rows[0]["PREV_BATCH_DATE"].ToString();
                            string Prev_Batch_Time = dtValue.Rows[0]["PREV_BATCH_TIME"] == null ? string.Empty : dtValue.Rows[0]["PREV_BATCH_TIME"].ToString();
                            if (Prev_Batch_Date.Trim().TrimEnd().TrimStart().Length > 0 && Prev_Batch_Time.Trim().TrimEnd().TrimStart().Length > 0)
                            {
                                PrevBatchDateTime = DateTime.Parse(Prev_Batch_Date).ToShortDateString() + " " + DateTime.Parse(Prev_Batch_Time).ToShortTimeString();
                                PrevMsgString = "Previous Batch Date Time is " + PrevBatchDateTime + "\n & Current Batch Date Time is " + BatchDateTime + "\n Do you want to Upload the Data?";
                            }
                            //Next Batch Date Time
                            string NextBatchDateTime = string.Empty;
                            string NextMsgString = string.Empty;
                            string Next_Batch_Date = dtValue.Rows[0]["NEXT_BATCH_DATE"] == null ? string.Empty : dtValue.Rows[0]["NEXT_BATCH_DATE"].ToString();
                            string Next_Batch_Time = dtValue.Rows[0]["NEXT_BATCH_TIME"] == null ? string.Empty : dtValue.Rows[0]["NEXT_BATCH_TIME"].ToString();
                            if (Next_Batch_Date.Trim().TrimEnd().TrimStart().Length > 0 && Next_Batch_Time.Trim().TrimEnd().TrimStart().Length > 0)
                            {
                                NextBatchDateTime = DateTime.Parse(Next_Batch_Date).ToShortDateString() + " " + DateTime.Parse(Next_Batch_Time).ToShortTimeString();
                                NextMsgString = "Next Batch Date Time is" + NextBatchDateTime + "\n & Current Batch Date Time is " + BatchDateTime + "\n Do you want to Upload the Data?";
                            }
                            string messageBoxTitle = "Scada King";
                            if (PrevMsgString.Trim().TrimEnd().TrimStart().Length > 0 && MessageBox.Show(PrevMsgString, messageBoxTitle, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                            {
                                returnResult = false;
                            }
                            if (NextMsgString.Trim().TrimEnd().TrimStart().Length > 0 && MessageBox.Show(NextMsgString, messageBoxTitle, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                            {
                                returnResult = false;
                            }
                        }
                        else
                        {
                            string MsgString = "No any batch time found on live for truck no : [ " + Truck_No + " ]";
                            string messageBoxTitle = "Scada King";
                            MessageBox.Show(MsgString, messageBoxTitle);
                        }
                    }
                    else
                    {
                        Global.WriteLog("Validate_Batch_DateTime() Response Exception: ", LogMessageType.Error, response.ErrorMessage);
                    }
                }
            }
            catch (Exception ex)
            {
                Global.WriteLog("Validate_Batch_DateTime() Application Exception: ", LogMessageType.Error, ex.Message);
            }
            return returnResult;
        }
        #endregion [ Validate The Datetime Of Batch Date Time ]



        #region [Fetch Data Validation Purpose]
        public static bool Validate_Truck_Numbers(string Truck_No, string BatchNo)
        {
            bool returnResult = false;
            string messageString = string.Empty;
            string messageBoxTitle = string.Empty;
            string Loc_TruckNo = string.Empty;
            string tmpBatchNo = BatchNo;
            Loc_TruckNo = Truck_No;
            try
            {
                if (string.IsNullOrEmpty(Truck_No) == true)
                {
                    messageBoxTitle = "Alert!!!";
                    MessageBox.Show("Truck No" + Truck_No + " is not found.", messageBoxTitle);
                }
                else
                {
                    ScadaKing_Web_Service.TransactionHeader TransactionHeader = new ScadaKing_Web_Service.TransactionHeader();
                    TransactionHeader.UserName = Program.ConfigService_LoginID;
                    TransactionHeader.Password = Program.ConfigService_Password;
                    TransactionHeader.DeviceID = Program.ConfigMachine_Id;
                    TransactionHeader.Config_App_Name = Program.ConfigAPP_Name;
                    TransactionHeader.Config_App_Version = Program.ConfigAPP_Version;
                    TransactionHeader.Win_Service_Version = Program.Win_Service_Version;
                    TransactionHeader.Win_Service_GUID = Program.Win_Service_GUID;
                    ScadaKing_Web_Service.Response_UploadData response = new ScadaKing_Web_Service.Response_UploadData();
                    ScadaKing_Web_Service.ScadaKing_Web_ServiceSoapClient objService = new ScadaKing_Web_Service.ScadaKing_Web_ServiceSoapClient();
                    response = objService.Fetch_Vehicle_Data_For_Validation(TransactionHeader, Truck_No);
                    if (response.SyncResponseData.Tables.Count > 0)
                    {
                        DataTable dtValue = new DataTable();
                        dtValue = response.SyncResponseData.Tables[0];

                        DataRow[] foundAuthors = dtValue.Select("TRUCK_NO = '" + Truck_No + "'");
                        if (foundAuthors.Length != 0)
                        {
                            returnResult = true;
                        }
                        else
                        {
                            if (dtValue.Rows.Count == 0)
                            {
                                messageString = "Truck No:[" + Truck_No + "] is not found for current Organization on Live...!!!";
                                messageBoxTitle = "Scada King";
                                MessageBox.Show(messageString, messageBoxTitle);
                            }
                            else
                            {
                                messageString = "Truck No: [ " + Truck_No + " ] is not matching in current Organization on Live, \n Do you want to edit the truck number [ " + Truck_No + " ] ...?";
                                messageBoxTitle = "Scada King";
                                if (MessageBox.Show(messageString, messageBoxTitle, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                                {
                                    frmChangeTruckNumber frmTruckNo = new frmChangeTruckNumber(Truck_No, dtValue, tmpBatchNo);
                                    if (frmTruckNo.ShowDialog() == DialogResult.OK)
                                    {
                                        returnResult = true;
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        Global.WriteLog("Validate_Truck_Numbers() Response Exception: ", LogMessageType.Error, response.ErrorMessage);
                        messageString = "Truck No:[" + Truck_No + "] is not found for current Organization on Live...!!!";
                        messageBoxTitle = "Scada King";
                        MessageBox.Show(messageString, messageBoxTitle);
                    }
                }
            }

            catch (Exception ex)
            {
                returnResult = false;
                throw ex;
            }

            return returnResult;
        }
        #endregion[Fetch Data Validation Purpose]

        #region
        public static bool Check_Last_Unloading_Batch_Status(string Truck_No)
        {
            string messageString = string.Empty;
            string messageBoxTitle = string.Empty;
            bool returnResult = false;
            try
            {
                if (string.IsNullOrEmpty(Truck_No) == true)
                {
                    messageBoxTitle = "Alert!!!";
                    MessageBox.Show("Truck No" + Truck_No + " is not found.", messageBoxTitle);
                }
                else
                {
                    ScadaKing_Web_Service.TransactionHeader TransactionHeader = new ScadaKing_Web_Service.TransactionHeader();
                    TransactionHeader.UserName = Program.ConfigService_LoginID;
                    TransactionHeader.Password = Program.ConfigService_Password;
                    TransactionHeader.DeviceID = Program.ConfigMachine_Id;
                    TransactionHeader.Config_App_Name = Program.ConfigAPP_Name;
                    TransactionHeader.Config_App_Version = Program.ConfigAPP_Version;
                    TransactionHeader.Win_Service_Version = Program.Win_Service_Version;
                    TransactionHeader.Win_Service_GUID = Program.Win_Service_GUID;
                    ScadaKing_Web_Service.Response_UploadData response = new ScadaKing_Web_Service.Response_UploadData();
                    ScadaKing_Web_Service.ScadaKing_Web_ServiceSoapClient objService = new ScadaKing_Web_Service.ScadaKing_Web_ServiceSoapClient();
                    response = objService.Check_Last_Batch_Unloading_Status(TransactionHeader, Truck_No);
                    if (response.SyncResponseData.Tables.Count > 0)
                    {
                        DataTable dtValue = new DataTable();
                        dtValue = response.SyncResponseData.Tables[0];
                        if (dtValue.Rows.Count > 0)
                        {
                            if (Convert.ToInt32(dtValue.Rows[0]["RESULT_CODE"]) == 100)
                            {
                                returnResult = true;
                            }
                            else
                            {
                                if (dtValue.Rows[0]["RESULT_CODE"] == null)
                                {
                                    messageString = "Previous batch unloading is not yet finished for [ " + Truck_No + " ]";
                                }
                                else
                                {
                                    messageString = dtValue.Rows[0]["RETURN_MESSAGE"].ToString();
                                }
                                messageBoxTitle = "Scada King";
                                MessageBox.Show(messageString, messageBoxTitle);
                            }
                        }
                    }
                    else
                    {
                        Global.WriteLog("Check_Last_Unloading_Batch_Status() Response Exception: ", LogMessageType.Error, response.ErrorMessage);
                        messageString = "Truck No:[" + Truck_No + "] is not found for current Organization on Live...!!!";
                        messageBoxTitle = "Scada King";
                        MessageBox.Show(messageString, messageBoxTitle, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            catch (Exception ex)
            {
                returnResult = false;
                throw ex;
            }
            return returnResult;
        }
        #endregion

        #region [Common method of Validation]
        public static bool Is_Validate(string Truck_No, string BatchDateTime, string BatchNo, string BatchStartTime, string BatchEndTime)
        {
            bool ReturnValue = true;
            try
            {
                if (Global.Validate_Device_Is_Mapped(Truck_No) == false)
                {
                    ReturnValue = false;
                }

                //if (Global.Validate_Vehicle_Radius(Truck_No, BatchStartTime, BatchEndTime) == false)
                //{
                //    ReturnValue = false;
                //}
                if (Global.Validate_Truck_Numbers(Truck_No, BatchNo) == false)
                {
                    ReturnValue = false;
                }
                if (Global.Check_Last_Unloading_Batch_Status(Truck_No) == false)
                {
                    ReturnValue = false;
                }
                if (Global.Validate_Batch_DateTime(Truck_No, BatchDateTime) == false)
                {
                    ReturnValue = false;
                }
            }
            catch (Exception ex)
            {
                ReturnValue = false;
                Global.WriteLog("Is_Validate Exception", LogMessageType.Error, ex.Message);
                throw ex;
            }
            return ReturnValue;
        }
        #endregion[Common method of Validation]
    }
    #endregion [ Class ]
}