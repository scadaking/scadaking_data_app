﻿using ScadaKing_Data_App.Class;
using System;
using System.Windows.Forms;

namespace ScadaKing_Data_App
{
    public partial class frmMain : Form
    {
        bool bExiting = false;
        public frmMain()
        {
            InitializeComponent();
        }

        private void btnShowMCGMData_Click(object sender, EventArgs e)
        {
            try
            {
                frmListingBatch frmListingBatch = new frmListingBatch();
                frmListingBatch.BatchType = 1;
                frmListingBatch.ShowDialog();
            }
            catch (Exception)
            {
                //General.ShowErrorMessage("Show Data:"+ex.Message);
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            try
            {
                frmMain_FormClosing(sender, new FormClosingEventArgs(CloseReason.ApplicationExitCall, false));
            }
            catch (Exception ex)
            {
                Global.WriteLog("btnExit_Click:", LogMessageType.Error, " Error: " + ex.Message);
            }
        }
        
        private void frmMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                if (bExiting == true)
                {
                    bExiting = false;
                    return;
                }
                if (General.ShowYesNoMessage("Do you want to Close the Application?", this.Text) == System.Windows.Forms.DialogResult.Yes)
                {
                    bExiting = true;
                   
                        System.Windows.Forms.Application.Exit();
                        System.Windows.Forms.Application.ExitThread();
                }
                else
                {
                    e.Cancel = true;
                }
            }
            catch (Exception ex)
            {
                General.ShowErrorMessage(ex.Message);
            }
        }

        private void frmMain_Load(object sender, EventArgs e)
        {

        }
    }
}
