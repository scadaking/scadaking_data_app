﻿namespace ScadaKing_Data_App
{
    partial class frmPreview
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.btnGenerateFile = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.btnResync = new System.Windows.Forms.Button();
            this.btnPreview_Batch = new System.Windows.Forms.Button();
            this.rdoMCGM = new System.Windows.Forms.RadioButton();
            this.rdoNonMCGM = new System.Windows.Forms.RadioButton();
            this.pnlWebBrowser = new System.Windows.Forms.Panel();
            this.wb1 = new System.Windows.Forms.WebBrowser();
            this.pnlHTMLGrid = new System.Windows.Forms.Panel();
            this.dgvHtmlHeader = new System.Windows.Forms.DataGridView();
            this.dgvHtmlSum = new System.Windows.Forms.DataGridView();
            this.dgvHtmlDetails = new System.Windows.Forms.DataGridView();
            this.txtSize = new System.Windows.Forms.TextBox();
            this.txtwith_This_Load = new System.Windows.Forms.TextBox();
            this.txtTotal_Order = new System.Windows.Forms.TextBox();
            this.txtReturned = new System.Windows.Forms.TextBox();
            this.txtProduction = new System.Windows.Forms.TextBox();
            this.txtVersion = new System.Windows.Forms.TextBox();
            this.lblVersion = new System.Windows.Forms.Label();
            this.txtTruck_Driver = new System.Windows.Forms.TextBox();
            this.txtBatcher_Name = new System.Windows.Forms.TextBox();
            this.txtEnd_Time = new System.Windows.Forms.TextBox();
            this.txtPlant_Serial_No = new System.Windows.Forms.TextBox();
            this.txtTruck_No = new System.Windows.Forms.TextBox();
            this.txtRecipe_Name = new System.Windows.Forms.TextBox();
            this.txtRecipe_Code = new System.Windows.Forms.TextBox();
            this.txtSite_Address = new System.Windows.Forms.TextBox();
            this.txtCustomer = new System.Windows.Forms.TextBox();
            this.txtBatch_No = new System.Windows.Forms.TextBox();
            this.txtStart_Time = new System.Windows.Forms.TextBox();
            this.txtBatch_Date = new System.Windows.Forms.TextBox();
            this.lbl_Batcher_Name = new System.Windows.Forms.Label();
            this.lblTotal_Order = new System.Windows.Forms.Label();
            this.lblWith_This_Load = new System.Windows.Forms.Label();
            this.lblSize = new System.Windows.Forms.Label();
            this.lblTruck_Driver = new System.Windows.Forms.Label();
            this.lbl_End_Time = new System.Windows.Forms.Label();
            this.lblProduction = new System.Windows.Forms.Label();
            this.lblReturned = new System.Windows.Forms.Label();
            this.lblPlant_Serial_No = new System.Windows.Forms.Label();
            this.lblRecipe_Code = new System.Windows.Forms.Label();
            this.lblRecipe_Name = new System.Windows.Forms.Label();
            this.lblTruck_No = new System.Windows.Forms.Label();
            this.lblBatch_No = new System.Windows.Forms.Label();
            this.lblStart_Time = new System.Windows.Forms.Label();
            this.lblCustomer = new System.Windows.Forms.Label();
            this.lblSite_Address = new System.Windows.Forms.Label();
            this.lblBatch_Date = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.pnlWebBrowser.SuspendLayout();
            this.pnlHTMLGrid.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvHtmlHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvHtmlSum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvHtmlDetails)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer1.IsSplitterFixed = true;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.btnUpdate);
            this.splitContainer1.Panel1.Controls.Add(this.btnGenerateFile);
            this.splitContainer1.Panel1.Controls.Add(this.btnExit);
            this.splitContainer1.Panel1.Controls.Add(this.btnResync);
            this.splitContainer1.Panel1.Controls.Add(this.btnPreview_Batch);
            this.splitContainer1.Panel1.Controls.Add(this.rdoMCGM);
            this.splitContainer1.Panel1.Controls.Add(this.rdoNonMCGM);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.pnlWebBrowser);
            this.splitContainer1.Panel2.Controls.Add(this.pnlHTMLGrid);
            this.splitContainer1.Size = new System.Drawing.Size(1268, 659);
            this.splitContainer1.SplitterDistance = 82;
            this.splitContainer1.TabIndex = 0;
            // 
            // btnUpdate
            // 
            this.btnUpdate.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.btnUpdate.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnUpdate.Enabled = false;
            this.btnUpdate.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpdate.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnUpdate.Location = new System.Drawing.Point(13, 6);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(260, 35);
            this.btnUpdate.TabIndex = 0;
            this.btnUpdate.Tag = "Re-Generate File";
            this.btnUpdate.Text = "Generate File && Upload Data";
            this.btnUpdate.UseVisualStyleBackColor = false;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // btnGenerateFile
            // 
            this.btnGenerateFile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnGenerateFile.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.btnGenerateFile.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnGenerateFile.Enabled = false;
            this.btnGenerateFile.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGenerateFile.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnGenerateFile.Location = new System.Drawing.Point(1030, 18);
            this.btnGenerateFile.Name = "btnGenerateFile";
            this.btnGenerateFile.Size = new System.Drawing.Size(110, 36);
            this.btnGenerateFile.TabIndex = 5;
            this.btnGenerateFile.Tag = "";
            this.btnGenerateFile.Text = "Generate File";
            this.btnGenerateFile.UseVisualStyleBackColor = false;
            this.btnGenerateFile.Visible = false;
            this.btnGenerateFile.Click += new System.EventHandler(this.btnGenerateFile_Click);
            // 
            // btnExit
            // 
            this.btnExit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExit.BackColor = System.Drawing.Color.Red;
            this.btnExit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnExit.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnExit.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.ForeColor = System.Drawing.Color.White;
            this.btnExit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExit.Location = new System.Drawing.Point(1146, 18);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(110, 36);
            this.btnExit.TabIndex = 6;
            this.btnExit.Text = "G&o Back";
            this.btnExit.UseVisualStyleBackColor = false;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnResync
            // 
            this.btnResync.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.btnResync.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnResync.Enabled = false;
            this.btnResync.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnResync.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnResync.Location = new System.Drawing.Point(146, 42);
            this.btnResync.Name = "btnResync";
            this.btnResync.Size = new System.Drawing.Size(127, 35);
            this.btnResync.TabIndex = 1;
            this.btnResync.Text = "R&e-Upload Data";
            this.btnResync.UseVisualStyleBackColor = false;
            this.btnResync.Visible = false;
            this.btnResync.Click += new System.EventHandler(this.btnResync_Click);
            // 
            // btnPreview_Batch
            // 
            this.btnPreview_Batch.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.btnPreview_Batch.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPreview_Batch.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPreview_Batch.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPreview_Batch.Location = new System.Drawing.Point(13, 42);
            this.btnPreview_Batch.Name = "btnPreview_Batch";
            this.btnPreview_Batch.Size = new System.Drawing.Size(127, 35);
            this.btnPreview_Batch.TabIndex = 2;
            this.btnPreview_Batch.Text = "Ref&resh";
            this.btnPreview_Batch.UseVisualStyleBackColor = false;
            this.btnPreview_Batch.Click += new System.EventHandler(this.btnPreview_Batch_Click);
            // 
            // rdoMCGM
            // 
            this.rdoMCGM.AutoSize = true;
            this.rdoMCGM.Checked = true;
            this.rdoMCGM.Cursor = System.Windows.Forms.Cursors.Hand;
            this.rdoMCGM.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdoMCGM.Location = new System.Drawing.Point(279, 14);
            this.rdoMCGM.Name = "rdoMCGM";
            this.rdoMCGM.Size = new System.Drawing.Size(223, 18);
            this.rdoMCGM.TabIndex = 3;
            this.rdoMCGM.TabStop = true;
            this.rdoMCGM.Tag = "2";
            this.rdoMCGM.Text = "File for Government Constructor";
            this.rdoMCGM.UseVisualStyleBackColor = true;
            this.rdoMCGM.CheckedChanged += new System.EventHandler(this.rdoSetMCGM_NonMCGM);
            // 
            // rdoNonMCGM
            // 
            this.rdoNonMCGM.AutoSize = true;
            this.rdoNonMCGM.Cursor = System.Windows.Forms.Cursors.Hand;
            this.rdoNonMCGM.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdoNonMCGM.Location = new System.Drawing.Point(279, 50);
            this.rdoNonMCGM.Name = "rdoNonMCGM";
            this.rdoNonMCGM.Size = new System.Drawing.Size(191, 18);
            this.rdoNonMCGM.TabIndex = 4;
            this.rdoNonMCGM.Tag = "1";
            this.rdoNonMCGM.Text = "File for Private Constructor";
            this.rdoNonMCGM.UseVisualStyleBackColor = true;
            this.rdoNonMCGM.CheckedChanged += new System.EventHandler(this.rdoSetMCGM_NonMCGM);
            // 
            // pnlWebBrowser
            // 
            this.pnlWebBrowser.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlWebBrowser.Controls.Add(this.wb1);
            this.pnlWebBrowser.Location = new System.Drawing.Point(0, 0);
            this.pnlWebBrowser.Name = "pnlWebBrowser";
            this.pnlWebBrowser.Size = new System.Drawing.Size(581, 571);
            this.pnlWebBrowser.TabIndex = 22;
            // 
            // wb1
            // 
            this.wb1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.wb1.Location = new System.Drawing.Point(0, 0);
            this.wb1.MinimumSize = new System.Drawing.Size(20, 20);
            this.wb1.Name = "wb1";
            this.wb1.Size = new System.Drawing.Size(581, 571);
            this.wb1.TabIndex = 0;
            // 
            // pnlHTMLGrid
            // 
            this.pnlHTMLGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlHTMLGrid.AutoScroll = true;
            this.pnlHTMLGrid.Controls.Add(this.dgvHtmlHeader);
            this.pnlHTMLGrid.Controls.Add(this.dgvHtmlSum);
            this.pnlHTMLGrid.Controls.Add(this.dgvHtmlDetails);
            this.pnlHTMLGrid.Controls.Add(this.txtSize);
            this.pnlHTMLGrid.Controls.Add(this.txtwith_This_Load);
            this.pnlHTMLGrid.Controls.Add(this.txtTotal_Order);
            this.pnlHTMLGrid.Controls.Add(this.txtReturned);
            this.pnlHTMLGrid.Controls.Add(this.txtProduction);
            this.pnlHTMLGrid.Controls.Add(this.txtVersion);
            this.pnlHTMLGrid.Controls.Add(this.lblVersion);
            this.pnlHTMLGrid.Controls.Add(this.txtTruck_Driver);
            this.pnlHTMLGrid.Controls.Add(this.txtBatcher_Name);
            this.pnlHTMLGrid.Controls.Add(this.txtEnd_Time);
            this.pnlHTMLGrid.Controls.Add(this.txtPlant_Serial_No);
            this.pnlHTMLGrid.Controls.Add(this.txtTruck_No);
            this.pnlHTMLGrid.Controls.Add(this.txtRecipe_Name);
            this.pnlHTMLGrid.Controls.Add(this.txtRecipe_Code);
            this.pnlHTMLGrid.Controls.Add(this.txtSite_Address);
            this.pnlHTMLGrid.Controls.Add(this.txtCustomer);
            this.pnlHTMLGrid.Controls.Add(this.txtBatch_No);
            this.pnlHTMLGrid.Controls.Add(this.txtStart_Time);
            this.pnlHTMLGrid.Controls.Add(this.txtBatch_Date);
            this.pnlHTMLGrid.Controls.Add(this.lbl_Batcher_Name);
            this.pnlHTMLGrid.Controls.Add(this.lblTotal_Order);
            this.pnlHTMLGrid.Controls.Add(this.lblWith_This_Load);
            this.pnlHTMLGrid.Controls.Add(this.lblSize);
            this.pnlHTMLGrid.Controls.Add(this.lblTruck_Driver);
            this.pnlHTMLGrid.Controls.Add(this.lbl_End_Time);
            this.pnlHTMLGrid.Controls.Add(this.lblProduction);
            this.pnlHTMLGrid.Controls.Add(this.lblReturned);
            this.pnlHTMLGrid.Controls.Add(this.lblPlant_Serial_No);
            this.pnlHTMLGrid.Controls.Add(this.lblRecipe_Code);
            this.pnlHTMLGrid.Controls.Add(this.lblRecipe_Name);
            this.pnlHTMLGrid.Controls.Add(this.lblTruck_No);
            this.pnlHTMLGrid.Controls.Add(this.lblBatch_No);
            this.pnlHTMLGrid.Controls.Add(this.lblStart_Time);
            this.pnlHTMLGrid.Controls.Add(this.lblCustomer);
            this.pnlHTMLGrid.Controls.Add(this.lblSite_Address);
            this.pnlHTMLGrid.Controls.Add(this.lblBatch_Date);
            this.pnlHTMLGrid.Location = new System.Drawing.Point(587, 3);
            this.pnlHTMLGrid.Name = "pnlHTMLGrid";
            this.pnlHTMLGrid.Size = new System.Drawing.Size(680, 565);
            this.pnlHTMLGrid.TabIndex = 0;
            // 
            // dgvHtmlHeader
            // 
            this.dgvHtmlHeader.AllowUserToAddRows = false;
            this.dgvHtmlHeader.AllowUserToDeleteRows = false;
            this.dgvHtmlHeader.AllowUserToResizeColumns = false;
            this.dgvHtmlHeader.AllowUserToResizeRows = false;
            this.dgvHtmlHeader.BackgroundColor = System.Drawing.Color.Cornsilk;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(176)))), ((int)(((byte)(156)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold);
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(176)))), ((int)(((byte)(156)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvHtmlHeader.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvHtmlHeader.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.LightBlue;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvHtmlHeader.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvHtmlHeader.EnableHeadersVisualStyles = false;
            this.dgvHtmlHeader.Location = new System.Drawing.Point(38, 213);
            this.dgvHtmlHeader.MultiSelect = false;
            this.dgvHtmlHeader.Name = "dgvHtmlHeader";
            this.dgvHtmlHeader.ReadOnly = true;
            this.dgvHtmlHeader.RowHeadersVisible = false;
            this.dgvHtmlHeader.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvHtmlHeader.Size = new System.Drawing.Size(1304, 99);
            this.dgvHtmlHeader.TabIndex = 131;
            this.dgvHtmlHeader.Scroll += new System.Windows.Forms.ScrollEventHandler(this.dgvHtmlDetails_Scroll);
            // 
            // dgvHtmlSum
            // 
            this.dgvHtmlSum.AllowUserToAddRows = false;
            this.dgvHtmlSum.AllowUserToDeleteRows = false;
            this.dgvHtmlSum.AllowUserToResizeColumns = false;
            this.dgvHtmlSum.AllowUserToResizeRows = false;
            this.dgvHtmlSum.BackgroundColor = System.Drawing.Color.Cornsilk;
            this.dgvHtmlSum.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvHtmlSum.ColumnHeadersVisible = false;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.LightBlue;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvHtmlSum.DefaultCellStyle = dataGridViewCellStyle3;
            this.dgvHtmlSum.Location = new System.Drawing.Point(38, 477);
            this.dgvHtmlSum.MultiSelect = false;
            this.dgvHtmlSum.Name = "dgvHtmlSum";
            this.dgvHtmlSum.ReadOnly = true;
            this.dgvHtmlSum.RowHeadersVisible = false;
            this.dgvHtmlSum.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvHtmlSum.Size = new System.Drawing.Size(1304, 64);
            this.dgvHtmlSum.TabIndex = 130;
            this.dgvHtmlSum.Scroll += new System.Windows.Forms.ScrollEventHandler(this.dgvHtmlDetails_Scroll);
            // 
            // dgvHtmlDetails
            // 
            this.dgvHtmlDetails.AllowUserToAddRows = false;
            this.dgvHtmlDetails.AllowUserToDeleteRows = false;
            this.dgvHtmlDetails.AllowUserToResizeColumns = false;
            this.dgvHtmlDetails.AllowUserToResizeRows = false;
            this.dgvHtmlDetails.BackgroundColor = System.Drawing.Color.Cornsilk;
            this.dgvHtmlDetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvHtmlDetails.ColumnHeadersVisible = false;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.LightBlue;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvHtmlDetails.DefaultCellStyle = dataGridViewCellStyle4;
            this.dgvHtmlDetails.Location = new System.Drawing.Point(38, 313);
            this.dgvHtmlDetails.MultiSelect = false;
            this.dgvHtmlDetails.Name = "dgvHtmlDetails";
            this.dgvHtmlDetails.ReadOnly = true;
            this.dgvHtmlDetails.RowHeadersVisible = false;
            this.dgvHtmlDetails.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvHtmlDetails.Size = new System.Drawing.Size(1304, 163);
            this.dgvHtmlDetails.TabIndex = 129;
            this.dgvHtmlDetails.Scroll += new System.Windows.Forms.ScrollEventHandler(this.dgvHtmlDetails_Scroll);
            // 
            // txtSize
            // 
            this.txtSize.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSize.Location = new System.Drawing.Point(662, 182);
            this.txtSize.Name = "txtSize";
            this.txtSize.Size = new System.Drawing.Size(150, 21);
            this.txtSize.TabIndex = 128;
            // 
            // txtwith_This_Load
            // 
            this.txtwith_This_Load.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtwith_This_Load.Location = new System.Drawing.Point(287, 182);
            this.txtwith_This_Load.Name = "txtwith_This_Load";
            this.txtwith_This_Load.Size = new System.Drawing.Size(150, 21);
            this.txtwith_This_Load.TabIndex = 127;
            // 
            // txtTotal_Order
            // 
            this.txtTotal_Order.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotal_Order.Location = new System.Drawing.Point(1066, 148);
            this.txtTotal_Order.Name = "txtTotal_Order";
            this.txtTotal_Order.Size = new System.Drawing.Size(150, 21);
            this.txtTotal_Order.TabIndex = 126;
            // 
            // txtReturned
            // 
            this.txtReturned.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtReturned.Location = new System.Drawing.Point(662, 148);
            this.txtReturned.Name = "txtReturned";
            this.txtReturned.Size = new System.Drawing.Size(150, 21);
            this.txtReturned.TabIndex = 125;
            // 
            // txtProduction
            // 
            this.txtProduction.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtProduction.Location = new System.Drawing.Point(287, 148);
            this.txtProduction.Name = "txtProduction";
            this.txtProduction.Size = new System.Drawing.Size(150, 21);
            this.txtProduction.TabIndex = 124;
            // 
            // txtVersion
            // 
            this.txtVersion.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVersion.Location = new System.Drawing.Point(1066, 182);
            this.txtVersion.Name = "txtVersion";
            this.txtVersion.Size = new System.Drawing.Size(150, 21);
            this.txtVersion.TabIndex = 113;
            this.txtVersion.Visible = false;
            // 
            // lblVersion
            // 
            this.lblVersion.AutoSize = true;
            this.lblVersion.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVersion.Location = new System.Drawing.Point(892, 186);
            this.lblVersion.Name = "lblVersion";
            this.lblVersion.Size = new System.Drawing.Size(55, 13);
            this.lblVersion.TabIndex = 98;
            this.lblVersion.Text = "Version :";
            this.lblVersion.Visible = false;
            // 
            // txtTruck_Driver
            // 
            this.txtTruck_Driver.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTruck_Driver.Location = new System.Drawing.Point(1066, 80);
            this.txtTruck_Driver.Name = "txtTruck_Driver";
            this.txtTruck_Driver.Size = new System.Drawing.Size(150, 21);
            this.txtTruck_Driver.TabIndex = 123;
            // 
            // txtBatcher_Name
            // 
            this.txtBatcher_Name.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBatcher_Name.Location = new System.Drawing.Point(1066, 46);
            this.txtBatcher_Name.Name = "txtBatcher_Name";
            this.txtBatcher_Name.Size = new System.Drawing.Size(150, 21);
            this.txtBatcher_Name.TabIndex = 122;
            // 
            // txtEnd_Time
            // 
            this.txtEnd_Time.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEnd_Time.Location = new System.Drawing.Point(662, 46);
            this.txtEnd_Time.Name = "txtEnd_Time";
            this.txtEnd_Time.Size = new System.Drawing.Size(150, 21);
            this.txtEnd_Time.TabIndex = 121;
            // 
            // txtPlant_Serial_No
            // 
            this.txtPlant_Serial_No.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPlant_Serial_No.Location = new System.Drawing.Point(1066, 12);
            this.txtPlant_Serial_No.Name = "txtPlant_Serial_No";
            this.txtPlant_Serial_No.Size = new System.Drawing.Size(150, 21);
            this.txtPlant_Serial_No.TabIndex = 120;
            // 
            // txtTruck_No
            // 
            this.txtTruck_No.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTruck_No.Location = new System.Drawing.Point(1066, 114);
            this.txtTruck_No.Name = "txtTruck_No";
            this.txtTruck_No.Size = new System.Drawing.Size(150, 21);
            this.txtTruck_No.TabIndex = 119;
            // 
            // txtRecipe_Name
            // 
            this.txtRecipe_Name.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRecipe_Name.Location = new System.Drawing.Point(662, 114);
            this.txtRecipe_Name.Name = "txtRecipe_Name";
            this.txtRecipe_Name.Size = new System.Drawing.Size(150, 21);
            this.txtRecipe_Name.TabIndex = 118;
            // 
            // txtRecipe_Code
            // 
            this.txtRecipe_Code.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRecipe_Code.Location = new System.Drawing.Point(287, 114);
            this.txtRecipe_Code.Name = "txtRecipe_Code";
            this.txtRecipe_Code.Size = new System.Drawing.Size(150, 21);
            this.txtRecipe_Code.TabIndex = 117;
            // 
            // txtSite_Address
            // 
            this.txtSite_Address.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSite_Address.Location = new System.Drawing.Point(662, 80);
            this.txtSite_Address.Name = "txtSite_Address";
            this.txtSite_Address.Size = new System.Drawing.Size(150, 21);
            this.txtSite_Address.TabIndex = 116;
            // 
            // txtCustomer
            // 
            this.txtCustomer.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCustomer.Location = new System.Drawing.Point(287, 80);
            this.txtCustomer.Name = "txtCustomer";
            this.txtCustomer.Size = new System.Drawing.Size(150, 21);
            this.txtCustomer.TabIndex = 115;
            // 
            // txtBatch_No
            // 
            this.txtBatch_No.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBatch_No.Location = new System.Drawing.Point(662, 17);
            this.txtBatch_No.Name = "txtBatch_No";
            this.txtBatch_No.Size = new System.Drawing.Size(150, 21);
            this.txtBatch_No.TabIndex = 114;
            // 
            // txtStart_Time
            // 
            this.txtStart_Time.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtStart_Time.Location = new System.Drawing.Point(287, 46);
            this.txtStart_Time.Name = "txtStart_Time";
            this.txtStart_Time.Size = new System.Drawing.Size(150, 21);
            this.txtStart_Time.TabIndex = 112;
            // 
            // txtBatch_Date
            // 
            this.txtBatch_Date.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBatch_Date.Location = new System.Drawing.Point(287, 12);
            this.txtBatch_Date.Name = "txtBatch_Date";
            this.txtBatch_Date.Size = new System.Drawing.Size(150, 21);
            this.txtBatch_Date.TabIndex = 111;
            // 
            // lbl_Batcher_Name
            // 
            this.lbl_Batcher_Name.AutoSize = true;
            this.lbl_Batcher_Name.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Batcher_Name.Location = new System.Drawing.Point(892, 50);
            this.lbl_Batcher_Name.Name = "lbl_Batcher_Name";
            this.lbl_Batcher_Name.Size = new System.Drawing.Size(92, 13);
            this.lbl_Batcher_Name.TabIndex = 110;
            this.lbl_Batcher_Name.Text = "Batcher Name :";
            // 
            // lblTotal_Order
            // 
            this.lblTotal_Order.AutoSize = true;
            this.lblTotal_Order.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotal_Order.Location = new System.Drawing.Point(892, 152);
            this.lblTotal_Order.Name = "lblTotal_Order";
            this.lblTotal_Order.Size = new System.Drawing.Size(77, 13);
            this.lblTotal_Order.TabIndex = 109;
            this.lblTotal_Order.Text = "Total Order :";
            // 
            // lblWith_This_Load
            // 
            this.lblWith_This_Load.AutoSize = true;
            this.lblWith_This_Load.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblWith_This_Load.Location = new System.Drawing.Point(132, 186);
            this.lblWith_This_Load.Name = "lblWith_This_Load";
            this.lblWith_This_Load.Size = new System.Drawing.Size(95, 13);
            this.lblWith_This_Load.TabIndex = 108;
            this.lblWith_This_Load.Text = "With This Load :";
            // 
            // lblSize
            // 
            this.lblSize.AutoSize = true;
            this.lblSize.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSize.Location = new System.Drawing.Point(517, 186);
            this.lblSize.Name = "lblSize";
            this.lblSize.Size = new System.Drawing.Size(36, 13);
            this.lblSize.TabIndex = 107;
            this.lblSize.Text = "Size :";
            // 
            // lblTruck_Driver
            // 
            this.lblTruck_Driver.AutoSize = true;
            this.lblTruck_Driver.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTruck_Driver.Location = new System.Drawing.Point(892, 84);
            this.lblTruck_Driver.Name = "lblTruck_Driver";
            this.lblTruck_Driver.Size = new System.Drawing.Size(83, 13);
            this.lblTruck_Driver.TabIndex = 106;
            this.lblTruck_Driver.Text = "Truck Driver :";
            // 
            // lbl_End_Time
            // 
            this.lbl_End_Time.AutoSize = true;
            this.lbl_End_Time.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_End_Time.Location = new System.Drawing.Point(517, 50);
            this.lbl_End_Time.Name = "lbl_End_Time";
            this.lbl_End_Time.Size = new System.Drawing.Size(64, 13);
            this.lbl_End_Time.TabIndex = 105;
            this.lbl_End_Time.Text = "End Time :";
            // 
            // lblProduction
            // 
            this.lblProduction.AutoSize = true;
            this.lblProduction.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblProduction.Location = new System.Drawing.Point(132, 152);
            this.lblProduction.Name = "lblProduction";
            this.lblProduction.Size = new System.Drawing.Size(74, 13);
            this.lblProduction.TabIndex = 104;
            this.lblProduction.Text = "Production :";
            // 
            // lblReturned
            // 
            this.lblReturned.AutoSize = true;
            this.lblReturned.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblReturned.Location = new System.Drawing.Point(517, 152);
            this.lblReturned.Name = "lblReturned";
            this.lblReturned.Size = new System.Drawing.Size(66, 13);
            this.lblReturned.TabIndex = 103;
            this.lblReturned.Text = "Returned :";
            // 
            // lblPlant_Serial_No
            // 
            this.lblPlant_Serial_No.AutoSize = true;
            this.lblPlant_Serial_No.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPlant_Serial_No.Location = new System.Drawing.Point(892, 15);
            this.lblPlant_Serial_No.Name = "lblPlant_Serial_No";
            this.lblPlant_Serial_No.Size = new System.Drawing.Size(94, 13);
            this.lblPlant_Serial_No.TabIndex = 102;
            this.lblPlant_Serial_No.Text = "Plant Serial No :";
            // 
            // lblRecipe_Code
            // 
            this.lblRecipe_Code.AutoSize = true;
            this.lblRecipe_Code.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRecipe_Code.Location = new System.Drawing.Point(132, 118);
            this.lblRecipe_Code.Name = "lblRecipe_Code";
            this.lblRecipe_Code.Size = new System.Drawing.Size(82, 13);
            this.lblRecipe_Code.TabIndex = 101;
            this.lblRecipe_Code.Text = "Recipe Code :";
            // 
            // lblRecipe_Name
            // 
            this.lblRecipe_Name.AutoSize = true;
            this.lblRecipe_Name.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRecipe_Name.Location = new System.Drawing.Point(517, 118);
            this.lblRecipe_Name.Name = "lblRecipe_Name";
            this.lblRecipe_Name.Size = new System.Drawing.Size(86, 13);
            this.lblRecipe_Name.TabIndex = 100;
            this.lblRecipe_Name.Text = "Recipe Name :";
            // 
            // lblTruck_No
            // 
            this.lblTruck_No.AutoSize = true;
            this.lblTruck_No.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTruck_No.Location = new System.Drawing.Point(892, 118);
            this.lblTruck_No.Name = "lblTruck_No";
            this.lblTruck_No.Size = new System.Drawing.Size(62, 13);
            this.lblTruck_No.TabIndex = 99;
            this.lblTruck_No.Text = "Truck No :";
            // 
            // lblBatch_No
            // 
            this.lblBatch_No.AutoSize = true;
            this.lblBatch_No.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBatch_No.Location = new System.Drawing.Point(517, 20);
            this.lblBatch_No.Name = "lblBatch_No";
            this.lblBatch_No.Size = new System.Drawing.Size(65, 13);
            this.lblBatch_No.TabIndex = 97;
            this.lblBatch_No.Text = "Batch No. :";
            // 
            // lblStart_Time
            // 
            this.lblStart_Time.AutoSize = true;
            this.lblStart_Time.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStart_Time.Location = new System.Drawing.Point(132, 50);
            this.lblStart_Time.Name = "lblStart_Time";
            this.lblStart_Time.Size = new System.Drawing.Size(73, 13);
            this.lblStart_Time.TabIndex = 96;
            this.lblStart_Time.Text = "Start Time :";
            // 
            // lblCustomer
            // 
            this.lblCustomer.AutoSize = true;
            this.lblCustomer.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCustomer.Location = new System.Drawing.Point(132, 84);
            this.lblCustomer.Name = "lblCustomer";
            this.lblCustomer.Size = new System.Drawing.Size(68, 13);
            this.lblCustomer.TabIndex = 95;
            this.lblCustomer.Text = "Customer :";
            // 
            // lblSite_Address
            // 
            this.lblSite_Address.AutoSize = true;
            this.lblSite_Address.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSite_Address.Location = new System.Drawing.Point(517, 84);
            this.lblSite_Address.Name = "lblSite_Address";
            this.lblSite_Address.Size = new System.Drawing.Size(84, 13);
            this.lblSite_Address.TabIndex = 94;
            this.lblSite_Address.Text = "Site Address :";
            // 
            // lblBatch_Date
            // 
            this.lblBatch_Date.AutoSize = true;
            this.lblBatch_Date.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBatch_Date.Location = new System.Drawing.Point(132, 15);
            this.lblBatch_Date.Name = "lblBatch_Date";
            this.lblBatch_Date.Size = new System.Drawing.Size(75, 13);
            this.lblBatch_Date.TabIndex = 93;
            this.lblBatch_Date.Text = "Batch Date :";
            // 
            // frmPreview
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1268, 659);
            this.Controls.Add(this.splitContainer1);
            this.Name = "frmPreview";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "HTML Preview";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmPreview_Load);
            this.Click += new System.EventHandler(this.frmPreview_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.pnlWebBrowser.ResumeLayout(false);
            this.pnlHTMLGrid.ResumeLayout(false);
            this.pnlHTMLGrid.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvHtmlHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvHtmlSum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvHtmlDetails)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Button btnGenerateFile;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Button btnResync;
        private System.Windows.Forms.Button btnPreview_Batch;
        private System.Windows.Forms.RadioButton rdoMCGM;
        private System.Windows.Forms.RadioButton rdoNonMCGM;
        private System.Windows.Forms.Panel pnlWebBrowser;
        private System.Windows.Forms.WebBrowser wb1;
        private System.Windows.Forms.Panel pnlHTMLGrid;
        private System.Windows.Forms.DataGridView dgvHtmlHeader;
        private System.Windows.Forms.DataGridView dgvHtmlSum;
        private System.Windows.Forms.DataGridView dgvHtmlDetails;
        private System.Windows.Forms.TextBox txtSize;
        private System.Windows.Forms.TextBox txtwith_This_Load;
        private System.Windows.Forms.TextBox txtTotal_Order;
        private System.Windows.Forms.TextBox txtReturned;
        private System.Windows.Forms.TextBox txtProduction;
        private System.Windows.Forms.TextBox txtVersion;
        private System.Windows.Forms.Label lblVersion;
        private System.Windows.Forms.TextBox txtTruck_Driver;
        private System.Windows.Forms.TextBox txtBatcher_Name;
        private System.Windows.Forms.TextBox txtEnd_Time;
        private System.Windows.Forms.TextBox txtPlant_Serial_No;
        private System.Windows.Forms.TextBox txtTruck_No;
        private System.Windows.Forms.TextBox txtRecipe_Name;
        private System.Windows.Forms.TextBox txtRecipe_Code;
        private System.Windows.Forms.TextBox txtSite_Address;
        private System.Windows.Forms.TextBox txtCustomer;
        private System.Windows.Forms.TextBox txtBatch_No;
        private System.Windows.Forms.TextBox txtStart_Time;
        private System.Windows.Forms.TextBox txtBatch_Date;
        private System.Windows.Forms.Label lbl_Batcher_Name;
        private System.Windows.Forms.Label lblTotal_Order;
        private System.Windows.Forms.Label lblWith_This_Load;
        private System.Windows.Forms.Label lblSize;
        private System.Windows.Forms.Label lblTruck_Driver;
        private System.Windows.Forms.Label lbl_End_Time;
        private System.Windows.Forms.Label lblProduction;
        private System.Windows.Forms.Label lblReturned;
        private System.Windows.Forms.Label lblPlant_Serial_No;
        private System.Windows.Forms.Label lblRecipe_Code;
        private System.Windows.Forms.Label lblRecipe_Name;
        private System.Windows.Forms.Label lblTruck_No;
        private System.Windows.Forms.Label lblBatch_No;
        private System.Windows.Forms.Label lblStart_Time;
        private System.Windows.Forms.Label lblCustomer;
        private System.Windows.Forms.Label lblSite_Address;
        private System.Windows.Forms.Label lblBatch_Date;

    }
}