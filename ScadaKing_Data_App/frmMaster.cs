﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using ScadaKing_Data_App.Class;
namespace ScadaKing_Data_App
{
    public partial class frmMaster : Form
    {
        string dbConnectionString = string.Empty;
        DataSet dsValues = new DataSet();
        string strdbAccessQuery = string.Empty;
        public frmMaster(string ConnectionString)
        {
            InitializeComponent();
            try
            {
                dbConnectionString = ConnectionString;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void cboMaster_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            try
            {
                if (cboMaster != null && cboMaster.SelectedValue != null && string.IsNullOrEmpty(cboMaster.SelectedValue.ToString().TrimEnd().TrimStart()) == false)
                {
                    strdbAccessQuery = @"select * from " + cboMaster.SelectedValue;
                    DataTable dtValues = new DataTable();
                    if (dsValues.Tables.Contains(cboMaster.SelectedValue.ToString()) == true)
                    {
                        dtValues = dsValues.Tables[cboMaster.SelectedValue.ToString()];
                        if (dtValues != null)
                        {
                            dgvMaster.DataSource = dtValues;
                            this.lblRecordCount.Text = string.Format(lblRecordCount.Tag.ToString(), dtValues.Rows.Count);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                General.ShowErrorMessage(ex.Message);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        void FillDropDown()
        {
            this.Cursor = Cursors.WaitCursor;
            DataTable dtValues = new DataTable();

            try
            {
                if (Program.PlantAppVersion == "1" || Program.PlantAppVersion == "3")
                {
                    try
                    {
                        strdbAccessQuery = string.Empty;
                        strdbAccessQuery = @"SELECT TOP 1 * FROM TEMP_MASTERS WHERE ROW_INDEX = 0";
                        dtValues = DbHelper.FillDataTable(strdbAccessQuery, dbConnectionString);
                    }
                    catch
                    {
                        strdbAccessQuery = string.Empty;
                        strdbAccessQuery = @"CREATE TABLE TEMP_MASTERS(
                            ROW_INDEX Counter(1,1)
                            ,NAME TEXT
                            )";
                        DbHelper.ExecuteNonQuery(strdbAccessQuery, dbConnectionString);

                        strdbAccessQuery = string.Empty;
                        strdbAccessQuery = @"SELECT TOP 1 * FROM TEMP_MASTERS WHERE ROW_INDEX = 0";
                        dtValues = DbHelper.FillDataTable(strdbAccessQuery, dbConnectionString);
                        if (dtValues.Columns.Count > 0)
                        {
                            strdbAccessQuery = string.Empty;
                            strdbAccessQuery = "INSERT INTO TEMP_MASTERS(NAME)VALUES('CUSTOMER_MASTER')";
                            DbHelper.ExecuteNonQuery(strdbAccessQuery, dbConnectionString);
                            strdbAccessQuery = "INSERT INTO TEMP_MASTERS(NAME)VALUES('SITE_MASTER')";
                            DbHelper.ExecuteNonQuery(strdbAccessQuery, dbConnectionString);
                            strdbAccessQuery = "INSERT INTO TEMP_MASTERS(NAME)VALUES('TRUCK_MASTER')";
                            DbHelper.ExecuteNonQuery(strdbAccessQuery, dbConnectionString);
                        }
                    }
                    strdbAccessQuery = @" SELECT DISTINCT NAME AS ROW_INDEX ,NAME AS DISPLAY_MASTER FROM TEMP_MASTERS";
                    dtValues = DbHelper.FillDataTable(strdbAccessQuery, dbConnectionString);
                    if (dtValues != null && dtValues.Rows.Count > 0)
                    {
                        cboMaster.Items.Clear();

                        cboMaster.DisplayMember = "DISPLAY_MASTER";
                        cboMaster.ValueMember = "ROW_INDEX";
                        cboMaster.DataSource = dtValues;
                        cboMaster.BackColor = Color.Cornsilk;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }
 

        private void frmMaster_Load(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                FillDropDown();
                btnRefresh_Click(sender, new EventArgs());
            }
            catch (Exception ex)
            {
                General.ShowErrorMessage(ex.Message);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                Fetch_Master_Data();
                cboMaster_SelectedIndexChanged(sender, new EventArgs());
            }
            catch (Exception ex)
            {
                General.ShowErrorMessage(ex.Message);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void Fetch_Master_Data()
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                dsValues = new DataSet();
                DataTable dtMaster = new DataTable();
                strdbAccessQuery = @"select * from CUSTOMER_MASTER";
                dtMaster = DbHelper.FillDataTable(strdbAccessQuery, dbConnectionString);
                dtMaster.TableName = "CUSTOMER_MASTER";
                dsValues.Tables.Add(dtMaster);

                dtMaster = new DataTable();
                strdbAccessQuery = @"select * from SITE_MASTER";
                dtMaster = DbHelper.FillDataTable(strdbAccessQuery, dbConnectionString);
                dtMaster.TableName = "SITE_MASTER";
                dsValues.Tables.Add(dtMaster);

                dtMaster = new DataTable();
                strdbAccessQuery = @"select * from TRUCK_MASTER";
                dtMaster = DbHelper.FillDataTable(strdbAccessQuery, dbConnectionString);
                dtMaster.TableName = "TRUCK_MASTER";
                dsValues.Tables.Add(dtMaster);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void btnDownload_Master_Click(object sender, EventArgs e)
        {
            try
            {
                if (cboMaster != null && cboMaster.SelectedValue != null && string.IsNullOrEmpty(cboMaster.SelectedValue.ToString().TrimEnd().TrimStart()) == false)
                {
                    string strTableName = cboMaster.SelectedValue.ToString();
                    if (General.ShowYesNoMessage("Do you want to download the data for [" + strTableName + "]", "Download Master Records") == System.Windows.Forms.DialogResult.Yes)
                    {
                        Download_Master_Records(strTableName);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #region Download Master data from live
        public void Download_Master_Records(string tableName)
        {
            try
            {
                string returnData = string.Empty;
                ScadaKing_Web_Service.TransactionHeader TransactionHeader = new ScadaKing_Web_Service.TransactionHeader();
                TransactionHeader.UserName = General.User_Name;
                TransactionHeader.Password = General.ConfigService_Password;
                TransactionHeader.DeviceID = General.ConfigMachine_Id;
                TransactionHeader.Config_App_Name = General.ConfigAPP_Name;
                TransactionHeader.Config_App_Version = General.ConfigAPP_Version;
                TransactionHeader.Win_Service_Version = General.Win_Service_Version;
                TransactionHeader.Win_Service_GUID = General.Win_Service_GUID;
                ScadaKing_Web_Service.ScadaKing_Web_ServiceSoapClient objService = new ScadaKing_Web_Service.ScadaKing_Web_ServiceSoapClient();
                ScadaKing_Web_Service.Response_UploadData response = new ScadaKing_Web_Service.Response_UploadData();                //using web services :-
                DataTable dtMaster = new DataTable();                
                response = objService.Download_Master_Data(TransactionHeader, tableName);
                if (response.SyncResponseData.Tables.Count == 0)
                {
                    General.ShowMessage("Failed to Download Data...!!!",this.Text);
                    return;
                }
                if (response.SyncResponseData.Tables.Count > 0)
                {
                    this.Cursor = Cursors.WaitCursor;
                    if (tableName == "CUSTOMER_MASTER")
                    {                        
                        dtMaster = response.SyncResponseData.Tables["CUSTOMER_MASTER"];
                        dtMaster.PrimaryKey = null;
                        if (dtMaster.Columns.Contains("ROW_INDEX") == true)
                        {
                            dtMaster.Columns.Remove("ROW_INDEX");
                        }

                        if (dtMaster.Columns.Contains("ORG_ID") == true)
                        {
                            dtMaster.Columns.Remove("ORG_ID");
                        }

                        if (dtMaster.Columns.Contains("ROW_ID") == true)
                        {
                            dtMaster.Columns.Remove("ROW_ID");
                        }

                        if (dtMaster.Columns.Contains("IsSync") == true)
                        {
                            dtMaster.Columns.Remove("IsSync");
                        }

                        if (dtMaster.Columns.Contains("IS_MCGM") == true)
                        {
                            dtMaster.Columns.Remove("IS_MCGM");
                        }
                        dtMaster.TableName = "CUSTOMER_MASTER";
                        Insert_Master_Data_Into_Local(dtMaster);
                        General.ShowMessage("Data has been successfully downloaded for Customer Master...!!!",this.Text);
                    }
                    else if (tableName == "SITE_MASTER")
                    {                        
                        dtMaster = response.SyncResponseData.Tables["SITE_MASTER"];
                        dtMaster.PrimaryKey = null;
                        if (dtMaster.Columns.Contains("ORG_ID") == true)
                        {
                            dtMaster.Columns.Remove("ORG_ID");
                        }

                        if (dtMaster.Columns.Contains("ROW_INDEX") == true)
                        {
                            dtMaster.Columns.Remove("ROW_INDEX");
                        }

                        if (dtMaster.Columns.Contains("ROW_ID") == true)
                        {
                            dtMaster.Columns.Remove("ROW_ID");
                        }

                        if (dtMaster.Columns.Contains("IsSync") == true)
                        {
                            dtMaster.Columns.Remove("IsSync");
                        }

                        dtMaster.TableName = "SITE_MASTER";
                        Insert_Master_Data_Into_Local(dtMaster);
                        General.ShowMessage("Data has been successfully downloaded for Site Master...!!!",this.Text);
                    }
                    else if (tableName == "TRUCK_MASTER")
                    {
                        dtMaster = response.SyncResponseData.Tables["TRUCK_MASTER"];
                        dtMaster.PrimaryKey = null;
                        if (dtMaster.Columns.Contains("ROW_ID") == true)
                        {
                            dtMaster.Columns.Remove("ROW_ID");
                        }

                        if (dtMaster.Columns.Contains("IsSync") == true)
                        {
                            dtMaster.Columns.Remove("IsSync");
                        }

                        if (dtMaster.Columns.Contains("SOIL_TYPE_ID") == true)
                        {
                            dtMaster.Columns.Remove("SOIL_TYPE_ID");
                        }

                        if (dtMaster.Columns.Contains("COMPACTION_SENSOR_TYPE_ID") == true)
                        {
                            dtMaster.Columns.Remove("COMPACTION_SENSOR_TYPE_ID");
                        }

                        if (dtMaster.Columns.Contains("IsMGM") == true)
                        {
                            dtMaster.Columns.Remove("IsMGM");
                        }

                        if (dtMaster.Columns.Contains("IS_OWNED") == true)
                        {
                            dtMaster.Columns.Remove("IS_OWNED");
                        }

                        if (dtMaster.Columns.Contains("MODULE_ID") == true)
                        {
                            dtMaster.Columns.Remove("MODULE_ID");
                        }

                        if (dtMaster.Columns.Contains("VEHICLE_ID") == true)
                        {
                            dtMaster.Columns.Remove("VEHICLE_ID");
                        }

                        if (dtMaster.Columns.Contains("ROW_INDEX") == true)
                        {
                            dtMaster.Columns.Remove("ROW_INDEX");
                        }

                        if (dtMaster.Columns.Contains("ORG_ID") == true)
                        {
                            dtMaster.Columns.Remove("ORG_ID");
                        }

                        dtMaster.TableName = "TRUCK_MASTER";
                        Insert_Master_Data_Into_Local(dtMaster);
                        General.ShowMessage("Data has been successfully downloaded for Truck Master...!!!",this.Text);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }
        #endregion Download Master data from live
        #region Insert Master Data into Local DB
        private void Insert_Master_Data_Into_Local(DataTable dtMaster)
        {
            try
            {
                #region insert data into replica db
                string strQuery = string.Empty;
                string strTableName = dtMaster.TableName;
                strQuery = @"delete * from " + strTableName + "";
                DbHelper.ExecuteNonQuery(strQuery, dataConnectionConfiguration.Source_Connection_String);
                DbHelper.ExecuteNonQuery(strQuery, dataConnectionConfiguration.Target_Connection_String);
                foreach (DataRow row in dtMaster.Rows)
                {
                    string strAccessQuery = string.Empty;
                    strAccessQuery = @"Insert into " + strTableName + "(" + SqlTableColumn(dtMaster);
                    strAccessQuery += @")values(";
                    strAccessQuery += SqlTableValues(row);
                    strAccessQuery += ")";
                    DbHelper.ExecuteNonQuery(strAccessQuery, dataConnectionConfiguration.Source_Connection_String);
                    DbHelper.ExecuteNonQuery(strAccessQuery, dataConnectionConfiguration.Target_Connection_String);
                }
                #endregion insert data into replica db
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion Insert Master Data into Local DB
        #region Table Qeury Script
        private string SqlTableColumn(DataTable dtObject)
        {
            string ReturnScript = string.Empty;
            try
            {
                string tableName = dtObject.TableName.ToString();
                string strColumns = string.Empty;
                int[] columnNumbers = new int[dtObject.Columns.Count];
                int columnNumbersIndex = 0;
                int columnIndex = 0;

                foreach (DataColumn dtcCurrenColumn in dtObject.Columns)
                {
                    strColumns += ((strColumns.Length > 0) ? ",[" + dtcCurrenColumn.ColumnName.ToString() + "]" : "[" + dtcCurrenColumn.ColumnName.ToString() + "]");
                    if (dtcCurrenColumn.DataType == typeof(DateTime))
                    {
                        columnNumbers[columnNumbersIndex] = columnIndex;
                        columnNumbersIndex++;
                    }
                    columnIndex++;
                }
                ReturnScript = strColumns;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ReturnScript;
        }
        private string SqlTableValues(DataRow CurrentRow)
        {
            string ReturnScript = string.Empty;
            try
            {
                string strValues = string.Empty;
                int ItmeCount = CurrentRow.ItemArray.Length;
                for (int i = 0; i < ItmeCount; i++)
                {
                    string strDataType = CurrentRow[i].GetType().ToString();
                    if (string.IsNullOrEmpty(strValues))
                    {
                        if (strDataType == "System.DateTime")
                        {
                            if (CurrentRow.Table.Columns[i].ColumnName == "Batch_Date")
                            {
                                DateTime dtValues = DateTime.Parse(CurrentRow[i].ToString());
                                string strdtValues = dtValues.ToShortDateString().ToString();
                                strValues += ",'" + strdtValues + "'";
                            }
                            else
                            {
                                DateTime dtValues = DateTime.Parse(CurrentRow[i].ToString());
                                string strdtValues = dtValues.ToString();
                                strValues += ",'" + strdtValues + "'";
                            }
                        }
                        else
                        {
                            strValues += "'" + CurrentRow.ItemArray.GetValue(i) + "'";
                        }
                    }
                    else
                    {
                        if (strDataType == "System.DateTime")
                        {
                            if (CurrentRow.Table.Columns[i].ColumnName == "Batch_Date")
                            {
                                DateTime dtValues = DateTime.Parse(CurrentRow[i].ToString());
                                string strdtValues = dtValues.ToShortDateString().ToString();
                                strValues += ",'" + strdtValues + "'";
                            }
                            else
                            {
                                DateTime dtValues = DateTime.Parse(CurrentRow[i].ToString());
                                string strdtValues = dtValues.ToString();
                                strValues += ",'" + strdtValues + "'";
                            }
                        }
                        else if (strDataType == "System.DBNull")
                        {
                            strValues += ",NULL";
                        }
                        else
                        {
                            strValues += ",'" + CurrentRow.ItemArray.GetValue(i) + "'";
                        }
                    }
                }
                ReturnScript = strValues;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ReturnScript;
        }
        #endregion Table Query Script
    }
}
