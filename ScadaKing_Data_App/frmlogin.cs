﻿using System;
using System.Windows.Forms;
using ScadaKing_Data_App.Class;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
namespace ScadaKing_Data_App
{
    public partial class frmlogin : Form
    {
        public frmlogin()
        {
            InitializeComponent();
        }

        private string GetApplication_Version()
        {
            string returnData = string.Empty;
            foreach (var item in System.Reflection.Assembly.GetExecutingAssembly().GetCustomAttributes(false))
            {
                if (item is System.Reflection.AssemblyFileVersionAttribute)
                {
                    returnData = ((System.Reflection.AssemblyFileVersionAttribute)item).Version;
                    break;
                }
            }

            return returnData;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private string GetApplication_Guid()
        {
            string returnData = string.Empty;
            foreach (var item in System.Reflection.Assembly.GetExecutingAssembly().GetCustomAttributes(false))
            {
                if (item is System.Runtime.InteropServices.GuidAttribute)
                {
                    returnData = ((System.Runtime.InteropServices.GuidAttribute)item).Value;
                    break;
                }
            }
            return returnData;
        }

        private void ShowMessage(string message)
        {
            ScadaKing_Data_App.Class.General.ShowMessage(message, this.Text);
        }

        private bool IsUserValidityExpired(DateTime effectivedate, DateTime expirationdate)
        {
            TimeZoneInfo timeZone = TimeZoneInfo.FindSystemTimeZoneById("Central America Standard Time");
            TimeZoneInfo indiaTimeZone = TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");
            DateTime indiaDateTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, timeZone);
            if (effectivedate.Date <= indiaDateTime.Date && indiaDateTime.Date <= expirationdate.Date)
            {
                return true;
            }
            return false;
        }

        private bool IsOrganisationActive(bool? organisationactive, long usertype)
        {
            bool ret = default(bool);

            switch ((Code.UserType)usertype)
            {
                case Code.UserType.SYSTEMADMINISTRATOR:
                case Code.UserType.SUPERADMINISTRATOR:
                case Code.UserType.MCGMADMIN:
                    ret = true;
                    break;
                default:
                    if (organisationactive == true)
                    {
                        ret = true;
                    }
                    break;
            }
            return ret;
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            try
            {
                //DataTable dtGetUsers_Id = new DataTable();
                if (string.IsNullOrEmpty(txtUserName.Text))
                {
                    General.ShowMessage("Enter your user name", this.Text);
                    this.txtUserName.Focus();
                    return;
                }

                else if (string.IsNullOrEmpty(txtPassword.Text))
                {
                    General.ShowMessage("Enter your password ", this.Text);
                    this.txtPassword.Focus();
                    return;
                }

                ScadaKing_Web_Service.UserDetails validUser = new ScadaKing_Web_Service.UserDetails();
                try
                {
                    ScadaKing_Data_App.BusinessLayer.Factory.ValidateUserFactory validateUserFactory = new ScadaKing_Data_App.BusinessLayer.Factory.ValidateUserFactory();
                    string Application_Version = GetApplication_Version();
                    string Application_Guid = GetApplication_Guid();
                    ScadaKing_Web_Service.TransactionHeader TransactionHeader = new ScadaKing_Web_Service.TransactionHeader();
                    ScadaKing_Web_Service.ScadaKing_Web_ServiceSoapClient objService = new ScadaKing_Web_Service.ScadaKing_Web_ServiceSoapClient();
                    TransactionHeader.UserName = this.txtUserName.Text;
                    TransactionHeader.Password = this.txtPassword.Text;
                    TransactionHeader.DeviceID = Program.ConfigMachine_Id;
                    TransactionHeader.Config_App_Name = Program.ConfigAPP_Name;
                    TransactionHeader.Config_App_Version = Program.ConfigAPP_Version;
                    TransactionHeader.Win_Service_Version = Program.Win_Service_Version;
                    TransactionHeader.Win_Service_GUID = Program.Win_Service_GUID;
                    validUser = objService.Validate_User(TransactionHeader);

                    //dtGetUsers_Id.Rows.Add(validUser);
                    //dtGetUsers_Id.AcceptChanges();
                }
                catch (Exception ex)
                {
                    Global.WriteLog("Login Exception:", LogMessageType.Error, ex.Message);
                }

                if (validUser.ErrorCode != 0)
                {
                    this.ShowMessage("This user is invalid. \n Please contact your system administrator.");
                    return;
                }
                General.orgId = Convert.ToInt64(validUser.ORG_ID);
                General.User_Name = Program.ConfigService_LoginID;
                General.ConfigMachine_Id = Program.ConfigMachine_Id;
                General.ConfigService_Password = Program.ConfigService_Password;
                General.ConfigAPP_Version = Program.ConfigAPP_Version;
                General.ConfigAPP_Name = Program.ConfigAPP_Name;
                General.Win_Service_GUID = Program.Win_Service_GUID;
                General.Win_Service_Version = Program.Win_Service_Version;
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
            }
            catch (Exception ex)
            {
                this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
                General.ShowErrorMessage(ex.Message);
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void txtUserName_Validated(object sender, EventArgs e)
        {
            General.ChangeObjectColor(sender, false);
        }

        private void txtUserName_Enter(object sender, EventArgs e)
        {
            General.ChangeObjectColor(sender, true);
        }

        private void txtPassword_Enter(object sender, EventArgs e)
        {
            General.ChangeObjectColor(sender, true);
        }

        private void txtPassword_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.btnLogin_Click(sender, e);
            }
        }

        private void txtPassword_Validated(object sender, EventArgs e)
        {
            General.ChangeObjectColor(sender, false);
        }

        private void frmlogin_Load(object sender, EventArgs e)
        {
            //General.ClearControls(this, "");
        }

        private void frmlogin_Resize(object sender, EventArgs e)
        {
            if (FormWindowState.Minimized == this.WindowState)
            {
                notifyIcon1.Visible = true;
                notifyIcon1.ShowBalloonTip(500);
                this.Hide();
            }

            else if (FormWindowState.Normal == this.WindowState)
            {
                notifyIcon1.Visible = false;
            }
        }
    }
}