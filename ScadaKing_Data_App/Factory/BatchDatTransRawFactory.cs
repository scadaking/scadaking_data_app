﻿using System; 
using System.Data;

namespace ScadaKing_Data_App.BusinessLayer.Factory
{
    class BatchDatTransRawFactory
    {
        ScadaKing_Data_App.BusinessLayer.DataLayer.BatchDatTransRaw _dataObject = null;

        public BatchDatTransRawFactory()
        {
            _dataObject = new ScadaKing_Data_App.BusinessLayer.DataLayer.BatchDatTransRaw();
        }
        internal DataTable GetOriginalBatchList(long orgID, string customerCode, long fromBatchNo, long toBatchNo, DateTime? fromBatchDate, DateTime? toBatchDate, int batchType)
        {
            DataTable dtValues = new DataTable();
            dtValues = _dataObject.GetOriginalBatchList(orgID, customerCode, fromBatchNo, toBatchNo, fromBatchDate, toBatchDate, batchType);
            return dtValues;
        }
        /*
        public ScadaKing_Web_Service.Data_Trans_Field[] GetOriginalBatchList(long orgID, string customerCode, long fromBatchNo, long toBatchNo, DateTime? fromBatchDate, DateTime? toBatchDate, int batchType)
        {
            return _dataObject.GetOriginalBatchList(orgID, customerCode, fromBatchNo, toBatchNo, fromBatchDate, toBatchDate, batchType);
        }
        */
        /*
        public MCGM_BATCH.ScadaKing_Web_Service.Response_LocalSQLData MCGM_Transfered_Batch(long BatchNO, bool show_Raw_Data)
        {
            return _dataObject.MCGM_Transfered_Batch(BatchNO, show_Raw_Data);
        }
         */


        internal bool Update_HTML_File_Creation_Status(string batch_No, string file_Created_Date)
        {
            return _dataObject.Update_HTML_File_Creation_Status(batch_No, file_Created_Date);
        }
          
      
    }
}
