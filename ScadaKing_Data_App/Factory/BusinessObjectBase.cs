﻿using System.ComponentModel;

namespace ScadaKing_Data_App.BusinessLayer.Factory
{
   public class BusinessObjectBase : INotifyPropertyChanged
    {
        #region Data Member

        bool _disablePropertyChangeEvent = false;
   
        #endregion

        #region Properties

        #region Internal Properties

        /// <summary>
        /// get/set the Disable Property change event value
        /// </summary>
        internal bool DisablePropertyChangeEvent
        {
            get { return _disablePropertyChangeEvent; }
            set { _disablePropertyChangeEvent = value; }
        }

     
        #endregion

        #region Public Properties

  
        public string OrganizationName
        {
            get;
            set;
        }

        #endregion

        #endregion

        #region Virtual Methods

        /// <summary>
        /// Add validation rules
        /// </summary>
        internal virtual void AddValidationRules()
        {

        }

        #endregion

        #region Implement INotifyPropertyChanged

        /// <summary>
        /// override Property Change event of INotifyPropertyChanged
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// method called when property has changed
        /// </summary>
        /// <param name="propertyName">The name of the property that has changed.</param>
        protected virtual void PropertyHasChanged(string propertyName)
        {
            if (!DisablePropertyChangeEvent)
            {
                // call overloaded method
                PropertyHasChanged(new PropertyChangedEventArgs(propertyName));
            }
        }

        /// <summary>
        /// Called when a property has changed
        /// </summary>
        /// <param name="e">PropertyChangedEventArgs</param>
        protected virtual void PropertyHasChanged(PropertyChangedEventArgs e)
        {
            if (!DisablePropertyChangeEvent)
            {
                 if (PropertyChanged != null)
                {
                    PropertyChanged(this, e);
                }
            }
        }

        #endregion

    }
}
