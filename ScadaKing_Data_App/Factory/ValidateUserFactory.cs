﻿
namespace ScadaKing_Data_App.BusinessLayer.Factory
{

    public class ValidateUserFactory
    {
        #region data Members

        ScadaKing_Data_App.BusinessLayer.DataLayer.Users _dataObject = null;

        #endregion

        #region Constructor

        public ValidateUserFactory()
        {
            _dataObject = new ScadaKing_Data_App.BusinessLayer.DataLayer.Users();
        }

        #endregion

        /// <summary>
        /// get list of Users by field
        /// </summary>
        /// <param name="fieldName">field name</param>
        /// <param name="value">value</param>
        /// <returns>list</returns>
        /// 
        /*
        public ScadaKing_Web_Service.ValidUser ValidateUser(string userName, string pwd, string Application_Version, string Application_Guid)
        {
            return _dataObject.ValidateUser(userName, pwd, Application_Version, Application_Guid);
        }
        */
    }

}
