﻿namespace ScadaKing_Data_App
{
    partial class frmHtmlPreview
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.wb1 = new System.Windows.Forms.WebBrowser();
            this.pnlWebBrowser = new System.Windows.Forms.Panel();
            this.btnPreview_Batch = new System.Windows.Forms.Button();
            this.rdoLiveData = new System.Windows.Forms.RadioButton();
            this.rdoLocalData = new System.Windows.Forms.RadioButton();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.btnGenerateFile = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.pnlWebBrowser.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // wb1
            // 
            this.wb1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.wb1.Location = new System.Drawing.Point(0, 0);
            this.wb1.MinimumSize = new System.Drawing.Size(20, 20);
            this.wb1.Name = "wb1";
            this.wb1.Size = new System.Drawing.Size(1256, 571);
            this.wb1.TabIndex = 0;
            // 
            // pnlWebBrowser
            // 
            this.pnlWebBrowser.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlWebBrowser.Controls.Add(this.wb1);
            this.pnlWebBrowser.Location = new System.Drawing.Point(0, 0);
            this.pnlWebBrowser.Name = "pnlWebBrowser";
            this.pnlWebBrowser.Size = new System.Drawing.Size(1256, 571);
            this.pnlWebBrowser.TabIndex = 22;
            // 
            // btnPreview_Batch
            // 
            this.btnPreview_Batch.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.btnPreview_Batch.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPreview_Batch.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPreview_Batch.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPreview_Batch.Location = new System.Drawing.Point(11, 19);
            this.btnPreview_Batch.Name = "btnPreview_Batch";
            this.btnPreview_Batch.Size = new System.Drawing.Size(127, 35);
            this.btnPreview_Batch.TabIndex = 2;
            this.btnPreview_Batch.Text = "Ref&resh";
            this.btnPreview_Batch.UseVisualStyleBackColor = false;
            this.btnPreview_Batch.Click += new System.EventHandler(this.btnPreview_Batch_Click);
            // 
            // rdoLiveData
            // 
            this.rdoLiveData.AutoSize = true;
            this.rdoLiveData.Cursor = System.Windows.Forms.Cursors.Hand;
            this.rdoLiveData.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdoLiveData.Location = new System.Drawing.Point(168, 27);
            this.rdoLiveData.Name = "rdoLiveData";
            this.rdoLiveData.Size = new System.Drawing.Size(82, 18);
            this.rdoLiveData.TabIndex = 3;
            this.rdoLiveData.Tag = "2";
            this.rdoLiveData.Text = "Live Data";
            this.rdoLiveData.UseVisualStyleBackColor = true;
            // 
            // rdoLocalData
            // 
            this.rdoLocalData.AutoSize = true;
            this.rdoLocalData.Checked = true;
            this.rdoLocalData.Cursor = System.Windows.Forms.Cursors.Hand;
            this.rdoLocalData.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdoLocalData.Location = new System.Drawing.Point(286, 27);
            this.rdoLocalData.Name = "rdoLocalData";
            this.rdoLocalData.Size = new System.Drawing.Size(89, 18);
            this.rdoLocalData.TabIndex = 4;
            this.rdoLocalData.TabStop = true;
            this.rdoLocalData.Tag = "1";
            this.rdoLocalData.Text = "Local Data";
            this.rdoLocalData.UseVisualStyleBackColor = true;
            // 
            // splitContainer1
            // 
            this.splitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer1.IsSplitterFixed = true;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.btnGenerateFile);
            this.splitContainer1.Panel1.Controls.Add(this.btnExit);
            this.splitContainer1.Panel1.Controls.Add(this.btnPreview_Batch);
            this.splitContainer1.Panel1.Controls.Add(this.rdoLiveData);
            this.splitContainer1.Panel1.Controls.Add(this.rdoLocalData);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.pnlWebBrowser);
            this.splitContainer1.Size = new System.Drawing.Size(1268, 659);
            this.splitContainer1.SplitterDistance = 82;
            this.splitContainer1.TabIndex = 1;
            // 
            // btnGenerateFile
            // 
            this.btnGenerateFile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnGenerateFile.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.btnGenerateFile.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnGenerateFile.Enabled = false;
            this.btnGenerateFile.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGenerateFile.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnGenerateFile.Location = new System.Drawing.Point(1030, 19);
            this.btnGenerateFile.Name = "btnGenerateFile";
            this.btnGenerateFile.Size = new System.Drawing.Size(110, 36);
            this.btnGenerateFile.TabIndex = 7;
            this.btnGenerateFile.Tag = "";
            this.btnGenerateFile.Text = "Generate File";
            this.btnGenerateFile.UseVisualStyleBackColor = false;
            this.btnGenerateFile.Click += new System.EventHandler(this.btnGenerateFile_Click);
            // 
            // btnExit
            // 
            this.btnExit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExit.BackColor = System.Drawing.Color.Red;
            this.btnExit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnExit.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnExit.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.ForeColor = System.Drawing.Color.White;
            this.btnExit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExit.Location = new System.Drawing.Point(1146, 18);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(110, 36);
            this.btnExit.TabIndex = 6;
            this.btnExit.Text = "G&o Back";
            this.btnExit.UseVisualStyleBackColor = false;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // frmHtmlPreview
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1268, 659);
            this.Controls.Add(this.splitContainer1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MinimizeBox = false;
            this.Name = "frmHtmlPreview";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "HTML Preview";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.pnlWebBrowser.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.WebBrowser wb1;
        private System.Windows.Forms.Panel pnlWebBrowser;
        private System.Windows.Forms.Button btnPreview_Batch;
        private System.Windows.Forms.RadioButton rdoLiveData;
        private System.Windows.Forms.RadioButton rdoLocalData;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Button btnGenerateFile;

    }
}