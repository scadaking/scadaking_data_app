﻿
namespace ScadaKing_Data_App.Code
{

    /// <summary>
    /// 
    /// </summary>
    public enum UserType
    {

        /// <summary>
        /// 
        /// </summary>
        SYSTEMADMINISTRATOR = 4,

        /// <summary>
        /// 
        /// </summary>
        ORGANISATIONADMINSTRATOR = 5,

        /// <summary>
        /// 
        /// </summary>
        ORGANISATIONUSER = 8,

        /// <summary>
        /// 
        /// </summary>
        SUPERADMINISTRATOR = 50,

        /// <summary>
        /// 
        /// </summary>
        MCGMADMIN = 87
    }
}
